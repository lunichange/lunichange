<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 28/11/2020
 * Time: 20:19
 */

namespace Dashboard;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => array(
                Controller\DashboardController::class => function($container) {

                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\DashboardController($postService);
                },
                Controller\CustomerController::class =>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\CustomerController($postService,$container);
                },
                Controller\WebServicesController::class =>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\WebServicesController($postService,$container);
                },
                Controller\TransactionsController::class =>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\TransactionsController($postService,$container);
                },
                Controller\MyProfileController::class =>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');


                    return new Controller\MyProfileController($postService,$container);
                },
                Controller\CustomersManagementController::class=>function($container)
                {
                    $postService =  $container->get('Application\Service\PostServiceInterface');

                    return new Controller\CustomersManagementController($postService,$container);
                },
                Controller\RequestsController::class=>function($container)
                {
                    $postService =  $container->get('Application\Service\PostServiceInterface');

                    return new Controller\RequestsController($postService,$container);
                },
                Controller\StatisticsController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\StatisticsController($postService,$container);
                },

                Controller\UsersManagementController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\UsersManagementController($postService,$container);
                },
                Controller\CurrencyManagementController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\CurrencyManagementController($postService,$container);
                },
                Controller\CurrenciesManagementController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\CurrenciesManagementController($postService,$container);
                },
                Controller\EquivalencesSettingsController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\EquivalencesSettingsController($postService,$container);
                },
                Controller\NumbersManagementController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\NumbersManagementController($postService,$container);
                },
                Controller\TermsOfUseSettingsController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\TermsOfUseSettingsController($postService,$container);
                },
                Controller\PaymentMethodsController::class=>function($container)
                {
                    $postService = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\PaymentMethodsController($postService,$container);
                }
               /* Controller\TreatmentController::class =>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');


                    return new Controller\TreatmentController($postService);
                },*/

            ),
        ];
    }
}