<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 04/12/2020
 * Time: 08:01
 */

namespace Dashboard\Factory;


use Dashboard\Controller\MyProfileController;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class MyProfileControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $postService = $container->get('Application\Service\PostServiceInterface');
        $config = $container->get('Config');

        return new MyProfileController($postService,$config);
    }
}