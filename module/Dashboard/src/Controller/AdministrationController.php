<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 22-Feb-20
 * Time: 9:51 PM
 */

namespace Dashboard\Controller;


use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Application\Service\PostServiceInterface;
use Laminas\Session\Container;


class AdministrationController extends AbstractActionController
{
    protected $postservice;

    protected $loginTable;
    protected $paysTable;
    protected $sidebarView;
    protected $navbarView;
    protected $layout;
    protected $sidebarVars;
    protected $navbarVars;
    protected $footerVars;
    protected $contentVars;

    protected $cur_USer;
    protected $localId;
    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;


    public function __construct(PostServiceInterface $postservice)
    {
        $this->postservice= $postservice;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();

        //header layout
        $this->sidebarView = new ViewModel();
        $this->sidebarVars = array();
        $this->sidebarView->setTemplate('layout/sidebar');
        //menu layout
        $this->navbarView = new ViewModel();
        $this->navbarVars = array();
        $this->navbarView->setTemplate('layout/navbar');


        $this->sidebarVars = array();
        $this->navbarVars = array();
        $this->contentVars = array();
    }

    public function indexAction()
    {


        $this->checkAuth();
        if(!empty($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil == 2)
        {
            $this->redirect()->toRoute('administration');
        }
        $this->sidebarVars['me_1']='m-menu__item--active';
        $this->sidebarVars['me_1_1']='m-menu__item--active';


        $this->renderPage();

        $this->_getHelper('headScript', $this->getServiceLocator())

             ->appendFile($this->getUriPath() . 'dashboard_custom_plugins/dataTables/jquery.dataTables.min.js')
            ->appendFile($this->getUriPath() . 'assets/dashboard/js/custom_script_index_new.js');


        $this->sidebarVars['themenu']= $this->postservice->retriveMenu(intval($this->sessionContainer->CodeProfil));

        return array(

        );
    }


    public function checkAuth()
    {
        if ($this->sessionContainer->IdUser == '' || (isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== '' && ($this->sessionContainer->CodeProfil > 1))) {
            return $this->redirect()->toRoute('login');
        }
    }



    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->getServiceLocator()
            ->get('viewhelpermanager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }
    public function returnHost()
    {
        $url = $this->getRequest()->getHeader('Referer')->getUri();
        $shema = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $querry = parse_url($url, PHP_URL_QUERY);
        $data = [];
        $data['host'] = $shema . "://" . $host;
        $data['querry'] = $querry;
        return $data;
    }
    public function getBaseUrl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost();
        return $baseUrl;
    }
}