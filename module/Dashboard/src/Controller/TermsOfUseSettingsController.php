<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 17/08/2020
 * Time: 01:06
 */

namespace Dashboard\Controller;


use Application\Service\PostServiceInterface;
use DateTime;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
class TermsOfUseSettingsController extends AbstractActionController
{
    protected $postservice;


    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;


    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;
    protected $table;
    protected $serviceLocator;


    public function __construct(PostServiceInterface $postService,$container)
    {
        $this->table = 'terms_of_use_en_tete';

        $this->postservice = $postService;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();

        // $this->headerView = new ViewModel();
        //$this->headerVars = array();
        //$this->headerView->setTemplate('layout/header');

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');



        //footerView
        //$this->footerView = new ViewModel();
        // $this->footerView->setTemplate('layout/footer');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();
    }

    public function indexAction()
    {

        $this->_getHelper('headScript', $this->serviceLocator)
            ->appendFile($this->getUriPath().'assets/pages/terms-of-use-settings/datatable_file.js')
            ->appendFile($this->getUriPath().'assets/pages/terms-of-use-settings/custom_script.js')
            ->appendFile($this->getUriPath().'assets/js/pages/crud/forms/editors/summernote.js')
            ->appendFile($this->getUriPath().'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')
        ;
        $this->renderPage();

        $getOrder = $this->postservice->defaultSelect($this->table,["order"],[],null,null,'all',null,'order ASC');

        return array(
                'loadRubric'=>$this->postservice->defaultSelect($this->table,[],['statut'=>'1'],null,null,'all',null,'order ASC'),
                'getOrders'=>$getOrder
        );

    }

    public function addRubricAction()
    {
        $request=$this->getRequest();

        $view = null;
        $tableJson = array();

            if($request->isXmlHttprequest())
            {
                $data = $request->getPost();
                foreach ($data as $keys=>$values)
                {
                    if(empty($values))
                    {
                        $tableJson['error'] = "Veuillez renseigner tous les champs!";
                    }
                    else
                    {
                        $data[$keys] = filter_var($values,FILTER_SANITIZE_STRING);
                    }
                }
                $checkIfExist = (bool)$this->postservice->defaultSelect($this->table,[],array('statut'=>'1','name_en_tete LIKE ?'=>$data['rubrique_name'].'%'),null,null,'unique',null,null);

                if($checkIfExist)
                {
                    $tableJson['error'] = "Cette rubrique existe déjà!";
                }
                else
                {
                    $dataToInsert = array();

                    $dataToInsert['name_en_tete'] = mb_strtoupper($data['rubrique_name']);
                    $dataToInsert['update_by'] = intval($this->sessionContainer->IdUser);
                    $dataToInsert['update_at'] = $this->DateTime();
                    $dataToInsert['order'] = intval($data['rubrique_order']);

                    $reqInsert = (bool) $this->postservice->defaultInsert($this->table,$dataToInsert);

                    if($reqInsert)
                    {
                        $tableJson['success'] = "Rubrique ajoutée !";
                    }
                    else
                    {
                        $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                    }
                }
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {
                $this->redirect()->toRoute('terms-of-use-settings');
            }
        return $view;
    }

    public function listOfTermsOfUseAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        $view = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();


            $arrayColumns = array(
                'order'=>'order',
                'lib_en_tete'=>'name_en_tete',
                'status'=>'statut',
                'id'=>'id_en_tete'
            );

            $countRow = count($this->postservice->defaultSelect($this->table,$arrayColumns,[],null,null,'all',null,'order ASC'));

            $offset = (intval($data['pagination']['page'])-1) * intval($data['pagination']['perpage']);

            $limit['end'] = $offset;
            $limit['start'] = $data['pagination']['perpage'];


            $total_pages = ceil($countRow / intval($data['pagination']['perpage']) );


            $tableJson = array('meta'=>array(
                "page"=> isset($data['pagination']['page']) && !empty($data['pagination']['page']) ? intval($data['pagination']['page']): 1,
                "pages"=> $total_pages,
                "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 1,
                "total"=> $countRow,
                "sort"=> "asc",
                "field"=> "order"
                 ),
                'data'=>$this->getTermsOfUse($arrayColumns,$limit)
            );

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }

        return $view;
    }

    public function listOfContenusAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        $view = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();


            $join = null;
            $arrayColumns = array(
                'contenu'=>'contenu',
                'status'=>'statut',
                'id'=>'id_contenu',
                'en_tete'=>'en_tete',
                'update_by'=>'update_by'
            );

            $join [] = array(
                'table'=>array(
                    't'=>$this->table
                ),
                'condition'=>'t.id_en_tete=c.en_tete'
            );

            $countRow = count($this->getContenusOfTermsOfUse($arrayColumns,null,null));


            $offset = (intval($data['pagination']['page'])-1) * intval($data['pagination']['perpage']);

            $limit['start'] = $data['pagination']['perpage'];
            $limit['end'] = $offset;




            $total_pages = ceil($countRow / intval($data['pagination']['perpage']) );

            $myArray = array();

            foreach($this->getContenusOfTermsOfUse($arrayColumns,$limit,null) as $keys=>$values)
            {

                foreach($values as $key=>$elements)
                {
                    if($key == 'contenu')
                    {
                        $result = html_entity_decode($elements);
                        if(strlen($result) >=1000)
                        {
                            $myArray[$keys][$key] = substr(html_entity_decode($elements),0,600);
                        }
                        else
                        {
                            $myArray[$keys][$key] = $result;
                        }



                    }
                    elseif($key === 'en_tete')
                    {
                        $myArray[$keys][$key] = $this->postservice->defaultSelect($this->table,[],array(
                            'id_en_tete'=>intval($values['en_tete'])
                        ),null,null,'unique',null,null);
                    }
                    else
                    {
                        $myArray[$keys][$key] = $elements;
                    }
                }

            }

            $tableJson = array('meta'=>array(
                "page"=> isset($data['pagination']['page']) && !empty($data['pagination']['page']) ? intval($data['pagination']['page']): 1,
                "pages"=> $total_pages,
                "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 1,
                "total"=> $countRow,
                "sort"=> "asc",
                "field"=> "order"
                   ),
                'data'=>$myArray
            );

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }

        return $view;
    }
    public function updateContenuAction()
    {
        $request = $this->getRequest();
        $tableJson = array();
        $res = $this->sessionContainer->StdGetForUpdate;

        if($request->isXmlHttpRequest())
        {

            $data = $request->getPost()->toArray();

            if(isset($data['select_load_rubric'],$data['terms_content']) && !empty($data['select_load_rubric']) && !empty($data['terms_content']))
            {
                $data['en_tete'] = intval($data['select_load_rubric']);
                unset($data['select_load_rubric']);

                $data['contenu'] = filter_var(htmlentities($data['terms_content']),FILTER_SANITIZE_STRING);
                unset($data['terms_content']);

                $checkIfElementExist = $this->postservice->defaultSelect($this->table,[],array(
                    'id_en_tete'=>intval($data['en_tete'])
                ),null,null,'unique',null,null);

                if(!empty($checkIfElementExist))
                {
                    if(strlen($data['contenu']) < 15)
                    {
                        $tableJson['error'] = "Contenu trop court. Quinze(15) caractères au minimum";
                    }
                    else
                    {
                        $reqUpdate = $this->postservice->defaultUpdate('terms_contenu',$data,array(
                            'id_contenu'=>intval($res['id_contenu'])
                        ));

                        if($reqUpdate)
                        {
                            unset($this->sessionContainer->StdGetForUpdate);
                            $tableJson['success'] = "Modification effectuée avec succès";
                        }
                        else
                        {
                            $tableJson['error'] = "Impossible d'effectuer le traitement";
                        }
                    }

                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
                }

            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
            }

            echo json_encode($tableJson);

        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }

        return $this->getResponse();
    }
    public function updateStatusAction()
    {
        $request = $this->getRequest();
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $table = $this->params()->fromQuery('table');
            $table = filter_var($table,FILTER_SANITIZE_STRING);
            $column = filter_var($this->params()->fromQuery('column'),FILTER_SANITIZE_STRING);

            $arrayUpdate = null;


                if($data['action'] === 'lock')
                {
                    $arrayUpdate = array(
                        'statut'=>'0'
                    );
                }
                else
                {
                    $arrayUpdate = array(
                        'statut'=>'1'
                    );
                }
                $req = $this->postservice->defaultUpdate($table,$arrayUpdate,array(
                    $column=>intval($data['conditions'])
                ));

                if($req)
                {
                    $tableJson['success'] = "Mise à jour du statut effectué";
                }
                else
                {
                    $tableJson['error'] = "Une erreur s'est produite au cours du traitement";
                }

            echo json_encode($tableJson);
        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }


        return $this->getResponse();
    }


    public function updateTermsAction()
    {
        $request = $this->getRequest();
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();


            foreach($data as $keys=>$elements)
            {
                if(empty($data[$keys]))
                {
                    $data[$keys] = filter_var($elements,FILTER_SANITIZE_STRING);
                }
            }

            $getOldRubriqueOwner = $this->postservice->defaultSelect($this->table,[],array(
                'order'=>intval($data['rubrique_order'])
            ),null,null,'unique',null,null);

            if(!empty($getOldRubriqueOwner))
            {
                if(isset($data['rubrique_name']) && !empty($data['rubrique_name']))
                {
                    if(strlen($data['rubrique_name']) >= 6)
                    {
                        $reUpdateOld = (bool)$this->postservice->defaultUpdate($this->table,array(
                            'order'=>intval($this->sessionContainer->StdGetForUpdate['order'])
                        ),array(
                            'id_en_tete'=>intval($getOldRubriqueOwner['id_en_tete'])
                        ));

                        $reqUpdateNew = (bool)$this->postservice->defaultUpdate($this->table,array(
                            'order'=>intval($data['rubrique_order']),
                            'name_en_tete'=>filter_var($data['rubrique_name'],FILTER_SANITIZE_STRING)

                        ),array(
                            'id_en_tete'=>intval($this->sessionContainer->StdGetForUpdate['id_en_tete'])
                        ));

                        if($reUpdateOld && $reqUpdateNew)
                        {
                            $tableJson['success'] = "Modification effectuée";
                        }
                        else
                        {
                            $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
                        }
                    }
                    else
                    {
                        $tableJson['error'] = "Votre demande ne peut aboutir";
                    }

                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
                }

            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
            }
            echo json_encode($tableJson);

        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }

        return $this->getResponse();
    }

    public function addContentByRubricAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost();

             foreach($data as $keys=>$values)
             {
                 if(empty($data[$keys]))
                 {
                     $tableJson['error'] = "Veuillez remplir tous les champs.";
                 }
                 $data[$keys] = filter_var($values,FILTER_SANITIZE_STRING);
             }

             $checkIfExist = $this->postservice->defaultSelect('terms_contenu',[],[
                 'en_tete'=>intval($data['select_load_rubric']),
                 'contenu LIKE ?'=>$data['terms_content'].'%'
             ],null,null,'unique',null,null);


             if(!empty($checkIfExist))
             {
                $tableJson['error'] = "Ce contenu a déjà été édité!";


             }
             else
             {
                 $dataToInsert = array();

                 $dataToInsert['contenu']      = htmlentities($data['terms_content']);
                 $dataToInsert['en_tete']      = intval($data['select_load_rubric']);
                 $dataToInsert['update_by']    = $this->sessionContainer->IdUser;

                 $reqInsert = (bool)$this->postservice->defaultInsert('terms_contenu',$dataToInsert);

                 if($reqInsert)
                 {
                     $tableJson['success'] = "Contenu ajouté avec succès.";
                 }
                 else
                 {
                     $tableJson['error'] = "Une erreur s'est produite lors de l'opération.";
                 }
             }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }

        return $view;
    }

    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = null;
        if($request->isXmlHttprequest())
        {
            $datas = $request->getPost()->toArray();
            $dataGetTable = $this->params ()->fromQuery ('table');
            $dataGetType = $this->params ()->fromQuery ('type');
            $dataConditions = array();
            // $this->sessionContainer->ElementForUpdate = array();
            foreach ($datas as $keys=>$values)
            {
                //$this->sessionContainer->ElementForUpdate[$keys] = $values;
                $dataConditions[$keys] = intval($values);
            }

            $request = $this->postservice->defaultSelect($dataGetTable,[],$dataConditions,null,null,
                $dataGetType,null,null);

            if((bool)$request)
            {
                $tableJson = $request;
                $this->sessionContainer->StdGetForUpdate = $request;
            }
            else
            {
                $tableJson['error'] = "Une erreur s'est produite. Veuillez rééssayer.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('currencies_management');
        }

        return $view;
    }
    public function loadRubricOrderAction()
    {
        $request = $this->getRequest();

        $view = null;
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {

            $tableOrder = [];

            for($i=0; $i <= 15; $i++)
            {
                $tableOrder[] = intval($i);
            }

            $myRequest = $this->postservice->defaultSelect($this->table,['order'],[],null,null,'all',null,'order ASC');

            if(!empty($myRequest))
            {
               $tableOrderFromDb = [];

               foreach($myRequest as $keys=>$values)
               {
                   $tableOrderFromDb [] = intval($values['order']);
               }

               $firstStep = array_diff($tableOrder,$tableOrderFromDb);
                sort($firstStep);
               $tableJson = $firstStep;
            }
            else
            {
                sort($tableOrder);
                $tableJson = $tableOrder;

            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('terms-of-use-settings');
        }

        return $view;
    }


    private function getTermsOfUse($columns = [] , $limit = null)
    {
        $req = null;

        $req = $this->postservice->defaultSelect($this->table,$columns,[],null,$limit,'all',null,'order ASC');


        return $req;

    }

    private function getContenusOfTermsOfUse($columns = [] , $limit = null,$join = [])
    {
        $req = null;

        $req = $this->postservice->defaultSelect(array(
            'c'=>'terms_contenu'
        ),$columns,[],$join,$limit,'all',null,'en_tete ASC');


        return $req;

    }

    private function DateTime()
    {
        //$oTimeZoneUtc = new \DateTimeZone('UTC');
        //date_default_timezone_set('UTC');
        $oTimeZoneLocal = new \DateTimeZone(date_default_timezone_get());
        $oDateTime = new DateTime('now', $oTimeZoneLocal);
        return $oDateTime->format('Y-m-d H:i:s');

    }
    public function renderPage()
    {

        //header layout
        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        $this->headerView = new ViewModel();

        $user_values = array();

        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);

        $this->layout->setTemplate('layout/layout');

    }
    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }

    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil == 2 || $this->sessionContainer->CodeProfil === 3)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }

    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }

}