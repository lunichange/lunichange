<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 28/11/2020
 * Time: 20:22
 */

namespace Dashboard\Controller;


use Laminas\Mvc\Controller\AbstractActionController;

use Laminas\Session\Container;
use Application\Service\PostServiceInterface;
use Laminas\View\Model\ViewModel;

class DashboardController extends AbstractActionController
{

    protected $postservice;
    private $_currentUser = array();

    protected $contentView;
    protected $headerView;
    protected $menuView;
    protected $footerView;


    //Variables for Layout
    protected $layout = array();
    protected $contentVars = array();
    protected $headerVars = array();
    protected $menuVars = array();


    protected $sessionContainer;
    protected $sessionManager;

    public function __construct(PostServiceInterface $postService)
    {
        $this->postservice = $postService;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== null)
        {
            $this->_currentUser = array(
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null ? $this->sessionContainer->CodeProfil : null,
                'username'=>isset($this->sessionContainer->Login) ? filter_var($this->sessionContainer->Login,FILTER_SANITIZE_STRING) : null
            );
        }
    }
    public function indexAction()
    {

        return $this->redirect()->toRoute('customer');
    }

    public function tableauDeBordAction()
    {
        if(isset($this->sessionContainer->Redirect) && $this->sessionContainer->Redirect == 0)
        {
            $this->sessionContainer->Redirect = 1;
            //return $this->redirect()->toRoute('customer');
        }
        if(!isset($this->sessionContainer->Redirect) || $this->sessionContainer->Redirect == 0)
        {
            $this->sessionContainer->Redirect = 0;

            return $this->redirect()->toRoute('customer');
        }


        $this->renderPage();

        $codeProfil = null;
        $transactions = null;
        $messages = array();
        $listUsers = array();
        $listCustomers = array();
        $arrayConditions = array();
        $messagesSite = array();

        if(isset($this->_currentUser['codeProfile']))
        {

            $transactions = $this->postservice->defaultSelect('transactions',[],array(),null,null,'all',null,null);

            if(intval($this->_currentUser['codeProfile']) == 0 || intval($this->_currentUser['codeProfile']) == 1)
            {

                $arrayConditions = array('code_profil > ?'=>intval($this->_currentUser['codeProfile']),'code_profil <> ?'=>2);
            }
            if(intval($this->_currentUser['codeProfile']) !== 2)
            {
                $listUsers = $this->postservice->defaultSelect($this->postservice->getDbTables()['customers']['table'],[],$arrayConditions,null,null,'all',null,'nom_complet ASC');

                $listCustomers = $this->postservice->defaultSelect($this->postservice->getDbTables()['customers']['table'],[],array('code_profil'=>2,'statut<>?'=>'4'),null,null,'all',null,'nom_complet ASC');

                $messagesSite = $this->postservice->defaultSelect('messages_site',[],array('statut'=>'0'),null,null,'all',null,null);

            }


            if(intval($this->_currentUser['codeProfile']) === 2)
            {
                $transactions = $this->postservice->defaultSelect('transactions',[],array('made_by'=>intval($this->_currentUser['id'])),null,null,'all',null,null);
                $messages = $this->postservice->defaultSelect('messages',[],array('user'=>$this->_currentUser['id'],'statut'=>'0'),null,null,'all',null,null);
            }


        }



        return array(

            'codeProfil'=>intval($this->_currentUser['codeProfile']),
            'user_data'=>$this->postservice->defaultSelect('luni_users',[],array('statut'=>'1',
                'id_user'=>intval($this->_currentUser['id'])),null,null,'unique',null,null),
            'transactions'=>$transactions,
            'messages'=>$messages,
            'listUsers'=>$listUsers,
            'listCustomers'=>$listCustomers,
            'messages_site'=>$messagesSite

        );


    }

    private function renderPage()
    {
        $this->checkAuth();

        $this->contentVars['user_code_profil'] = intval(isset($this->_currentUser['codeProfile']) ? $this->_currentUser['codeProfile'] : null);
        //header layout
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= isset($this->_currentUser['username']) ? $this->_currentUser['username'] : null;
        $this->menuVars['user_id']= $this->_currentUser['id'];

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->postservice->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');
    }

    private function checkAuth()
    {
        if(empty($this->_currentUser) || !isset($this->_currentUser['id']) || $this->_currentUser['id'] == null)
        {
            return $this->redirect()->toRoute('logout');
        }
       return 0;
    }
}