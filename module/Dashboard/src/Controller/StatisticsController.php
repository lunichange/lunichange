<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 31-May-20
 * Time: 11:23 AM
 */

namespace Dashboard\Controller;

use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class StatisticsController extends AbstractActionController
{
    protected $postservice;

    protected $paysTable;
    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;


    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $serviceLocator;
    protected $table;


    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        //$this->dbHelperService = $dbHelperService;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();

        $this->currentUser = [
            'id'=>isset($this->sessionContainer->IdUser) && !empty($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser ? intval($this->sessionContainer->IdUser) : null,
            'codeProfile'=>isset($this->sessionContainer->CodeProfil) && !empty($this->sessionContainer->CodeProfil) ? intval($this->sessionContainer->CodeProfil) : null,
            'username'=>$this->sessionContainer->UserName,
        ];


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');


        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }
    public function indexAction()
    {

        $this->checkAuth ();
        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')


            ->appendFile($this->getUriPath() . 'assets/plugins/datatables/datatables.bundle.js')

            ->appendFile($this->getUriPath() . 'assets/pages/statistics/load_statistics.js')


            ->appendFile($this->getUriPath() . 'assets/pages/global/Helpers_new.js')

        ;
        $this->renderPage ();

        $join = array();
        $getManagersOfTransactions = array();




        $join [] =array('table'=>array('t'=>'transactions'),'condition'=>'t.made_by=c.id_user');

        $getTransactionsDoneBy = $this->postservice->defaultSelect('transactions',[],array('done_by<>?'=>""),null,null,'all','done_by',null);

        if(!empty($getTransactionsDoneBy))
        {
            foreach($getTransactionsDoneBy as $elements)
            {

                $getManagersOfTransactions[] = $this->postservice->defaultSelect('luni_users',[],array('id_user'=>intval($elements['done_by']),'code_profil >= ?'=>intval($this->sessionContainer->CodeProfil)),null,null,'unique','id_user',null);

            }
        }



        return array(

            'codeProfil'=>$this->sessionContainer->CodeProfil,
            'gestionnairesTransactions'=>$getManagersOfTransactions
        );

    }

    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }
    public function renderPage()
    {

        //header layout
        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        $this->headerView = new ViewModel();

        $user_values = array();

        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT),null);
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }

    public function listStatisticsAction()
    {
        $request = $this->getRequest();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $arrayConditions = array();
            $arrayConditionsValidate = array();
            $arrayConditionsCancel = array();

            $periode = $view = null;

            if (isset($data['columns']) && !empty($data['columns']))
            {
                if(isset($data['columns'][0]['search']['value']) && $data['columns'][0]['search']['value'] !=="")
                {

                    array_push($arrayConditions,array(
                        'done_by'=>intval($data['columns'][0]['search']['value'])
                    ));
                    array_push($arrayConditionsValidate,array(
                        'done_by'=>intval($data['columns'][0]['search']['value'])
                    ));
                    array_push($arrayConditionsCancel,array(
                        'done_by'=>intval($data['columns'][0]['search']['value'])
                    ));
                }
                else
                {
                    array_push($arrayConditions,array());
                    array_push($arrayConditionsValidate,array());
                    array_push($arrayConditionsCancel,array());
                }
                if(isset($data['columns'][1]['search']['value']) && $data['columns'][1]['search']['value'] !=="")
                {
                    $dataColumns = ltrim($data['columns'][1]['search']['value']);
                    $periode = $dataColumns;

                    if(strpos($dataColumns,' - ') !== false)
                    {
                        $explodeTable = explode(' - ',$dataColumns);

                        $startDate = trim($explodeTable[0]);
                        $startDate = str_replace('/','-',$startDate);
                        $startDate = date('Y-m-d',strtotime($startDate));

                        $endDate = trim($explodeTable[1]);
                        $endDate = str_replace('/','-',$endDate);
                        $endDate = date('Y-m-d',strtotime($endDate));
                        
                        array_push($arrayConditions,array(
                                "DATE_FORMAT(date_transaction,'%Y-%m-%d') >= ?"=>$startDate,
                                "DATE_FORMAT(date_transaction,'%Y-%m-%d') <= ?"=>$endDate
                            ));
                        array_push($arrayConditionsValidate,array(
                            "DATE_FORMAT(date_transaction,'%Y-%m-%d') >= ?"=>$startDate,
                            "DATE_FORMAT(date_transaction,'%Y-%m-%d') <= ?"=>$endDate
                        )); 
                        array_push($arrayConditionsCancel,array(
                             "DATE_FORMAT(date_transaction,'%Y-%m-%d') >= ?"=>$startDate,
                             "DATE_FORMAT(date_transaction,'%Y-%m-%d') <= ?"=>$endDate
                        ));
                    }

                }
                else
                {
                    $periode = "Toutes les périodes";
                    $today = date('Y-m-d 00:00:00');
                    /*array_push($arrayConditions,array(
                        'date_transaction >= ?'=>$today,
                        'date_transaction <= ?'=>$today
                    ));*/
                  /*  array_push($arrayConditionsValidate,array(
                        'date_transaction >= ?'=>$today,
                        'date_transaction <= ?'=>$today
                    ));
                    array_push($arrayConditionsCancel,array(
                        'date_transaction >= ?'=>$today,
                        'date_transaction <= ?'=>$today
                    ));*/
                }
            }

            $arrayConditionsValidate [] = array('status_transaction'=>'1');
            $arrayConditionsCancel [] = array('status_transaction'=>'2');

            $allRequest = array(
                0=>array('transactionsValidate'=>count($this->postservice->defaultSelect('transactions',[],$arrayConditionsValidate,null,null,'all',null,null)),
                'transactionsCancel'=>count($this->postservice->defaultSelect('transactions',[],$arrayConditionsCancel,null,null,'all',null,null)),
                'allTransactions'=>count($this->postservice->defaultSelect('transactions',[],$arrayConditions,null,null,'all',null,null)),
                'period'=>$periode
                ));

            $tableJson ['aaData'] = $allRequest;
            $tableJson['iTotalRecords'] = 1;
            $tableJson['iTotalDisplayRecords'] = 1;
            $tableJson['sColumns'] ="";
            $tableJson['sEchos'] = isset($data['draw']) && !empty($data['draw']) ? intval($data['draw']) : null;

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('statistics');
        }
        return $view;
    }

    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil == 2 || $this->sessionContainer->CodeProfil == 3)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }

    }

    protected function _getHelper($helper,$serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }
}