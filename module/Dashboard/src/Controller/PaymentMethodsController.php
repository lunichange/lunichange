<?php


namespace Dashboard\Controller;


use Application\Service\PostServiceInterface;
use Laminas\Json\Json;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\AbstractContainer;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class PaymentMethodsController extends AbstractActionController
{

    protected $postService;
    private $_currentUser = array();

    protected $contentView;
    protected $headerView;
    protected $menuView;
    protected $footerView;


    //Variables for Layout
    protected $layout = array();
    protected $contentVars = array();
    protected $headerVars = array();
    protected $menuVars = array();


    protected $sessionContainer;
    protected $sessionManager;

    protected $serviceLocator;


    public function __construct(PostServiceInterface $postService,$container)
    {
        $this->postService = $postService;
        $this->serviceLocator = $container;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== null)
        {
            $this->_currentUser = array(
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null ? $this->sessionContainer->CodeProfil : null,
                'username'=>isset($this->sessionContainer->Login) ? filter_var($this->sessionContainer->Login,FILTER_SANITIZE_STRING) : null
            );
        }
    }

    public function indexAction()
    {
        $this->renderPage();

        $getPaymentMethods = $this->postService->defaultSelect('payments_methods',[],[],null,null,'all',null,'method ASC');

        $this->_getHelper('headScript', $this->serviceLocator)
            ->appendFile($this->getUriPath().'assets/plugins/sweet-alert/sweetalert.min.js')
            ->appendFile($this->getUriPath().'assets/plugins/sweet-alert/app.js')
            ->appendFile($this->getUriPath() .'assets/pages/payment-methods/custom_script.js?'.uniqid())
            ->appendFile($this->getUriPath().'assets/js/pages/crud/widgets/bootstrap-select.js')
            ->appendFile($this->getUriPath().'assets/pages/global/Helpers.js?'.uniqid())
            ;

        $request = $this->getRequest();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $tableJson = [];

            if(isset($data['devise']))
            {

                $getDevise = $this->postService->defaultSelect('payments_methods',[],[
                    'method<>?'=>$data['devise'],
                    'statut'=>'1'
                ],null,null,'all',null,'method ASC');

                if(empty($getDevise))
                {
                    $tableJson['error'] = "Impossible de continuer le traitement demandé";
                }

                if(!isset($tableJson['error']) || $tableJson['error'])
                {
                    $tableJson['success'] = $getDevise;
                }
            }

            if(isset($data['devise_update'],$data['method_payment']))
            {
                $checkIfDeviseExist = $this->postService->defaultSelect('settings_payment_methods_devises',[],[
                    'devise'=>intval($data['devise_update'])
                ],null,null,'unique',null,null);

                if(empty($checkIfDeviseExist))
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
                }
                if(!isset($tableJson['error']) || $tableJson['error'] == null)
                {
                    $reqUpdate = $this->postService->defaultUpdate('settings_payment_methods_devises',[
                        'payment_method'=>$data['method_payment']
                    ],['devise'=>intval($data['devise_update'])]);
                    if($reqUpdate)
                    {
                        $tableJson['success'] = "Opération effectuée avec succès";
                    }
                }
            }
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        $getConfigurationGlobal = $this->postService->defaultSelect('configuration_payment_method_mod',[],[
            'global_mode'=>'1',
            'method_payment<>?'=>null
        ],null,null,'unique',null,null);



        $join [] = array(
            'table'=>array(
                'd'=>'devises'
            ),
            'condition'=>'d.id_devise=s.devise'

        );
        $join[]= array(
            'table'=>array(
                'p'=>'payments_methods'
            ),
            'condition'=>'p.method=s.payment_method'
        );
        $getSettingsPaymentMethods = $this->postService->defaultSelect(array(
            's'=>'settings_payment_methods_devises'
        ),[],[],$join,null,'all',null,null);

        return [
            'payment_methods'=>$getPaymentMethods,
            'configurationGlobale'=>$getConfigurationGlobal,
            'settingsPaymentMethods'=>$getSettingsPaymentMethods
        ];
    }


    public function listDevisesAction():JsonModel
    {
        $request = $this->getRequest();
        $tableJson = [];

        if(!$request->isXmlHttpRequest())
        {
            echo "Request must be an asynchronous request";
        }

        $getSettingsPaymentMethods = $this->postService->defaultSelect('settings_payment_methods_devises',['devise'],[],null,null,'all',null,null);
        $requestTable = null;
        $reqDevises = $this->postService->defaultSelect('devises',[],[
            'categorie_devise'=>2,
            //'code_devise<>?'=> "MOOV-IN-NI-BU-CI-MAL"
        ],null,null,'all',null,'lib_devise ASC');

        if(!empty($getSettingsPaymentMethods))
        {
            $reqDevises = null;
            foreach ($getSettingsPaymentMethods as $values)
            {
                $requestTable[] = intval($values['devise']);
            }


            foreach ($requestTable as $elements)
            {
                $req =  $this->postService->defaultSelect('devises',[],[
                    'id_devise<>?'=>$elements,
                    'categorie_devise'=>2,

                ],null,null,'unique',null,null);
                $reqDevises [] = $req;
              $requestTable[] = intval($req['id_devise']);
            }
          $requestTable = array_unique($requestTable);

          $getAllDevises = $this->postService->defaultSelect('devises',[],[
              'categorie_devise'=>2,
             // 'code_devise<>?'=>"MOOV-IN-NI-BU-CI-MAL"
          ],null,null,'all',null,null);
          $allDeviseArray = [];
          foreach ($getAllDevises as $kk=>$allDevise)
          {
              $allDeviseArray[]=intval($allDevise['id_devise']);
          }

          $arrayDiff = array_diff($allDeviseArray,$requestTable);

          $reqDevises = [];
          foreach ($arrayDiff as $values)
          {

              $reqDevises[] =  $this->postService->defaultSelect('devises',[],[
                  'id_devise'=>$values

              ],null,null,'unique',null,null);
          }
        }

        //

        if($reqDevises == null || empty($reqDevises))
        {
            $tableJson['error'] = "Toutes les devises de type compte mobile ont été configuré";
        }

        if(!isset($tableJson['error']) || $tableJson['error'] == null)
        {
            $tableJson['success'] = $reqDevises;
        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }


    public function treatFormAction():JsonModel
    {
        $request = $this->getRequest();
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            if(isset($data['checkbox_devise']))
            {
                if(empty($data['checkbox_devise']))
                {
                    $tableJson['error'] = "Veuillez choisir une monnaie";
                }
            }

            foreach($data['checkbox_devise'] as $values)
            {
                $checkIfExist = $this->postService->defaultSelect('settings_payment_methods_devises',[],[
                    'devise'=>intval($values)
                ],null,null,'unique',null,null);

                $getDevise = $this->postService->defaultSelect('devises',[],[
                    'id_devise'=>intval($values)
                ],null,null,'unique',null,null);

                if(!empty($checkIfExist))
                {
                    $tableJson['error'] = "La devise ".$getDevise['lib_devise']." a déjà été paramétré";
                }

                if(!isset($tableJson['error']))
                {
                    $this->postService->defaultInsert('settings_payment_methods_devises',[
                        'payment_method'=>$data['payment-methods'],
                        'devise'=>intval($values)
                    ]);
                }
            }

            if(!isset($tableJson['error']) || $tableJson['error'] == null)
            {
                $tableJson['success'] = "Configuration des monnaies effectuée avec succès";
            }

        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }
    public function operationOnPaymentAction():JsonModel
    {
        $request = $this->getRequest();
        $tableJson = array();
        if(!$request->isXmlHttpRequest())
        {
            echo 'No action required';
        }

        $data = $request->getPost()->toArray();
        $condition = null;

        $getCurrentPaymentMethod = $this->postService->defaultSelect('payments_methods',[],[
            'id_method_payment'=>intval($data['id'])
        ],null,null,'unique',null,null);

        $getCurrentOppositePaymentMethod = $this->postService->defaultSelect('payments_methods',[],[
            'id_method_payment<>?'=>intval($data['id'])
        ],null,null,'unique',null,null);

        if(empty($getCurrentPaymentMethod) || empty($getCurrentOppositePaymentMethod))
        {
            $tableJson['error'] = "Impossible de poursuivre le traitement demandé.";
        }
        switch($data['configuration'])
        {

            case 'standard':
                switch($data['action'])
                {
                    case 'enable':

                        $this->postService->defaultUpdate('payments_methods',[
                            'statut'=>'1'
                        ],[
                            'id_method_payment'=>intval($data['id'])
                        ]);

                        break;
                    case 'disabled':
                        $condition[] = array(
                            'statut'=>'0'
                        );

                        $this->postService->defaultUpdate('payments_methods',[
                            'statut'=>'0'
                        ],[
                            'id_method_payment'=>intval($data['id'])
                        ]);
                        break;
                }
            break;

            case 'global':

                $this->postService->defaultUpdate('configuration_payment_method_mod',[
                    'global_mode'=>'1',
                    'method_payment'=>$getCurrentPaymentMethod['method']
                ],[]);
                $this->postService->defaultUpdate('payments_methods',[
                    'statut'=>'2'
                ],[
                    'id_method_payment'=>$getCurrentPaymentMethod['id_method_payment']
                ]);
                $this->postService->defaultUpdate('payments_methods',[
                    'statut'=>'0'
                ],[
                    'id_method_payment<>?'=>$getCurrentPaymentMethod
                ]);
                break;

            default :
                $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
        }

        if(!isset($tableJson['error']) || $tableJson['error'] == null)
        {
            $tableJson['success'] = "Opération effectué avec succès";
        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }

    private function renderPage()
    {
        $this->checkAuth();


        $this->contentVars['user_code_profil'] = intval(isset($this->_currentUser['codeProfile']) ? $this->_currentUser['codeProfile'] : null);
        //header layout
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= isset($this->_currentUser['username']) ? $this->_currentUser['username'] : null;
        $this->menuVars['user_id']= $this->_currentUser['id'];

        $this->menuVars['themenu']= $this->postService->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->postService->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');
    }

    public function random($car)
    {
        $string = "";
        $chaine = "1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Laminas\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }

    public function checkAuth()
    {
        if ($this->sessionContainer->IdUser == '' || (isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== '' && ($this->sessionContainer->CodeProfil > 1))) {
            return $this->redirect()->toRoute('login');
        }
    }




}