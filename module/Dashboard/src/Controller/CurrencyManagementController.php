<?php
	/**
	 * Created by PhpStorm.
	 * User: carlos
	 * Date: 13-Apr-20
	 * Time: 9:39 PM
	 */
	
	namespace Dashboard\Controller;
	
	
	use Application\Service\PostServiceInterface;

	use Laminas\Mvc\Controller\AbstractActionController;
	use Laminas\Session\Container;
    use Laminas\Uri\Uri;
    use Laminas\View\Model\JsonModel;
	use Laminas\View\Model\ViewModel;
	
	class CurrencyManagementController extends AbstractActionController
	{
		protected $postservice;

		protected $headerView;
		protected $menuView;
		protected $sliderView;
		protected $footerView;
		protected $layout;
		protected $headerVars;
		protected $menuVars;
		protected $sliderVars;
		protected $footerVars;
		protected $contentVars;
		

		protected $currentUser;
		protected $sessionManager;
		protected $sessionContainer;

		protected $table;
		protected $serviceLocator;
		public function __construct(PostServiceInterface $postservice, $container)
		{
			$this->postservice = $postservice;
			$this->serviceLocator = $container;
			
			$this->sessionContainer = new Container('lunichange');
			$this->sessionManager = $this->sessionContainer->getManager();
			//Layout
			$this->contentVars = array();
			$this->table = 'luni_monnaies';
			//header layout
			// $this->headerView = new ViewModel();
			//$this->headerVars = array();
			//$this->headerView->setTemplate('layout/header');
			
			//menu layout
			$this->menuView = new ViewModel();
			$this->menuVars = array();
			$this->menuView->setTemplate('layout/menu');
			
			
			
			//footerView
			$this->footerView = new ViewModel();
			 $this->footerView->setTemplate('layout/footer');
			
			$this->headerVars = array();
			$this->footerVars = array();
			$this->contentVars = array();
			
		}
		public function indexAction()
		{
			
			$this->checkAuth ();
			$this->_getHelper('headScript', $this->serviceLocator)
				
				->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')
				->appendFile($this->getUriPath() . 'assets/pages/global/forms_general.js?'.uniqid())
				->appendFile($this->getUriPath() . 'assets/pages/currencyManagement/ajax_list_of_currency_new.min.js')
				->appendFile($this->getUriPath() . 'assets/pages/currencyManagement/custom_script_new.js')
			//	->appendFile($this->getUriPath() . 'assets/pages/global/Helpers.js')
			
			;
			$this->renderPage ();
			
			
			$messagesNotification = array();
			
			if(isset($this->sessionContainer->Messages) && $this->sessionContainer->Messages !== '' && $this->sessionContainer->Messages!=NULL && !empty($this->sessionContainer->Messages))
			{
				$messagesNotification = $this->sessionContainer->Messages;
				unset($this->sessionContainer->Messages);
			}
			
			return array(
					'typeMonnaie'=>$this->getTypesMonnaie (),
					'categoriesMonnaie'=>$this->getCategorieMonnaie (),
					'listeMonnaies'=>$this->getAllMonnaie (array(
						'statut'=>'1'
					)),
					'messagesNotification'=>$messagesNotification
			);
		}
	
		
		public function addCurrencyAction()
		{
			$request = $this->getRequest();
			$tableJson = array();
			$view = null;
			if($request->isXmlHttpRequest())
			{
				$data = $request->getPost()->toArray();
				
				$dataEmpty = false;
				
				foreach ($data as $keys=> $datum)
				{
					if(empty($data[$keys]) || $data[$keys] == '')
					{
						$dataEmpty = true;
					}
				}
				
				if($dataEmpty)
				{
					$tableJson['error'] = "Veuillez remplir tous les champs.";
				}
				else
				{
					$datas['libelle_famille'] = filter_var(trim(htmlentities ($data['formadd_designation_monnaie'])),FILTER_SANITIZE_STRING);
					$datas['categorie_devise'] = filter_var (trim ($data['formadd_categorie_monnaie']),FILTER_SANITIZE_NUMBER_INT);
					$datas['type_monnaie'] = filter_var (trim ($data['formadd_type_monnaie']),FILTER_SANITIZE_STRING);
					if((bool)$this->getOneMonnaie (array(
						'libelle_famille'=>$datas['libelle_famille']
					)))
					{
						$tableJson['error'] = "Cette monnaie existe déjà.";
					}
					else
					{
						$insertionMonnaie = (bool)$this->postservice->defaultInsert ($this->postservice->getDbTables ()['luni_monnaies']['table'],$datas);
						
						if($insertionMonnaie)
						{
							$tableJson['success'] = "success";
							$this->sessionContainer->Messages = array(
								'type'=>array('erreur'=>'error','danger'=>'danger','success'=>'success'),
								'text'=>'Monnaie ajoutée.'
							);
							
						}
						else
						{
							$tableJson['error'] = "Une erreur s'est produite lors de l'opération";
						}
					}
				}
				
				
				$view = new JsonModel($tableJson);
				$view->setTerminal(true);
			}
			else
            {
                $this->redirect()->toRoute('currency-management');
            }
			
			return $view;
		}
		
		
		public function updateCurrencyAction()
		{
			$request =$this->getRequest ();
			$view = null;
			$tableJson = array();
			
			if($request->isXmlHttpRequest())
			{
				$data = $request->getPost()->toArray();
				$dataEmpty = false;
				
				foreach ($data as $keys=>$values)
				{
					if(empty($data[$keys]))
					{
						$dataEmpty = true;
					}
				}
				
				if($dataEmpty)
				{
					$tableJson['error'] = "Veuillez renseigner tous les champs.";
					
				}
				else
				{
					if(isset($data['input_hidden_update_currency']) && intval ($data['input_hidden_update_currency']) == $this->sessionContainer->StdGetForUpdate['id_famille'])
					{
						unset($this->sessionContainer->StdGetForUpdate);
						$checkifExist = $this->postservice->defaultSelect ($this->postservice->getDbTables ()['luni_monnaies']['table'],NULL,array(
							'id_famille<>?'=>filter_var ($data['input_hidden_update_currency'],FILTER_SANITIZE_NUMBER_INT),
							'libelle_famille'=>filter_var (trim(htmlentities ($data['input_update_designation_currency'])),FILTER_SANITIZE_STRING)
						),null,null,'unique',null,null);
						
						if((bool)$checkifExist)
						{
							$tableJson['error'] = "Cette désignation existe déjà pour une autre monnaie.";
						}
						else
						{
							$datasToInsert['libelle_famille'] = filter_var (trim (htmlentities ($data['input_update_designation_currency'],FILTER_SANITIZE_STRING)));
							$datasToInsert['categorie_devise'] = filter_var ($data['select_update_categorie_monnaie'],FILTER_SANITIZE_NUMBER_INT);
							$datasToInsert['type_monnaie'] = filter_var ($data['select_update_type_monnaie'],FILTER_SANITIZE_STRING);
							
							$requestUpdate = $this->postservice->defaultUpdate ($this->postservice->getDbTables ()['luni_monnaies']['table'],
								$datasToInsert,array ('id_famille'=>filter_var ($data['input_hidden_update_currency'],FILTER_SANITIZE_NUMBER_INT)));
							if((bool)$requestUpdate)
							{
								$tableJson['success'] = "Modification effectuée avec succès.";
							}
							else
							{
								$tableJson['error'] = "Une erreur s'est produite au cours de l'opération";
							}
						}
						
					}
					else
					{
						$tableJson['error'] = "Une erreur s'est produite au cours du traitement";
					}
				}
				$view = new JsonModel($tableJson);
				$view->setTerminal (true);
			}
			
			return $view;
		}
		public function listCurrencyAction()
		{
			$request = $this->getRequest ();
			$tableJson = array();
			$view = null;
			if($request->isXmlHttpRequest())
			{
				$join = null;
				
				$join [] = array('table'=>array('t'=>$this->postservice->getDbTables ()['type_monnaie']['table']),
					'condition'=>'t.'.$this->postservice->getDbTables ()['type_monnaie']['columns']['code_type_monnaie'].'= luni_monnaies.'.$this->postservice->getDbTables ()['luni_monnaies']['columns']['type_monnaie']
				
				);
				
				$join [] = array('table'=>array('c'=>$this->postservice->getDbTables ()['categorie_devise']['table']),
					'condition'=>'c.'.$this->postservice->getDbTables ()['categorie_devise']['columns']['id_cat_devise'].'= luni_monnaies.'.$this->postservice->getDbTables ()['luni_monnaies']['columns']['categorie_devise']
				
				);
				$colums = array(
					'status'=>'statut',
					'libelle_famille'=>'libelle_famille',
					'id_famille'=>'id_famille'
				);
				
				
				$getAllCurrency = $this->postservice->defaultSelect ('luni_monnaies',$colums,array(),$join,null,'all',null,null);
			
				$view  = new JsonModel(array('meta'=>array(
					"page"=> 1,
			        "pages"=> 1,
			        "perpage"=> -1,
			        "total"=> count($getAllCurrency),
			        "sort"=> "asc",
			        "field"=> "RecordID"
				),
					'data'=>$getAllCurrency));
				$view->setTerminal (true);
			
			}
			
			return $view;
		}
		public function updateStatusCurrencyAction()
		{
			$request = $this->getRequest ();
			$view = null;
			$tableJson = array();
			if($request->isXmlHttpRequest())
			{
				$getDataId = $this->params ()->fromQuery ('id');
				$getStatusValue = $this->params ()->fromQuery ('valeur');
				
				$checkIfExist = $this->getOneMonnaie (array(
					'id_famille'=>filter_var ($getDataId,FILTER_SANITIZE_NUMBER_INT)
				));
				
				if((bool)$checkIfExist)
				{
					if($this->updateCurrency (array('statut'=>$getStatusValue),array('id_famille'=>filter_var ($getDataId,FILTER_SANITIZE_NUMBER_INT))))
					{
						$tableJson['success'] = "Status mis à jour";
					}
					else
					{
						$tableJson['error'] = "Une erreur s'est produite au cours de l'opération";
					}
				}
				else
				{
					$tableJson['error'] = "Une erreur s'est produite au cours de l'opération";
				}
				$view = new JsonModel($tableJson);
				$view->setTerminal (true);
			}
			
			return $view;
		}
		public function stdGetAction()
		{
			$request =$this->getRequest ();
			$view = null;
			$tableJson = array();
			if($request->isXmlHttprequest())
			{
				$datas = $request->getPost()->toArray();
				$dataGetTable = $this->params ()->fromQuery ('table');
				$dataConditions = array();
					foreach ($datas as $keys=>$values)
					{
						$dataConditions[$keys] = $values;
					}
					$request = $this->postservice->defaultSelect ($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
					'unique',null,null);
					if((bool)$request)
					{
						$tableJson = $request;
						$this->sessionContainer->StdGetForUpdate = $request;
					}
					else
					{
						$tableJson['error'] = "Une erreur s'est produite. Veuillez rééssayer.";
					}
				
				$view = new JsonModel($tableJson);
				$view->setTerminal (true);
			}
			
			return $view;
		}
		
		
		
		public function getCategorieMonnaie()
		{
			
			return $this->postservice->defaultSelect ($this->postservice->getDbTables ()['categorie_devise']['table'],NULL,array(
				'statut'=>'1'),null,null,'all',null,
				$this->postservice->getDbTables ()['categorie_devise']['columns']['lib_cat_devise'].' ASC');
		}
		public function getTypesMonnaie()
		{
			return $this->postservice->defaultSelect ($this->postservice->getDbTables ()['type_monnaie']['table'],[],array(),
				null,null,'all',
				null,$this->postservice->getDbTables ()['type_monnaie']['columns']['lib_type_monnaie'].' ASC');
		}
		public function getAllMonnaie($hayshack)
		{
			return $this->postservice->defaultSelect(
				$this->postservice->getDbTables ()['luni_monnaies']['table'],[],
				$hayshack,
				null,null,'all',
				null,$this->postservice->getDbTables ()['luni_monnaies']['columns']['libelle'].' ASC'
			);
		}
		
		public function getAllMonnaieWithJoinAndLimit($hayshack,$join,$limit,$grpe,$sort)
		{
			$table = array('m'=>$this->postservice->getDbTables ()['luni_monnaies']['table']);
			if(isset($hayshack) && !empty($hayshack) && is_array($hayshack))
			{
				return $this->postservice->defaultSelect($table,null,$hayshack,$join,$limit,'all',$grpe,$sort);
			}
			return 0;
		}
		public function getOneMonnaie($hayshack)
		{
			return $this->postservice->defaultSelect(
				$this->postservice->getDbTables ()['luni_monnaies']['table'],array(),
				$hayshack,
				null,null,'unique',
				null,null
			);
		}
		
		
		private function updateCurrency($datas,$hayshack)
		{
			
			return (bool) $this->postservice->defaultUpdate ('luni_monnaies',$datas,$hayshack);
		}
		protected function _getHelper($helper, $serviceLocator)
		{
			return $serviceLocator
				->get('ViewHelperManager')
				->get($helper);
		}
		
		public function getUriPath()
		{
			$basePath = $this->getRequest()->getBasePath();
			$uri = new Uri($this->getRequest()->getUri());
			$uri->setPath($basePath);
			$uri->setQuery(array());
			$uri->setFragment('');
			$baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
			return $baseUrl;
		}

        private function getOneUser($id,$join)
        {
            return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
        }
		public function renderPage()
		{
			
			//header layout
			$this->headerView = new ViewModel();

            $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);

			$this->menuVars['user_name']= $this->sessionContainer->Login;
			$this->menuVars['user_id']= $this->sessionContainer->IdUser;
			
			$this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));

			$user_values = array();
            if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
            {
                $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
                // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
                $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


            }
            $this->menuVars['user_data'] = $user_values;
			
			//menu layout
			$this->menuView = new ViewModel();
			$this->menuView->setVariables($this->menuVars);
			$this->menuView->setTemplate('layout/menu');
			
			//footerView
			$this->footerView = new ViewModel();
			$this->footerView->setTemplate('layout/footer');
			
			$this->layout = $this->layout();
			
			$this->layout->addChild($this->menuView, 'menu');
			$this->layout->addChild($this->footerView, 'footer');
			$this->layout->setVariables($this->contentVars);
			
			
			$this->layout->setTemplate('layout/layout');
			
			
		}
		
		public function checkAuth()
		{
			
			if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
			{
				$this->redirect()->toRoute('logout');
			}
			elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
			{
				if($this->sessionContainer->CodeProfil === 2)
				{
					$this->redirect ()->toRoute ('logout');
				}
				
			}
			/*elseif($this->sessionContainer->CodeProfil == 2)
		   {
			   $this->redirect()->toRoute('customer');
		   }
		   elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
		   {
			   $this->redirect()->toRoute("administration");
		   }*/
			/*
			elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
			{
				$this->redirect()->toRoute('customer');
			}
			else
			{
				$this->redirect()->toRoute('administration');
			}*/
		}
		
		
	}