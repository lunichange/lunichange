<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 19-Apr-20
 * Time: 5:10 AM
 */

namespace Dashboard\Controller;

use Application\Service\PostServiceInterface;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
class EquivalencesSettingsController extends AbstractActionController
{

    protected $postservice;


    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;

    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $table;

    protected $serviceLocator;

    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();
        $this->table = 'luni_monnaies';


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');



        //footerView
        //$this->footerView = new ViewModel();
        // $this->footerView->setTemplate('layout/footer');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }

    public function indexAction()
    {
        $this->checkAuth();


        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')
            ->appendFile($this->getUriPath() .'assets/js/pages/custom/user/add-user.js')
            ->appendFile($this->getUriPath() . 'assets/plugins/datatables/datatables.bundle.js')
            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/datatables/search-options/advanced-search_currencies-management_new.js')
            ->appendFile($this->getUriPath() .'assets/pages/equivalences-settings/custom_script.js?'.uniqid())
            ->appendFile($this->getUriPath() .'assets/pages/equivalences-settings/custom_edit_script.js?'.uniqid())

            ->appendFile($this->getUriPath() .'assets/pages/equivalences-settings/ajax_list_devises_equivalences.js?'.uniqid())
            ->appendFile($this->getUriPath() .'assets/pages/equivalences-settings/make_equivalence_global.js')
           // ->appendFile($this->getUriPath() .'assets/pages/equivalences-settings/ajax_form_script.js')
            ->appendFile($this->getUriPath() .'assets/js/pages/components/extended/blockui.js?'.uniqid())

            ;
        $this->renderPage();
           $loadMonnaies = $this->postservice->defaultSelect('luni_monnaies',array(),array('statut'=>'1'),null,
               null,'all',null,'libelle_famille ASC');
           $loadDevises = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array(
               'statut'=>'1'),null,null,'all',null,'lib_devise ASC');

           $arrayConditionsElectronicCurrencies = array();
           $arrayConditionsElectronicCurrencies [] = array(
               'statut'=>'1',
               'categorie_devise <> ?'=>2
           );

           $getEquivalencesElectronicCurrencies = $this->postservice->defaultSelect('equivalence_globale',[],[],null,null,'all',null,null);

           $recupId = $getRequest= $joinEquivalence= array();
           foreach($getEquivalencesElectronicCurrencies as $values)
           {
               $recupId [] = intval($values['devise']);
           }



           $joinEquivalence [] = array('table'=>array('e'=>'equivalence_globale'),'condition'=>'e.devise=d.id_devise');

            $electronicCurrencies = $this->postservice->defaultSelect('devises',[],array(
                'statut'=>'1',
                'categorie_devise <> ?'=>2
            ),null,null,'all',null,'lib_devise ASC');

            $getCurrenciesEquivGlobal = $this->postservice->defaultSelect(array('d'=>'devises'),[],array('statut'=>'1'),$joinEquivalence,null,'all',null,'lib_devise ASC');

           return array(
            'loadMonnaie'=>$loadMonnaies,
            'loadElectronicCurrencies'=>$electronicCurrencies,
            'loadDevises'=>$loadDevises,
            'getCurrenciesEquivGlobal'=>$getCurrenciesEquivGlobal
        );
    }

    public function loadCurrenciesFromCurrencyAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $datas = $request->getPost()->toArray();
            $dataEmpty = false;

            foreach($datas as $keys=>$values)
            {
                if($datas[$keys] === "" || empty($datas[$keys]))
                {
                    $dataEmpty = true;
                    $tableJson['error'] = "Veuillez renseigner toutes les informations";
                }
            }

            if(isset($datas['source']))
            {$source = intval($datas['source']);}
            if(isset($datas['destination']))
            {$destination = intval($datas['destination']);}

            if($dataEmpty == false && !isset($tableJson['error']) || empty($tableJson['error']))
            {
                $tableJson['source'] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],
                    array(
                        $this->postservice->getDbTables()['devises']['columns']['famille_devise']=>$source
                    ),null,null,'all',null,null);
                $tableJson['destination'] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],
                    array(
                        $this->postservice->getDbTables()['devises']['columns']['famille_devise']=>$destination
                    ),null,null,'all',null,null);
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }

        return $view;
    }

    public function addGlobalSettingsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            if(isset($data['electronic_currency'],$data['purchase_price'],$data['sale_price'],$data['quantite_min']) && !empty($data['electronic_currency']))
            {

                $checkifSettingsAlreadyExist = $this->postservice->defaultSelect('equivalence_globale',[],array('devise'=>intval($data['electronic_currency'])),null,null,'unique',null,null);

                if(empty($checkifSettingsAlreadyExist))
                {
                    $insertData = array();
                    $insertData['devise'] = intval($data['electronic_currency']);
                    $insertData['prix_achat'] = floatval(filter_var($data['purchase_price'],FILTER_SANITIZE_STRING));
                    $insertData['prix_vente'] = floatval(filter_var($data['sale_price'],FILTER_SANITIZE_STRING));
                    $insertData['quantite_min'] = floatval(filter_var($data['quantite_min'],FILTER_SANITIZE_STRING));

                    $reqInsert = $this->postservice->defaultInsert('equivalence_globale',$insertData);

                    if($reqInsert)
                    {
                        $tableJson['success'] = "Paramétrage effectué.";
                    }
                    else
                    {
                        $tableJson['error'] = "Une erreur s'est produite lors de l'insertion.";
                    }
                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement. Ce paramétrage existe déjà !";
                }
            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer le traitement.";
            }



            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('equivalences-settings');
        }



        return $view;

    }

    public function updateGlobalSettingsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            if(isset($data['update_electronic_currency'],$data['update_purchase_price'],$data['update_sale_price']) && !empty($data['update_electronic_currency']))
            {
                $updateData = array();

                $updateData['prix_achat'] = floatval(filter_var($data['update_purchase_price'],FILTER_SANITIZE_STRING));
                $updateData['prix_vente'] = floatval(filter_var($data['update_sale_price'],FILTER_SANITIZE_STRING));
                $updateData['quantite_min'] = floatval(filter_var($data['update_quantite_min'],FILTER_SANITIZE_STRING));

                $reqUpdate = $this->postservice->defaultUpdate('equivalence_globale',$updateData,array('devise'=>intval($data['update_electronic_currency'])));

                if($reqUpdate)
                {
                    $tableJson['success'] = "Modification effetuée.";
                }
                else
                {
                    $tableJson['error'] = "Une erreur s'est prduite au cours du traitement.";
                }


            }
            else
            {
                $tableJson['error'] = "Traitement impossible à effectuer.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('equivalences-settings');
        }

        return $view;
    }

    public function loadDevisesEquivalencesAction()
    {

        $request = $this->getRequest();
        $view =  $tableJson =  null;
        $myArray = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $arrayConditions[] = array('d.statut'=>'1');
            $limit = null;

            if(isset($_GET['length']) && !empty($_GET['length']) && $_GET['length'] !== -1)
            {
                $limit['start'] = $_GET['length'];
                $limit['end'] = $_GET['start'];
            }

            if(isset($_GET['search']) && !empty($_GET['search']['value']))
            {
                $arrayConditions [] = array(
                    'd.lib_devise LIKE ?'=>'%'.$_GET['search']['value'].'%'
                );
            }

            $joins [] =array('table'=>array('d'=>'devises'),'condition'=>'d.id_devise=v.source');

            $myRequest = $this->getEquivalences($arrayConditions,[],$joins,$limit,'all',null,null);
            $rowCount = count($this->getEquivalences($arrayConditions,[],$joins,null,'all',null,null));
            foreach ($myRequest as $keys=>$values)
            {

                foreach ($values as $j=>$elements)
                {
                    $myArray[$keys][$j] = $elements;
                    if($j ==="destination")
                    {

                        $myArray[$keys][$j] = $this->postservice->defaultSelect('devises',[],array(
                            'id_devise'=>intval($elements)
                        ),null,null,'unique',null,null);

                    }
                    if($j === "source")
                    {
                        $myArray[$keys][$j] = $this->postservice->defaultSelect('devises',[],array(
                            'id_devise'=>intval($elements)
                        ),null,null,'unique',null,null);
                    }
                }

            }

            $tableJson ['aaData'] = $myArray;
            $tableJson['iTotalRecords'] = count($myRequest);
            $tableJson['iTotalDisplayRecords'] = $rowCount;
            $tableJson['sColumns'] ="";
            $tableJson['sEchos'] = isset($data['draw']) && !empty($data['draw']) ? intval($data['draw']) : null;

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('equivalences-settings');
        }

        return $view;

    }

    public function switchDeviseAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            if(isset($data['source'],$data['destination']))
            {
                $source = intval($data['source']);
                $destination = intval($data['destination']);

                $getElementFromEquivalence = $this->postservice->defaultSelect('equivalences',[],array('source'=>$source,'destination'=>$destination),null,null,'unique',null,null);

                if(!empty($getElementFromEquivalence))
                {
                    $reqUpdate = $this->postservice->defaultUpdate('equivalences',array(
                        'source'=>$destination,
                        'destination'=>$source
                    ),array('source'=>$source,'destination'=>$destination));
                    if($reqUpdate)
                    {

                        $tableJson['success'] = "Changement de devise forte effectué.";
                    }
                    else
                    {
                        $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                    }
                }
                else
                {
                    $tableJson['error'] = "Impossible de traiter votre demande.";
                }

            }
            else
            {
                $tableJson['error'] = "Votre demande ne peut aboutir.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('equivalences-settings');
        }

        return $view;
    }
    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttprequest())
        {
            $datas = $request->getPost()->toArray();
            $dataGetTable = $this->params ()->fromQuery ('table');
            $dataGetType = $this->params ()->fromQuery ('type');
            $dataConditions = array();
            // $this->sessionContainer->ElementForUpdate = array();
            foreach ($datas as $keys=>$values)
            {
                //$this->sessionContainer->ElementForUpdate[$keys] = $values;
                $dataConditions[$keys] = $values;
            }
            $request = $this->postservice->defaultSelect ($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
                $dataGetType,null,null);
            if((bool)$request)
            {
                $tableJson = $request;
                $this->sessionContainer->StdGetForUpdate = $request;
            }
            else
            {
                $tableJson['error'] = "Une erreur s'est produite. Veuillez rééssayer.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }

        return $view;
    }

    public function addEquivalencesSettingsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $tableJson = $data;
            $dataEmpty = false;

            foreach ($data as $keys=>$values)
            {
                if(empty(trim($data[$keys])))
                {
                    $dataEmpty = true;
                }
            }
            if(!$dataEmpty)
            {
                $id_source = intval($data['hidden_add_source']);
                $id_destination = intval($data['hidden_add_destination']);
                $taux_achat = floatval(trim($data['taux_achat']));
                $taux_vente = floatval(trim($data['taux_vente']));
                $quantite_min_achat  = floatval(trim($data['quantite_min_achat']));
                $quantite_min_vente  = floatval(trim($data['quantite_min_vente']));

                $checkIfSourceExiste = $this->postservice->defaultSelect('devises',[],array('id_devise'=>$id_source),null,null,'unique',null,null);
                $checkIfDestinationExiste = $this->postservice->defaultSelect('devises',[],array('id_devise'=>$id_destination),null,null,'unique',null,null);

                if(!empty($checkIfSourceExiste) && !empty($checkIfDestinationExiste))
                {
                    if($taux_achat < 0 || $taux_vente < 0)
                    {
                        $tableJson['error'] = "Veuillez vérifier les taux.";
                    }
                    elseif($quantite_min_achat < 0 || $quantite_min_vente < 0)
                    {
                        $tableJson['error'] = "Veuillez vérifier les soldes.";
                    }
                    else
                    {
                        if($taux_vente < $taux_achat)
                        {
                            $tableJson['error'] = "Le taux de vente doit-être supérieure ou égale au taux d'achat si vous voulez faire des bénéfices.";
                        }
                        else
                        {
                            $dataToInsert['source'] = $id_source;
                            $dataToInsert['destination'] = $id_destination;
                            $dataToInsert['taux_achat'] = $taux_achat;
                            $dataToInsert['taux_vente'] = $taux_vente;
                            $dataToInsert['quantite_min_achat'] = $quantite_min_achat;
                            $dataToInsert['quantite_min_vente'] = $quantite_min_vente;

                            $requestInsert = $this->postservice->defaultInsert('equivalences',$dataToInsert);
                            if((bool)$requestInsert)
                            {
                                $tableJson['success'] = "Paramétrage effectué.";
                            }
                            else
                            {
                                $tableJson['error'] = "Une erreur s'est produite au cours du traitement.";
                            }
                        }
                    }
                }
                else
                {
                    $tableJson['error'] = "Impossible de traiter ces informations.";
                }

            }
            else
            {
                $tableJson['error'] = "Veuillez renseigner tous les champs.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('equivalences-settings');
        }

        return $view;
    }
    private function getEquivalences($hayshack,$columns,$joins,$limit,$type,$grpe,$sort)
    {
            return $this->postservice->defaultSelect(array('v'=>'equivalences'),$columns,$hayshack,$joins,$limit,$type,$grpe,$sort);
    }


    private function getGlobalEquivalences($hayshack,$columns,$joins,$limit,$type,$grpe,$sort)
    {
            return $this->postservice->defaultSelect(array('v'=>'equivalence_globale'),$columns,$hayshack,$joins,$limit,$type,$grpe,$sort);
    }

    public function loadElementsInputAction()
    {
        $request = $this->getRequest();
        $tableJson = array();
        $view = null;

        if($request->isXmlHttpRequest())
        {
            $datas = $request->getPost()->toArray();

            if(isset($datas['source'],$datas['destination']) && !empty($datas['source']) && !empty($datas['destination']))
            {
                $id_source = filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT);
                $id_destination = filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT);
                if(isset($datas['edit']) && $datas['edit'] == true)
                {
                    $joins =  array();


                    $checkIfSourceIsSource = $this->getOneEquivalence(array('source'=>$id_source,'destination'=>$id_destination),[],null);
                    $checkIfSourceIsDestination = $this->getOneEquivalence(array('source'=>$id_destination,'destination'=>$id_source),[],null);

                    $joinsSource [] = array(
                        'table'=>array('e'=>$this->postservice->getDbTables()['equivalences']['table']),
                        'condition'=>'e.'.$this->postservice->getDbTables()['equivalences']['columns']['source'].'=d.'.$this->postservice->getDbTables()['devises']['columns']['id_devise']
                    );

                    $joinsDestination [] = array(
                        'table'=>array('e'=>$this->postservice->getDbTables()['equivalences']['table']),
                        'condition'=>'e.'.$this->postservice->getDbTables()['equivalences']['columns']['destination'].'=d.'.$this->postservice->getDbTables()['devises']['columns']['id_devise']
                    );

                    if((bool)$checkIfSourceIsSource)
                    {

                        $tableJson['source'] = $this->postservice->defaultSelect(
                            array('d'=>$this->postservice->getDbTables()['devises']['table']),[],
                            array(
                                $this->postservice->getDbTables()['devises']['columns']['id_devise']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['source']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['destination']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT),

                            ),$joinsSource,null,'unique',null,null
                        );

                        $tableJson['destination'] = $this->postservice->defaultSelect(
                            array('d'=>$this->postservice->getDbTables()['devises']['table']),[],
                            array(
                                $this->postservice->getDbTables()['devises']['columns']['id_devise']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['source']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['destination']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT),


                            ),$joinsDestination,null,'unique',null,null
                        );
                    }
                    elseif ((bool)$checkIfSourceIsDestination)
                    {


                        $tableJson['source'] = $this->postservice->defaultSelect(
                            array('d'=>$this->postservice->getDbTables()['devises']['table']),[],
                            array(
                                $this->postservice->getDbTables()['devises']['columns']['id_devise']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['source']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['destination']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT),

                            ),$joinsSource,null,'unique',null,null
                        );
                        $tableJson['destination'] = $this->postservice->defaultSelect(
                            array('d'=>$this->postservice->getDbTables()['devises']['table']),[],
                            array(
                                $this->postservice->getDbTables()['devises']['columns']['id_devise']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['source']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT),
                                $this->postservice->getDbTables()['equivalences']['columns']['destination']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT),

                            ),$joinsDestination,null,'unique',null,null
                        );
                    }
                    else
                    {
                        $tableJson['error'] = 'Aucune configuration n\'existe pour les devises choisies. Pour paramètrer ces devises allez dans l\'onglet paramétrage des équivalences.';
                    }

                }
                else
                {
                    $controlCheckSource = $this->postservice->defaultSelect('equivalences',[],['source'=>$id_source,'destination'=>$id_destination],null,null,'unique',null,null);
                    $controlcheckDestination = $this->postservice->defaultSelect('equivalences',[],array('source'=>$id_destination,'destination'=>$id_source),null,null,'unique',null,null);

                    if(empty($controlCheckSource) && empty($controlcheckDestination))
                    {
                        $tableJson['source'] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],
                            array(
                                $this->postservice->getDbTables()['devises']['columns']['id_devise']=>filter_var($datas['source'],FILTER_SANITIZE_NUMBER_INT)
                            ),null,null,'unique',null,null);

                        $tableJson['destination'] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],
                            array(
                                $this->postservice->getDbTables()['devises']['columns']['id_devise']=>filter_var($datas['destination'],FILTER_SANITIZE_NUMBER_INT)
                            ),null,null,'unique',null,null);
                    }
                    else
                    {
                        $tableJson['error'] = "Un paramétrage existe déjà pour ces devises. Allez dans l'onglet: Modifier un paramétrage si vous conmptez faire des modifications.";
                    }


                }

            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

        }


        return $view;
    }


    public function editEquivalencesSettingsAction()
    {

        $request = $this->getRequest();
        $view = null;
        $tableJson = null;
        $dataEmpty = false;

        if ($request->isXmlHttpRequest()) {
            $datas = $request->getPost()->toArray();
            $tableJson = array();
            $dataEmpty = false;

            foreach ($datas as $keys => $values)
            {
                if (empty(trim($datas[$keys])))
                {
                    $dataEmpty = true;
                    break;
                }
            }

            if(!$dataEmpty)
            {
                $id_source = filter_var($datas['hidden_edit_source'],FILTER_SANITIZE_NUMBER_INT);
                $id_destination = filter_var($datas['hidden_edit_destination'],FILTER_SANITIZE_NUMBER_INT);

                $taux_achat = trim(floatval($datas['taux_achat']));
                $taux_vente = trim(floatval($datas['taux_vente']));

                $quantite_min_achat = trim(floatval($datas['quantite_min_achat']));
                $quantite_min_vente = trim(floatval($datas['quantite_min_vente']));

                if($taux_achat < 0 || $taux_vente < 0)
                {
                    $tableJson['error'] = "Veuillez vérifier les taux.";
                }
                elseif($quantite_min_vente < 0 || $quantite_min_achat < 0)
                {
                    $tableJson['error'] = "Veuillez vérifier les taux.";
                }
                else
                {
                    $dataToInsert['taux_achat'] = $taux_achat;
                    $dataToInsert['taux_vente'] = $taux_vente;
                    $dataToInsert['quantite_min_achat'] = $quantite_min_achat;
                    $dataToInsert['quantite_min_vente'] = $quantite_min_vente;
                     $req = (bool) $this->postservice->defaultUpdate('equivalences',$dataToInsert,array(
                        'source'=>$id_source,
                        'destination'=>$id_destination
                    ));
                     if($req)
                     {
                         $tableJson['success'] = "Modification réussie.";
                     }
                     else
                     {
                         $tableJson['error'] = "Une erreur s'est produite au cours de l'insertion.";
                     }
                }
            }
            else
            {
                $tableJson['error'] = "Veuillez remplir tous les champs.";
            }


            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

        }
        return $view;
    }
    public function listGlobalSettingsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        $arrayConditions = array();
        $myArray = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            $limit = null;

            if(isset($_GET['length']) && !empty($_GET['length']) && $_GET['length'] !== -1)
            {
                $limit['start'] = $_GET['length'];
                $limit['end'] = $_GET['start'];
            }

            if(isset($_GET['search']) && !empty($_GET['search']['value']))
            {
                $arrayConditions [] = array(
                    'd.lib_devise LIKE ?'=>'%'.$_GET['search']['value'].'%'
                );
            }

            $joins [] =array('table'=>array('d'=>'devises'),'condition'=>'d.id_devise=v.devise');

            $myRequest = $this->getGlobalEquivalences($arrayConditions,[],$joins,$limit,'all',null,null);
            $rowCount = count($this->getGlobalEquivalences($arrayConditions,[],$joins,null,'all',null,null));
            foreach ($myRequest as $keys=>$values)
            {

                foreach ($values as $j=>$elements)
                {
                    $myArray[$keys][$j] = $elements;
                    if($j ==="devise")
                    {

                        $myArray[$keys][$j] = $this->postservice->defaultSelect('devises',[],array(
                            'id_devise'=>intval($elements)
                        ),null,null,'unique',null,null);

                    }


                }

            }

            $tableJson ['aaData'] = $myArray;
            $tableJson['iTotalRecords'] = count($myRequest);
            $tableJson['iTotalDisplayRecords'] = $rowCount;
            $tableJson['sColumns'] ="";
            $tableJson['sEchos'] = isset($data['draw']) && !empty($data['draw']) ? intval($data['draw']) : null;

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('equivalences-settings');
        }


        return $view;
    }
    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }
    public function renderPage()
    {

        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        //header layout
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }

    private function getOneEquivalence($hayshack,$columns,$join)
    {
        return $this->postservice->defaultSelect('equivalences',$columns,$hayshack,$join,null,'unique',null,null);
    }


    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil == 2)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }

    }
}