<?php
namespace Dashboard\Mapper;


use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\I18n\Validator\DateTime;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Debug\Debug;
use Dashboard\Mapper\AbstractHomeDbSqlMapper;
use MailMan\Message as MailManMessage;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class HomeDbSqlMapper extends AbstractHomeDbSqlMapper implements PostMapperInterface
{
    protected $dbAdapter;

    protected $hydrator;

    protected $postPrototype;
    private $dbTables = array(
        'customers'=>array(
            'table'=>'luni_users',
            'columns'=>array(
                'id_user'=>'id_user',
                'nom_user'=>'nom',
                'prenoms_user'=>'prenoms',
                'email_user'=>'email',
                'password_user'=>'password',
                'date_creation'=>'date_creation',
                'last_connected'=>'derniere_connexion',
                'online'=>'online',
                'lock_u'=>'lock_u',
                'code_profil'=>'code_profil',
                'statut'=>'statut',
                'derniere_deconnexion'=>'derniere_deconnexion'
            )
        ),
       'documents'=>array(
            'table'=>'documents',
            'columns'=>array(
                'id_document'=>'id_document',
                'type_document'=>'type_document',
                'chemin_document'=>'chemin_document',
                'numero_piece'=>'numero_piece',
                'document'=>'document',
                'id_user'=>'id_user',
                'lock_doc'=>'lock_doc',
                'statut'=>'statut'
            )
       ),
       'pays'=>array(
            'table'=>'pays',
            'columns'=>array(
                'id_pays'=>'id_pays',
                'lib_pays'=>'lib_pays',
                'code_pays'=>'code_pays',
                'statut'=>'statut'
            )
       ),
       'access_menu'=>array(
           'table'=>'luni_access_menu',
           'columns'=>array(
               'id_ac_menu'=>'id_ac_menu',
               'id_menu'=>'id_menu',
               'code_profil'=>'code_profil',
               'statut'=>'statut'
           )
       ),
        'menu'=>array(
            'table'=>'luni_menu',
            'columns'=>array(
                'id_menu'=>'id_menu',
                'label_menu'=>'label_menu',
                'link'=>'link',
                'controller'=>'controller',
                'icon'=>'icon',
                'ranking'=>'ranking'
            )
        ),

        'categorie_devise'=>array(
            'table'=>'categorie_devise',
            'columns'=>array(
                'id_cat_devise'=>'id_categorie_devise',
                'lib_cat_devise'=>'lib_categorie_devise',
                'statut'=>'statut'
            )
        ),

        'devises'=>array(
            'table'=>'devises',
            'columns'=>array(
                'id_devise'=>'id_devise',
                'lib_devise'=>'lib_devise',
                'img_devise'=>'image_devise',
                'cat_devise'=>'categorie_devise',
                'code_devise'=>'code_devise',
                'statut'=>'statut',
                'famille_devise'=>'famille_devise'
            )
        ),
        "luni_monnaies"=>array(

            'table'=>'luni_monnaies',
            'columns'=>array(
                'id'=>'id_famille',
                'libelle'=>'libelle_famille',
                'capitalize'=>'capitalize',
                'categorie_devise'=>'categorie_devise',
                'type_monnaie'=>'type_monnaie',
                'img_famille'=>'img_famille',
	            'statut'=>'statut'
            )
        ),
        'type_monnaie'=>array(
        	'table'=>'luni_type_monnaie',
	        'columns'=>array(
	        	'id_type_monnaie'=>'id_type_monnaie',
		        'lib_type_monnaie'=>'lib_type_monnaie',
		        'code_type_monnaie'=>'code_type_monnaie'
	        )
        ),
        'transactions_temp'=>array(
            'table'=>'transactions_temp',
            'columns'=>array(
                'id'=>'id_transaction',
                'devise_source'=>'devise_source',
                'devise_destination'=>'devise_destination',
                'quantite_source'=>'quantite_source',
                'quantite_destination'=>'quantite_destination',
                'numero_depot'=>'numero_depot',
                'adresse_reception'=>'adresse_reception',
                'made_by'=>'made_by'
            )
        ),
        'equivalences'=>array(
            'table'=>'equivalences',
            'columns'=>array(
                'id_equivalence'=>'id_equivalence',
                'source'=>'source',
                'destination'=>'destination',
                'taux'=>'taux',
                'solde_minimum'=>'quantite_minimum'
            )
        ),
        'equivalence_globale'=>array(
            'table'=>'equivalence_globale',
            'columns'=>array(
                'id_equiv'=>'id_equiv',
                'devise'=>'devise',
                'prix_achat'=>'prix_achat',
                'prix_vente'=>'prix_vente'
            )
        ),
        'profil_user'=>array(
            'table'=>'profil_user',
            'columns'=>array(
                'id_profil'=>'id_profil',
                'lib_profil'=>'lib_profil',
                'code_profil'=>'code_profil',
                'access_level'=>'access_level'
            )
        ),
        'devises_input'=>array(
            'table'=>'devises_input',
            'columns'=>array(
                'id_devise_input'=>'id_devise_input',
                'id_devise'=>'id_devise',
                'type_input'=>'type_input'
            )
        ),
        'numeros_transactions'=>array(
            'table'=>'numeros_transactions',
            'columns'=>array(
                "id_numero"=>'id_numero',
                'libelle_numero'=>'libelle_numero',
                'status'=>'status',
                'devise'=>'devise',
                'number_name'=>'number_name'
            )
        ),
        'transactions'=>array(
            'table'=>'transactions',
            'columns'=>array(
                'id_transaction'=>'id_transaction',
                'context'=>'context',
                'code_transaction'=>'code_transaction',
                'devise_source'=>'devise_source',
                'devise_cible'=>'devise_cible',
                'date_transaction'=>'date_transaction',
                'numero_envoi'=>'numero_envoi',
                'adresse_reception'=>'adresse_reception',
                'quantite_source'=>'quantite_source',
                'quantite_cible'=>'quantite_cible'
            )
        ),
        'messages_site'=>array(
            'table'=>'messages_site',
            'columns'=>array(
                'id_message'=>'id_message',
                'contenu'=>'contenu',
                'email'=>'email',
                'subject'=>'subject',
                'customer'=>'customer',
                'contact'=>'contact',
                'statut'=>'statut'
            )
        )
    );


    public function __construct(\Zend\Db\Adapter\AdapterInterface $dbAdapter, HydratorInterface $hydrator)
    {
        $this->dbAdapter = $dbAdapter;
        $this->hydrator = $hydrator;
    }

    /**
     * @param $table
     * @param $columns
     * @param $conditions
     * @param $joins
     * @param $limit
     * @param $type
     * @param $grpBy
     * @param $sort
     * @return array|mixed
     */
    public function defaultSelect ($table, $columns, $conditions, $joins, $limit, $type, $grpBy, $sort)
	{
		$sql = new Sql($this->dbAdapter);
		$select = $sql->select($table);
		if(is_array ($columns) && !empty($columns) && $columns !== NULL)
		{
			$select->columns ($columns);
		}
		
		
		if($joins)
		{
		    if(isset($joins['type'],$joins['data']))
            {

                    foreach($joins['data'] as $join)
                    {
                        $select->join($join['table'],$join['condition'],array(),$joins['type']);
                    }

            }
            else
            {
                foreach($joins as $join)
                {
                    $select->join($join['table'],$join['condition']);
                }
            }

		}
		if($grpBy)
		{
			$select->group($grpBy);
		}
		if($conditions)
		{
			$cond = array();
			if(is_array ($conditions))
			{
				$elementIsArray = false;
				
				foreach ($conditions as $keys=>$elements)
				{
					if(is_array ($conditions[$keys]))
					{
						$elementIsArray = true;
					}
				}
				if($elementIsArray)
				{
					foreach ($conditions as $keys=>$elements)
					{
						foreach ($elements as $kk=>$values)
						{
							$cond[$kk][] = $values;
						}
					}
				}
				else
				{
					$cond = $conditions;
				}

                if(isset($cond['or'],$cond['results']) && !empty($cond['or']))
                {
                    $select->where($cond['results'],PredicateSet::OP_OR);
                }
                else
                {
                    $select->where($cond);
                }
				//$select->where($cond);
			}
			
		}
		if($sort)
		{
			$select->order($sort);
		}
		if($limit)
		{
			if(isset($limit['start'],$limit['end']))
			{
				//
				$select->limit(intval($limit['start']));
				$select->offset (intval ($limit['end']));
			}
			else
            {
                $select->limit(intval($limit));
            }
			
			
			
			
		}
		
		$stmt = $sql->prepareStatementForSqlObject($select);
		$result = $stmt->execute();
		
		if($type == 'unique')
		{
			if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
				return $result->current();
			}
			return array();
		}
		else
		{
			if ($result instanceof ResultInterface && $result->isQueryResult()) {
				$resultSet = new ResultSet();
				$resultSet->initialize($result);
				return $resultSet->toArray();
			}
			return array();
		}
	}

	public function defaultUpdate ($table, $data, $conditions)
	{
		
		$sql = new Sql($this->dbAdapter);
		$update = $sql->update($table);
		$update->set($data);
		$update->where($conditions);
		$stmt= $sql->prepareStatementForSqlObject($update);
		$stmt->execute();
		return 1;
	}
	
	public function defaultInsert ($table, $data)
	{
		$sql = new Sql($this->dbAdapter);
		$data_in = filter_var_array($data, FILTER_SANITIZE_STRING);
		$insert = $sql->insert($table);
		$insert->values($data_in);
		$stmt= $sql->prepareStatementForSqlObject($insert);
		$stmt->execute();
		
		return 1;
	}
	
	public function defaultDelete ($table, $conditions)
	{
		// TODO: Implement defaultDelete() method.
		$sql = new Sql($this->dbAdapter);
		$delete = $sql->delete($table);
		$delete->where($conditions);
		$stmt = $sql->prepareStatementForSqlObject($delete);
		$stmt->execute();
		return 1;
	}
	
	public function userLogin ($login, $pwd)
	{
		// TODO: Implement userLogin() method.
		
		$sql = new Sql($this->dbAdapter);
		$select = $sql->select($this->dbTables['customers']['table']);
		$select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)'), 'id_user'=>'id_user', 'password'=>'password'));
		$select->where(array("email=?" => $login));
		$select->where(array("lock_u=?"=>'0', "statut=?"=>'1'), PredicateSet::OP_AND);
		$stmt = $sql->prepareStatementForSqlObject($select);
		$result = $stmt->execute();
		
		
		if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
			
			$alldata = $result->current();
			
			if (intval($alldata['num'])) {
				
				$passcheck = password_verify($pwd, $alldata['password']);
				
				if ($passcheck) {
					$select = $sql->select(array('u'=>$this->dbTables['customers']['table']));
					$select->join(array('p'=>$this->dbTables['profil_user']['table']),'u.code_profil= p.code_profil');
					$select->where(array('u.id_user=?' => $alldata['id_user']));
					$stmt = $sql->prepareStatementForSqlObject($select);
					$result = $stmt->execute();
					
					if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
						$r = $result->current();
						return array(
							'message'=>'',
							'id_user'=>$r['id_user'],
							'profil_name'=>$r['lib_profil'],
							'code_profil'=>$r['code_profil'],
							'login'=>$r['email'],
							'user_name'=>$r['nom_complet']
						);
					}
				}
				return array(
					'message'=>'D&eacute;sol&eacute; mot de passe incorrect.',
					'user_data'=>array()
				);
				
			} else {
				return array(
					'message'=>'Vos identifiants sonts incorrectes ou votre compte n\'est pas actif.',
					'user_data'=>array()
				);
			}
			
		} else {
			return array(
				'message'=>'os identifiants sonts incorrectes ou votre compte n\'est pas actif.',
				'user_data'=>array()
			);
		}
	}
	
	public function getDbTables()
	{
		// TODO: Implement getDbTables() method.
		if(!empty($this->dbTables))
			return $this->dbTables;
		
		return array();
	}
	
	public function checkIfUserExist($login)
	{
		$sql = new Sql($this->dbAdapter);
		
		$select = $sql->select ($this->dbTables['customers']['table']);
		$select->where(array(
			
			$this->dbTables['customers']['columns']['email'].'=?'=>filter_var (trim($login),FILTER_VALIDATE_EMAIL),
		
		));
		$stmt = $sql->prepareStatementForSqlObject($select);
		$result = $stmt->execute();
		$returnValue = false;
		if($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows())
		{
			$returnValue = (bool) $result->current ();
		}
		
		return $returnValue;
	}

    public function sendMail($message, $key,$token,$destinataire,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template)
    {
        //Envoie des notifications mail
        $incMail = 0;


        try {
            //Les paramètres d'envoie
            $template_array = [];
            $template_array['key'] = $key;
            $template_array['token'] = $token;
            $template_array['btn_url'] = $btn_url;
            $template_array['btn_label'] = $btn_label;
            $template_array['message'] = $message;
            $template_array['nom_user'] = $destinataire;
            $template_array['titre'] = $titre;


            //Envoie du mail de notification
            $mailService = $this->getServiceLocator()->get('MailMan\LUNI');
            $view       = new PhpRenderer();
            $resolver   = new TemplateMapResolver();
            $resolver->setMap(array(
                $template => __DIR__ . '/../../../view/mails/'.$template.'.phtml'
            ));
            $view->setResolver($resolver);

            $content  = new ViewModel();
            $content->setTemplate($template)->setVariables($template_array);
            $content->setVariables($template_array);
            $message = new MailManMessage();
            $message->setSubject($subject);
            $message->addHtmlPart($view->render($content));
            $message->addTo($email);
            $message->addFrom($from_mail, $from_id);
            $res = $mailService->send($message);
            $logMessage = "Succès Envoi de mail à " . $destinataire . "  au mail " . $email . " le" . date("d-m-Y H:m:s") . " Résultat : " . $res;
            $this->mailLog($logMessage);
        } catch (\Exception $ex) {

            $logMessage = "Erreur Envoi de mail à " .$destinataire . " au mail " . $email . " le" . date("d-m-Y H:m:s") . " Résultat : " . $ex;
            $this->mailLog($logMessage);
        }

    }

    public function getSalutation()
    {
        $salutations = "Bonjour";
        $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
        $getFormat = $nowUtc->format('H');
        $getFormat = intval($getFormat);

        if($getFormat > 12)
        {
            $salutations = "Bonsoir";
        }

        return $salutations;
    }
    /**
     * Envoi de rapport mail
     * @param $messageMail
     * @param $destinateurMail
     * @param $destinateurNom
     * @param $destinataireMail
     */
    public function mailRapport($messageMail, $destinateurMail, $destinateurNom, $destinataireMail)
    {
        $mailService = $this->getServiceLocator()->get('MailMan\HBD');
        $message = new MailManMessage();
        $message->addTextPart($messageMail);
        $message->setSubject('RAPPORT HBD MTN DU ' . date('d-m-Y'));
        $message->addFrom($destinateurMail, $destinateurNom);
        $message->addTo($destinataireMail);
        $mailService->send($message);
        $message = null;
    }

    /**
     * Log des mails
     * @param null $message
     */
    private function mailLog($message = null)
    {
        if (isset($message)) {
            $config = $this->getServiceLocator()->get('Config');
            $log_path = $config['log_path'];
            $logger = new Logger();
            $write = new Stream($log_path . 'mail.log');
            $logger->addWriter($write);
            $logger->log(Logger::ALERT, 'MAIL MESSAGE');
            $message = $message . " \n";
            $logger->alert($message);
        }
    }
	
	public function loadMenuGroup($id_group, $module)
	{
		$temp_hold = array();
		$sql = new Sql($this->dbAdapter);
		
		$select2 = $sql->select($this->dbTables['access_menu']['table']);
		$select2->where(array(
			$this->dbTables['access_menu']['columns']['statut'] =>'1',
			$this->dbTables['access_menu']['columns']['code_profil'] =>intval($id_group)
		));

		
		$stmt2 = $sql->prepareStatementForSqlObject($select2);
		$result2 = $stmt2->execute();
		if ($result2 instanceof ResultInterface && $result2->isQueryResult()) {
			$resultSet2 = new ResultSet();
			$resultSet2->initialize($result2);
			foreach ($resultSet2->toArray() as $eachto) {
				$temp_hold[$eachto['id_menu']] = intval($eachto['id_menu']);
			}
		}
		
		
		return $temp_hold;
	}

    public function password_strength($password)
    {
        $password_length = 6;

        $returnVal = True;

        if ( strlen($password) < $password_length ) {
            $returnVal = False;
        }

        if ( !preg_match("#[0-9]+#", $password) ) {
            $returnVal = False;
        }

        if ( !preg_match("#[a-z]+#", $password) ) {
            $returnVal = False;
        }

        return $returnVal;

    }
	public function retriveMenu($id_group,$module)
	{
		$id_group = intval ($id_group);
		
		$outarray = array();
		
		$sql = new Sql($this->dbAdapter);
		$select = $sql->select('luni_menu');
		$select->where(array('statut' => '1', 'level' => 0));
		$select->order('ranking');
		
		$stmt = $sql->prepareStatementForSqlObject($select);
		$result = $stmt->execute();
		
		$temp_hold = $this->loadMenuGroup($id_group,$module);
		
	
		if ($result instanceof ResultInterface && $result->isQueryResult()) {
			$resultSet = new ResultSet();
			$resultSet->initialize($result);
			
			
			
			foreach ($resultSet->toArray() as $eachtopmenu) {
				
				$obj = new \stdClass();
				$obj->id = $eachtopmenu['id_menu'];
				$obj->label = $eachtopmenu['label_menu'];
				$obj->link = $eachtopmenu['link'];
				$obj->controller = $eachtopmenu['controller'];
				$obj->css_id = $eachtopmenu['css_id'];
				$obj->icon = $eachtopmenu['icon'];
				$obj->child = array();
				
				$eachtopmenu['id_menu'] = intval ($eachtopmenu['id_menu']);
			
				
					
					
					if (in_array(intval($eachtopmenu['id_menu']), $temp_hold))
						$outarray[$eachtopmenu['id_menu']] = $obj;
					
				
				
				
				$select2 = $sql->select('luni_menu');
				$select2->where(array('statut' =>'1', 'level' => $eachtopmenu['id_menu']));
				$select2->order('ranking');
				
				$stmt2 = $sql->prepareStatementForSqlObject($select2);
				$result2 = $stmt2->execute();
				
				
				if ($result2 instanceof ResultInterface && $result2->isQueryResult()) {
					$resultSet2 = new ResultSet();
					$resultSet2->initialize($result2);
					$arrt = $resultSet2->toArray();
					
					foreach ($arrt as $eachchildmenu) {
						if ($id_group) {
							if (!in_array($eachchildmenu['id_menu'], $temp_hold))
								continue;
						}
						$obj2 = new \stdClass();
						$obj2->id = $eachchildmenu['id_menu'];
						$obj2->label = $eachchildmenu['label_menu'];
						$obj2->link = $eachchildmenu['link'];
						$obj2->controller = $eachchildmenu['controller'];
						$obj2->css_id = $eachchildmenu['css_id'];
						$obj2->icon = $eachchildmenu['icon'];
						$obj->child[] = $obj2;
					}
					if ((!empty($obj->child)))
						$outarray[$eachtopmenu['id_menu']] = $obj;
				}
				
			}
			return $outarray;
		}
		return false;
		
	}
	
    public function listUsers($codeProfil,$idUser)
    {

        $resultRequest = array();
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select($this->dbTables['customers']['table']);

        if($codeProfil != "all" && $codeProfil !== null)
        {
            $select->where(array($this->dbTables['customers']['columns']['code_profil']=>$codeProfil,$this->dbTables['customers']['columns']['id_user'].'<>?'=>$idUser));

        }
        elseif($codeProfil === null)
        {
            $select->where(array($this->dbTables['customers']['columns']['id_user'].'<>?'=>$idUser));


        }

        elseif($idUser== null)
        {

            $select->where(array($this->dbTables['customers']['columns']['code_profil'].'>=?'=>$codeProfil));

        }


        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }

        return $resultRequest;
    }

    public function listOfUsers($user_account,$user_account_search,$user_id,$limit,$order)
    {
        if($user_account !== null)
        {
            $resultRequest = array();
            $sql = new Sql($this->dbAdapter);
            $column = array('fullName', 'email', 'id', 'derniere_connexion', 'online', 'lock_u');


            $select = $sql->select($this->dbTables['customers']['table']);
            $select->columns(
                array(
                    'fullName'=>new Expression("CONCAT(nom,' ',prenoms)"),
                    'email'=>'email',
                    'id'=>'id_user',
                    'derniere_connexion'=>'derniere_connexion',
                    'online'=>'online',
                    'lock_u'=>'lock_u'

                )
            );
            if($user_account == 0 || $user_account == 1)
            {
                if($user_account_search == null || $user_account == 'all')
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>$user_account,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }
                else
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>$user_account_search,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }
            }
            else
            {
                if($user_account_search == null || $user_account == 'all')
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>2,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }
                else
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>$user_account_search,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }


            }


            if($order !== null)
            {
                $select->order($column[$order['0']['column']].' '.$order['0']['dir'].' ');
            }
            else
            {
                $select->order('fullName ASC');
            }

            if($limit !== null)
            {
                $select->limit(intval($limit['end']))->offset(intval($limit['start']));
            }
            $stmt = $sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            if($result instanceof ResultInterface && $result->isQueryResult())
            {
                $resultSet = new ResultSet();
                $resultSet->initialize($result);
                $resultRequest = $resultSet->toArray();
            }

            return $resultRequest;
        }

    }

    public function listOfMonnaie($search,$limit,$order)
    {
        $resultRequest = array();
        $columns = array('id_famille','libelle_famille','categorie_devise','type_monnaie','statut');

        if(is_array($limit) && $limit !== null)
        {
            foreach ($limit as $keys=>$values)
            {
                $limit[$keys] = intval($values);
            }
        }
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['familles_devise']['table']);
        $select->columns(array(
            'id'=>'id_famille',
            'libelle'=>'libelle_famille',
            'capitalize'=>'capitalize',
            'categorie_devise'=>'categorie_devise',
            'type_monnaie'=>'type_monnaie',
            'statut'=>'statut'
        ));
        if($limit !== null)
        {
            $select->limit($limit['end'])->offset($limit['start']);
        }

        if($order !== null)
        {
            $select->order($columns[$order['0']['column']].' '.$order['0']['dir'].' ');
        }
        else
        {
            $select->order($this->dbTables['familles_devise']['columns']['libelle'].' ASC');
        }
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }
        return $resultRequest;

    }
    public function listOfDevises($search,$limit,$order)
    {
        $resultRequest = array();
        $column = array('lib_devise', 'image_devise', 'id_devise', 'categorie_devise', 'solde', 'solde_minimum','statut');
        if(is_array($limit) && $limit !== null)
        {
            foreach ($limit as $keys=>$values)
            {
                $limit[$keys] = intval($values);
            }
        }
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['devises']['table']);
        $select->columns(array(

            'id'=>'id_devise',
            'libelle'=>'lib_devise',
            'image'=>'image_devise',
            'categorie'=>'categorie_devise',
            'statut'=>'statut',
            'solde'=>'solde',
            'solde_min'=>'solde_minimum'
        ));

        if($limit !== null)
        {
            $select->limit($limit['end'])->offset($limit['start']);
        }

        if($order !== null)
        {
            $select->order($column[$order['0']['column']].' '.$order['0']['dir'].' ');
        }
        else
        {
            $select->order('lib_devise ASC');
        }


        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }

        return $resultRequest;


    }

    public function listDevises($conditions)
    {
        $resultRequest = array();

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['devises']['table']);
        if(isset($conditions) && $conditions !== null)
        {
            $select->where($conditions);
        }
        $select->where(array(
            'statut'=>'1'
        ));
        $select->columns(array(

            'id'=>'id_devise',
            'libelle'=>'lib_devise',
            'image'=>'image_devise',
            'categorie'=>'categorie_devise',
            'statut'=>'statut',
            'solde'=>'solde',
            'solde_min'=>'solde_minimum'
        ));


            $select->order('lib_devise ASC');



        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }

        return $resultRequest;

    }


    public function getOneUser($id)
    {
        $id = intval($id);
        $tableRequest = [];
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['customers']['table']);
        $select->columns(array(
            'id_user'=>'id_user',
            'nom'=>'nom_complet',
            'email'=>'email',
            'code_profil'=>'code_profil',
            'telephone'=>'telephone',
            'ville'=>'ville',
            'pays_residence'=>'pays',
            'region'=>'region',
            'profil_complet'=>'profil_complet',
            'adresse'=>'adresse'

        ));
        $select->where(array($this->dbTables['customers']['columns']['id_user']=>$id));
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {
            $tableRequest = $result->current();
        }
        return $tableRequest;
    }
    public function getUser($condition)
    {

        $tableRequest = [];
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->getDbTables()['customers']['table']);

        if(is_array($condition) && !empty($condition))
        {

                $select->where($condition);

        }
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {
            $tableRequest = $result->current();
        }
        return $tableRequest;
    }

    public function listFamilleDevise()
    {
        $returnRequest = array();
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['familles_devise']['table']);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $returnRequest = $resultSet->toArray();
        }

        return $returnRequest;
    }

    public function getSpecifiqueFamilleDevise($condition,$type)
    {
        $returnRequest = array();
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['familles_devise']['table']);
        if(is_array($condition))
        {
            $select->where($condition);
        }
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {

            if($type == "all")
            {
                $resultSet = new ResultSet();
                $resultSet->initialize($result);
                $returnRequest = $resultSet->toArray();
            }
            elseif($type == "unique")
            {
                $returnRequest = $result->current();
            }

        }

        return $returnRequest;
    }
	
	/**
	 * @param $codeProfilOfUserActive
	 * @return array
	 * @throws \Exception
	 */
	public function getProfilAccordingToProfilOnline($codeProfilOfUserActive)
    {
        $codeProfilOfUserActive = intval($codeProfilOfUserActive);
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['profil_user']['table']);
        $resultRequest = array();
        switch ($codeProfilOfUserActive)
        {
            case 0:
                $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'>?'=>0));
                break;
            case 1:
                $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'>?'=>1));
                break;
            default:
                $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'<>?'=>2));
                
        }
        $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'<>?'=>2));
        $select->order($this->dbTables['profil_user']['columns']['lib_profil']);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if ($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }
        return $resultRequest;
    }


    public function getAdminUsers($codeProfil)
    {
        $codeProfilOfUserActive = intval($codeProfil);
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['customers']['table']);
        $resultRequest = array();
        switch ($codeProfilOfUserActive)
        {
            case 0:
                $select->where(array($this->dbTables['customers']['columns']['code_profil'].'>?'=>0));
                break;
            case 1:
                $select->where(array($this->dbTables['customers']['columns']['code_profil'].'>?'=>1));
                break;
            default:
                $select->where(array($this->dbTables['customers']['columns']['code_profil'].'<>?'=>2));

        }


        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if ($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }
        return $resultRequest;
    }

    public function addUser($data)
    {
        $sql = new Sql($this->dbAdapter);
        $dataToInsert = filter_var_array($data,FILTER_SANITIZE_STRING);

        $insert = $sql->insert($this->dbTables['customers']['table']);
        $insert->values($dataToInsert);
        $stmt = $sql->prepareStatementForSqlObject($insert);
        $stmt->execute();


        return 1;
    }

    public function getCategorieDevise()
    {
        $tableReturn = array();
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['categorie_devise']['table']);
        $select->where([
            $this->dbTables['categorie_devise']['columns']['statut']=>"1"
        ]);
        $select->order($this->dbTables['categorie_devise']['columns']['lib_cat_devise'].' ASC');
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $tableReturn = $resultSet->toArray();
        }
        return $tableReturn;
    }




    public function random($car)
    {
        $string = "";
        $chaine = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    public function Conversion($sasie)
    {
        // TODO: Implement Conversion() method.
    }

    public function Million($nombre, $unite, $dizaine, $centaine, $mille, $million)
    {
        // TODO: Implement Million() method.
    }

    public function Mille($nombre, $unite, $dizaine, $centaine, $mille)
    {
        // TODO: Implement Mille() method.
    }

    /**
     * @param $inmillier
     * @param $nombre
     * @param $unite
     * @param $dizaine
     * @param $centaine
     */
    public function Centaine($inmillier, $nombre, $unite, $dizaine, $centaine)
    {
        // TODO: Implement Centaine() method.
    }

    public function Dizaine($inmillier, $nombre)
    {
        // TODO: Implement Dizaine() method.
    }

    public function Unite($unite)
    {
        // TODO: Implement Unite() method.
    }
}