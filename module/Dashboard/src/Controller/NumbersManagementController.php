<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 16-Apr-20
 * Time: 2:23 PM
 */

namespace Dashboard\Controller;

use Application\Service\PostServiceInterface;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class NumbersManagementController extends AbstractActionController
{
    protected $postservice;


    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;


    protected $_currentUser;
    protected $sessionManager;
    protected $sessionContainer;

    protected $table;
    protected $serviceLocator;
    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();
        $this->table = 'numeros_transactions';


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');



        //footerView
        //$this->footerView = new ViewModel();
        // $this->footerView->setTemplate('layout/footer');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }
    public function indexAction()
    {

        $this->checkAuth ();
        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')

            ->appendFile($this->getUriPath() . 'assets/plugins/datatables/datatables.bundle.js')
            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/datatables/search-options/advanced-search_numbers-management.js')
            ->appendFile($this->getUriPath() . 'assets/plugins/intl-tel-input/build/js/intlTelInput.js')
            ->appendFile($this->getUriPath() . 'assets/pages/numbers-management/custom_script.js')
            ->appendFile($this->getUriPath() . 'assets/pages/numbers-management/form_ajax_add_number_new.js')
            //->appendFile($this->getUriPath() . 'assets/pages/currencies-management/custom_script.js')


            ->appendFile($this->getUriPath() . 'assets/pages/global/Helpers.js')

        ;
        $this->renderPage ();

        $getCategorieDevise = $this->postservice->defaultSelect('categorie_devise',[],array('statut'=>'1','id_categorie_devise<>?'=>5),null,null,'all',null,'lib_categorie_devise ASC');

        return array(
            'categories_devise'=>$getCategorieDevise
        );
    }


    public function listNumbersAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $arrayConditions = array();
            $limit = null;

            $myArray = array();

            if($data['length'] !== -1)
            {
                $limit['start'] = $data['length'];
                $limit['end'] = $data['start'];
            }
            if(isset($data['columns']) && !empty($data['columns']))
            {




                if(!empty($data['columns'][1]['search']['value']))
                {
                    //var_dump ($data['columns'][0]['search']['value']);
                    array_push ($arrayConditions,array(
                        'type_numero'=> intval($data['columns'][1]['search']['value'])
                    ));
                }


                elseif(!empty($data['columns'][2]['search']['value']))
                {
                    array_push ($arrayConditions,array(
                        'n.status'=>"'".$data['columns'][2]['search']['value']."'"
                    ));
                }
                elseif(!empty($data['columns'][3]['search']['value']))
                {
                    array_push ($arrayConditions,array(
                        'libelle_numero LIKE ?'=>'%'.$data['columns'][3]['search']['value'].'%'
                    ));
                }

            }
           // $join['type'] = 'left';
            $join[] = array('table'=>array('c'=>'categorie_devise'),'condition'=>'c.id_categorie_devise=n.type_numero');
            //$join [] = array('table'=>array('d'=>'devises'),'condition'=>'d.id_devise=n.devise');


            $getNumbers = $this->listNumbers(array(),$arrayConditions,$join,$limit,'all',null,'id_numero ASC, libelle_numero ASC');
            $countRow = $this->listNumbers(array(),$arrayConditions,$join,null,'all',null,'id_numero ASC, libelle_numero ASC');


            foreach ($getNumbers as $keys=>$values)
            {

                foreach ($values as $j=>$elements)
                {
                    $myArray[$keys][$j] = $elements;
                    if($j ==="devise")
                    {

                        $myArray[$keys][$j] = $this->postservice->defaultSelect('devises',[],array(
                            'id_devise'=>intval($elements)
                        ),null,null,'unique',null,null);

                    }
                }

            }

            $view = new JsonModel(
                [
                    "iTotalRecords"=>count($getNumbers),
                    "iTotalDisplayRecords"=>count($countRow),
                    "sEcho"=>intval($data['draw']),
                    "sColumns"=> "",
                    "aaData"=>$myArray

                ]
            );
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('numbers-management');
        }

        return $view;
    }

    public function addNumberAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            if(isset($data['type_numero_add_number_form'],$data['libelle_number'],$data['type_input']) && !empty($data['kt_select_devise_add_number']))
            {
               if(empty($data['libelle_number']) || empty($data['type_input']) || empty($data['type_numero_add_number_form']))
               {
                   $tableJson['error'] = "Veuillez renseigner tous les champs.";
               }
               else
               {
                   $arrayInputs = array('tel','text','email','textarea');

                   if(!in_array($data['type_input'],$arrayInputs))
                   {
                       $tableJson['error'] = "Impossible d'effectuer le traitement demandé.";
                   }
                   else
                   {

                       if($data['type_input'] === "email")
                       {
                           if(!filter_var($data['libelle_number'],FILTER_VALIDATE_EMAIL))
                           {
                               $tableJson['error'] = "Adresse AdvCash invalide.";
                           }
                       }
                       elseif($data['type_input'] === 'tel')
                       {
                           if(strlen($data['libelle_number']) < 8 || strlen($data['libelle_number']) > 8)
                           {
                               $tableJson['error'] = "Format du numéro de téléphone incorrecte.";
                           }
                           if(strlen($data['number_name']) < 5)
                           {
                               $tableJson['error'] = "Nom trop court. Cinq(05) caractères au minimum";
                           }
                           if(strlen($data['number_name']) > 50)
                           {
                               $tableJson['error'] = "Nom trop long. Cinquante(50) caractères au maximum";
                           }
                           if(empty($data['number_name']) || empty($data['libelle_number']))
                           {
                               $tableJson['error'] = "Veuillez renseigner tous les champs.";
                           }
                       }

                           $checkIfNumberExist = null;
                          if(isset($data['kt_select_devise_add_number']))
                          {
                              $checkIfNumberExist = $this->postservice->defaultSelect($this->table,[],array('libelle_numero'=>$data['libelle_number'],'devise'=>intval($data['kt_select_devise_add_number'])),null,null,'unique',null,null);
                          }
                          else
                          {
                              $checkIfNumberExist = $this->postservice->defaultSelect($this->table,[],array('libelle_numero'=>$data['libelle_number']),null,null,'unique',null,null);

                          }

                          if(!empty($checkIfNumberExist))
                          {
                              $tableJson['error'] = "Ce numéro existe déjà pour ce cas.";
                          }
                          else
                          {
                              if(!isset($tableJson['error']) || empty($tableJson['error']))
                              {
                                  $dataToInsert['libelle_numero'] = $data['libelle_number'];
                                  $dataToInsert['status'] = '1';
                                  $dataToInsert['type_numero'] = filter_var($data['type_numero_add_number_form'],FILTER_SANITIZE_NUMBER_INT);
                                  if(isset($data['number_name']) && !empty($data['number_name']))
                                  {
                                      $dataToInsert['number_name'] = filter_var($data['number_name'],FILTER_SANITIZE_STRING);
                                  }
                                  if(isset($data['kt_select_devise_add_number']) && !empty($data['kt_select_devise_add_number']))
                                  {
                                      $dataToInsert['devise'] = filter_var($data['kt_select_devise_add_number'],FILTER_SANITIZE_NUMBER_INT);
                                  }

                                  $reqInsert = $this->postservice->defaultInsert($this->table,$dataToInsert);

                                  if($reqInsert)
                                  {
                                      $tableJson['success'] = "Numéro créé avec succès!";
                                  }
                                  else
                                  {
                                      $tableJson['error'] = "L'opération ne peut aboutir.";
                                  }
                              }

                          }

                   }
               }
            }


            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }

        return $view;
    }


    public function updateNumberNameAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();


            $dataGetId= $this->params ()->fromQuery ('id_numero');
            $getData = $this->params ()->fromQuery ('data');

            $checkIfNumberExist = $this->postservice->defaultSelect('numeros_transactions',[],array('id_numero'=>intval($dataGetId)),null,null,'unique',null,null);

            if($checkIfNumberExist)
            {
                if(strlen($getData) < 5)
                {
                   $tableJson['error'] = "Nom trop court. 05 caractères minimum";
                }
                else
                {
                    $reqUpdate = $this->postservice->defaultUpdate('numeros_transactions',array('number_name'=>$getData),['id_numero'=>intval($dataGetId)]);
                    if($reqUpdate)
                    {
                        $tableJson['ok'] = "Modification du nom effectuée.";
                    }
                }

            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer le traitement.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);



        return $view;
    }

    public function updateLibelleNumberAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();


        $dataGetId= $this->params ()->fromQuery ('id_numero');
        $getData = $this->params ()->fromQuery ('data');

        $checkIfNumberExist = $this->postservice->defaultSelect('numeros_transactions',[],array('id_numero'=>intval($dataGetId)),null,null,'unique',null,null);

        if($checkIfNumberExist)
        {

                $reqUpdate = $this->postservice->defaultUpdate('numeros_transactions',array('libelle_numero'=>$getData),['id_numero'=>intval($dataGetId)]);
                if($reqUpdate)
                {
                    $tableJson['ok'] = "Modification effectuée.";
                }


        }
        else
        {
            $tableJson['error'] = "Impossible d'effectuer le traitement.";
        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);



        return $view;
    }
    public function updateStatusNumberAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $reqUpdate = array();

            if($data['action'] === 'disabled')
            {
                $reqUpdate = $this->postservice->defaultUpdate('numeros_transactions',array('status'=>'0'),array('id_numero'=>intval($data['id_numero'])));

            }
            elseif($data['action'] === 'activate')
            {
                $reqUpdate = $this->postservice->defaultUpdate('numeros_transactions',array('status'=>'1'),array('id_numero'=>intval($data['id_numero'])));

            }
            if($reqUpdate)
            {
                $tableJson['success'] = "Mise à jour effectuée.";
            }
            else
            {
                $tableJson['error'] = "L'opération n'a pas pu être effectuée.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('numbers-management');
        }

        return $view;
    }

    public function deleteNumberAction()
    {

        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            if(isset($data['id_numero']) && !empty($data['id_numero']))
            {
                $checkIfNumberExist = $this->postservice->defaultSelect($this->table,array(),array('id_numero'=>$data['id_numero']),null,null,'unique',null,null);

                if(!empty($checkIfNumberExist))
                {
                    $checkIfInTransaction = $this->postservice->defaultSelect('transactions',array(),array('numero_envoi LIKE ?'=>'%'.$data['id_numero'].'%'),null,null,'unique',null,null);


                  if(empty($checkIfInTransaction))
                  {

                      $deleteNumber = $this->postservice->defaultDelete($this->table,array('id_numero'=>$data['id_numero']));
                      if($deleteNumber)
                      {
                          $tableJson['success'] = "Numéro supprimé !";
                      }
                      else
                      {
                          $tableJson['error'] = "L'opération ne peut aboutir. Une erreur s'est produite.";
                      }
                  }
                  else
                  {
                      $tableJson['error'] = "Ce numéro est impliqué dans une transaction. Si vous ne voulez plus l'utiliser vous pouvez le désactivé.";
                  }

                }
                else
                {
                    $tableJson['error'] = "L'opération ne peut aboutir. Veuillez vérifier les informations.";
                }
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {

                $tableJson['error'] = "L'opération ne peut aboutir. Veuillez vérifier les informations.";
            }
        }
        else
        {
            $this->redirect()->toRoute('numbers-management');
        }


        return $view;
    }
    private function listNumbers($colmuns,$conditions,$joins,$limit,$type,$grpe,$sort)
    {
        return $this->postservice->defaultSelect(array('n'=>$this->table),$colmuns,$conditions,$joins,$limit,$type,$grpe,$sort);
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttprequest())
        {
            $datas = $request->getPost()->toArray();
            $dataGetTable = $this->params ()->fromQuery ('table');
            $dataGetType = $this->params ()->fromQuery ('type');
            $dataConditions = array();
            // $this->sessionContainer->ElementForUpdate = array();
            foreach ($datas as $keys=>$values)
            {
                //$this->sessionContainer->ElementForUpdate[$keys] = $values;
                $dataConditions[$keys] = $values;
            }
            $request = $this->postservice->defaultSelect ($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
                $dataGetType,null,null);
            if((bool)$request)
            {
                $tableJson = $request;
                $this->sessionContainer->StdGetForUpdate = $request;
            }
            else
            {
                $tableJson['error'] = "Une erreur s'est produite. Veuillez rééssayer.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('currencies_management');
        }

        return $view;
    }


    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }
    public function renderPage()
    {

        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);

        //header layout
        $this->headerView = new ViewModel();

        $user_values = array();

        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }



    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil == 2)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }
        /*elseif($this->sessionContainer->CodeProfil == 2)
       {
           $this->redirect()->toRoute('customer');
       }
       elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
       {
           $this->redirect()->toRoute("administration");
       }*/
        /*
        elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
        {
            $this->redirect()->toRoute('customer');
        }
        else
        {
            $this->redirect()->toRoute('administration');
        }*/
    }

    private function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
        // $resizeWidth = 100;
        // $resizeHeight = 100;
        $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
        imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
        return $imageLayer;
    }

    private function convertImageToBase64($image,$imageFileType)
    {
        $image_base64 = base64_encode(file_get_contents($image));
        $image = 'data:image/'.$imageFileType.';charset=utf-8;base64,'.filter_var($image_base64,FILTER_SANITIZE_STRING);

        return $image;
    }

}