<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 13/10/2020
 * Time: 13:02
 */

namespace Dashboard\Controller;



use Application\Mapper\WebServiceDbSqlMapper;
use Application\Service\PostServiceInterface;
use Laminas\Json\Json;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class RequestsController extends AbstractActionController
{
    protected $postservice;

    protected $table;
    protected $headerView;
    protected $asideView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $asideVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;


    protected $_currentUser;
    protected $sessionManager;
    protected $sessionContainer;

    protected $serviceLocator;

    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        $this->serviceLocator= $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        $this->_currentUser = [
            'id'=>isset($this->sessionContainer->IdUser) && !empty($this->sessionContainer->IdUser) ? intval($this->sessionContainer->IdUser) : null,
            'codeProfile'=>$this->sessionContainer->CodeProfil,
            'username'=>$this->sessionContainer->UserName
        ];


        //Layout
        $this->contentVars = array();
        $this->table = 'luni_users';
        //header layout

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');

        //aside layout
        $this->asideView = new ViewModel();
        $this->asideVars = array();
        $this->asideView->setTemplate('layout/aside');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();



    }

    public function indexAction()
    {
        $this->asideVars['menu_active'] = "kt-widget__item--active";
        $this->asideVars['current_action'] = $this->params('action');
        $this->checkAuth();



        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/pages/requests/custom_script.min.js');


        $this->renderPage();

        return array(

        );
    }
    public function updateDocumentAction()
    {
        $request = $this->getRequest();

        $view = null;

        $tableJson = array();

            if(isset($this->sessionContainer->StdGetForUpdate) && $this->sessionContainer->StdGetForUpdate !== null)
            {
                $action = $this->params ()->fromQuery ('action') ;
                $status = $this->params ()->fromQuery('statut');
                $res = $this->sessionContainer->StdGetForUpdate[0];

                $getUser = $this->postservice->getOneUser($res['id_user'],null);

                if($action !== null && $action === 'validate' && $status == '1')
                {


                    $salutations = "Bonjour";
                    $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                    $getFormat = $nowUtc->format('H');
                    $getFormat = intval($getFormat);

                    if($getFormat > 12)
                    {
                        $salutations = "Bonsoir";
                    }


                    $key = "";
                    $token ="";
                    $nom_complet = $getUser['nom'];
                    $btn = '';
                    $url = '';
                    $titre = '';
                    $subject = 'Demande de validation d\'identité';
                    $email = $this->sessionContainer->Login;
                    $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                    $f_id = WebServiceDbSqlMapper::FROM_ID;

                    $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                    $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre demande de validation d\'identité du: '.$res['publish_date'].' a été accepté.</p><br>';
                    $msg ['nb'][]= '';
                    $msg ['nb'][]= '';
                    $msg ['base_url'] = "";
                    $msg ['btn_url'] = '';
                    $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                    $mailsend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');


                    if($mailsend === null)
                    {
                        $reqUpdate = $this->postservice->defaultUpdate($this->postservice->getDbTables()['documents']['table'],array(
                            'statut_document'=>$status,
                            'lock_doc'=>'1',
                        ),array(
                            'id_document'=>intval($this->sessionContainer->StdGetForUpdate[0]['id_document'])
                        ));

                        $reqUpdateProfilUser = $this->postservice->defaultUpdate($this->postservice->getDbTables()['customers']['table'],array(
                            'profil_complet'=>'1'
                        ),array(
                            'id_user'=>intval($getUser['id_user'])
                        ));

                        if($reqUpdate && $reqUpdateProfilUser)
                        {
                            echo json_encode(1);
                        }
                        else
                        {
                            echo json_encode(-1);
                        }
                    }
                    else
                    {
                        echo json_encode(0);
                    }
                }
                elseif($action !== null && $action === 'reject' && $status == '2')
                {
                    if($request->isXmlHttpRequest())
                    {
                        $data = $request->getPost()->toArray();

                        if(isset($data['motif']) && !empty($data['motif']))
                        {

                            $key = "";
                            $token ="";
                            $nom_complet = $getUser['nom'];
                            $btn = '';
                            $url = '';
                            $titre = '';
                            $subject = 'Demande de validation d\'identité';
                            $email = $getUser['email'];
                            $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                            $f_id = WebServiceDbSqlMapper::FROM_ID;


                            $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$this->getSalutation()." cher client, </h7>";
                            $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre demande de validation d\'identité du: '.$res['publish_date'].' a été réjeté.</p>
                                    <p><b style="text-decoration: underline !important">Motif:</b>&nbsp; <b>'.$data['motif'].'</b></p>';
                            $msg ['nb'][]= '';
                            $msg ['nb'][]= '';
                            $msg ['base_url'] = "";
                            $msg ['btn_url'] = '';
                            $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                            $mailsend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');



                            if($mailsend === null)
                            {
                                $reqUpdate = (bool)$this->postservice->defaultUpdate($this->postservice->getDbTables()['documents']['table'],array(
                                    'statut_document'=>$status,
                                    'motif'=>filter_var($data['motif'],FILTER_SANITIZE_STRING),
                                    'lock_doc'=>'0'
                                ),array(
                                    'id_document'=>intval($this->sessionContainer->StdGetForUpdate[0]['id_document'])
                                ));
                                if($reqUpdate)
                                {
                                    $tableJson['success'] = "Opération réussie";
                                }
                                else
                                {
                                    $tableJson['error'] = 'Une erreur s\'est produite au cours du traitement';
                                }
                            }
                            else
                            {
                                $tableJson['error'] = 'Une erreur s\'est produite lors de l\'envoi du mail';
                            }
                        }
                    }
                    $view = new JsonModel($tableJson);
                    $view->setTerminal(true);
                    return $view;
                }



            }

        return $this->getResponse();
    }

    public function getRequestsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = null;
        $arrayConditions = [];

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('requests',['action'=>'index']);
        }

        $data = $request->getPost()->toArray();

        //var_dump($data);
        //die();

        $myArray = array();
        $arrayColumns = array(
            'id_document'=>'id_document',
            'type_document'=>'type_document',
            'chemin_document'=>'chemin_document',
            'numero_piece'=>'numero_piece',
            'document'=>'document',
            'document_selfie'=>'document_selfie',
            'motif'=>'motif',
            'id_user'=>'id_user',
            'lock_doc'=>'lock_doc',
            'publish_date'=>'publish_date',
            'statut_document'=>'statut_document'
        );


        $arrayConditions[] = array(
                'statut_document'=>'0'
            );

        if(isset($data['query']) && !empty($data['query']))
        {

            if(!empty($data['query'][0]))
            {
                array_push($arrayConditions,[
                        'nom_complet LIKE ?'=>'%'.ltrim(filter_var($data['query'][0],FILTER_SANITIZE_STRING)).'%'

                    ]
                );
            }
            else
            {
                if(!isset($arrayConditions['statut_document']))
                {
                    array_push($arrayConditions,[
                        'statut_document'=>'0'
                    ]);
                }

            }

            if(isset($data['query']['status']) && !empty($data['query']['status']))
            {
                unset($arrayConditions['statut_document']);
                array_push($arrayConditions,[
                    'statut_document'=>filter_var($data['query']['status'],FILTER_SANITIZE_STRING)
                ]);
            }
            else
            {
                if(!isset($arrayConditions['statut_document']))
                {
                    array_push($arrayConditions,[
                        'statut_document'=>'0'
                    ]);
                }
            }

            if(isset($data['query']['type']) && !empty($data['query']['type']))
            {
                if(!isset($arrayConditions['statut_document']))
                {
                    array_push($arrayConditions,[
                        'statut_document'=>'0'
                    ]);
                }
                array_push($arrayConditions,[
                    'type_document'=>strtoupper(filter_var($data['query']['type'],FILTER_SANITIZE_STRING))
                ]);
            }

        }


        $limit = null;
        $sort = 'publish_date DESC,id_document DESC';

        if(isset($data['length']) && $data['length'] !== -1)
        {
            $limit['start'] = $data['length'];
            $limit['end'] = $data['start'];
        }
        if(isset($data['field'],$data['sort']) && $data['field'] != '')
        {
            $sort = $data['field'].' '.strtoupper($data['sort']);
        }

        $joins = null;

        $joins[] = array(
            'table'=>array(
                't'=>'type_piece'
            ),
            'condition'=>'t.code_type_piece=d.type_document'
        );

        $joins[] = array(
            'table'=>[
                'u'=>'luni_users'
            ],
            'condition'=>'u.id_user=d.id_user'
        );

        $myRequest = $this->postservice->defaultSelect([
            'd'=>'documents'
        ],$arrayColumns,$arrayConditions,$joins,$limit,'all',null,$sort);
        $rowCount = count($this->postservice->defaultSelect(['d'=>'documents'],[],$arrayConditions,$joins,null,'all',null,$sort));
        foreach ($myRequest as $keys=>$values)
        {

            foreach ($values as $j=>$elements)
            {
                $myArray[$keys][$j] = $elements;
            }

        }


        $total_pages = ceil(count($myRequest) / intval($data['pagination']['perpage']) );



        $tableJson = [

            'meta'=>array(
                "page"=> isset($data['pagination']['page']) && !empty($data['pagination']['page']) ? intval($data['pagination']['page']): 1,
                "pages"=> $total_pages,
                "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 1,
                "total"=> $rowCount,
                "sort"=> "desc",
                "field"=> "date_transaction"
            ),
            'data'=>$myArray,
            'current_user'=>intval($this->_currentUser['codeProfile'])
        ];



        $view  = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }


    public function returnHost()
    {
        $url = $this->getRequest()->getHeader('Referer')->getUri();
        $shema = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $querry = parse_url($url, PHP_URL_QUERY);
        $data = [];
        $data['host'] = $shema . "://" . $host;
        $data['querry'] = $querry;

        return $data;
    }
    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser == NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        if(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !==NULL)
        {
            /* if($this->sessionContainer->CodeProfil == 2)
             {
                 $this->redirect ()->toRoute ('dashboard');
             }*/

        }
        /*elseif($this->sessionContainer->CodeProfil == 2)
       {
           $this->redirect()->toRoute('customer');
       }
       elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
       {
           $this->redirect()->toRoute("administration");
       }*/
        /*
        elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
        {
            $this->redirect()->toRoute('customer');
        }
        else
        {
            $this->redirect()->toRoute('administration');
        }*/
    }

    private function getSalutation()
    {
        $salutations = "Bonjour";
        $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
        $getFormat = $nowUtc->format('H');
        $getFormat = intval($getFormat);

        if($getFormat > 12)
        {
            $salutations = "Bonsoir";
        }

        return $salutations;
    }
    public function renderPage()
    {

        //header layout
        $this->headerView = new ViewModel();

        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);

        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT),null);

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }

    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->table),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }



}