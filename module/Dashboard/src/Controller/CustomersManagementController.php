<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 17-Apr-20
 * Time: 7:43 PM
 */

namespace Dashboard\Controller;


use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class CustomersManagementController extends AbstractActionController
{
    protected $postservice;

    protected $loginTable;
    protected $paysTable;
    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;

    protected $cur_USer;
    protected $localId;
    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;
    protected $table;

    protected $serviceLocator;

    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();
        $this->table = 'luni_monnaies';
        //header layout
        // $this->headerView = new ViewModel();
        //$this->headerVars = array();
        //$this->headerView->setTemplate('layout/header');

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');


        $this->serviceLocator = $container;
        //footerView
        //$this->footerView = new ViewModel();
        // $this->footerView->setTemplate('layout/footer');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }

    public function indexAction()
    {
        $this->checkAuth ();


        $this->renderPage();

        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')
            ->appendFile($this->getUriPath() . 'assets/pages/customers-management/ajax_list_of_customers_new.js')

        ;
        $getCustomers = $this->postservice->defaultSelect('luni_users',array(),array('code_profil'=>2,'date_creation<>?'=>""),null,null,'all','date_creation','date_creation ASC');

        $tableDates = array();

        foreach($getCustomers as $elements)
        {
            $tableDates[] = $elements['date_creation'];
        }


        return array(

            'date_picker_limits'=>array(
                'start'=>date('d/m/Y',strtotime($tableDates[0])),
                'end'=>date('d/m/Y',strtotime($tableDates[count($tableDates)-1]))
            )

        );
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


    /**
     * @return null|JsonModel
     */
    public function listCustomersAction()
    {
        $request = $this->getRequest();
        $view = $limit = null;
        $myArray = array();


        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $conditions = array();

            $arrayColumns = array(
                'id_user'=>'id_user',
                'nom_user'=>'nom_complet',
                'email_user'=>'email',
                'date_creation'=>'date_creation',
                'derniere_connexion'=>'derniere_connexion',
                'lock_u'=>'lock_u',
                'online'=>'online',
                'talephone'=>'telephone',
                'pays'=>'pays',
                'status'=>'statut',
                'ville'=>'ville',
                'region'=>'region',
                'adresse'=>'adresse',
                'telephone'=>'telephone',
                'dial_code'=>'dial_code'
            );
            $join['type'] = 'left';
            $join['data'] [] = array('table'=>array('p'=>'pays'),'condition'=>'p.id_pays=c.pays');
           // $join['data'] [] = array('table'=>array('v'=>'villes'),'condition'=>'v.id_ville=c.ville');
            $conditions [] = array(
                'code_profil'=>2,
                'c.statut <> ?'=>'4'
            );

            if(isset($data['query']) && !empty($data['query']))
            {
                if(!empty($data['query'][0]))
                {
                    array_push($conditions,array(
                        'nom_complet LIKE ?'=>'%'.$data['query'][0].'%'
                    ));
                }
                if(isset($data['query']['Status']))
                {
                   // $data['query']['Status'] = filter_var($data['query']['Status'],FILTER_SANITIZE_NUMBER_INT);

                    array_push($conditions,array('c.statut = ?'=>$data['query']['Status']));
                }

                if(isset($data['query']['Type']))
                {
                    array_push($conditions,array('online'=>$data['query']['Type']));
                }
                if(isset($data['query']['periodDate']) && $data['query']['periodDate'] !== "")
                {



                    $tableDate = explode(" - ",$data['query']['periodDate']);
                    $firstDate = $tableDate[0];
                    $firstDate = str_replace('/','-',$firstDate);
                    $firstDate = date('Y-m-d',strtotime($firstDate));
                    $lastDate = array_pop($tableDate);
                    $lastDate = str_replace('/','-',$lastDate);
                    $lastDate = date('Y-m-d',strtotime($lastDate));


                    array_push($conditions,array(
                        "DATE_FORMAT(date_creation, '%Y-%m-%d') >= ?"=>$firstDate,
                       'DATE_FORMAT(date_creation, \'%Y-%m-%d\') <= ?'=>$lastDate
                    ));
                   /* array_push($conditions,array(
                        'date_creation >= ?'=>$lastDate
                    ));*/
                  //  die();
                }
                if(isset($data['query']['destroyPeriodDate']))
                {

                    unset($conditions["DATE_FORMAT(date_creation, '%Y-%m-%d') >= ?"]);
                    unset($conditions['DATE_FORMAT(date_creation, \'%Y-%m-%d\') <= ?']);
                }

            }

           //var_dump($data);
           $countRow = count($this->getAllCustomers($arrayColumns,$conditions,$join,null,null,'nom_complet ASC'));

            /*
             *  "meta": {
        "page": 1,
        "pages": 1,
        "perpage": -1,
        "total": 40,
        "sort": "asc",
        "field": "RecordID"
    },
    "data"
             */

           // var_dump($data);


            $nombrePage = 1;
            if($data['pagination']['perpage'] >= $countRow)
            {
                $nombrePage = 1;
               // $limit['start'] = $countRow;
               // $limit['end'] =  0;
            }
            else
            {
                $nombrePage = round($countRow / $data['pagination']['perpage'],PHP_ROUND_HALF_UP);
               // $limit['end'] = $data['pagination']['perpage'];
            }




            $offset = (intval($data['pagination']['page'])-1) * intval($data['pagination']['perpage']);

            $limit['end'] = $offset;
            $limit['start'] = $data['pagination']['perpage'];


            $total_pages = ceil($countRow / intval($data['pagination']['perpage']) );
            $myRequest = $this->getAllCustomers($arrayColumns,$conditions,$join,$limit,null,'nom_complet ASC');

            foreach ($myRequest as $keys=>$values)
            {

                foreach ($values as $j=>$elements)
                {
                    $myArray[$keys][$j] = $elements;
                    if($j ==="pays")
                    {

                        $myArray[$keys][$j] = $this->postservice->defaultSelect('pays',[],array(
                            'id_pays'=>intval($elements)
                        ),null,null,'unique',null,null);

                    }
                }

            }

           // var_dump($data);
            $view = new JsonModel(
                array(
                    "meta"=>array(
                'page'=>intval($data['pagination']['page']),
                'pages'=>$total_pages,
                'perpage'=>isset($data['pagination'])?intval($data['pagination']['perpage']):1,
                "total"=>$countRow,
                "sort"=>"asc",
                "field"=>"nom_user"),
                "data"=>$myArray
                ));
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customers-management');
        }

        return $view;

    }


    public function getAllCustomers($columns,$hayshack,$joins,$limit,$grpe,$sort)
    {
        return $this->postservice->defaultSelect(array('c'=>$this->postservice->getDbTables()['customers']['table']),$columns,$hayshack,$joins,$limit,
            'all',$grpe,$sort);
    }
    public function updateStatusAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            if(isset($data['id_user'],$data['action']) && !empty($data['id_user']) && !empty($data['action']))
            {
                $checkIfUserExit = $this->postservice->defaultSelect('luni_users',[],array('id_user',$data['id_user']),null,null,'unique',null,null);
                if(count($checkIfUserExit) !== 0)
                {
                    $arrayData = array();
                    if($data['action'] === 'lock')
                    {
                        $arrayData = array('statut'=>'0');
                    }
                    else
                    {
                        $arrayData = array('statut'=>'1');
                    }

                    $reqUpdate = $this->postservice->defaultUpdate('luni_users',$arrayData,array('id_user'=>$data['id_user']));

                    if($reqUpdate)
                    {
                        $tableJson['success'] = "Status du client mis à jour.";
                    }
                    else
                    {
                        $tableJson['error'] = "Impossible d'effectuer l'action demandée";
                    }
                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer l'action demandée";
                }
            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer l'action demandée";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }

        return $view;
    }

    public function deleteCustomerAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            $checkIfUserMakeTransaction = $this->postservice->defaultSelect('transactions',[],array('made_by'=>$data['id_user'],'status_transaction >= ?'=>'0'),null,null,'all',null,null);
            if(count($checkIfUserMakeTransaction) === 0)
            {
                $reqDelete = $this->postservice->defaultUpdate('luni_users',array('statut'=>'4'),array('id_user'=>$data['id_user']));
                if($reqDelete)
                {
                    $tableJson['success']= "Compte supprimé.";
                }
                else
                {
                    $tableJson['error'] = "Une erreur s'est produite lors du traitement.";
                }
            }
            else
            {
                $tableJson['error'] = "Ce compte est déjà associé a des transactions.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }


        return $view;
    }
    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }
    public function renderPage()
    {

        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        //header layout
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT),null);

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }



    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil == 2)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }
        /*elseif($this->sessionContainer->CodeProfil == 2)
       {
           $this->redirect()->toRoute('customer');
       }
       elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
       {
           $this->redirect()->toRoute("administration");
       }*/
        /*
        elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
        {
            $this->redirect()->toRoute('customer');
        }
        else
        {
            $this->redirect()->toRoute('administration');
        }*/
    }
}