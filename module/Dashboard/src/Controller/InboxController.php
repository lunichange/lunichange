<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 09/10/2020
 * Time: 18:54
 */

namespace Dashboard\Controller;

use Application\Mapper\MainHelperManager;
use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Dashboard\Service\PostServiceInterface;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Stdlib\Parameters;
use Zend\Barcode\Barcode;
use ZendPdf;

class InboxController extends AbstractActionController
{

    protected $postservice;

    protected $loginTable;
    protected $paysTable;
    protected $headerView;
    protected $asideView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $asideVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;
    protected $config;

    protected $cur_USer;
    protected $localId;
    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;


    public function __construct(PostServiceInterface $postservice)
    {
        $this->postservice = $postservice;
        //$this->dbHelperService = $dbHelperService;

        $this->sessionContainer = new Container('luni');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();
        $this->table = 'luni_users';
        //header layout
        // $this->headerView = new ViewModel();
        //$this->headerVars = array();
        //$this->headerView->setTemplate('layout/header');

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');

        //aside layout
        $this->asideView = new ViewModel();
        $this->asideVars = array();
        $this->asideView->setTemplate('layout/aside');

        //footerView
        //$this->footerView = new ViewModel();
        // $this->footerView->setTemplate('layout/footer');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();



    }

    public function indexAction()
    {
        $this->asideVars['menu_active'] = "kt-widget__item--active";
        $this->asideVars['current_action'] = $this->params('action');
        $this->checkAuth();

        $this->_getHelper('headScript', $this->getServiceLocator())

            ->appendFile($this->getUriPath() . 'assets/pages/inbox/custom_script.js')

        ;
        $this->renderPage();


        $user_values = array();
        $get_pays = array();
        /**
         * assets/js/pages/custom/inbox/inbox.js
         */

       /*
           if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
            {


                $joins['type'] ='left';
                $joins['data'] [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
                $joins['data'] [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
                $user_values ['user_data'] = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);

                $get_pays = $this->postservice->defaultSelect('pays',array(),['id_pays'=>intval($user_values['user_data']['pays'])],null,null,'unique',null,null);

            }

       */
         $currentPage = 1;
         $limit = null;


         $dataGetPage = $this->params ()->fromQuery ('page');

          if(isset($_GET['page']) && !empty($_GET['page']))
          {
              $currentPage = intval($dataGetPage);
          }


        $getAllMessages = $this->postservice->defaultSelect($this->postservice->getDbTables()['messages_site']['table'],[],[],null,null,'all',null,null);
        // On détermine le nombre d'articles par page
        $parPage = 10;

        // On calcule le nombre de pages total
        $pages = ceil(count($getAllMessages) / $parPage);

        // Calcul du 1er article de la page
        $premier = ($currentPage * $parPage) - $parPage;

        $limit['start'] = $premier;
        $limit['end'] = $parPage;




        $getMessagesFromSite = $this->postservice->defaultSelect($this->postservice->getDbTables()['messages_site']['table'],[],array(),null,null,'all',
           'customer','publish_date DESC,customer ASC');

       $getInbox = $this->postservice->defaultSelect($this->postservice->getDbTables()['messages_site']['table'],[],array(
           'statut'=>'0'
       ),null,null,'all',null,null);



        return array(
            'messages'=>$getMessagesFromSite,
            'InboxMsg'=>count($getInbox),
            'pagination'=>array(
                'pages'=>$pages,
                'current_page'=>$currentPage,
                'totalPage'=>count($getAllMessages)

            )
        );
    }



    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->getServiceLocator()
            ->get('viewhelpermanager')
            ->get($helper);
    }


    public function returnHost()
    {
        $url = $this->getRequest()->getHeader('Referer')->getUri();
        $shema = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $querry = parse_url($url, PHP_URL_QUERY);
        $data = [];
        $data['host'] = $shema . "://" . $host;
        $data['querry'] = $querry;

        return $data;
    }
    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser == NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !==NULL)
        {
            /* if($this->sessionContainer->CodeProfil == 2)
             {
                 $this->redirect ()->toRoute ('dashboard');
             }*/

        }
        /*elseif($this->sessionContainer->CodeProfil == 2)
       {
           $this->redirect()->toRoute('customer');
       }
       elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
       {
           $this->redirect()->toRoute("administration");
       }*/
        /*
        elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
        {
            $this->redirect()->toRoute('customer');
        }
        else
        {
            $this->redirect()->toRoute('administration');
        }*/
    }

    public function renderPage()
    {

        //header layout
        $this->headerView = new ViewModel();

        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);

        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT),null);

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }

    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->table),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }


    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttprequest())
        {
            $datas = $request->getPost()->toArray();
            $dataGetTable = $this->params ()->fromQuery ('table');
            $dataGetType = $this->params ()->fromQuery ('type');
            $dataGetOrder = null;
            if($this->params()->fromQuery('order') !== null || isset($_GET['order']))
            {
                $dataGetOrder = $this->params()->fromQuery('order');
            }
            $dataConditions = array();
            // $this->sessionContainer->ElementForUpdate = array();
            if(!empty($datas))
            {
                if(isset($datas['update'],$datas['column']))
                {
                    foreach($datas['column'] as $keys=>$values)
                    {
                        $dataConditions[$keys] = $values;
                    }
                }
                else
                {
                    foreach ($datas as $keys=>$values)
                    {
                        //$this->sessionContainer->ElementForUpdate[$keys] = $values;
                        $dataConditions[$keys] = $values;
                    }
                }

            }

            $request = $this->postservice->defaultSelect ($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
                $dataGetType,null,$dataGetOrder);
            if(isset($datas['update'], $datas['column']))
            {
                $this->postservice->defaultUpdate($dataGetTable,array(
                    $datas['update']['column']=>$datas['update']['value']
                ),$dataConditions);
            }

            if((bool)$request)
            {
                $tableJson = $request;
                $this->sessionContainer->StdGetForUpdate = $request;
            }
            else
            {
                $tableJson['error'] = "Une erreur s'est produite. Veuillez rééssayer.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('my-profile');
        }

        return $view;
    }


}