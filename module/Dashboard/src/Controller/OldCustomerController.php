<?php
/**
 * Created by PhpStorm.

 * Date: 29/11/2020
 * Time: 00:05
 */

namespace Dashboard\Controller;


use Application\Mapper\WebServiceDbSqlMapper;
use Laminas\Json\Json;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\ViewModel;
use Application\Service\PostServiceInterface;
use Laminas\View\Model\JsonModel;
use DateTime;
class CustomerController extends AbstractActionController
{
    protected $postservice;
    private $_currentUser = array();

    protected $contentView;
    protected $headerView;
    protected $menuView;
    protected $footerView;


    //Variables for Layout
    protected $layout = array();
    protected $contentVars = array();
    protected $headerVars = array();
    protected $menuVars = array();


    protected $sessionContainer;
    protected $sessionManager;

    protected $serviceLocator;
    public function __construct(PostServiceInterface $postService,$container)
    {
        $this->postservice = $postService;
        $this->serviceLocator = $container;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== null)
        {
            $this->_currentUser = array(
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null ? $this->sessionContainer->CodeProfil : null,
                'username'=>isset($this->sessionContainer->Login) ? filter_var($this->sessionContainer->Login,FILTER_SANITIZE_STRING) : null
            );
        }
    }

    public function indexAction()
    {

        $this->_getHelper('headScript',$this->serviceLocator)

            ->appendFile($this->getUriPath(). 'assets/pages/customer/custom_script_update_final_new.js?'.uniqid())
            ->appendFile($this->getUriPath(). 'assets/pages/customer/form_ajax_transactions_new.js')
            ->appendFile($this->getUriPath(). 'assets/plugins/intl-tel-input/build/js/intlTelInput.js')
        ;

        /*var_dump($this->sessionContainer->TransactionDatas);
        die();*/

        $this->renderPage();

        $myArray = array();
        $checkIfTransactionInProgress = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions_temp']['table'],[],array(
            'made_by'=>$this->sessionContainer->IdUser,
            'is_fedapay'=>'0'
        ),null,null,'unique',null,null);

        $transactionInProgress = false;

        if(!empty($checkIfTransactionInProgress))
        {
            $transactionInProgress = true;
            $this->sessionContainer->TransactionInProgress = $transactionInProgress;

            foreach ($checkIfTransactionInProgress as $keys=>$values)
            {
                switch ($keys)
                {
                    case "devise_source":
                        $myArray[$keys] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array(
                            'id_devise'=> (int)$checkIfTransactionInProgress['devise_source']
                        ),null,null,'unique',null,null);
                        try
                        {
                            $intValue = (int) $myArray[$keys]['famille_devise'];
                            $myArray[$keys]['famille_devise'] = $this->returnFamilleDevise($intValue);

                        }
                        catch (\InvalidArgumentException $e)
                        {
                            print_r($e->getMessage());
                        }

                        break;
                    case "devise_destination":
                        $myArray[$keys] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array(
                            'id_devise'=> (int)$checkIfTransactionInProgress['devise_destination']
                        ),null,null,'unique',null,null);
                        try
                        {
                            $intValue = (int)$myArray[$keys]['famille_devise'];
                            $myArray[$keys]['famille_devise'] = $this->returnFamilleDevise($intValue);

                        }
                        catch (\InvalidArgumentException $e)
                        {
                            print_r($e->getMessage());
                        }
                        break;
                    default:

                        $myArray[$keys] = $checkIfTransactionInProgress[$keys];


                }



            }
        }


        $joins [] = array('table'=>array('c'=>'categorie_devise'),'condition'=>'c.id_categorie_devise=d.categorie_devise');
        $getDevises = $this->postservice->defaultSelect(array('d'=>'devises'),array(),['d.statut'=>'1','dispo_source'=>'1'],$joins,null,'all',null,"lib_devise ASC,lib_categorie_devise ASC");

        $categories_devises = $this->postservice->defaultSelect('categorie_devise',[],array('statut'=>'1',),null,null,'all',null,'id_categorie_devise ASC');

        $codeDeviseIsMtn = false;

        if(isset($myArray['devise_source']['code_devise'])&& ($myArray['devise_source']['code_devise'] === 'MTN-BJ' || $myArray['devise_source']['code_devise'] == 'MTN-CI'))
        {
            $codeDeviseIsMtn = true;
        }
        return array(
            'devises'=>$getDevises,
            'categorie_devises'=>$categories_devises,
            'transactionInProgress'=>$transactionInProgress,
            'getTransactionInProgress'=>$myArray,
            'codeDeviseIsMtn'=>$codeDeviseIsMtn,
            'user_data'=>$this->postservice->getUser(['id_user'=>$this->_currentUser['id']])

        );
    }

    private function renderPage()
    {
        $this->checkAuth();


        $this->contentVars['user_code_profil'] = intval(isset($this->_currentUser['codeProfile']) ? $this->_currentUser['codeProfile'] : null);
        //header layout
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= isset($this->_currentUser['username']) ? $this->_currentUser['username'] : null;
        $this->menuVars['user_id']= $this->_currentUser['id'];

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->postservice->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');
    }



    private function checkAuth()
    {

        if(empty($this->_currentUser) || !isset($this->_currentUser['id']) || $this->_currentUser['id'] == null)
        {
            return $this->redirect()->toRoute('logout');
        }
        if(isset($this->_currentUser['id'],$this->_currentUser['codeProfile']) && $this->_currentUser['codeProfile'] !== null && $this->_currentUser['codeProfile'] !== 2)
        {
            return $this->redirect()->toRoute('dashboard');
        }
        return 0;
    }


    public function loadDevisesAccordingToAnotherAction()
    {
        $request = $this->getRequest();

        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $datas = $request->getPost()->toArray();

            $dataConditions = array();

            foreach ($datas as $keys=>$values)
            {

                $dataConditions[$keys] = $values;
            }
            $tableJson['categories_devises'] = $this->postservice->defaultSelect ($this->postservice->getDbTables ()['categorie_devise']['table'],[],array('statut'=>'1'),null,null,
                'all',null,null);


            $tableJson['devises'] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],$dataConditions,null,null,'all',null,'lib_devise ASC');



            $view =  new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }

        return $view;
    }


    private function addTransaction()
    {

    }



    public function loadElementsInputsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $checkIfDeviseElectronic = false;
            $checkIfDeviseElectronicEquivalenceExist = false;

            if(isset($data['source'],$data['destination']))
            {
                $getNumberForSource = $this->postservice->defaultSelect('numeros_transactions',[],array('status'=>'1','devise'=>intval($data['source'])),null,null,'all',null,null);

                $getDeviseSource = $this->postservice->defaultSelect('devises',[],array('id_devise'=>intval($data['source'])),null,null,'unique',null,null);
                $getDeviseDestination = $this->postservice->defaultSelect('devises',[],array('id_devise'=>intval($data['destination'])),null,null,'unique',null,null);

                $getUser = $this->postservice->defaultSelect('luni_users',[],array('code_profil'=>2,'id_user'=>intval($this->sessionContainer->IdUser)),null,null,'unique',null,null);

                if((intval($getDeviseSource['categorie_devise']) === 1 || intval($getDeviseSource['categorie_devise']) === 4) && (intval($getDeviseDestination['categorie_devise'])=== 1 || intval($getDeviseDestination['categorie_devise']) === 4))
                {
                    $checkIfDeviseElectronic = true;

                }
                if($checkIfDeviseElectronic === true)
                {
                    $checkSourceEquivGlobal = $this->postservice->defaultSelect($this->postservice->getDbTables()['equivalence_globale']['table'],[],array(
                        'devise'=>intval($getDeviseSource['id_devise'])
                    ),null,null,'unique',null,null);

                    $checkIfDestinationEquivGlobal = $this->postservice->defaultSelect($this->postservice->getDbTables()['equivalence_globale']['table'],[],array(
                        'devise'=>intval($getDeviseDestination['id_devise'])
                    ),null,null,'unique',null,null);

                    if(!empty($checkSourceEquivGlobal) && !empty($checkIfDestinationEquivGlobal))
                    {
                        $checkIfDeviseElectronicEquivalenceExist = true;
                    }
                }
                if(count($getUser) !== 0 && ($getUser['telephone'] === null || $getUser['profil_complet']== '0'))
                {
                    $tableJson['error'] = "Veuillez complèter votre profil avant de pouvoir effectuer une transaction.";
                }
                /*  if(count($getNumberForSource) !== 0 && $getDeviseSource['categorie_devise'] !==5)
                  {*/

                $joins[] = array('table'=>array('d'=>'devises'),'condition'=>'d.id_devise=e.source');
                //$joins[] = array('table'=>array('p'=>'devises'),'condition'=>'p.id_devise=e.destination');
                $joinsDestination [] = array('table'=>array('d'=>'devises'),'condition'=>'d.id_devise=e.destination');

                $getSourceIsSource = $this->postservice->defaultSelect(array('e'=>'equivalences'),[],array(
                    'source'=>intval($data['source']),'destination'=>intval($data['destination'])),$joins,null,'unique',null,null
                );
                $getSourceIsDestination = $this->postservice->defaultSelect(array('e'=>'equivalences'),array(),array('source'=>intval($data['destination']),'destination'=>intval($data['source'])),$joinsDestination,null,'unique',null,'lib_devise ASC');

                if(empty($getSourceIsSource) && empty($getSourceIsDestination) && $checkIfDeviseElectronicEquivalenceExist ==false)
                {
                    $view = new JsonModel([
                        'error'=>'Impossible d\'effectuer l\'opération demandée.Veuillez nous contacter pour plus d\'information.'
                    ]);
                    $view->setTerminal(true);
                    return $view;
                }




                if((intval($getDeviseSource['categorie_devise']) === 2 && intval($getDeviseDestination['categorie_devise'])=== 2) && empty($getSourceIsSource) && empty($getSourceIsDestination))
                {
                    $tableJson['error'] = 'Cette transaction n\'est pas possible pour l\'instant. Veuillez rééssayer ultérieurement.';
                }
                elseif($checkIfDeviseElectronicEquivalenceExist === false && $checkIfDeviseElectronic === true)
                {
                    $tableJson['error'] = 'Cette transaction n\'est pas possible pour l\'instant. Veuillez rééssayer ultérieurement.';

                }
                elseif(count($getUser) !== 0 && ($getUser['telephone'] === null || $getUser['profil_complet'] ==='0'))
                {
                    $tableJson['error'] = "Veuillez complèter votre profil avant de pouvoir effectuer une transaction.";
                }
                else
                {

                    $joinsRecup [] = array('table'=>array('m'=>'luni_monnaies'),'condition'=>'m.id_famille=d.famille_devise');
                    $arrayColumns = array(
                        'id_devise'=>'id_devise',
                        'lib_devise'=>'lib_devise',
                        'categorie_devise'=>'categorie_devise',
                        'status'=>'statut',
                        'solde'=>'solde',
                        'code_devise'=>'code_devise',
                        'solde_min'=>'solde_minimum',
                        'famille_devise'=>'famille_devise'
                    );
                    $type_action=$taux= null;
                    $quantite_min = 10;
                    $quantite_min_what_it_gives = $quantite_min_what_it_receives = 5;

                    if($checkIfDeviseElectronic === true)
                    {
                        $getSourceEquivGlobal = $this->postservice->defaultSelect('equivalence_globale',[],array('devise'=>$getDeviseSource['id_devise']),null,null,'unique',null,null);

                        $getDestinationEquivGlobal = $this->postservice->defaultSelect('equivalence_globale',[],array('devise'=>$getDeviseDestination['id_devise']),null,null,'unique',null,null);

                        $quantite_min_what_it_gives = $getSourceEquivGlobal['quantite_min'];
                        $quantite_min_what_it_receives = $getDestinationEquivGlobal['quantite_min'];
                    }
                    else
                    {
                        if(!empty($getSourceIsSource))
                        {
                            $quantite_min_what_it_gives = $getSourceIsSource['quantite_min_vente'];
                            $quantite_min_what_it_receives = $getSourceIsSource['quantite_min_achat'];
                        }
                        elseif(!empty($getSourceIsDestination))
                        {
                            $quantite_min_what_it_gives = $getSourceIsDestination['quantite_min_achat'];
                            $quantite_min_what_it_receives = $getSourceIsDestination['quantite_min_vente'];
                        }
                    }

                    $recup_source = $this->postservice->defaultSelect(array('d'=>'devises'),[],array('id_devise'=>filter_var($data['source'],FILTER_SANITIZE_NUMBER_INT)),$joinsRecup,null,'unique',null,null);
                    $recup_destination = $this->postservice->defaultSelect(array('d'=>'devises'),$arrayColumns,array('id_devise'=>filter_var($data['destination'],FILTER_SANITIZE_NUMBER_INT)),$joinsRecup,null,'unique',null,null);
                    if(!empty($getSourceIsSource))
                    {
                        $type_action = 'achat';
                        $taux = $getSourceIsSource['taux_achat'];
                        $quantite_min = $getSourceIsSource['quantite_min_achat'];

                    }
                    elseif(!empty($getSourceIsDestination))
                    {
                        $type_action = 'vente';
                        $taux = $getSourceIsDestination['taux_vente'];
                        $quantite_min = $getSourceIsDestination['quantite_min_vente'];
                    }

                    $what_it__gives_placeholder_and_label = null;
                    $what_it_receive_placeholder_and_label =null;
                    if($recup_source['type_monnaie'] !== 'crypto')
                    {
                        $what_it__gives_placeholder_and_label = strtolower($recup_source['libelle_famille']);
                    }
                    else
                    {
                        $what_it__gives_placeholder_and_label = $recup_source['lib_devise'];
                    }

                    if($recup_destination['type_monnaie'] !== 'crypto')
                    {
                        $what_it_receive_placeholder_and_label = strtolower($recup_destination['libelle_famille']);
                    }
                    else
                    {
                        $what_it_receive_placeholder_and_label = $recup_destination['lib_devise'];
                    }


                    $input_elements = array
                    (
                        'what_it_gives'=>array(
                            'name'=>'currency_gave',
                            'id'=>'currency_gave',
                            'required'=>true,
                            'placeholder_label'=>' Qte de '.$what_it__gives_placeholder_and_label.' à convertir',
                            'min'=>$quantite_min_what_it_gives,
                            'class'=>'form-control col-md-12',
                            'label_class'=>'form-control-label',
                            'type'=>'number'


                        ),
                        'what_it_receives'=>array(
                            'name'=>'currency_receive',
                            'id'=>'currency_receive',
                            'required'=>true,
                            'placeholder_label'=> 'Qte de '.$what_it_receive_placeholder_and_label.' à recevoir',
                            'min'=>$quantite_min_what_it_receives,
                            'class'=>'form-control col-md-12',
                            'label_class'=>'form-control-label',
                            'type'=>'number'
                        ),
                        'hidden_source'=>array(
                            'name'=>'hidden_currency_gave',
                            'id'=>'hidden_currency_gave',
                            'value'=>$recup_source['id_devise'],
                            'type'=>'hidden'
                        ),
                        'hidden_destination'=>array(
                            'name'=>'hidden_currency_receive',
                            'id'=>'hidden_currency_receive',
                            'value'=>$recup_destination['id_devise'],
                            'type'=>'hidden'
                        ),
                        'hidden_type_action'=>array(
                            'name'=>'type_action',
                            'id'=>'type_action',
                            'value'=>$type_action,
                            'type'=>'hidden'
                        )
                    );

                    $this->sessionContainer->SourceForEquivalence = $recup_source['id_devise'];
                    $this->sessionContainer->DestinationForEquivalence = $recup_destination['id_devise'];
                    $this->sessionContainer->TypeActionForEquivalence = $type_action;

                    $input_elements['adresse_reception'] =  array(
                        'name'=>'address_receive',
                        'id'=>'address_receive',
                        'placeholder_label'=> 'Adresse réception '.$what_it_receive_placeholder_and_label,
                        'type'=>'text',
                        'class'=>'form-control col-md-12',
                        'label_class'=>'form-control-label',
                        'required'=>true,
                    );

                    $input_elements ['confirm_adresse_reception'] = array(
                        'name'=>'confirm_address_receive',
                        'id'=>'confirm_address_receive',
                        'placeholder_label'=>'Confirmer adresse de réception '.$what_it_receive_placeholder_and_label,
                        'type'=>'text',
                        'class'=>'form-control col-md-12',
                        'label_class'=>'form-control-label',

                        'required'=>true
                    );


                    switch ($recup_destination['type_monnaie'])
                    {
                        case 'crypto':

                            $input_elements['type_input'] = "text";

                            $input_elements['adresse_reception']['maxLength'] = 250;

                            $input_elements['confirm_adresse_reception']['data_country_code'] = 'ci';
                            $input_elements['confirm_adresse_reception']['maxLength'] = 250;


                            break;
                        case 'forte':
                        case 'faible':
                            if(intval($recup_destination['categorie_devise']) === 2)//Compte mobile
                            {

                                $input_elements['type_input'] = "tel";
                                $input_elements['adresse_reception']['type'] = 'tel';
                                $input_elements['adresse_reception']['maxLength'] = 18;

                                $input_elements['confirm_adresse_reception']['type'] = 'tel';
                                switch ($recup_destination['code_devise'])
                                {
                                    case "MTN-CI":
                                        {
                                            $input_elements['adresse_reception']['data_country_code'] = 'ci';


                                            $input_elements['confirm_adresse_reception']['data_country_code'] = 'ci';
                                        }
                                        break;

                                    case "MTN-BJ":
                                    case "MOOV-BJ":
                                        $input_elements['adresse_reception']['data_country_code'] = "bj";

                                        $input_elements['confirm_adresse_reception']['data_country_code'] = "bj";
                                        break;

                                    case "MOOV-TG":
                                        $input_elements['adresse_reception']['data_country_code'] = "tg";

                                        $input_elements['confirm_adresse_reception']['data_country_code'] = "tg";

                                        break;
                                    case "ORM-SEN":

                                        $input_elements['adresse_reception']['data_country_code'] = "sn";

                                        $input_elements['confirm_adresse_reception']['data_country_code'] = "sn";

                                        break;
                                    case 'ORM-CI':

                                        $input_elements['adresse_reception']['data_country_code'] = "ci";

                                        $input_elements['confirm_adresse_reception']['data_country_code'] = "ci";

                                        break;

                                    case "MOOV-IN-NI-BU-CI-MAL":

                                        $input_elements['adresse_reception']['data_country_code'] = array('ne','bf','ci','ml');

                                        $input_elements['confirm_adresse_reception']['data_country_code'] = array('ne','bf','ci','ml');

                                        break;

                                    default:

                                        $input_elements['adresse_reception']['type'] = 'text';

                                        $input_elements['confirm_adresse_reception']['type'] = 'text';

                                }


                            }
                            if(intval($recup_destination['categorie_devise']) === 1) // Banque en ligne
                            {

                                switch ($recup_destination['code_devise'])
                                {
                                    case "ADV-USD":
                                    case "ADV-EURO":
                                        $input_elements['type_input'] = "email";
                                        $input_elements['adresse_reception']['type'] = "email";
                                        $input_elements['confirm_adresse_reception']['type'] = "email";
                                        break;

                                    default:
                                        $input_elements['type_input'] = "text";
                                        $input_elements['adresse_reception']['type'] = "text";
                                        $input_elements['confirm_adresse_reception']['type'] = "text";
                                }


                            }

                            if(intval($recup_destination['categorie_devise']) === 5)
                            {
                                $input_elements['categorie_devise'] = "trr";
                                $getPays = $this->postservice->defaultSelect('pays',[],[
                                    'statut'=>'1'
                                ],null,null,'all',null,'lib_pays ASC');

                                unset($input_elements['confirm_adresse_reception']);
                                $input_elements['adresse_reception'] = [
                                    'global_settings'=>[
                                        'class'=>'form-control col-md-12',
                                        'label_class'=>'form-control-label',

                                    ],
                                    'firstName'=>
                                        [
                                            'name'=>'firstName',
                                            'id'=>'firstName',
                                            'placeholder_label'=> 'Prénom(s)',
                                            'type'=>'text',
                                            'required'=>true,
                                            'minLength'=>2,
                                            'maxLength'=>100,

                                        ],
                                    'lastName'=>[
                                        'name'=>'lastName',
                                        'id'=>'lastName',
                                        'placeholder_label'=> 'Nom',
                                        'type'=>'text',
                                        'required'=>true,
                                        'minLength'=>2,
                                        'maxLength'=>100
                                    ],
                                    'country'=>[
                                        'name'=>'country',
                                        'id'=>'kt-select-country',
                                        'required'=>true,
                                        'options'=>$getPays,
                                        'placeholder_label'=>'Pays',
                                        'type'=>'select',
                                        'class'=>'form-control selectpicker col-md-12'
                                    ],
                                    'city'=>[
                                        'name'=>'city',
                                        'id'=>'city',
                                        'type'=>'text',
                                        'class'=>'form-control select2 col-md-12',
                                        'placeholder_label'=>'Ville',
                                        'minLength'=>2,
                                        'maxLength'=>100
                                    ]
                                ];

                            }


                            break;
                    }

                    $tableJson['results'] = $input_elements;
                }
                /* }
                 else
                 {

                     if(intval($recup_destination['categorie_devise']) === 5)
                     {
                         $input_elements['categorie_devise'] = "trr";
                         $getPays = $this->postservice->defaultSelect('pays',[],[
                             'statut'=>'1'
                         ],null,null,'all',null,'lib_pays ASC');

                         $input_elements['adresse_reception'] = [
                             'global_settings'=>[
                                 'class'=>'form-control col-md-12',
                                 'label_class'=>'form-control-label',



                             ],
                             'firstName'=>
                                 [
                                     'name'=>'firstName',
                                     'id'=>'firstName',
                                     'placeholder_label'=> 'Prénom(s)',
                                     'type'=>'text',
                                     'required'=>true,
                                     'minLength'=>2,
                                     'maxLength'=>100,

                                 ],
                             'lastName'=>[
                                 'name'=>'lastName',
                                 'id'=>'lastName',
                                 'placeholder_label'=> 'Nom',
                                 'type'=>'text',
                                 'required'=>true,
                                 'minLength'=>2,
                                 'maxLength'=>100
                             ],
                             'country'=>[
                                 'name'=>'country',
                                 'id'=>'kt-select-country',
                                 'required'=>true,
                                 'options'=>$getPays,
                                 'type'=>'select',
                                 'class'=>'form-control select2 col-md-12'
                             ],
                             'city'=>[
                                 'name'=>'city',
                                 'id'=>'city',
                                 'required'=>false,
                                 'type'=>'text',
                                 'class'=>'form-control select2 col-md-12',
                                 'placeholder_label'=>'Ville'
                             ]
                         ];

                     }


                 }*/
            }

            $view =  new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }

        return $view;
    }
    public function callbackAction()
    {
        // $request = $this->getRequest();



        die(var_dump($this->getRequest()->getPost()));
        var_dump($post);
        $datas = json_encode($post['data']);
        $this->postService->defaultInsert('test_paydunya',[
            'text'=>'Mon test paydunya',
            'date'=>date('Y-m-d H:i:s'),
            'datas'=>$datas
        ]);
        $this->renderPage();


        return [];
    }
    public function makeTransactionAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            $dataGet = $this->params()->fromQuery('dialCode');
            $array_type_inputs = array("tel","text","email");
            $nomNumero = null;


            $getDevisesSource = $this->postservice->defaultSelect('devises',array(),array('id_devise'=>intval($this->sessionContainer->SourceForEquivalence)),null,null,'unique',null,null);
            $getDevisesDestination = $this->postservice->defaultSelect('devises',array(),array('id_devise'=>intval($this->sessionContainer->DestinationForEquivalence)),null,null,'unique',null,null);

            $getTransactionsCible = $this->postservice->defaultSelect('transactions',array(),array('devise_cible'=>intval($this->sessionContainer->DestinationForEquivalence),'status_transaction'=>'0'),null,null,'all',null,null);


            $addressDataExist = false;

            if(!isset($data['currency_gave'],$data['currency_receive'],$data['hidden_currency_gave']))
            {
                $view = new JsonModel([
                    'error'=>"Une erreur s'est produite. Le traitement ne peut aboutir"
                ]);
                $view->setTerminal(true);

                return $view;
            }

            if(!isset($data['currency_gave'],$data['currency_receive'],$data['hidden_currency_gave'],$data['hidden_currency_receive']))
            {
                $view = new JsonModel([
                    'error'=>'Impossible d\'effectuer l\'opération demandée'
                ]);
                $view->setTerminal(true);

                return $view;
            }
            if(isset($data['currency_gave'],$data['currency_receive'],$data['hidden_currency_gave']))
            {
                if(isset($data['address_receive'],$data['confirm_address_receive']))
                {
                    $addressDataExist = true;
                }

                if(intval($this->sessionContainer->SourceForEquivalence) === intval($data['hidden_currency_gave']) && intval($this->sessionContainer->DestinationForEquivalence) === intval($data['hidden_currency_receive']))
                {
                    if($addressDataExist)
                    {
                        if(!in_array($data['type_input'],$array_type_inputs))
                        {
                            $view = new JsonModel([
                                'error'=>"Impossible d'effectuer le traitement demandé"
                            ]);
                            $view->setTerminal(true);

                            return $view;
                        }
                        if(empty($data['currency_receive']) || empty($data['currency_gave']) || empty($data['address_receive']) || empty($data['confirm_address_receive']))
                        {
                            $view = new JsonModel([
                                'error'=> "Veuillez renseigner tous les champs."
                            ]);
                            $view->setTerminal(true);

                            return $view;
                        }

                        if($data['confirm_address_receive'] !== $data['address_receive'])
                        {
                            $tableJson['error'] = "Les adresses de réception doivent-être identiques.";

                            $view = new JsonModel([
                                'error'=>'Les adresses de réception doivent-être identiques.'
                            ]);
                            $view->setTerminal(true);

                            return $view;
                        }

                        if($this->sessionContainer->TypeActionForEquivalence === $data['type_action'])
                        {
                            switch ($data['type_action'])
                            {
                                case 'achat':
                                    if(floatval($data['currency_receive']) < floatval($data['currency_gave']))
                                    {
                                        $view = new JsonModel([
                                            'error'=>'Impossible d\'effectuer une transaction avec les informations renseignées.'
                                        ]);
                                        $view->setTerminal(true);

                                        return $view;
                                    }
                                    break;

                                case 'vente':

                                    if(floatval($data['currency_receive']) > floatval($data['currency_gave']))
                                    {
                                        $view = new JsonModel([
                                            'error'=>'Impossible d\'effectuer une transaction avec les informations renseignées.'
                                        ]);
                                        $view->setTerminal(true);

                                        return $view;
                                    }

                                    break;

                            }

                            switch ($data['type_input'])
                            {
                                case 'text':
                                    $getResult = $this->controlElectronicsDevises($data['address_receive'],$getDevisesDestination['code_devise']);

                                    if(!empty($getResult))
                                    {
                                        $tableJson['error'] = $getResult;
                                    }
                                    break;

                                case 'email':
                                    if(!filter_var($data['address_receive'],FILTER_VALIDATE_EMAIL))
                                    {
                                        $tableJson['error'] = "L'adresse de réception AdvCash n'est pas valide. Ex:johndoe@gmail.com.";
                                    }

                                    break;
                                default:

                                    if(strlen($data['address_receive']) > 15)
                                    {
                                        $tableJson['error'] = "Format du numero de téléphone non valide.";
                                    }

                            }
                        }
                    }
                    else
                    {
                        if(isset($data['firstName'],$data['lastName'],$data['country'],$data['city']))
                        {
                            $getResult = $this->controlSpeedTransfers($data);
                            if (!empty($getResult) && $getResult !== null) {
                                $tableJson['error'] = $getResult;
                            }
                        }
                    }

                    if(empty($getTransactionsCible) && floatval($data['currency_receive']) > floatval($getDevisesDestination['solde']))
                    {

                        $tableJson['error'] = "Impossible de poursuivre cette transaction. Notre solde de <b>".$getDevisesDestination['lib_devise'].'</b> n\'est pas suffisant. Le solde actuel est de: <b>'.$getDevisesDestination['solde'].'</b>';

                        $view = new JsonModel($tableJson);
                        $view->setTerminal(true);

                        return $view;
                    }
                    if(empty($getTransactionsCible) && floatval($getDevisesDestination['solde']) > floatval($data['currency_receive']))
                    {
                        if((floatval($getDevisesDestination['solde']) - floatval($data['currency_receive'])) <= floatval($getDevisesDestination['solde_minimum']))
                        {
                            $tableJson['error'] = "Solde insuffisant. Nous ne diposons pas d'assez de réserve pour poursuive la transaction. Veuillez rééssayer plus tard. Merci";

                            $view = new JsonModel($tableJson);
                            $view->setTerminal(true);

                            return $view;
                        }

                    }
                    if(!empty($getTransactionsCible))
                    {
                        $mySomme = 0;

                        foreach ($getTransactionsCible as $keys=> $values)
                        {
                            $mySomme += floatval($values['quantite_cible']);

                        }

                        if(floatval($getDevisesDestination['solde']) > $mySomme)
                        {
                            if((floatval($getDevisesDestination['solde']) - $mySomme) <= floatval($data['currency_receive']))
                            {
                                $tableJson['error'] = "Impossible de poursuivre cette transaction. Notre solde de <b>".$getDevisesDestination['lib_devise'].'</b> n\'est pas suffisant. Le solde actuel est de: <b>'.$getDevisesDestination['solde'].'</b>';

                                $view = new JsonModel($tableJson);
                                $view->setTerminal(true);

                                return $view;
                            }
                            elseif((floatval($getDevisesDestination['solde']) - $mySomme) > floatval($data['currency_receive']) && (floatval($getDevisesDestination['solde']) - $mySomme) <= floatval($getDevisesDestination['solde_minimum']))
                            {
                                $tableJson['error'] = "Solde insuffisant. Nous ne diposons pas d'assez de réserve pour poursuive la transaction. Veuillez rééssayer plus tard. Merci";
                                $view = new JsonModel($tableJson);
                                $view->setTerminal(true);

                                return $view;
                            }
                        }
                    }



                    if(!isset($tableJson['error']) || empty($tableJson['error']))
                    {

                        $numberSender = array();
                        $getNumberToSendMoney = $this->postservice->defaultSelect('numeros_transactions',[],array('type_numero'=>intval($getDevisesSource['categorie_devise']),'devise'=>intval($getDevisesSource['id_devise']),'status'=>'1'),null,null,'all',null,null);

                        if(!empty($getNumberToSendMoney))
                        {
                            foreach ($getNumberToSendMoney as $item)
                            {

                                if(intval($item['type_numero']) == 2)
                                {
                                    $numberSender[] = "+229 ".$item['libelle_numero'];

                                }
                                else
                                {
                                    $numberSender[] = $item['libelle_numero'];
                                }

                            }

                        }
                        else
                        {

                            $getNumberToSendMoneyPhone = $this->postservice->defaultSelect('numeros_transactions',[],array('type_numero'=>2),null,null,'all',null,null);

                            foreach ($getNumberToSendMoneyPhone as $item)
                            {

                                $numberSender[] = "+229 ".$item['libelle_numero'];

                            }

                        }
                        $rand_key = array_rand($numberSender,1);

                        if(strpos($numberSender[$rand_key],"+229") === 0)
                        {
                            $getName = explode(" ",$numberSender[$rand_key]);
                            $getNameNumber = $this->postservice->defaultSelect('numeros_transactions',[],array('libelle_numero'=>$getName[1]),null,null,'unique',null,null);
                            if($getNameNumber['libelle_numero'] !== null)
                            {
                                $nomNumero = $getNameNumber['number_name'];
                            }

                        }


                        $famille_devise_source = $this->postservice->defaultSelect('luni_monnaies',[],array('id_famille'=>$getDevisesSource['famille_devise']),null,null,'unique',null,null);
                        $famille_devise_destination = $this->postservice->defaultSelect('luni_monnaies',[],array('id_famille'=>$getDevisesDestination['famille_devise']),null,null,'unique',null,null);

                        if($data['type_input'] === 'tel')
                        {
                            $data['address_receive'] =  isset($dataGet) && !empty($dataGet) ? '+'.$dataGet.''.$data['address_receive'] : null;
                        }


                        $tableJson['success'] = array(
                            'devise_source'=>$getDevisesSource,
                            'devise_destination'=>$getDevisesDestination,
                            'quantite_a_convertir'=>floatval($data['currency_gave']),
                            'quantite_a_recevoir'=>floatval($data['currency_receive']),
                            'adresse_reception'=>$data['address_receive'],
                            'monnaie_source'=>strtolower($famille_devise_source['libelle_famille']),
                            'monnaie_destination'=>strtolower($famille_devise_destination['libelle_famille']),
                            'numero_envoi' =>$numberSender[$rand_key],
                            'nom_numero'=>$nomNumero,
                            'type_devise'=>$getDevisesSource['categorie_devise'],
                            'userDatas'=>$this->postservice->getOneUser($this->_currentUser['id'],null)

                        );
                        $this->sessionContainer->TransactionDatas = array(
                            'devise_source'=>$getDevisesSource,
                            'devise_destination'=>$getDevisesDestination,
                            'quantite_a_convertir'=>floatval($data['currency_gave']),
                            'quantite_a_recevoir'=>floatval($data['currency_receive']),
                            'adresse_reception'=>$data['address_receive'],
                            'monnaie_source'=>strtolower($famille_devise_source['libelle_famille']),
                            'monnaie_destination'=>strtolower($famille_devise_destination['libelle_famille']),
                            'numero_envoi' =>$numberSender[$rand_key],
                            'nom_numero'=>$nomNumero

                        );

                        if(!$addressDataExist)
                        {
                            $tableJson['success']['nom'] = filter_var($data['lastName'],FILTER_SANITIZE_STRING);
                            $tableJson['success']['prenom'] = filter_var(ltrim($data['firstName']),FILTER_SANITIZE_STRING);
                            $tableJson['success']['pays'] = filter_var(ltrim($data['country']),FILTER_SANITIZE_STRING);
                            $tableJson['success']['ville'] = isset($data['city']) && !empty($data['city']) ? $data['city'] : "";

                            $this->sessionContainer->TransactionDatas['nom'] = filter_var($data['lastName'],FILTER_SANITIZE_STRING);
                            $this->sessionContainer->TransactionDatas['prenom'] = filter_var(ltrim($data['firstName']),FILTER_SANITIZE_STRING);
                            $this->sessionContainer->TransactionDatas['pays'] = filter_var(ltrim($data['country']),FILTER_SANITIZE_STRING);
                            $this->sessionContainer->TransactionDatas['ville'] = isset($data['city']) && !empty($data['city']) ? $data['city'] : "";


                        }

                    }

                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement demandé.";
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }


        return $view;
    }

    private function controlSpeedTransfers($datas)
    {
        $message = "";


        foreach($datas as $keys=>$elements)
        {
            if(empty($datas[$keys]))
            {
                $view  = new JsonModel([
                    'error'=>"Veuillez renseigner tous les champs"
                ]);

                $view->setTerminal(true);

                return $view;
            }
            $datas[$keys] = ltrim(filter_var($elements,FILTER_SANITIZE_STRING));
        }


        if(isset($datas['firstName']) && strlen($datas['firstName']) < 2)
        {
            $message = "Prénom trop court. Deux(02) caractères au minimum";
        }

        if(isset($datas['lastName']) && strlen($datas['lastName']) < 2)
        {
            $message = "Nom trop court.Deux(02) caractères au minimum";
        }

        return $message;
    }

    private function controlElectronicsDevises($datas,$codeDevise)
    {
        $datas  =filter_var($datas,FILTER_SANITIZE_STRING);

        $rulesArray = [
            'ADV'=>[

            ],
            'PER'=>[
                'validate'=>[
                    'letter'=>[
                        'USD'=>'U',
                        'EUR'=>'E'
                    ],
                    'message'=>[
                        'USD'=>'Le format actuel pour l\'adresse perfect money dollar n\'est pas correcte. Il doit de la forme: <b>U184502356</b>',
                        'EUR'=>'Le format actuel pour l\'adresse perfect money euro n\'est pas correcte. Il doit de la forme: <b>E184502356</b>'
                    ]
                ],
                'preg-match'=>[
                    'pattern'=>'[0-9]+',
                    'message'=>'Le format actuel pour l\'adresse perfect money n\'est pas correcte. Il doit de la forme: <b>U184502356 ou E184502356</b>'
                ],
                'length'=>[
                    'minLength'=>6,
                    'maxLength'=>11,
                    'message'=>"Le format actuel pour l'adresse perfect money n'est pas correcte. Il doit de la forme: <b>U184502356 ou E184502356</b>"
                ],
                'browse'=>true
            ],

            'PAY'=>[
                'validate'=>[
                    'letter'=>"P",
                    'message'=>"Le format actuel pour l'adresse payeer(euro/dollar) n'est pas correcte. Il doit de la forme: <b>P1845023568</b>"
                ],
                'preg-match'=>[
                    'pattern'=>'[0-9]+',
                    'message'=>"Le format actuel pour l'adresse payeer(euro/dollar) n'est pas correcte. Il doit  de la forme: <b>P1845023568</b>"
                ],
                'length'=>[
                    'minLength'=>6,
                    'maxLength'=>12,
                    'message'=>'Le format actuel pour l\'adresse payeer(euro/dollar)ee n\'est pas correcte. Il doit de la forme: <b>P1845023568</b>'
                ],
                'browse'=>true
            ],
            'BTC'=>[

                'length'=>[
                    'minLength'=>34,
                    'maxLength'=>34,
                    'message'=>"L'adresse de réception Bitcoin n'est pas valide."
                ],
                'browse'=>false
            ],
            'ETH'=>[

                'length'=>[
                    'minLength'=>30,
                    'maxLength'=>40,
                    'message'=>"L'adresse de réception Ethreum n'est pas valide."
                ]
            ],


            'TRR'=>[

                'length'=>[
                    'minLength'=>2,
                    'maxLength'=>100
                ],
                'browse'=>false
            ]

        ];
        $message = null;

        $getCodeDevise = array_search("-",$codeDevise);
        $arrayCodeDevise = null;
        $deviseValue = null;
        if($getCodeDevise !== false)
        {
            $arrayCodeDevise = explode('-',$getCodeDevise);

            if(!empty($arrayCodeDevise))
            {
                $getCodeDevise = $arrayCodeDevise[0];
                $deviseValue = array_pop($arrayCodeDevise);
            }
        }
        else
        {
            $getCodeDevise = $codeDevise;
        }
        foreach($rulesArray as $keys=>$elements)
        {
            if($keys == $getCodeDevise)
            {
                $getFirstLetter = $datas[0];
                $getDatas = $datas;
                unset($getDatas[0]);
                if(isset($rulesArray[$keys]['validate']))
                {
                    switch(isset($rulesArray[$keys]['validate']))
                    {

                        case true:
                            //Récupérer le premier caractère de l'adresse de réception
                            switch (is_array($rulesArray[$keys]['validate']['letter']))
                            {
                                case true:
                                    if(!in_array($getFirstLetter,$rulesArray[$keys]['validate']['letter']))
                                    {
                                        foreach ($rulesArray[$keys]['validate']['message'] as $kk=>$vv)
                                        {
                                            if($kk == $deviseValue)
                                            {
                                                $message = $rulesArray[$keys]['validate']['message'][$kk];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(strlen($datas) < $rulesArray[$keys]['length']['minLength'] || strlen($datas) > $rulesArray[$keys]['length']['maxLength'])
                                        {
                                            $message = $rulesArray[$keys]['length']['message'];
                                        }

                                        if(!preg_match($rulesArray[$keys]['preg-match']['pattern'],$getDatas))
                                        {
                                            $message = $rulesArray[$keys]['preg-match']['message'];
                                        }

                                    }
                            }
                            break;
                        case false:
                            if($getFirstLetter !== $rulesArray[$keys]['validate']['letter'])
                            {
                                $message = $rulesArray[$keys]['validate']['message'];
                            }

                            if($getFirstLetter === $rulesArray[$keys]['validate']['letter'])
                            {
                                $message = $rulesArray[$keys]['validate']['message'];
                            }

                            if(!preg_match($rulesArray[$keys]['preg-match']['pattern'],$getDatas))
                            {
                                $message = $rulesArray[$keys]['preg-match']['message'];
                            }

                            if(strlen($datas) < $rulesArray[$keys]['length']['minLength'] || strlen($datas) > $rulesArray[$keys]['length']['maxLength'])
                            {
                                $message = $rulesArray[$keys]['length']['message'];
                            }

                            break;
                    }
                }

            }
        }

        return $message;

    }

    public function saveTempTransactionsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            if(isset($data['save_transaction']) && $data['save_transaction'] == true)
            {

                $dataToInsert = array();

                $getDeviseSource = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array(
                    'id_devise'=>$this->sessionContainer->TransactionDatas['devise_source']['id_devise']
                ),null,null,'unique',null,null);
                $needId=null;


                if(intval($getDeviseSource['categorie_devise']) === 2)
                {
                    $needId = "yes";
                }
                else
                {
                    $needId = "no";
                }
                $getSessionDatas = $this->sessionContainer->TransactionDatas;
                $dataToInsert['devise_source'] = $this->sessionContainer->TransactionDatas['devise_source']['id_devise'];
                $dataToInsert['devise_destination'] = $this->sessionContainer->TransactionDatas['devise_destination']['id_devise'];
                $dataToInsert['made_by'] = $this->sessionContainer->IdUser;
                $dataToInsert['quantite_source'] = $this->sessionContainer->TransactionDatas['quantite_a_convertir'];
                $dataToInsert['quantite_destination'] = $this->sessionContainer->TransactionDatas['quantite_a_recevoir'];
                $dataToInsert['numero_depot'] = $this->sessionContainer->TransactionDatas['numero_envoi'];
                $dataToInsert['adresse_reception'] = $this->sessionContainer->TransactionDatas['adresse_reception'];
                $dataToInsert['date_transaction']  = $this->DateTime();
                if(isset($getSessionDatas['nom'],$getSessionDatas['prenom'],$getSessionDatas['pays'],$getSessionDatas['ville']))
                {
                    $dataToInsert['nom_transaction'] = filter_var($getSessionDatas['nom'],FILTER_SANITIZE_STRING);
                    $dataToInsert['prenom_transaction'] = filter_var($getSessionDatas['prenom'],FILTER_SANITIZE_STRING);
                    $dataToInsert['pays_transaction'] =  $getSessionDatas['pays'];
                    $dataToInsert['ville_transaction'] = $getSessionDatas['ville'];
                }


                $checkIfTransactionExist = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions_temp']['table'],[],array(
                    'made_by'=>$this->sessionContainer->IdUser
                ),null,null,'unique',null,null);
                if(empty($checkIfTransactionExist))
                {
                    $requestInsert = (bool) $this->postservice->defaultInsert($this->postservice->getDbTables()['transactions_temp']['table'],$dataToInsert);

                    if($requestInsert)
                    {
                        $tableJson['success'] = "Votre transaction a été prise en compte.";
                        $tableJson['needId'] = $needId;
                    }
                    else
                    {
                        $tableJson['error']= "Impossible d'effectuer le traitement demandé.";
                    }
                }
                else
                {
                    $dataToUpdate= array();
                    $getSessionDatas = $this->sessionContainer->TransactionDatas;

                    $dataToUpdate['devise_source'] = $this->sessionContainer->TransactionDatas['devise_source']['id_devise'];
                    $dataToUpdate['devise_destination'] = $this->sessionContainer->TransactionDatas['devise_destination']['id_devise'];
                    $dataToUpdate['quantite_source'] = $this->sessionContainer->TransactionDatas['quantite_a_convertir'];
                    $dataToUpdate['quantite_destination'] = $this->sessionContainer->TransactionDatas['quantite_a_recevoir'];
                    $dataToUpdate['numero_depot'] = $this->sessionContainer->TransactionDatas['numero_envoi'];
                    $dataToUpdate['adresse_reception'] = $this->sessionContainer->TransactionDatas['adresse_reception'];
                    $dataToUpdate['date_transaction']  = $this->DateTime();

                    if(isset($getSessionDatas['nom'],$getSessionDatas['prenom'],$getSessionDatas['pays'],$getSessionDatas['ville']))
                    {
                        $dataToUpdate['nom_transaction'] = filter_var($getSessionDatas['nom'],FILTER_SANITIZE_STRING);
                        $dataToUpdate['prenom_transaction'] = filter_var($getSessionDatas['prenom'],FILTER_SANITIZE_STRING);
                        $dataToUpdate['pays_transaction'] =  $getSessionDatas['pays'];
                        $dataToUpdate['ville_transaction'] = $getSessionDatas['ville'];
                    }
                    $requestUpdate = (bool) $this->postservice->defaultUpdate($this->postservice->getDbTables()['transactions_temp']['table'],$dataToUpdate,
                        array(
                            'made_by'=>$this->sessionContainer->IdUser
                        ));

                    if($requestUpdate)
                    {
                        unset($this->sessionContainer->TransactionDatas);
                        $tableJson['success'] = "Votre transaction a été prise en compte.";
                        $tableJson['needId'] = $needId;

                    }
                    else
                    {
                        $tableJson['error'] = "Impossible d'effectuer le traitement demandé.";
                    }
                }


            }
            elseif(isset($data['direct_transaction']) && !empty($data['direct_transaction']))
            {

            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer le traitement demandé.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }


        return $view;
    }

    public function checkIfTransactionInProgressAction()
    {
        $request = $this->getRequest();
        $view = null;

        if($request->isXmlHttpRequest())
        {

            if(isset($this->sessionContainer->TransactionInProgress) && $this->sessionContainer->TransactionInProgress === true)
            {
                $view = new JsonModel(array('result'=>$this->sessionContainer->TransactionInProgress));
                $this->sessionContainer->TransactionInProgress = [];
                unset($this->sessionContainer->TransactionInProgress);
            }
            else
            {
                $view = new JsonModel(array('result'=>false));
            }

            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }
        return $view;
    }

    public function continueTransactionAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();



            if($this->parseBoolean($data['continueTransaction']) == true)
            {

                $getTempTransaction = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions_temp']['table'],[],array(
                    'made_by'=>$this->sessionContainer->IdUser
                ),null,null,'unique',null,null);


                $myArray = array();
                if(!empty($getTempTransaction))
                {
                    foreach ($getTempTransaction as $keys=>$values)
                    {
                        switch ($keys)
                        {
                            case "devise_source":
                                $myArray[$keys] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array(
                                    'id_devise'=> (int)$getTempTransaction['devise_source']
                                ),null,null,'unique',null,null);
                                try
                                {
                                    $intValue = (int) $myArray[$keys]['famille_devise'];
                                    $myArray[$keys]['famille_devise'] = $this->returnFamilleDevise($intValue);

                                }
                                catch (\InvalidArgumentException $e)
                                {
                                    print_r($e->getMessage());
                                }

                                break;
                            case "devise_destination":
                                $myArray[$keys] = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array(
                                    'id_devise'=> (int)$getTempTransaction['devise_destination']
                                ),null,null,'unique',null,null);
                                try
                                {
                                    $intValue = (int) $myArray[$keys]['famille_devise'];
                                    $myArray[$keys]['famille_devise'] = $this->returnFamilleDevise($intValue);

                                }
                                catch (\InvalidArgumentException $e)
                                {
                                    print_r($e->getMessage());
                                }
                                break;
                            default:
                                $myArray[$keys] = $getTempTransaction[$keys];
                                break;


                        }
                    }

                    if((int)$myArray['devise_source']['categorie_devise'] === 2)
                    {
                        $tableJson['success'] = 'get_transaction_id';
                    }
                    else
                    {

                        $tableJson['success'] = 'not_get_transaction_id';
                    }


                }
                else
                {
                    $tableJson['error'] = "Cette transaction n'est plus disponible";
                }

            }
            elseif($this->parseBoolean($data['continueTransaction']) == false)
            {
                $getTempTransaction = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions_temp']['table'],[],array(
                    'made_by'=>$this->sessionContainer->IdUser
                ),null,null,'unique',null,null);



                if(!empty($getTempTransaction))
                {
                    $reqDelete = $this->postservice->defaultDelete($this->postservice->getDbTables()['transactions_temp']['table'],array(
                        'made_by'=>intval($this->sessionContainer->IdUser),
                        'id_transaction'=>intval($getTempTransaction['id_transaction'])
                    ));

                    if($reqDelete)
                    {
                        $tableJson['success'] = 'success';
                        $tableJson['return'] = 'Transaction non prise en compte';
                    }
                }

            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }


        return $view;
    }

    private function parseBoolean($string)
    {
        switch($string){
            case "true": case "yes": case "1": return true;
            case "false": case "no": case "0": case null: return false;
            default: return boolval($string);
        }
    }
    private function returnFamilleDevise($familleDevise)
    {
        $returnValue = null;

        if(!is_int($familleDevise))

            throw new \InvalidArgumentException("Type integer is required",256,NULL);

        switch ($familleDevise)
        {
            case 1:
                $returnValue = "Dollar";
                break;
            case 2:
                $returnValue = "Euro";
                break;
            case 3:
            case 12:
                $returnValue = "FCFA";
                break;
            case 4:
                $returnValue = "XRP";
                break;
            case 5:
                $returnValue = "ETH";
                break;
            case 6:
                $returnValue = "BTC";
                break;
            case 7:
                $returnValue = "LTC";
                break;

            case 8:
                $returnValue = "TRP";
                break;

            case 9:
                $returnValue = "XMR";
                break;
        }

        return $returnValue;
    }

    public function checkoutAction()
    {

        if(!isset($this->sessionContainer->TransactionDatas) || !isset($this->sessionContainer->TransactionFedaPay))
        {
            $this->redirect()->toRoute('customer');
        }
        $message = null;


        if(!isset($this->sessionContainer->TransactionFedaPay) || !isset($this->sessionContainer->TransactionDatas))
        {
            $this->redirect()->toRoute('transactions');
        }
        if(isset($_GET['id'],$_GET['status'],$this->sessionContainer->TransactionFedaPay,$this->sessionContainer->TransactionDatas))
        {
            if($this->sessionContainer->TransactionFedaPay['ID'] !== intval($_GET['id']))
            {
                $this->redirect()->toRoute('transactions');
            }


            $getTempTransactions = $this->postservice->defaultSelect('transactions_temp',[],[
                'id_fedapay'=>$this->sessionContainer->TransactionFedaPay['ID'],
                'made_by'=>$this->sessionContainer->IdUser,
                'is_fedapay'=>'1'
            ],null,null,'unique',null,null);

            if(empty($getTempTransactions))
            {
                $this->redirect()->toRoute('transactions');
            }
            if(!empty($getTempTransactions))
            {
                $id = intval($this->sessionContainer->TransactionFedaPay['ID']);
                $deviseSource = $this->postservice->defaultSelect('devises',[],[
                    'id_devise'=>$getTempTransactions['devise_source']
                ],null,null,'unique',null,null);
                $deviseDestination = $this->postservice->defaultSelect('devises',[],[
                    'id_devise'=>$getTempTransactions['devise_destination']
                ],null,null,'unique',null,null);


                \FedaPay\FedaPay::setApiKey($this->getFedaPayKey($this->serviceLocator->get('Config')['fedapayEnvironment']));
                \FedaPay\FedaPay::setEnvironment($this->serviceLocator->get('Config')['fedapayEnvironment']);

                $transaction = \FedaPay\Transaction::retrieve($id);
                switch($transaction->status)
                {
                    case 'approved':

                        $id_transaction = "LUNI_ID_".$this->postservice->generateRandom('6');
                        $code_transaction = $this->postservice->generateRandom('6');

                        $reqInsert = $this->postservice->defaultInsert('transactions',[
                            'context'=>'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].'.',
                            'code_transaction'=>$code_transaction,
                            'date_transaction'=>$this->DateTime(),
                            'made_by'=>$this->sessionContainer->IdUser,
                            'transaction_id'=>$id_transaction,
                            'devise_source'=>$deviseSource['id_devise'],
                            'devise_cible'=>$deviseDestination['id_devise'],
                            'quantite_source'=>$getTempTransactions['quantite_source'],
                            'quantite_cible'=>$getTempTransactions['quantite_destination'],
                            'numero_envoi'=>'Fedapay',
                            'adresse_reception'=>$getTempTransactions['adresse_reception'],

                        ]);

                        if(!empty($reqInsert))
                        {


                            $messageTelegrame = "La transaction initiée par le client ".$this->sessionContainer->UserName. " a été traité par Fedapay.";
                            $messageTelegrame .= " Informations de la transaction:  ";
                            $messageTelegrame .= "Id transaction Fedapay : ".$transaction['id'].". ";
                            $messageTelegrame .= "Context de la transaction:";
                            $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                            $messageTelegrame .= "Date transaction: ".$this->DateTime().".  ";
                            $messageTelegrame .= "Montant donné : ".$getTempTransactions['quantite_source']." |  ";
                            $messageTelegrame .= "Montant demandé : ".$getTempTransactions['quantite_destination'].". ";
                            $messageTelegrame .= "Adresse réception : ".$getTempTransactions['adresse_reception'];



                            $this->postservice->sendMessageTelegram($messageTelegrame);

                            $this->postservice->defaultDelete('transactions_temp',[
                                'id_fedapay'=>$id,
                                'made_by'=>$this->sessionContainer->IdUser
                            ]);
                            unset($this->sessionContainer->TransactionFedaPay);
                            unset($this->sessionContainer->TransactionDatas);
                            $message = '<div class="col-md-12">
                                           <div class="alert alert-success me-card-1 fade show" role="alert">
                                               <div class="alert-icon"><i class="fa fa-check"></i></div>
                                                 <div class="alert-text">
                                                     <p class="text-align-justify font-size-1_0"> 
                                                      Votre transaction <b>Fedapay</b> a bien abouti.Veuillez vous serez déservi dans quelques instants.</p>
                                                     <p class="text-align-justify font-size-0_9">
                                                         <small><b class="text-decoration-underline">NB:</b></small>
                                                        Si vous n\'êtes pas déservi au bout de cinq(05) minutes, veuillez contacter le service support via le chat ou via notre numéro client : <a href="+22962330637">+229 62330637</a>
                                                    </p>
                                                    
                                                    <a class="btn btn-elevate-air btn-primary margin-top-15 pull-right" href="/transactions">Cliquer pour continuer</a>
                                                </div>
                                                <div class="alert-close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>';
                        }

                        break;
                    case 'pending':


                        $messageTelegrame = "La transaction initiée par le client ".$this->sessionContainer->UserName. " n'a pas encore été traité par Fedapay.";
                        $messageTelegrame .= " Informations de la transaction:  ";
                        $messageTelegrame .= "Id transaction Fedapay : ".$transaction['id'].". ";
                        $messageTelegrame .= "Context de la transaction:";
                        $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                        $messageTelegrame .= "Date transaction: ".$this->DateTime().".  ";
                        $messageTelegrame .= "Montant donné : ".$getTempTransactions['quantite_source']." |  ";
                        $messageTelegrame .= "Montant demandé : ".$getTempTransactions['quantite_destination'].". ";
                        $messageTelegrame .= "Adresse réception : ".$getTempTransactions['adresse_reception'];



                        $this->postservice->sendMessageTelegram($messageTelegrame);


                        $message = '<div class="col-md-12">
                                           <div class="alert alert-success me-card-1 fade show" role="alert">
                                               <div class="alert-icon"><i class="fa fa-check"></i></div>
                                                 <div class="alert-text">
                                                     <p class="text-align-justify font-size-1_0"> 
                                                      Votre transaction <b>Fedapay</b> est en attente.Veuillez vous serez déservi dans quelques instants.</p>
                                                     <p class="text-align-justify font-size-0_9">
                                                         <small><b class="text-decoration-underline">NB:</b></small>
                                                        Si vous n\'êtes pas déservi au bout de cinq(05) minutes, veuillez contacter le service support via le chat ou via notre numéro client : <a href="+22962330637">+229 62330637</a>
                                                    </p>
                                                    
                                                    <a class="btn btn-elevate-air btn-primary margin-top-15 pull-right" href="/transactions">Cliquer pour continuer</a>
                                                </div>
                                                <div class="alert-close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>';

                        break;

                    case 'declined':
                    case 'canceled':

                        $messageTelegrame = "La transaction initiée par le client ".$this->sessionContainer->UserName. " a été décliné/annulé par Fedapay.";
                        $messageTelegrame .= " Informations de la transaction:  ";
                        $messageTelegrame .= "Id transaction Fedapay : ".$transaction['id'].". ";
                        $messageTelegrame .= "Context de la transaction:";
                        $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                        $messageTelegrame .= "Date transaction: ".$this->DateTime().".  ";
                        $messageTelegrame .= "Montant donné : ".$getTempTransactions['quantite_source']." |  ";
                        $messageTelegrame .= "Montant demandé : ".$getTempTransactions['quantite_destination'].". ";
                        $messageTelegrame .= "Adresse réception : ".$getTempTransactions['adresse_reception'];



                        $this->postservice->sendMessageTelegram($messageTelegrame);

                        $this->postservice->defaultDelete('transactions_temp',[
                            'id_fedapay'=>$id,
                            'made_by'=>$this->sessionContainer->IdUser
                        ]);

                        $message = '<div class="col-md-12">
                                           <div class="alert alert-success me-card-1 fade show" role="alert">
                                               <div class="alert-icon"><i class="fa fa-check"></i></div>
                                                 <div class="alert-text">
                                                     <p class="text-align-justify font-size-1_0"> 
                                                      Votre transaction <b>Fedapay</b> a été annulé.</p>
                                                     <p class="text-align-justify font-size-0_9">
                                                         <small><b class="text-decoration-underline">NB:</b></small>
                                                        En cas de réclamation, veuillez contacter le service support via le chat ou via notre numéro client : <a href="+22962330637">+229 62330637</a>
                                                    </p>
                                                    
                                                    <a class="btn btn-elevate-air btn-primary margin-top-15 pull-right" href="/transactions">Cliquer pour continuer</a>
                                                </div>
                                                <div class="alert-close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>';
                        break;


                }

            }
        }

        return array(
            'message'=>$message
        );
    }

    private function getFedaPayKey($environment,$typeKey= 'secret')
    {

        if($typeKey !== 'secret')
        {
            $key = $this->serviceLocator->get('Config')['fedapayTestKey'];
            if($environment === 'live')
            {
                $key = $this->serviceLocator->get('Config')['fedapayLiveKey'];
            }

            return $key;
        }

        $key = $this->serviceLocator->get('Config')['fedaPaySecretKeys']['sandbox'];

        if($environment === 'live')
        {
            $key = $this->serviceLocator->get('Config')['fedaPaySecretKeys']['live'];
        }


        return $key;
    }

    public function finishTransactionAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();


        if($request->isXmlHttpRequest())
        {

            $data = $request->getPost()->toArray();

            if(isset($data['id_transaction']))
            {
                if(!empty($data['id_transaction']))
                {

                    $checkIfTempTransactionExist = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions_temp']['table'],[],
                        array('made_by'=>(int) $this->sessionContainer->IdUser),null,null,'unique',null,null);

                    if(!empty($checkIfTempTransactionExist))
                    {
                        $dataToInsert = array();
                        $getDeviseSource = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array('id_devise'=> (int)$checkIfTempTransactionExist['devise_source']),null,
                            null,'unique',null,null);

                        $getDeviseDestination = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array('id_devise'=> (int)$checkIfTempTransactionExist['devise_destination']),null,
                            null,'unique',null,null);

                        $dataToInsert['context'] = "Convertion de ".$getDeviseSource['lib_devise']." à ".$getDeviseDestination['lib_devise'];
                        $dataToInsert['code_transaction'] = $this->postservice->generateRandom('5');
                        $dataToInsert['devise_source'] = (int) $getDeviseSource['id_devise'];
                        $dataToInsert['devise_cible'] = (int) $getDeviseDestination['id_devise'];
                        $dataToInsert['made_by'] = $this->sessionContainer->IdUser;
                        $dataToInsert['quantite_source'] = $checkIfTempTransactionExist['quantite_source'];
                        $dataToInsert['quantite_cible'] = $checkIfTempTransactionExist['quantite_destination'];
                        $dataToInsert['transaction_id'] = $data['id_transaction'];
                        $dataToInsert['date_transaction'] = $this->DateTime();
                        $dataToInsert['date_start_expired'] = time();
                        $dataToInsert['date_end_expired'] = time() + (0 * 0 * 10 * 0);
                        $dataToInsert['numero_envoi'] = $checkIfTempTransactionExist['numero_depot'];
                        $dataToInsert['adresse_reception'] = $checkIfTempTransactionExist['adresse_reception'];

                        $checkIfTransactionExistByIdTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$data['id_transaction']),null,null,'unique',null,null);
                        $checkIfTransactionExistByCodeTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$dataToInsert['code_transaction']),null,null,'unique',null,null);

                        $salutations = "Bonjour";
                        $nowUtc = null;
                        try {
                            $nowUtc = new \DateTime('now', new \DateTimeZone('UTC'));
                        } catch (\Exception $e) {
                        }
                        $getFormat = $nowUtc->format('H');
                        $getFormat = intval($getFormat);

                        if($getFormat > 12)
                        {
                            $salutations = "Bonsoir";
                        }

                        $key = "";
                        $token ="";
                        $nom_complet = $this->sessionContainer->UserName;
                        $btn = '';
                        $url = '';
                        $titre = '';
                        $subject = 'Votre compte LUNICHANGE';
                        $email = $this->sessionContainer->Login;
                        $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                        $f_id = WebServiceDbSqlMapper::FROM_ID;

                        $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                        $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre code ou ticket de transaction est: <b>'.$dataToInsert['code_transaction'].'</b>. Cela vous sera utile en cas de réclamation..</p><br><p><b style="font-weight: 300 !important; text-decoration: underline !important">Identifiant de la transaction:</b> <b>'.$dataToInsert['transaction_id'].'</b></p>';
                        $msg ['nb'][]= '';
                        $msg ['nb'][]= '';
                        $msg ['base_url'] = "";
                        $msg ['btn_url'] = '';



                        $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                        //



                        if (empty($checkIfTransactionExistByIdTransaction) && empty($checkIfTransactionExistByCodeTransaction))
                        {
                            if(isset($checkIfTempTransactionExist['nom_transaction']) && !empty($checkIfTempTransactionExist['nom_transaction']))
                            {
                                $dataToInsert['nom'] = filter_var($checkIfTempTransactionExist['nom_transaction'],FILTER_SANITIZE_STRING);
                                $dataToInsert['prenom'] = filter_var($checkIfTempTransactionExist['prenom_transaction'],FILTER_SANITIZE_STRING);
                                $dataToInsert['pays'] = $checkIfTempTransactionExist['pays_transaction'];
                                $dataToInsert['ville'] = $checkIfTempTransactionExist['ville_transaction'];
                            }
                            $requestInsert = $this->postservice->defaultInsert('transactions',$dataToInsert);

                            if ($requestInsert)
                            {
                                $mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');
                                if($mailSend === null)
                                {

                                    $deleteTransactionTemp = (bool) $this->postservice->defaultDelete($this->postservice->getDbTables()['transactions_temp']['table'],array('made_by'=>$this->sessionContainer->IdUser));

                                    if($deleteTransactionTemp)
                                    {
                                        $messageTelegrame = "Le client ".$this->sessionContainer->UserName. " a effectué une demande de transaction.";
                                        $messageTelegrame .= " Informations de la transaction.  ";
                                        $messageTelegrame .= "ID : ".$data['id_transaction'].". ";
                                        $messageTelegrame .= "Ticket de la transaction : ".$dataToInsert['code_transaction'].". ";
                                        $messageTelegrame .= "Context de la transaction:";
                                        $messageTelegrame .= $dataToInsert['context'].".  ";
                                        $messageTelegrame .= "Date transaction: ".$dataToInsert['date_transaction'].".  ";
                                        $messageTelegrame .= "Montant donné : ".$dataToInsert['quantite_source']." |  ";
                                        $messageTelegrame .= "Montant demandé : ".$dataToInsert['quantite_cible']." |  ";
                                        $messageTelegrame .= "Adresse de réception du client: ".$dataToInsert['adresse_reception'];



                                        $this->sendMessageTelegram($messageTelegrame);

                                        $tableJson['success'] = "Opération terminée. Aucune erreur détectée au cours de l'opération.";
                                        $this->sessionContainer->MessageTransaction['message'] = "Votre transaction a été prise en compte.Si votre transaction n'est pas validée au bout de <b style='font-weight: bold !important; text-decoration: underline'>10 minutes</b>, veuillez contacter le service support.";
                                        $this->sessionContainer->SessionTimeForDecount[$dataToInsert['code_transaction']] = time();

                                    }

                                }


                            } else {
                                $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                            }
                        } else {
                            $tableJson['error'] = "Désolé vous ne pouvez pas effectuer cette opération. Cet ID de transaction a déjà été utilisé dans une autre transaction.<br><b style='text-decoration:underline !important;'>NB: S'il s'agit de votre numéro de téléphone comme ID, veuillez rajouter un suffixe afin de poursuivre votre transaction. Ex: 90030303A</b>";
                        }

                    }
                    else
                    {
                        $tableJson['error'] = "Impossible de finaliser l'opération.";
                    }


                }
                else
                {
                    $tableJson['error'] = "Veuillez renseigner le champs.";
                }
            }
            else
            {

                $dataToInsert = array();

                $getResults = $this->sessionContainer->TransactionDatas;
                if(empty($getResults))
                {

                    if(!$request->isXmlHttpRequest())
                    {
                        echo '<h1 style="margin-top: 20px !important; text-align: center !important;">Impossible de poursuivre la l\'opération.</h1>';

                        sleep(5);

                        $this->redirect()->toRoute('customer');
                    }
                    $view = new JsonModel([
                        'error'=>"Impossible de poursuivre l'opération."
                    ]);
                    $view->setTerminal(true);

                    return $view;
                }


                $getDeviseSource =  $getResults['devise_source'];
                $getDeviseDestination = $getResults['devise_destination'];


                $dataToInsert['context'] = "Convertion de ".$getDeviseSource['lib_devise']." à ".$getDeviseDestination['lib_devise'];
                $dataToInsert['code_transaction'] = $this->postservice->generateRandom('6');
                $dataToInsert['devise_source'] = (int) $getDeviseSource['id_devise'];
                $dataToInsert['devise_cible'] = (int) $getDeviseDestination['id_devise'];
                $dataToInsert['made_by'] = $this->sessionContainer->IdUser;
                $dataToInsert['quantite_source'] =  $getResults['quantite_a_convertir'];
                $dataToInsert['quantite_cible'] =  $getResults['quantite_a_recevoir'];
                $dataToInsert['transaction_id'] = "LUNI_ID_".$this->postservice->generateRandom('6');
                $dataToInsert['date_transaction'] = $this->DateTime();
                $dataToInsert['date_start_expired'] = time();
                $dataToInsert['date_end_expired'] = time() + (0 * 0 * 10 * 0);

                if(isset($this->sessionContainer->TransactionFedaPay) || $this->sessionContainer->TransactionFedaPay !== null)
                {
                    unset($this->sessionContainer->TransactionFedaPay);
                }

                $dataToInsert['numero_envoi'] = $getResults['numero_envoi'];

                $dataToInsert['adresse_reception'] = $getResults['adresse_reception'];

                $checkIfTransactionExistByIdTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$data['id_transaction']),null,null,'unique',null,null);
                $checkIfTransactionExistByCodeTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$dataToInsert['code_transaction']),null,null,'unique',null,null);

                $salutations = "Bonjour";
                $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                $getFormat = $nowUtc->format('H');
                $getFormat = intval($getFormat);

                if($getFormat > 12)
                {
                    $salutations = "Bonsoir";
                }

                $key = "";
                $token ="";
                $nom_complet = $this->sessionContainer->UserName;
                $btn = '';
                $url = '';
                $titre = '';
                $subject = 'Votre compte LUNICHANGE: Transactions';
                $email = $this->sessionContainer->Login;
                $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                $f_id = WebServiceDbSqlMapper::FROM_ID;

                $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre code ou ticket de transaction est: <b>'.$dataToInsert['code_transaction'].'</b>. Cela vous sera utile en cas de réclamation..</p><br><p><b style="font-weight: 300 !important; text-decoration: underline !important">Identifiant de la transaction:</b> <b>'.$dataToInsert['transaction_id'].'</b></p>';
                $msg ['nb'][]= '';
                $msg ['nb'][]= '';
                $msg ['base_url'] = "";
                $msg ['btn_url'] = '';



                $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                //



                if (empty($checkIfTransactionExistByIdTransaction) && empty($checkIfTransactionExistByCodeTransaction))
                {
                    $requestInsert = $this->postservice->defaultInsert('transactions',$dataToInsert);

                    if ($requestInsert)
                    {
                        $mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');
                        if($mailSend === null)
                        {

                            $deleteTransactionTemp = (bool) $this->postservice->defaultDelete($this->postservice->getDbTables()['transactions_temp']['table'],array('made_by'=>$this->sessionContainer->IdUser));

                            if($deleteTransactionTemp)
                            {
                                $messageTelegrame = "Le client ".$this->sessionContainer->UserName. " a effectué une demande de transaction.";
                                $messageTelegrame .= " Informations de la transaction.  ";
                                $messageTelegrame .= "ID : ".$data['id_transaction'].". ";
                                $messageTelegrame .= "Ticket de la transaction : ".$dataToInsert['code_transaction'].". ";
                                $messageTelegrame .= "Context de la transaction:";
                                $messageTelegrame .= $dataToInsert['context'].".  ";
                                $messageTelegrame .= "Date transaction: ".$dataToInsert['date_transaction'].".  ";
                                $messageTelegrame .= "Montant donné : ".$dataToInsert['quantite_source']." |  ";
                                $messageTelegrame .= "Montant demandé : ".$dataToInsert['quantite_cible']." |  ";
                                $messageTelegrame .= "Adresse de réception du client: ".$dataToInsert['adresse_reception'];



                                $this->sendMessageTelegram($messageTelegrame);



                                unset($this->sessionContainer->TransactionDatas);
                                $this->sessionContainer->MessageTransaction['message'] = "Votre transaction a été prise en compte.Si votre transaction n'est pas validée au bout de <b style='font-weight: bold !important; text-decoration: underline'>15 minutes</b>, veuillez nous écrire via le chat.";

                                $tableJson['success'] = "Opération terminée. Aucune erreur détectée au cours de l'opération.";

                            }

                        }


                    } else {
                        $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                    }
                } else {
                    $tableJson['error'] = "Désolé vous ne pouvez pas effectuer cette opération. Cet ID de transaction a déjà été utilisé dans une autre transaction.<br><b style='text-decoration:underline !important;'>NB: S'il s'agit de votre numéro de téléphone comme ID, veuillez rajouter un suffixe afin de poursuivre votre transaction. Ex: 90030303A</b>";
                }



            }

        }
        else
        {

            if(!$request->isXmlHttpRequest() && !isset($data['id_transaction']) && isset($this->sessionContainer->TransactionDatas))
            {

                $dataToInsert = array();

                $getResults = $this->sessionContainer->TransactionDatas;
                if(empty($getResults))
                {

                    if(!$request->isXmlHttpRequest())
                    {
                        echo '<h1 style="margin-top: 20px !important; text-align: center !important;">Impossible de poursuivre la l\'opération.</h1>';

                        sleep(5);

                        $this->redirect()->toRoute('customer');
                    }
                    $view = new JsonModel([
                        'error'=>"Impossible de poursuivre l'opération."
                    ]);
                    $view->setTerminal(true);

                    return $view;
                }


                $getDeviseSource =  $getResults['devise_source'];
                $getDeviseDestination = $getResults['devise_destination'];


                $dataToInsert['context'] = "Convertion de ".$getDeviseSource['lib_devise']." à ".$getDeviseDestination['lib_devise'];
                $dataToInsert['code_transaction'] = $this->postservice->generateRandom('6');
                $dataToInsert['devise_source'] = (int) $getDeviseSource['id_devise'];
                $dataToInsert['devise_cible'] = (int) $getDeviseDestination['id_devise'];
                $dataToInsert['made_by'] = $this->sessionContainer->IdUser;
                $dataToInsert['quantite_source'] =  $getResults['quantite_a_convertir'];
                $dataToInsert['quantite_cible'] =  $getResults['quantite_a_recevoir'];
                $dataToInsert['transaction_id'] = "LUNI_ID_".$this->postservice->generateRandom('6');
                $dataToInsert['date_transaction'] = $this->DateTime();
                $dataToInsert['date_start_expired'] = time();
                $dataToInsert['date_end_expired'] = time() + (0 * 0 * 10 * 0);

                $dataToInsert['numero_envoi'] = "FedaPay";
                if(!isset($this->sessionContainer->TransactionFedaPay) || $this->sessionContainer->TransactionFedaPay)
                {
                    $dataToInsert['numero_envoi'] = $getResults['numero_envoi'];
                }

                $dataToInsert['adresse_reception'] = $getResults['adresse_reception'];

                $checkIfTransactionExistByIdTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$data['id_transaction']),null,null,'unique',null,null);
                $checkIfTransactionExistByCodeTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$dataToInsert['code_transaction']),null,null,'unique',null,null);

                $salutations = "Bonjour";
                $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                $getFormat = $nowUtc->format('H');
                $getFormat = intval($getFormat);

                if($getFormat > 12)
                {
                    $salutations = "Bonsoir";
                }

                $key = "";
                $token ="";
                $nom_complet = $this->sessionContainer->UserName;
                $btn = '';
                $url = '';
                $titre = '';
                $subject = 'Votre compte LUNICHANGE: Transactions';
                $email = $this->sessionContainer->Login;
                $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                $f_id = WebServiceDbSqlMapper::FROM_ID;

                $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre code ou ticket de transaction est: <b>'.$dataToInsert['code_transaction'].'</b>. Cela vous sera utile en cas de réclamation..</p><br><p><b style="font-weight: 300 !important; text-decoration: underline !important">Identifiant de la transaction:</b> <b>'.$dataToInsert['transaction_id'].'</b></p>';
                $msg ['nb'][]= '';
                $msg ['nb'][]= '';
                $msg ['base_url'] = "";
                $msg ['btn_url'] = '';



                $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                //



                if (empty($checkIfTransactionExistByIdTransaction) && empty($checkIfTransactionExistByCodeTransaction))
                {
                    $requestInsert = $this->postservice->defaultInsert('transactions',$dataToInsert);

                    if ($requestInsert)
                    {
                        $mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');
                        if($mailSend === null)
                        {

                            $deleteTransactionTemp = (bool) $this->postservice->defaultDelete($this->postservice->getDbTables()['transactions_temp']['table'],array('made_by'=>$this->sessionContainer->IdUser));

                            if($deleteTransactionTemp)
                            {
                                $messageTelegrame = "Le client ".$this->sessionContainer->UserName. " a effectué une demande de transaction.";
                                $messageTelegrame .= " Informations de la transaction.  ";
                                $messageTelegrame .= "ID : ".$data['id_transaction'].". ";
                                $messageTelegrame .= "Ticket de la transaction : ".$dataToInsert['code_transaction'].". ";
                                $messageTelegrame .= "Context de la transaction:";
                                $messageTelegrame .= $dataToInsert['context'].".  ";
                                $messageTelegrame .= "Date transaction: ".$dataToInsert['date_transaction'].".  ";
                                $messageTelegrame .= "Montant donné : ".$dataToInsert['quantite_source']." |  ";
                                $messageTelegrame .= "Montant demandé : ".$dataToInsert['quantite_cible']." |  ";
                                $messageTelegrame .= "Adresse de réception du client: ".$dataToInsert['adresse_reception'];



                                $this->sendMessageTelegram($messageTelegrame);

                                if(isset($this->sessionContainer->TransactionFedaPay) && $this->sessionContainer->TransactionFedaPay)
                                {
                                    unset($this->sessionContainer->TransactionFedaPay);
                                }

                                unset($this->sessionContainer->TransactionDatas);
                                $this->sessionContainer->MessageTransaction['message'] = "Votre transaction a été prise en compte.Si votre transaction n'est pas validée au bout de <b style='font-weight: bold !important; text-decoration: underline'>15 minutes</b>, veuillez nous écrire via le chat.";


                                if(!$request->isXmlHttpRequest())
                                {

                                    $this->redirect()->toRoute('transactions');
                                }

                                $tableJson['success'] = "Opération terminée. Aucune erreur détectée au cours de l'opération.";
                                $this->sessionContainer->SessionTimeForDecount[$dataToInsert['code_transaction']] = time();


                            }

                        }


                    } else {
                        $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                    }
                } else {
                    $tableJson['error'] = "Désolé vous ne pouvez pas effectuer cette opération. Cet ID de transaction a déjà été utilisé dans une autre transaction.<br><b style='text-decoration:underline !important;'>NB: S'il s'agit de votre numéro de téléphone comme ID, veuillez rajouter un suffixe afin de poursuivre votre transaction. Ex: 90030303A</b>";
                }



            }
            else
            {
                $tableJson['error'] = "Le traitement demandé ne peut aboutir";
            }
        }



        if(!$request->isXmlHttpRequest())
        {
            return $this->getResponse();
        }
        $view = new JsonModel($tableJson);
        $view->setTerminal(true);



        return $view;
    }
    public function listFieulsAction()
    {
        $this->_getHelper('headScript',$this->serviceLocator)

            ->appendFile($this->getUriPath(). 'assets/pages/list-fieuls/custom_script.js')
        ;
        $this->renderPage();

        return array();
    }


    private function sendMessageTelegram($message)
    {



        $telegrambot = "1477791644:AAGnbXBPsnBZ135rKH4kry7elLA0bYl60Ww";
        $telegramchatid = "-1001408210866";
        $url='https://api.telegram.org/bot'.$telegrambot.'/sendMessage';$data=array('chat_id'=>$telegramchatid,'text'=>$message);
        $options=array('http'=>array('method'=>'POST','header'=>"Content-Type:application/x-www-form-urlencoded\r\n",'content'=>http_build_query($data),),);
        $context=stream_context_create($options);
        $result=file_get_contents($url,false,$context);

        return $result;
    }


    public function mentoringAction()
    {


        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')


            ->appendFile($this->getUriPath() . 'assets/plugins/intl-tel-input/build/js/intlTelInput.js')
            ->appendFile($this->getUriPath() . 'assets/pages/global/Helpers.js')
            ->appendFile($this->getUriPath() . 'assets/pages/customer/mentoring.js')
            ->appendFile($this->getUriPath() . 'assets/pages/customer/mentoring_datatable.js')

        ;

        $this->renderPage();


        $getDevises = $this->postservice->defaultSelect('devises',[],[
            'categorie_devise'=>2,
            'statut'=>'1',
            'pays <>?'=>''
        ],null,null,'all',null,'lib_devise ASC');



        return array(
            'getUser'=>$this->postservice->getOneUser($this->_currentUser['id'],null),
            'lienSite'=>$this->serviceLocator->get('Config')['lienSite'],
            'devises'=>$getDevises
        );
    }

    public function fieulsAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('customer',["action"=>'list-fieuls']);
        }

        $data = $request->getPost()->toArray();

        $getUser = $this->postservice->getUser([
            'id_user'=>$this->_currentUser['id']
        ]) ;
        $arrayConditions[] = array(
            'parent'=>$getUser['id_user']
            // 'reference_parent'=>$getUser['reference_parent']
        );
        $myArray = array();
        $arrayColumns = array(
            'id_user'=>'id_user',
            'nom_complet'=>'nom_complet',
            'nom'=>'nom',
            'prenom'=>'prenom',
            'email'=>'email',
            'date_creation'=>'date_creation',
            'parent'=>'parent'
        );

        if(isset($data['query']) && !empty($data['query']))
        {

            if(!empty($data['query'][0]))
            {
                array_push($arrayConditions,[
                        'nom_complet LIKE ?'=>'%'.ltrim(filter_var($data['query'][0],FILTER_SANITIZE_STRING)).'%'

                    ]
                );
            }

        }


        $limit = null;
        $sort = 'date_creation DESC';

        if(isset($data['length']) && $data['length'] !== -1)
        {
            $limit['start'] = $data['length'];
            $limit['end'] = $data['start'];
        }
        if(isset($data['field'],$data['sort']) && $data['field'] != '')
        {
            $sort = $data['field'].' '.strtoupper($data['sort']);
        }

        $joins = null;

        $myRequest = $this->postservice->defaultSelect('luni_users',$arrayColumns,$arrayConditions,$joins,$limit,'all',null,$sort);
        $rowCount = count($this->postservice->defaultSelect('luni_users',[],$arrayConditions,$joins,null,'all',null,$sort));
        foreach ($myRequest as $keys=>$values)
        {

            foreach ($values as $j=>$elements)
            {
                $myArray[$keys][$j] = $elements;
            }

        }


        $total_pages = ceil(count($myRequest) / intval($data['pagination']['perpage']) );



        $tableJson = [

            'meta'=>array(
                "page"=> isset($data['pagination']['page']) && !empty($data['pagination']['page']) ? intval($data['pagination']['page']): 1,
                "pages"=> $total_pages,
                "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 1,
                "total"=> $rowCount,
                "sort"=> "desc",
                "field"=> "date_transaction"
            ),
            'data'=>$myArray,
            'current_user'=>intval($this->_currentUser['codeProfile'])
        ];



        $view  = new JsonModel($tableJson);
        $view->setTerminal(true);



        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }
    public function anotherFinishTransactionAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();



            $checkIfTempTransactionExist = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions_temp']['table'],[],
                array('made_by'=>(int) $this->sessionContainer->IdUser),null,null,'unique',null,null);


            if(!empty($checkIfTempTransactionExist))
            {
                $dataToInsert = array();
                $getDeviseSource = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array('id_devise'=> (int)$checkIfTempTransactionExist['devise_source']),null,
                    null,'unique',null,null);

                $getDeviseDestination = $this->postservice->defaultSelect($this->postservice->getDbTables()['devises']['table'],[],array('id_devise'=> (int)$checkIfTempTransactionExist['devise_destination']),null,
                    null,'unique',null,null);

                $dataToInsert['context'] = "Convertion de ".$getDeviseSource['lib_devise']." à ".$getDeviseDestination['lib_devise'];
                $dataToInsert['code_transaction'] = $this->postservice->generateRandom('6');
                $dataToInsert['devise_source'] = (int) $getDeviseSource['id_devise'];
                $dataToInsert['devise_cible'] = (int) $getDeviseDestination['id_devise'];
                $dataToInsert['made_by'] = $this->sessionContainer->IdUser;
                $dataToInsert['quantite_source'] = $checkIfTempTransactionExist['quantite_source'];
                $dataToInsert['quantite_cible'] = $checkIfTempTransactionExist['quantite_destination'];
                $dataToInsert['transaction_id'] = "ID_".$this->postservice->generateRandom('6');
                $dataToInsert['date_transaction'] = $this->DateTime();
                $dataToInsert['date_start_expired'] = time();
                $dataToInsert['date_end_expired'] = time() + (0 * 0 * 10 * 0);
                $dataToInsert['numero_envoi'] = $checkIfTempTransactionExist['numero_depot'];
                $dataToInsert['adresse_reception'] = $checkIfTempTransactionExist['adresse_reception'];

                if($checkIfTempTransactionExist['nom_transaction'] !== "" && $checkIfTempTransactionExist['nom_transaction'] !== null)
                {
                    $dataToInsert['nom'] = filter_var($checkIfTempTransactionExist['nom_transaction'],FILTER_SANITIZE_STRING);
                    $dataToInsert['prenom'] = filter_var($checkIfTempTransactionExist['prenom_transaction'],FILTER_SANITIZE_STRING);
                    $dataToInsert['pays'] = $checkIfTempTransactionExist['pays_transaction'];
                    $dataToInsert['ville'] = $checkIfTempTransactionExist['ville_transaction'];
                }

                $checkIfTransactionExistByIdTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$dataToInsert['transaction_id']),null,null,'unique',null,null);
                $checkIfTransactionExistByCodeTransaction = $this->postservice->defaultSelect('transactions',[],array('transaction_id'=>$dataToInsert['code_transaction']),null,null,'unique',null,null);

                $salutations = "Bonjour";
                $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                $getFormat = $nowUtc->format('H');
                $getFormat = intval($getFormat);

                if($getFormat > 12)
                {
                    $salutations = "Bonsoir";
                }

                $key = "";
                $token ="";
                $nom_complet = $this->sessionContainer->UserName;
                $btn = '';
                $url = '';
                $titre = '';
                $subject = 'Votre compte LUNICHANGE';
                $email = $this->sessionContainer->Login;
                $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                $f_id = WebServiceDbSqlMapper::FROM_ID;

                $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre code ou ticket de transaction est: <b>'.$dataToInsert['code_transaction'].'</b>. Cela vous sera utile en cas de réclamation..</p><br><p><b style="font-weight: 300 !important; text-decoration: underline !important">Identifiant de la transaction:</b> <b>'.$dataToInsert['transaction_id'].'</b></p>';
                $msg ['nb'][]= '';
                $msg ['nb'][]= '';
                $msg ['base_url'] = "";
                $msg ['btn_url'] = '';



                $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                if (empty($checkIfTransactionExistByIdTransaction) && empty($checkIfTransactionExistByCodeTransaction))
                {
                    $requestInsert = $this->postservice->defaultInsert('transactions',$dataToInsert);

                    $transaction_id = $dataToInsert['transaction_id'];

                    if ($requestInsert)
                    {
                        $mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');
                        if($mailSend === null)
                        {

                            $deleteTransactionTemp = (bool) $this->postservice->defaultDelete($this->postservice->getDbTables()['transactions_temp']['table'],array('made_by'=>$this->sessionContainer->IdUser));

                            if($deleteTransactionTemp)
                            {
                                $messageTelegrame = "Le client ".$this->sessionContainer->UserName. " a effectué une demande de transaction.";
                                $messageTelegrame .= " Informations de la transaction.  ";
                                $messageTelegrame .= "ID : ".$transaction_id.". ";
                                $messageTelegrame .= "Ticket de la transaction : ".$dataToInsert['code_transaction'].". ";
                                $messageTelegrame .= "Context de la transaction:";
                                $messageTelegrame .= $dataToInsert['context'].".  ";
                                $messageTelegrame .= "Date transaction: ".$dataToInsert['date_transaction'].".  ";
                                $messageTelegrame .= "Montant donné : ".$dataToInsert['quantite_source']." |  ";
                                $messageTelegrame .= "Montant demandé : ".$dataToInsert['quantite_cible']." |  ";
                                $messageTelegrame .= "Adresse de réception du client: ".$dataToInsert['adresse_reception'];



                                $this->sendMessageTelegram($messageTelegrame);

                                $tableJson['success'] = "Opération terminée. Aucune erreur détectée au cours de l'opération.";
                                $this->sessionContainer->MessageTransaction['message'] = "Votre transaction a été prise en compte.Si votre transaction n'est pas validée au bout de <b style='font-weight: bold !important; text-decoration: underline'>10 minutes</b>, veuillez contacter le service support.";
                                $this->sessionContainer->SessionTimeForDecount[$dataToInsert['code_transaction']] = time();

                            }

                        }


                    } else {
                        $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                    }
                } else {
                    $tableJson['error'] = "Désolé vous ne pouvez pas effectuer cette opération. Cet ID de transaction a déjà été utilisé dans une autre transaction.<br><b style='text-decoration:underline !important;'>NB: S'il s'agit de votre numéro de téléphone comme ID, veuillez rajouter un suffixe afin de poursuivre votre transaction. Ex: 90030303A</b>";
                }



            }
            else
            {
                $tableJson['error'] = "Impossible de finaliser l'opération.";
            }





            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('customer');
        }

        return $view;
    }
    public function treatInputsAction()
    {
        $request = $this->getRequest();
        $view=null;
        $tableJson = null;
        $result = null;
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            if(isset($this->sessionContainer->SourceForEquivalence,$this->sessionContainer->DestinationForEquivalence))
            {
                $getDevisesSourceIsSource = $this->postservice->defaultSelect('equivalences',array(),array(
                    'source'=>intval($this->sessionContainer->SourceForEquivalence),
                    'destination'=>intval($this->sessionContainer->DestinationForEquivalence)
                ),null,null,'unique',null,null);

                $getDevisesSourceIsDestination = $this->postservice->defaultSelect('equivalences',array(),array(
                    'destination'=>intval($this->sessionContainer->SourceForEquivalence),
                    'source'=>intval($this->sessionContainer->DestinationForEquivalence)
                ),null,null,'unique',null,null);
                $getSource = $this->postservice->defaultSelect('devises',[],array('id_devise'=>intval($this->sessionContainer->SourceForEquivalence)),null,null,'unique',null,null);
                $getDestination = $this->postservice->defaultSelect('devises',[],array('id_devise'=>intval($this->sessionContainer->DestinationForEquivalence)),null,null,'unique',null,null);

                $checkIfDeviseElectronic = false;

                if(!empty($getSource) && !empty($getDestination))
                {

                    if((intval($getSource['categorie_devise']) === 1 || intval($getSource['categorie_devise']) === 4) && (intval($getDestination['categorie_devise'])=== 1 || intval($getDestination['categorie_devise']) === 4))
                    {
                        $checkIfDeviseElectronic = true;

                    }

                    if($checkIfDeviseElectronic === true)
                    {
                        $getSourceFromEquivalence = $this->postservice->defaultSelect('equivalence_globale',[],array('devise'=>intval($getSource['id_devise'])),null,null,'unique',null,null);
                        $getDestinationFromEquivalence = $this->postservice->defaultSelect('equivalence_globale',[],array('devise'=>intval($getDestination['id_devise'])),null,null,'unique',null,null);


                        if(!empty($getDestinationFromEquivalence) && !empty($getSourceFromEquivalence))
                        {
                            if(isset($data['input_destination']) && !empty($data['input_destination']))
                            {
                                $calcul = (floatval($getDestinationFromEquivalence['prix_vente']) *floatval($data['input_destination']))/floatval($getSourceFromEquivalence['prix_achat']);
                                $result = round($calcul,5,PHP_ROUND_HALF_EVEN);
                            }
                            else
                            {
                                $calcul = (floatval($getSourceFromEquivalence['prix_achat'])*floatval($data['input_source']))/floatval($getDestinationFromEquivalence['prix_vente']);

                                $result = round($calcul,5,PHP_ROUND_HALF_EVEN);
                            }
                            $tableJson['results'] = $result;
                        }
                        else
                        {
                            $tableJson['error'] = "Impossible3 d'effectuer le traitement";
                        }

                    }
                    else
                    {
                        if(!empty($getDevisesSourceIsSource))
                        {


                            if(isset($data['input_destination']) && !empty($data['input_destination']))
                            {
                                $result = round(floatval($data['input_destination'])/ $getDevisesSourceIsSource['taux_achat'],5,PHP_ROUND_HALF_EVEN);

                            }
                            else
                            {
                                $result = round(floatval($data['input_source'])* $getDevisesSourceIsSource['taux_achat'],5,PHP_ROUND_HALF_EVEN);

                            }


                            $tableJson['results'] = $result;
                        }
                        elseif(!empty($getDevisesSourceIsDestination))
                        {


                            if(isset($data['input_destination']) && !empty($data['input_destination']))
                            {
                                $result = round(floatval($data['input_destination']) * $getDevisesSourceIsDestination['taux_vente'],5,PHP_ROUND_HALF_EVEN);

                            }
                            else
                            {
                                $result = round(floatval($data['input_source']) / $getDevisesSourceIsDestination['taux_vente'],5,PHP_ROUND_HALF_EVEN);

                            }


                            $tableJson['results'] = $result;
                        }
                        else
                        {
                            $tableJson['error'] = "Impossible(4) d'effectuer le traitement.";
                        }
                    }

                }
                else
                {
                    $tableJson['error'] = "Impossible(1) d'effectuer le traitement.";
                }


            }
            else
            {
                $tableJson['error'] = "Impossible(2) d'effectuer les traitements.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

        }
        else
        {
            $this->redirect()->toRoute('customer');
        }
        return $view;
    }

    public function askRecallAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        $view = null;
        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('customer',['action'=>'mentoring']);
        }

        $datas = $request->getPost()->toArray();
        $getDevises = $this->postservice->defaultSelect('devises',[],[
            'categorie_devise'=>2,
            'statut'=>'1',
            'pays <>?'=>''
        ],null,null,'all',null,'lib_devise ASC');

        foreach($datas as $keys=>$values)
        {
            if($datas[$keys] == '')
            {
                $tableJson['error'] = "Veuillez remplir tous les champs";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
                break;
            }

            $datas[$keys] = filter_var($values,FILTER_SANITIZE_STRING);
        }

        $getUser = $this->postservice->getOneUser($this->_currentUser['id'],null);

        if(intval($getUser['gain']) < 5400)
        {
            $tableJson['error'] = "Vous n'êtes pas actuellement autorisé à effectuer de retrait";

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if(intval($datas['montant']) < 5400)
        {
            $tableJson['error'] = 'Le retrait minimum est de: 5400 FCFA';
        }

        if(strlen($datas['titulaire']) < 3)
        {
            $tableJson['error'] = "Format du nom du propriétaire du numéro de télephone incorrecte. Trois(03) caractères au minimum";

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
            return $view;
        }

        $getDevise = $this->postservice->defaultSelect('devises',[],array(
            'id_devise'=>intval($datas['method_paiement'])
        ),null,null,'unique',null,null);

        if($datas['codeCountry'] !== $getDevise['pays'])
        {
            $tableJson['error'] = "Une erreur s'est produite. Veuillez vérifier la méthode de paiement";

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
            return $view;
        }
        $ticketDemande =  null;


        do{
            $ticketDemande = $this->postservice->generateRandom(strlen($this->random('4')));
        }while((bool)$this->postservice->defaultSelect('transactions_bonus',[],['ticket_demande'=>$ticketDemande],null,null,'unique',null,null));



        if(!isset($tableJson['error']) || $tableJson['error'] == null)
        {
            $datas['proprietaire'] = filter_var($datas['titulaire'],FILTER_SANITIZE_STRING);
            unset($datas['titulaire']);
            $reqInsert = $this->postservice->defaultInsert(
                'transactions_bonus',
                [
                    'moyen_paiement'=>intval($datas['method_paiement']),
                    'montant'=>intval($datas['montant']),
                    'date'=>date('Y-m-d H:i:s'),
                    'customer'=>$this->_currentUser['id'],
                    'proprietaire'=>$datas['proprietaire'],
                    'telephone'=>$datas['dialCode'].''.$datas['telephone'],
                    'ticket_demande'=>$ticketDemande
                ]
            );
            if((bool)$reqInsert)
            {
                $messageTelegrame = "Le client ".$this->sessionContainer->UserName. " a effectué une demande de retrait.";
                $messageTelegrame .= " Informations de la transaction.  ";
                $messageTelegrame .= "Montant du retrait : ".$datas['montant'].". ";
                $messageTelegrame .= "Ticket de la demande : ".$ticketDemande.". ";
                $messageTelegrame .= "Méthode de paiement demandée par le client: ".$getDevise['lib_devise']."|";
                $messageTelegrame .= "Téléphone concerné: ".$datas['dialCode'].''.$datas['telephone'].'|';
                $messageTelegrame .= "Propriétaire du compte: ".filter_var($datas['proprietaire'],FILTER_SANITIZE_STRING);
                $messageTelegrame .= "Date transaction: ".date('Y-m-d H:i:s').".  ";

                $this->postservice->sendMessageTelegram($messageTelegrame);
                $tableJson['success'] = "Demande de retrait envoyée.";

                $this->sessionContainer->MessageWelcome = $tableJson['success'];
            }

        }


        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }

    public function listTransactionsBonusAction()
    {

        $request = $this->getRequest();

        $view = null;
        $tableJson = null;
        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('customer',['action'=>'mentoring']);
        }

        $data = $request->getPost()->toArray();

        $joins = null;
        $arrayConditions[] = array(

            'customer'=>$this->_currentUser['id']

        );
        $myArray = array();
        $arrayColumns = array(
            'id_transaction_bonus'=>'id_transaction_bonus',
            'moyen_paiement'=>'moyen_paiement',
            'montant'=>'montant',
            'commentaire'=>'commentaire',
            'telephone'=>'telephone',
            'ticket_demande'=>'ticket_demande',
            'statut_transaction'=>'statut_transaction_bonus',
            'date_transaction'=>'date'

        );

        $joins[] = array(
            'table'=>['d'=>'devises'],
            'condition'=>'d.id_devise=t.moyen_paiement'
        );
        if(isset($data['query']) && !empty($data['query']))
        {

            if(!empty($data['query'][0]))
            {
                array_push($arrayConditions,[

                        'ticket_demande LIKE ?'=>'%'.ltrim(filter_var($data['query'][0],FILTER_SANITIZE_STRING)).'%'

                    ]
                );
            }

        }


        $limit = null;
        $sort = 'date DESC';

        if(isset($data['length']) && $data['length'] !== -1)
        {
            $limit['start'] = $data['length'];
            $limit['end'] = $data['start'];
        }
        if(isset($data['field'],$data['sort']) && $data['field'] != '')
        {
            $sort = $data['field'].' '.strtoupper($data['sort']);
        }



        $myRequest = $this->postservice->defaultSelect(array(
            't'=>'transactions_bonus'),$arrayColumns,$arrayConditions,$joins,$limit,'all',null,$sort);
        $rowCount = count($this->postservice->defaultSelect([
            't'=>'transactions_bonus'
        ],[],$arrayConditions,$joins,null,'all',null,$sort));
        foreach ($myRequest as $keys=>$values)
        {

            foreach ($values as $j=>$elements)
            {
                $myArray[$keys][$j] = $elements;
            }

        }


        $total_pages = ceil(count($myRequest) / intval($data['pagination']['perpage']) );



        $tableJson = [

            'meta'=>array(
                "page"=> isset($data['pagination']['page']) && !empty($data['pagination']['page']) ? intval($data['pagination']['page']): 1,
                "pages"=> $total_pages,
                "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 1,
                "total"=> $rowCount,
                "sort"=> "desc",
                "field"=> "date_transaction"
            ),
            'data'=>$myArray,
            'current_user'=>intval($this->_currentUser['codeProfile'])
        ];




        $view = new JsonModel($tableJson);
        $view->setTerminal(true);


        return $view;
    }

    public function random($car)
    {
        $string = "";
        $chaine = "1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Laminas\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }

    private function DateTime()
    {
        //$oTimeZoneUtc = new \DateTimeZone('UTC');
        //date_default_timezone_set('UTC');
        $oTimeZoneLocal = new \DateTimeZone(date_default_timezone_get());
        $oDateTime = new DateTime('now', $oTimeZoneLocal);
        return $oDateTime->format('Y-m-d H:i:s');

    }

}