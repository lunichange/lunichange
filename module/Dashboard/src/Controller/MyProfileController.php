<?php


namespace Dashboard\Controller;


use Application\Mapper\WebServiceDbSqlMapper;
use Application\Service\PostServiceInterface;
use Dashboard\Service\PostService;
use Laminas\Json\Json;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ServiceManager\ServiceManager;
use Laminas\View\Model\ViewModel;

use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;



class MyProfileController extends AbstractActionController
{
    protected $postservice;

    protected $table;
    protected $headerView;
    protected $asideView;
    protected $menuView;

    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $asideVars;
    protected $menuVars;

    protected $footerVars;
    protected $contentVars;





    private $_currentUser = array();
    protected $sessionManager;
    protected $sessionContainer;


    protected $serviceLocator;

    public function __construct(PostServiceInterface $postService, $container)
    {
        $this->postservice = $postService;

        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser)
        {
            $this->_currentUser = [
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null ? $this->sessionContainer->CodeProfil : null,
                'username'=>isset($this->sessionContainer->Login) ? filter_var($this->sessionContainer->Login,FILTER_SANITIZE_STRING) : null,
                'fullname'=>isset($this->sessionContainer->UserName) && $this->sessionContainer->UserName ? filter_var($this->sessionContainer->UserName,FILTER_SANITIZE_STRING) : null
            ];
        }

        //Layout
        $this->contentVars = array();
        $this->table = 'luni_users';


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');

        //aside layout
        $this->asideView = new ViewModel();
        $this->asideVars = array();
        $this->asideView->setTemplate('layout/aside');


        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();



    }

    public function indexAction()
    {

        $this->asideVars['menu_active'] = 'kt-widget__item--active';
        $this->asideVars['current_action'] = $this->params('action');

        $this->renderPage();

        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')
            ->appendFile($this->getUriPath() . 'assets/pages/my-profile/edit-profil_custom_script.js?'.uniqid())
            ->appendFile($this->getUriPath() . 'assets/pages/my-profile/custom_script.js?'.uniqid())
            ->appendFile($this->getUriPath() . 'assets/plugins/intl-tel-input/build/js/intlTelInput.js')
            ->appendFile($this->getUriPath() . 'assets/pages/global/Helpers.js?'.uniqid())
            ->appendFile($this->getUriPath() . 'assets/pages/my-profile/edit-profil_form_ajax_new.js?'.uniqid())


        ;

        $user_values = array();
        $get_pays = array();


        if($this->_currentUser['id'] !== ""  && $this->_currentUser['id'] !== null)
        {
            $joins['type'] ='left';
            $joins['data'] [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            $joins['data'] [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values ['user_data'] = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);

            $get_pays = $this->postservice->defaultSelect('pays',array(),['id_pays'=>intval($user_values['user_data']['pays'])],null,null,'unique',null,null);

        }


        return array(
            'user_info'=>$user_values,
            'pays_user'=>$get_pays
        );
    }



    public function adresseAction()
    {
        $this->asideVars['current_action'] = $this->params('action');
        $this->renderPage();
        if($this->_currentUser['id'] !== ""  && $this->_currentUser['id'] !== null)
        {
            $joins['type'] ='left';
            $joins['data'] [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            $joins['data'] [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values ['user_data'] = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);

            $get_pays = $this->postservice->defaultSelect('pays',array(),['id_pays'=>intval($user_values['user_data']['pays'])],null,null,'unique',null,null);
        }

        $request = $this->getRequest();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $tableJson = array();

            $isEverythingAlright = true;
            foreach($data as $kk =>$values)
            {
                if($data[$kk] !== "")
                {
                    $data[$kk] = filter_var($values,FILTER_SANITIZE_STRING);
                }
                if($data[$kk] == "")
                {
                    $tableJson['error'] = "Veuillez remplir tous les champs";
                    $isEverythingAlright = false;
                    break;
                }
            }
            if(strlen($data['region']) < 3 || strlen($data['pays']) < 2 || strlen($data['ville']) < 3 || strlen($data['adresse']) < 3)
            {
                $tableJson['error'] = "Une erreur s'est produite.Veuillez vérifier vos informations";
                $isEverythingAlright = false;
            }

            if($isEverythingAlright)
            {
                $reqUpdate = $this->postservice->defaultUpdate('luni_users',$data,[
                    'id_user'=>$this->_currentUser['id']
                ]);
                if((bool)$reqUpdate)
                {
                    $tableJson['success'] = "Vos informations ont été mises à jour";
                    $this->sessionContainer->MessageWelcome = "Vos informations ont été mises à jour";
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        return array(
            'user_info'=>$user_values,
            'pays_user'=>$get_pays
        );

    }

    public function editPasswordAction()
    {
        $this->asideVars['menu_active'] = "kt-widget__item--active";
        $this->asideVars['current_action'] = $this->params('action');

        $this->renderPage();

        return array();
    }
    public function identificationAction()
    {
        $this->asideVars['menu_active'] = "kt-widget__item--active";
        $this->asideVars['current_action'] = $this->params('action');
        $this->contentVars['current_action'] = $this->params('action');
        $this->renderPage();
        $table = [
            'type_piece'=>'type_piece',
            'documents'=>'documents'
        ];
        $treatment = null;
        $join = null;
        $getTypePiece = $this->postservice->defaultSelect($table['type_piece'],[],array('statut'=>'1'),null,
            null,'all',null,'lib_type_piece ASC');


        $join [] = array(
            'table'=>array(
                't'=>'type_piece'
            ),
            'condition'=>'t.code_type_piece=d.type_document'
        );
        $this->_getHelper('headScript',$this->serviceLocator)


            ->appendFile($this->getUriPath().'assets/pages/my-profile/identification_script.js')
            ->appendFile($this->getUriPath().'assets/pages/global/Helpers.js')
            ->appendFile($this->getUriPath().'assets/pages/my-profile/datatableFile.js')
            ->appendFile($this->getUriPath().'assets/pages/my-profile/custom_upload_file.js')

        ;



        $getDocuments = $this->postservice->defaultSelect(array(
            'd'=>$table['documents']
        ),[
            'id_document'=>'id_document',
            'type_document'=>'type_document',
            'numero_piece'=>'numero_piece',
            'document'=>'document',
            'document_selfie'=>'document_selfie',
            'chemin_document'=>'chemin_document',
            'lock_doc'=>'lock_doc',
            'status_document'=>'statut_document'
        ],array(
            'id_user'=>intval($this->_currentUser['id'])
        ),$join,null,'all',null,null);


        $getUser = $this->postservice->defaultSelect($this->table,[],array(
            'id_user'=>$this->_currentUser['id']
        ),null,null,'unique',null,null);

        $messageDemande = null;

        if(isset($this->sessionContainer->MessageDemande) &&($this->sessionContainer->MessageDemande !== null || $this->sessionContainer->MessageDemande !== ''))
        {
            $messageDemande = $this->sessionContainer->MessageDemande;

            unset($this->sessionContainer->MessageDemande);
        }

        if(!empty($getSelfie) && !empty($getDocuments))
        {
            unset($this->sessionContainer->MessageDemande);
        }
        $request = $this->getRequest();

        if($request->isPost())
        {
            $data = $request->getPost()->toArray();

            $treatment = $this->addDocument([
                'files'=>$_FILES,
                'data'=>$data
            ]);

        }


        return array(
            'type_pieces'=>$getTypePiece,
            'documents'=>$getDocuments,
            'id_user'=>intval($this->_currentUser['id']),
            'messageDemande'=>$messageDemande,
            'getUser'=>$getUser,
            'resultTreatment'=>$treatment,
            'dataPost'=>isset($data) && !empty($data)? $data : []

        );

    }

    public function updateIdAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('my-profile',['action'=>'']);
        }
        $data = $request->getPost()->toArray();



        $uploadDir = $this->serviceLocator->get('Config')['document_path'];
        $allowTypes = array('pdf', 'jpg', 'png', 'jpeg');

        $nameFileFolder = $this->_currentUser['id'];
        $uploadStatus = null;

        $isOk = true;
        $path =  null;
        $uploadedFile = null;
        $res = isset($this->sessionContainer->StdGetForUpdate) && $this->sessionContainer->StdGetForUpdate !== null ? $this->sessionContainer->StdGetForUpdate : [];

        if($_FILES['id_card']['name'] == "")
        {
            $tableJson['error'] = "Veuillez renseigner un fichier";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        foreach($data as $kk=>$values)
        {
            if(empty($data[$kk]))
            {
                $tableJson['error'] = "Veuillez renseigner tous les champs";
                $isOk = false;
                break;
            }
            if(!empty($data[$kk]))
            {
                $data[$kk] = filter_var($values,FILTER_SANITIZE_STRING);
            }
        }
        if(!$isOk)
        {
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        $fileName = $this->clean(basename($_FILES['id_card']['name']));

        if(strlen($fileName) > 40)
        {
            $tableJson['error'] = "Nom du fichier trop long. Quarante(40) caractères au maximum";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if(is_dir($uploadDir.$nameFileFolder))
        {

            unlink($uploadDir.$res['chemin_document'].'/'.$res['document']);

        }

        $fileType = pathinfo($uploadDir.$nameFileFolder.'/' .$fileName, PATHINFO_EXTENSION);
        $fileName = uniqid().'_ID_CARD.'.$fileType;
        $targetFilePath = $uploadDir.$nameFileFolder.'/' .$fileName;


        if(!in_array($fileType, $allowTypes))
        {
            $tableJson['error'] = "Format du fichier invalide";

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if($_FILES['id_card']['size'] > 5485760)
        {
            $tableJson['error'] = "La taille de votre fichier ne doit pas dépasser 5MB";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        $compress = $this->compress($_FILES["id_card"]["tmp_name"], $_FILES["id_card"]["tmp_name"],100);

        switch ($data['type_piece'])
        {
            case 'CNI':
                if(strlen($data['numero_piece']) < 5)
                {
                    $tableJson['error'] = "Le numéro de la carte d'identité n'est pas correcte.";
                }
                break;
            case 'LEPI':
                if(strlen($data['numero_piece']) < 5)
                {
                    $tableJson['error'] = 'Numéro de la carte LEP non valide';
                }
                break;
            case 'PASS':
                if(strlen($data['numero_piece']) < 12)
                {
                    $tableJson['error'] = "Format du passeport non valide";
                }
                break;

            default:
                $tableJson['error'] = "Le traitement ne peut aboutir. Les données fournies ne  sont pas correctes";
        }

        if(!isset($tableJson['error']) || $tableJson['error'] == null)
        {
            if(move_uploaded_file($compress,$targetFilePath))
            {
                $uploadedFile = $fileName;
                if($uploadedFile !== null )
                {
                    $dataToUpdate['document'] = filter_var($uploadedFile,FILTER_SANITIZE_STRING);
                }


                $mailSend = $this->sendMail('update');
                if($mailSend)
                {
                    $dataToUpdate['type_document'] = filter_var($data['type_piece'],FILTER_SANITIZE_STRING);
                    $dataToUpdate['numero_piece'] = filter_var($data['numero_piece'],FILTER_SANITIZE_STRING);
                    $dataToUpdate['statut_document'] = '0';
                    $dataToUpdate['publish_date']  = date('Y-m-d H:i:s');
                    $dataToUpdate['motif'] = NULL;


                    $redUpdateDoc = $this->postservice->defaultUpdate('documents',$dataToUpdate,array(
                        'id_document'=>intval($res['id_document'])
                    ));
                    $getUser = $this->postservice->getUser(array(
                        'id_user'=>$this->_currentUser['id']
                    ));

                    $reqMsgSite = $this->postservice->defaultInsert(
                        $this->postservice->getDbTables()['messages_site']['table'],array(
                            'contenu'=>'L\'utilisateur'.filter_var($getUser['nom_complet'],FILTER_SANITIZE_STRING).' a mis à jour son dossier de verification d\'identité',
                            'subject'=>'Mise à jour de la demande de validation d\'identité',
                            'email'=>$getUser['email'],
                            'customer'=>$getUser['nom_complet'],
                            'date_message'=>date('Y-m-d h:i:s'),
                            'nloop'=>15,
                            'repeat'=>5

                        )
                    );

                    if((bool)$reqMsgSite)
                    {
                        if($redUpdateDoc)
                        {
                            $this->sessionContainer->MessageWelcome = "Votre demande d'identification a été envoyé. Elle sera traitée avec le plus grand soin. Nous vous reviendrons.";
                            unset($this->sessionContainer->StdGetForUpdate);
                            $tableJson['success'] = "Informations modifiées";
                        }
                    }

                }

            }
        }
        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }




    public function compress($source, $destination, $quality)
    {

        $info = getimagesize($source);
        $image = null;

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);


        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);



        return $destination;
    }


    /**
     * @param $post
     * @return array|int
     * @throws \Exception
     */
    private function addDocument($post)
    {
        $results = array();
        if(!is_array($post))
        {
            throw new \Exception('Impossible d\'effectuer le traitement demandé',E_USER_WARNING);
            return -1;
        }

        if(!isset($post['files']) || !isset($post['data']))
        {
            throw new \Exception('Impossible d\'effectuer le traitement demandé',E_USER_WARNING);
            return -1;
        }


        $allowTypes = array('pdf', 'jpg', 'png', 'jpeg');
        $nameFileFolder = $this->_currentUser['id'];


        if(isset($post['files']['id_card'],$post['files']['document_selfie']))
        {
            if($post['files']['id_card']['name'] == "" || $post['files']['document_selfie'] == "")
            {
                $results['errorMsg'] = "Veuillez renseigner le document d'identité et le selfie.";
                return $results;
            }
        }




        $fileName = basename($post['files']['id_card']['name']);
        $fileSelfieName = basename($post['files']['document_selfie']['name']);
        $uploadDir = $this->serviceLocator->get('Config')['document_path'];


        if(!is_dir($uploadDir.$nameFileFolder))
        {
            mkdir($uploadDir.$nameFileFolder);
        }


        $fileType = pathinfo($uploadDir.$nameFileFolder.'/' .$fileName, PATHINFO_EXTENSION);
        $fileSelfieType = pathinfo($uploadDir.$nameFileFolder.'/' .$fileSelfieName, PATHINFO_EXTENSION);

        $fileName = uniqid().'_ID_CARD.'.$fileType;
        $fileSelfieName = uniqid().'_SELFIE.'.$fileType;
        $targetFilePath = $uploadDir.$nameFileFolder.'/' .$fileName;
        $targetSelfieFilePath = $uploadDir.$nameFileFolder.'/' .$fileSelfieName;


        if(!in_array($fileType, $allowTypes) && in_array($fileSelfieType,$allowTypes))
        {
            $results['errorMsg'] = 'Désolé, seuls les fichiers PDF et images sont autorisés à être téléchargés.';

            return $results;
        }

        if($post['files']['id_card']['size'] > 5485760 || $post['files']['document_selfie']['size'] > 5485760)
        {
            $results['errorMsg'] = "Les tailles de vos fichiers ne doivent pas dépasser 5MB";
            return $results;
        }
        if($fileSelfieName == $fileName)
        {
            $results['errorMsg'] = "Les fichiers ne doivent pas être identiques";
            return $results;
        }

        switch ($post['data']['type_piece'])
        {
            case 'CNI':
                if(strlen($post['data']['numero_piece']) < 5)
                {
                    $results['errorMsg'] = "Le numéro de la carte d'identité n'est pas correcte.";
                }
                break;
            case 'LEPI':
                if(strlen($post['data']['numero_piece']) < 5)
                {
                    $results['errorMsg'] = 'Numéro de la carte LEP non valide';
                }
                break;
            case 'PASS':
                if(strlen($post['data']['numero_piece']) < 12)
                {
                    $results['errorMsg'] = "Format du passeport non valide";
                }
                break;

            default:
                $results['errorMsg'] = "Le traitement ne peut aboutir. Les données fournies ne  sont pas correctes";
        }

        $checkIfDocExist = $this->postservice->defaultSelect('documents',[],array(
            'id_user'=>$this->_currentUser['id']
        ),null,null,'unique',null,null);

        if((bool)$checkIfDocExist)
        {
            $results['errorMsg'] = "Vous avez déjà un dossier constitué au niveau de l'administration de LUNICHANGE";
            return $results;
        }


        if(file_exists($targetFilePath) || file_exists($targetSelfieFilePath))
        {
            $results['errorMsg'] = "Ces fichiers vous appartenant existe déjà dans notre base de données";
            return $results;
        }
        if(file_exists($targetFilePath))
        {
            $results['errorMsg'] = "Vous avez déjà un document d'identité existant";
            return $results;
        }
        if(file_exists($targetSelfieFilePath))
        {
            $results['errorMsg'] = "Vous avez déjà un document selfie existant";
            return $results;
        }
        if(strlen($fileName) > 100)
        {
            $results['errorMsg'] = "Nom du fichier du document d'identité trop long. Quarante(40) caractères au maximum.";
            return $results;
        }

        if(strlen($fileSelfieName) > 100)
        {
            $results['errorMsg'] = 'Nom du fichier du document selfie trop long. Quarante(40) caractères au maximum.';
            return $results;
        }
        $fileName = $this->clean($fileName);
        $fileSelfieName = $this->clean($fileSelfieName);



        $dataToInsert = [
            'type_document'=>filter_var($post['data']['type_piece'],FILTER_SANITIZE_STRING),
            'numero_piece'=>filter_var($post['data']['numero_piece'],FILTER_SANITIZE_STRING),
            'id_user'=>$this->_currentUser['id'],
            'publish_date'=>date('Y-m-d H:i:s'),
            'chemin_document'=>$this->_currentUser['id']
        ];
        $insert = $this->postservice->defaultInsert('documents',$dataToInsert);
        if((bool)$insert)
        {

            ob_start();
            $uploadStatus =  move_uploaded_file($post['files']["id_card"]["tmp_name"], $targetFilePath);
            $uploadSelfieStatus = (move_uploaded_file($post['files']["document_selfie"]["tmp_name"], $targetSelfieFilePath));
            ob_get_clean();
            if($uploadStatus && $uploadSelfieStatus)
            {

                $reqUpdate = (bool) $this->postservice->defaultUpdate('documents',[
                    'document'=>$fileName,
                    'document_selfie'=>$fileSelfieName
                ],[

                    'id_user'=>$this->_currentUser['id']
                ]);
                if($reqUpdate)
                {

                    /*$this->postservice->defaultUpdate($this->table,[
                        'profil_complet'=>'0'
                    ],[
                        'id_user'=>$this->_currentUser['id']
                    ]);*/
                    $this->sessionContainer->MessageWelcome = "Document d'identité ajouté avec succès";

                    $this->redirect()->toRoute('my-profile',['action'=>'identification']);
                }
            }

        }

        return $results;
    }

    public function informationsAction()
    {
        $this->asideVars['current_action'] = $this->params('action');
        $this->renderPage();

        $user_values = array();
        $get_pays = array();

        $joins['type'] ='left';
        $joins['data'] [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
        $joins['data'] [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
        $user_values ['user_data'] = $this->getOneUser(intval($this->_currentUser['id']),$joins);

        $get_pays = $this->postservice->defaultSelect('pays',array(),['id_pays'=>intval($user_values['user_data']['pays'])],null,null,'unique',null,null);






        return array(
            'user_info'=>$user_values,
            'pays_user'=>$get_pays
        );
    }

    public function updateInfoPersoAction()
    {
        $request = $this->getRequest();
        $datas = $request->getPost()->toArray();
        $tableJson = null;

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('my-profile');
        }

        $getPost = array();
        $empty =false;
        $everyThingIsAlright =true;

        foreach($datas as $keys=>$values)
        {
            if($keys=='telephone' || $keys == 'fname' || $keys == 'lname' || $keys == 'email' || $keys == 'date_naissance' || $keys == 'sexe' || $keys == "adresse" || $keys == 'region' || $keys == 'ville' || $keys == 'country')
            {
                $getPost[$keys]= filter_var($values,FILTER_SANITIZE_STRING);
            }
        }

        foreach($getPost as $kk =>$elements)
        {
            if($kk !== 'ville' && $kk !== 'region' && $kk !== 'adresse')
            {
                if(empty($getPost[$kk]))
                {
                    $tableJson['error'] = 'Veuillez remplir tous les champs';
                    $empty = true;
                    $everyThingIsAlright = false;
                    break;
                }
            }

        }
        if(isset($empty) && $empty == true)
        {
            unset($empty);

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if(strlen($getPost['fname']) < 2)
        {
            $tableJson['error'] = "Format du prénom invalide. Deux(02) caractères au minimum";

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        if(strlen($getPost['lname']) < 2)
        {

            $tableJson['error'] = "Format du nom invalide. Deux (02) caractères au minimum";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;

        }

        $checkUserExist = $this->postservice->defaultSelect('luni_users',[],[
            'or'=>'or',
            'results'=>array(
                'telephone'=>$getPost['telephone'],
                'email'=>$getPost['email']
            )
        ],null,null,'unique',null,null);


        if(!empty($checkUserExist))
        {
            if(intval($checkUserExist['id_user']) !== $this->_currentUser['id'])
            {

                $tableJson['error'] = "Un utilisateur autre que vous possède déjà ces informations";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
        }

        if(!filter_var($getPost['email'],FILTER_VALIDATE_EMAIL))
        {

            $tableJson['error'] = "Adresse mail invalide";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        if(strlen($getPost['date_naissance']) < 10 || strlen($getPost['date_naissance']) > 10)
        {
            $tableJson['error'] = "Format de la date de naissance incorrecte";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        if(!in_array($getPost['sexe'],['F','M']))
        {
            $tableJson['error'] = "Impossible de traiter la demande. Veuillez vérifier les informations.";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        if(isset($getPost['adresse']) && $getPost['adresse'] !== '' && strlen($getPost['adresse']) < 3)
        {
            $tableJson['error'] = "Adresse trop courte trois (03) caractères au minimum";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if(isset($getPost['ville']) && $getPost['ville'] !== '' && strlen($getPost['ville']) < 3)
        {
            $tableJson['error'] = "Ville incorrecte. Trois (03) caractères au minimum";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if(isset($getPost['region']) && $getPost['region'] !== '' && strlen($getPost['region']) < 2)
        {
            $tableJson['error'] = "Région trop courte trois (03) caractères au minimum";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        try
        {
            if(!$this->checkDate($getPost['date_naissance']))
            {
                $everyThingIsAlright = false;
                $tableJson['error'] = "La date n'est pas correcte";
            }
        }catch(\Exception $e)
        {
            print_r($e->getMessage());
        }


        if($everyThingIsAlright)
        {
            if(isset($getPost['country']) && $getPost['country'] !== '')
            {
                $getPost['pays'] = filter_var($getPost['country'],FILTER_SANITIZE_STRING);

                unset($getPost['country']);
            }
            foreach($getPost as $elements=>$values)
            {
                $getPost[$elements] = filter_var($values,FILTER_SANITIZE_STRING);
            }
            $getPost['nom_complet'] = $getPost['fname'].' '.$getPost['lname'];
            $getPost['nom_complet'] = htmlentities(filter_var($getPost['nom_complet'],FILTER_SANITIZE_STRING));
            $getPost['dial_code'] = intval($datas['dialCode']);
            $getPost['nom'] = $getPost['lname'];
            $getPost['prenom'] = $getPost['fname'];
            unset($getPost['lname']);
            unset($getPost['fname']);
            $reqInsert = $this->postservice->defaultUpdate($this->postservice->getDbTables()['customers']['table'],$getPost,array(
                'id_user'=>$this->_currentUser['id']
            ));

            if($reqInsert)
            {
                $this->sessionContainer->Login = $getPost['email'];
                $this->_currentUser['username'] = $getPost['email'];
                $this->_currentUser['fullname'] = $getPost['nom_complet'];

                $tableJson['success'] = "Votre profil à été mis à jour.";
                $this->sessionContainer->MessageWelcome = $tableJson['success'];
                if($this->_currentUser['codeProfile'] == '2')
                {
                    $this->sessionContainer->UserName = $getPost['nom_complet'];
                }
            }

        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }


    private function checkDate($date)
    {
        $date = filter_var($date,FILTER_SANITIZE_STRING);
        $isEverythingAlright = true;

        if(strlen($date) !== 10)
        {
            $isEverythingAlright = false;
            throw new \Exception("Impossible de traiter la date fournie",E_USER_WARNING);
        }
        if(strpos($date,'-') == -1)
        {
            $isEverythingAlright = false;
            throw new \Exception("Impossible de traiter la date fournie",E_USER_WARNING);
        }
        if($isEverythingAlright)
        {
            $month = date('m',strtotime($date));
            $day = date('d',strtotime($date));
            $year = date('Y',strtotime($date));

            if(checkdate($month,$day,$year))
            {
                return 1;
            }
        }
        return 0;
    }


    private function clean($string)
    {
        $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
        //$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    public function validateInformationAction()
    {


        if($this->sessionContainer->CodeProfil <> 2)
        {
            $this->redirect()->toRoute('my-profile');
        }

        $this->asideVars['menu_active'] = "kt-widget__item--active";
        $this->asideVars['current_action'] = $this->params('action');


        $this->checkAuth();
        $this->renderPage();
        $table = [
            'type_piece'=>'type_piece',
            'documents'=>'documents'
        ];


        $this->_getHelper('headScript',$this->getServiceLocator())

            ->appendFile($this->getUriPath().'assets/pages/my-profile/validate-information/validate_info.js')
            ->appendFile($this->getUriPath().'assets/pages/my-profile/validate-information/datatableFile.js')
            ->appendFile($this->getUriPath().'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')
        ;

        $join = null;
        $getTypePiece = $this->postservice->defaultSelect($table['type_piece'],[],array('statut'=>'1'),null,
            null,'all',null,'lib_type_piece ASC');


        $join [] = array(
            'table'=>array(
                't'=>'type_piece'
            ),
            'condition'=>'t.code_type_piece=d.type_document'
        );


        $getDocuments = $this->postservice->defaultSelect(array(
            'd'=>$table['documents']
        ),[
            'id_document'=>'id_document',
            'type_document'=>'type_document',
            'numero_piece'=>'numero_piece',
            'document'=>'document',
            'document_selfie'=>'document_selfie',
            'chemin_document'=>'chemin_document',
            'lock_doc'=>'lock_doc',
            'status_document'=>'statut_document'
        ],array(
            'id_user'=>intval($this->sessionContainer->IdUser)
        ),$join,null,'all',null,null);



        $messageDemande = null;

        if(isset($this->sessionContainer->MessageDemande) &&($this->sessionContainer->MessageDemande !== null || $this->sessionContainer->MessageDemande !== ''))
        {
            $messageDemande = $this->sessionContainer->MessageDemande;

            unset($this->sessionContainer->MessageDemande);
        }

        if(!empty($getSelfie) && !empty($getDocuments))
        {
            unset($this->sessionContainer->MessageDemande);
        }


        return array(
            'type_pieces'=>$getTypePiece,
            'documents'=>$getDocuments,
            'id_user'=>intval($this->sessionContainer->IdUser),
            'messageDemande'=>$messageDemande,

        );
    }

    public function mentoringAction()
    {
        $this->asideVars['current_action'] = $this->params('action');

        $this->_getHelper('headScript',$this->serviceLocator)

            ->appendFile($this->getUriPath().'assets/pages/my-profile/mentoring_script.js')
            //->appendFile($this->getUriPath().'assets/pages/my-profile/datatableFile.js')
            // ->appendFile($this->getUriPath().'assets/js/pages/crud/file-upload/ktavatar.js')
        ;
        $this->renderPage();


        $getUser = $this->postservice->defaultSelect($this->table,[],[
            'id_user'=>$this->_currentUser['id']
        ],null,null,'unique',null,null);

        $getTransactions = $this->postservice->defaultSelect('transactions',[],[
            'made_by'=>$this->_currentUser['id']
        ],null,null,'all',null,null);

        $request = $this->getRequest();

        $getFieuls = $this->postservice->defaultSelect($this->table,[],[
            'parent'=>$this->_currentUser['id']
        ],null,null,'all',null,null);
        if($request->isXmlHttpRequest())
        {
            $view = null;
            $tableJson = null;
            if($getUser['parent_key'] !== null)
            {
                $tableJson['error'] = "Vous avez déjà un lien de parrainage";

                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
                return $view;
            }

            $generateKey = $this->generateKey();

            if($generateKey !== null)
            {

                $reqUpdate = null;

                if(intval($getUser['gain'] ) !== 0 && $getUser['gain'] !== null && !empty($getUser))
                {
                    $reqUpdate = $this->postservice->defaultUpdate($this->table,[
                        'parent_key'=>$generateKey,

                    ],[
                        'id_user'=>$this->_currentUser['id']
                    ]);
                }
                else
                {

                    $gain = 5.00;
                    $reqUpdate = $this->postservice->defaultUpdate($this->table,[
                        'parent_key'=>$generateKey,
                        'gain'=>$gain
                    ],[
                        'id_user'=>$this->_currentUser['id']
                    ]);
                }

                if((bool)$reqUpdate)
                {
                    $this->sessionContainer->MessageWelcome =  "Lien de parrainage généré avec succès";
                    $tableJson['success'] = 'success';
                }
            }
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }


        return array(
            'getUser'=>$getUser,
            'nbreTransaction'=>count($getTransactions),
            'lienSite'=>$this->serviceLocator->get('Config')['lienSite'],
            'fieuls'=>$getFieuls
        );

    }


    public function addSelfieAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        $view = null;

        if($request->isXmlHttpRequest())
        {


            if(isset($_FILES['selfie']['name']) && !empty($_FILES['selfie']['name']))
            {
                $uploadDir = $this->getServiceLocator()->get('Config')['selfie_path'];
                $allowTypes = array('pdf', 'jpg', 'png', 'jpeg');

                $nameFileFolder = isset($this->sessionContainer->IdUser) && !empty($this->sessionContainer->IdUser) ? $this->sessionContainer->IdUser: "";
                $uploadStatus = null;
                $uploadedFile= $path = null;

                if(!empty($_FILES["selfie"]["name"]))
                {

                    $uploadStatus = 1;

                    $fileName = basename($_FILES["selfie"]["name"]);

                    if(!is_dir($uploadDir.$nameFileFolder))
                    {
                        mkdir($uploadDir.$nameFileFolder);
                    }

                    $targetFilePath = $uploadDir.$nameFileFolder.'/' .$fileName;
                    $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

                    if(in_array($fileType, $allowTypes))
                    {
                        // Upload file to the server

                        if($_FILES['selfie']['size'] > 5485760)
                        {
                            $tableJson['error'] = "La taille de votre fichier ne doit pas dépasser 5MB";
                        }

                    }

                    else
                    {
                        $uploadStatus = 0;
                        $tableJson['error'] = 'Désolé, seuls les fichiers PDF et images sont autorisés à être téléchargés.';
                    }


                    $checkIfDocExist = $this->postservice->defaultSelect('document_selfie',[],array(
                        'id_user'=>$this->sessionContainer->IdUser
                    ),null,null,'unique',null,null);


                    if(count($checkIfDocExist) !=0)
                    {
                        $tableJson['error'] = "Vous avez déjà un dossier constitué au niveau de l'administration de LUNICHANGE";
                    }
                    else
                    {
                        if(!file_exists($targetFilePath))
                        {

                            if(move_uploaded_file($_FILES["selfie"]["tmp_name"], $targetFilePath))
                            {
                                $uploadedFile = $fileName;
                                $path = $nameFileFolder;

                            }
                            else
                            {

                                $uploadStatus = 0;
                                $tableJson['error'] = 'Désolé, il y a eu une erreur lors du téléchargement de votre fichier.';
                            }
                        }
                        else
                        {
                            $tableJson['error'] = "Ce fichier vous appartenant existe déjà dans notre base de données";
                        }
                        if($uploadStatus == 1 && (!isset($tableJson['error']) || $tableJson['error'] == null || $tableJson['error'] === ''))
                        {
                            $redInsertDoc = $this->postservice->defaultInsert('document_selfie',array(
                                'path_doc_selfie'=>$path,
                                'document'=>filter_var($uploadedFile,FILTER_SANITIZE_STRING),

                                'id_user'=>intval($this->sessionContainer->IdUser)
                            ));

                            if($redInsertDoc)
                            {   $getDocument = $this->postservice->defaultSelect($this->postservice->getDbTables()['documents']['table'],
                                [],array(
                                    'id_user'=>$this->sessionContainer->IdUser
                                ),null,null,'unique',null,null);
                                $mailSend = $this->sendMail();

                                if($mailSend == 1)
                                {
                                    if(empty($getDocument))
                                    {
                                        $this->sessionContainer->MessageDemande = "Votre demande d'identification a été envoyé. Elle sera traitée avec le plus grand soin. Veuillez finaliser la procédure en ajoutant votre selfie";
                                    }
                                    else
                                    {
                                        $this->sessionContainer->MessageDemande = "Votre demande d'identification a été envoyé. Elle sera traitée avec le plus grand soin. Nous vous reviendrons.";

                                    }

                                    $tableJson['success'] = "Selfie ajouté avec succès";
                                }
                                else
                                {
                                    $tableJson['error'] = "Erreur au cours du traitement";
                                }


                            }
                        }

                    }


                }
                else
                {
                    $tableJson['error'] = "Veuillez renseigner le document relatif à la pièce demandée";
                }
            }
            else
            {
                $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
            }
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('my-profile');
        }

        return $view;
    }

    public function updateSelfieAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        $view = null;

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('my-profile',['action'=>'identification']);
        }
        if(!isset($_FILES['selfie']['name']) || $_FILES['selfie']['name'] == '')
        {
            $tableJson['error'] = "Impossible d'effectuer le traitement demandé";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        if(isset($this->sessionContainer->StdGetForUpdate) && !empty($this->sessionContainer->StdGetForUpdate))
        {
            $res = $this->sessionContainer->StdGetForUpdate;
            $uploadDir = $this->serviceLocator->get('Config')['document_path'];
            $allowTypes = array('pdf', 'jpg', 'png', 'jpeg');

            $nameFileFolder = $this->_currentUser['id'];
            $uploadedFile= $path = null;

            $fileName = $this->clean(basename($_FILES["selfie"]["name"]));
            if(strlen($fileName) > 40)
            {
                $tableJson['error'] = "Nom du fichier trop long. Quarante(40) caractères au maximum";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
                return $view;
            }
            if(is_dir($uploadDir.$nameFileFolder))
            {


                unlink($uploadDir.$nameFileFolder.'/'.$res['document_selfie']);

                $fileType = pathinfo($uploadDir.$nameFileFolder.'/' .$fileName, PATHINFO_EXTENSION);
                $fileName = uniqid().'_SELFIE.'.$fileType;
                $targetFilePath = $uploadDir.$nameFileFolder.'/' .$fileName;


                if(!in_array($fileType, $allowTypes))
                {
                    $tableJson['error'] = 'Désolé, seuls les fichiers images sont autorisés à être téléchargés.';
                    $view = new JsonModel($tableJson);
                    $view->setTerminal(true);
                    return $view;
                }

                $checkIfDocExist = $this->postservice->defaultSelect('documents',[],array(
                    'id_user'=>$this->_currentUser['id']
                ),null,null,'unique',null,null);

                if(count($checkIfDocExist) ==0)
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement";

                    $view = new JsonModel($tableJson);
                    $view->setTerminal(true);
                    return $view;
                }
                if(!file_exists($targetFilePath))
                {

                    if(move_uploaded_file($_FILES["selfie"]["tmp_name"], $targetFilePath))
                    {
                        $uploadedFile = filter_var($fileName,FILTER_SANITIZE_STRING);
                        $redUpdateDoc = $this->postservice->defaultUpdate('documents',array(

                            'document_selfie'=> filter_var($uploadedFile,FILTER_SANITIZE_STRING),
                            'publish_date'=> date('Y-m-d H:i:s')

                        ),array(
                            'id_document'=>intval($res['id_document'])
                        ));

                        if($redUpdateDoc)
                        {
                            // $this->sessionContainer->MessageDemande = "Votre demande d'identification a été envoyé. Elle sera traitée avec le plus grand soin. Nous vous reviendrons.";
                            unset($this->sessionContainer->StdGetForUpdate);

                            $mailSend = $this->sendMail('update');
                            if($mailSend)
                            {
                                $tableJson['success'] = "Informations modifiées";
                                $this->sessionContainer->MessageWelcome = $tableJson['success'];
                                $view = new JsonModel($tableJson);
                                $view->setTerminal(true);

                                return $view;
                            }



                        }

                    }

                }
                else
                {
                    $tableJson['error'] = "Ce fichier vous appartenant existe déjà dans notre base de données";
                }
            }
        }
        $view = new JsonModel($tableJson);
        return $view;
    }


    public function updatePasswordAction()
    {
        $request = $this->getRequest();

        $view = null;

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('my-profile');
        }

        $data = $request->getPost()->toArray();
        $tableJson = null;


        foreach($data as $kk=>$values)
        {
            if($values !== '')
            {
                $data[$kk] = filter_var($values,FILTER_SANITIZE_STRING);
            }
            if($data[$kk] == '' || empty($data[$kk]))
            {

                $tableJson['error'] = 'Veuillez renseigner tous les champs';
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
        }

        $getPassword = $this->postservice->defaultSelect($this->postservice->getDbTables()['customers']['table'],[],['id_user'=>$this->_currentUser['id']],null,null,'unique',null,null);

        if(!password_verify($data['password'],$getPassword['password']))
        {
            $tableJson['error'] = "Mot de passe actuel erroné";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        else
        {
            if(strlen($data['rpassword']) < 6)
            {
                $tableJson['error'] = "Le nouveau mot de passe doit faire au moins six(06) caractères.";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            if($data['rpassword'] !== $data['confirm_password'])
            {
                $tableJson['error'] = 'Les mots de passes doivent être identiques.';
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            if(!$this->password_strength($data['confirm_password']))
            {
                $tableJson['error'] = WebServiceDbSqlMapper::MESSAGE_FOR_PASSWORD_ERROR;
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            if($data['confirm_password'] === $getPassword['nom_complet'] || $data['confirm_password'] === $getPassword["email"])
            {
                $tableJson['error'] = "Votre mot de passe n'est pas sûr. Veuillez en choisir un autre pour plus de sécurité.";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            if(password_verify($data['confirm_password'],$getPassword['password']))
            {
                $tableJson['error'] = "Le nouveau mot de passe doit être différent de l'ancien";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;

            }
            $data['password'] = password_hash($data['confirm_password'],PASSWORD_DEFAULT);
            unset($data['confirm_password']);
            unset($data['rpassword']);
            $reqUpdate = $this->postservice->defaultUpdate($this->postservice->getDbTables()['customers']['table'],$data,[
                'id_user'=>$this->_currentUser['id']
            ]);
            if((bool)$reqUpdate)
            {
                unset($tableJson['error']);
                $tableJson['success'] = "Le mot de passe a été modifié avec succès.";
                $this->sessionContainer->MessageWelcome = $tableJson['success'];
            }
        }




        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;

    }

    public function password_strength($password)
    {
        $password_length = 6;

        $returnVal = True;

        if ( strlen($password) < $password_length ) {
            $returnVal = False;
        }

        if ( !preg_match("#[0-9]+#", $password) ) {
            $returnVal = False;
        }

        if ( !preg_match("#[a-z]+#", $password) ) {
            $returnVal = False;
        }

        return $returnVal;

    }

    private function generateKey()
    {

        $generateMin = $this->random(mt_rand(1,3));

        $getCustomers = $this->postservice->defaultSelect($this->table,[],[
            'code_profil'=>'2'
        ],null,null,'all',null,null);

        $bornMax = count($getCustomers);

        $firstStep = mt_rand($generateMin,$bornMax);

        $key = null;
        do{
            $key = $this->random(strlen(filter_var($firstStep,FILTER_SANITIZE_STRING)));
        }while((bool)$this->postservice->defaultSelect($this->table,[],['parent_key'=>$key],null,null,'unique',null,null));


        //die(var_dump($key));

        return intval($key);
    }

    public function random($car)
    {
        $string = "";
        $chaine = "1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    public function checkAuth()
    {
        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser == NULL)
        {
            return $this->redirect()->toRoute('logout');
        }
        return 0;
    }

    public function renderPage()
    {

        $this->checkAuth();
        //header layout
        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= $this->_currentUser['username'];
        $this->menuVars['user_id']= $this->_currentUser['id'];

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_STRING));

        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }

        $this->asideVars['user_info'] = $user_values;
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //aside layout
        $this->asideView = new ViewModel();
        $this->asideView->setVariables($this->asideVars);
        $this->asideView->setTemplate('layout/aside');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->addChild($this->asideView, 'aside');
        $this->layout->setVariables($this->contentVars);
        $this->layout->setTemplate('layout/my_profile');


    }

    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->table),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }

    private function getAllUsers($columns,$hayshack,$joins,$limit,$grpe,$sort)
    {
        return $this->postservice->defaultSelect($this->table,$columns,$hayshack,$joins,$limit,'all',$grpe,$sort);
    }

    private function getProfile($profil)
    {
        return $this->postservice->defaultSelect("profil_user",array(),array('code_profil'=>$profil),null,null,'unique',null,null);
    }

    private function getAllProfiles($columns,$hayshack,$joins,$limit,$grpe,$sort)
    {
        return $this->postservice->defaultSelect('profil_user',$columns,$hayshack,$joins,$limit,'all',$grpe,$sort);
    }



    protected function _getHelper($helper, $serviceLocator)
    {
        return $this->serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function returnHost()
    {
        $url = $this->getRequest()->getHeader('Referer')->getUri();
        $shema = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $querry = parse_url($url, PHP_URL_QUERY);
        $data = [];
        $data['host'] = $shema . "://" . $host;
        $data['querry'] = $querry;

        return $data;
    }


    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Laminas\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }



    public function sendMail($action = '')
    {
        $getAdmins = $this->postservice->getAdminUsers($this->sessionContainer->CodeProfil);


        $mailSend = 0;
        if(!empty($getAdmins))
        {
            if($action == 'update')
            {

                $subject = 'Demande de vérification d\'identité modifié<br>';
                $message = 'Une demande de vérification d\'identité à été envoyé par :'.$this->sessionContainer->UserName.' | Mail:'.$this->sessionContainer->Login;


            }
            else
            {
                $subject = 'Demande de vérification d\'identité envoyée<br>';
                $message = 'Une demande de vérification d\'identité à été envoyé par :'.$this->sessionContainer->UserName.' | Mail:'.$this->sessionContainer->Login;

            }



            $this->postservice->sendMessageTelegram($subject.' '.$message);
            $mailSend = 1;


        }



        return $mailSend;
    }
    public function sendMessageTelegram($message)
    {

        $telegrambot = "1477791644:AAGnbXBPsnBZ135rKH4kry7elLA0bYl60Ww";
        $telegramchatid = "-1001408210866";
        $url='https://api.telegram.org/bot'.$telegrambot.'/sendMessage';$data=array('chat_id'=>$telegramchatid,'text'=>$message);
        $options=array('http'=>array('method'=>'POST','header'=>"Content-Type:application/x-www-form-urlencoded\r\n",'content'=>http_build_query($data),),);
        $context=stream_context_create($options);
        $result=file_get_contents($url,false,$context);

        return $result;
    }
}