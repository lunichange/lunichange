<?php

namespace Dashboard\Controller;


use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;
class WebServicesController extends AbstractActionController
{
    private $currentUser = array();
    protected $postService;


    protected $sessionManager;
    protected $sessionContainer;
    protected $serviceLocator;

    public function __construct(PostServiceInterface $postService,$container)
    {
        $this->postService = $postService;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser !== null)
        {
            $this->currentUser = array(
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>$this->sessionContainer->CodeProfil,
                'username'=>$this->sessionContainer->Login,
                'fullname'=>isset($this->sessionContainer->UserName) && $this->sessionContainer->UserName !== null ? filter_var($this->sessionContainer->UserName,FILTER_SANITIZE_STRING) : null,
                'timeStamp'=>isset($this->sessionContainer->LastLoginTimeStamp) && $this->sessionContainer->LastLoginTimeStamp !== null ? $this->sessionContainer->LastLoginTimeStamp : null
            );
        }
    }

    public function showNotificationAction()
    {
        $request = $this->getRequest();
        $view =  null;
        $tableJson = array();

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('dashboard',['action'=>'tableau-de-bord']);
        }
        $tableJson['error'] = true;
        if(isset($this->sessionContainer->MessageWelcome) && !empty($this->sessionContainer->MessageWelcome))
        {
            $tableJson['success'] = $this->sessionContainer->MessageWelcome ;
            $tableJson['error'] = false;

        }

        unset($this->sessionContainer->MessageWelcome);
        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }

    public function getRequestsAction($hayshack = null)
    {
        $request = $this->getRequest();
        $getTable = $this->params ()->fromQuery ('table');
        $getTypeRequest = $this->params()->fromQuery('type');

        $datas = $request->getPost()->toArray();
        $response = null;
        $join = null;
        $cond = array();
        $sort = null;

        if($request->isXmlHttpRequest())
        {

            $allDatas = null;
            if($getTable === 'documents')
            {
                $sort = 'statut_document ASC';
            }
            if(isset($datas['pagination']))
            {
                if($datas['pagination']['perpage'] !== null && intval($datas['pagination']['perpage']) < 20)
                {
                    $datas['pagination']['perpage'] = 20;
                }
                else
                {
                    $datas['pagination']['perpage'] = intval($datas['pagination']['perpage']);
                }
            }


            if(isset($_GET['join'],$_GET['cond_join'],$_GET['alias']) && $_GET['join'] !== null)
            {

                $getIfJoinTable = strpos($_GET['join'],'-');
                $getIfCondTable = strpos($_GET['cond_join'],'-');
                $getIfCond = strpos($_GET['cond'],'-');

                $joinTable = array();

                if($getIfJoinTable !== false)
                {
                    $getJoinTable = explode('-',filter_var($_GET['join'],FILTER_SANITIZE_STRING));
                    foreach($getJoinTable as $keys=>$values)
                    {
                        $getJoin = explode(':',$values);


                        $joinTable['alias'][] = $getJoin[0];
                        $joinTable['table'][] = $getJoin[1];

                    }
                }
                else
                {
                    $getJoin = explode(':',$_GET['join']);


                    $joinTable['alias'] = $getJoin[0];

                    $joinTable['table'] = $getJoin[1];



                }

                if($getIfCondTable !== false)
                {
                    $getCondTable = explode('-',$_GET['cond_join']);
                    $join = array();
                    foreach ($getCondTable as $keys=>$elements)
                    {
                        $getCond = explode(':',$elements);

                        for($k=0;$k<count($joinTable['alias']);$k++)
                        {
                            if($keys === $k)
                            {
                                array_push($join,array(
                                    'table'=>array_unique(array($joinTable['alias'][$k]=>$joinTable['table'][$k])),
                                    'condition'=>$getCond[0].'='.$getCond[1]

                                ));
                            }

                        }


                    }


                }
                else
                {
                    $getCond = explode(':',$_GET['cond_join']);

                    $join[] = array(
                        'table'=>array(
                            $joinTable['alias']=>$joinTable['table']
                        ),
                        'condition'=>$getCond[0].'='.$getCond[1]);


                }

                if($getIfCond !== false)
                {
                    foreach($getIfCond as $keys=>$values)
                    {
                        $getCond= explode(':',$values);


                        array_push($cond,array(
                            $getCond[0]=>$getCond[1]
                        ));

                    }
                }
                else
                {
                    $getCond = explode(':',$_GET['cond']);


                    array_push($cond,array(
                        $getCond[0]=>$getCond[1]
                    ));


                }

                $countRow = count($this->postService->defaultSelect(array(
                    $_GET['alias']=>$getTable),[],$cond,null,null,filter_var($getTypeRequest,FILTER_SANITIZE_STRING),null,null));
                $offset = (intval($datas['pagination']['page'])-1) * intval($datas['pagination']['perpage']);

                $limit['end'] = $offset;
                $limit['start'] = $datas['pagination']['perpage'];


                $total_pages = ceil($countRow / intval($datas['pagination']['perpage']) );



                $reqResult = $this->postService->defaultSelect(array(
                    $_GET['alias']=>$getTable),[],$cond,$join,$limit,filter_var($getTypeRequest,FILTER_SANITIZE_STRING),null,$sort);





                $allDatas = array(
                    'meta'=>array(
                        'page'=> isset($datas['pagination']['page']) && !empty($datas['pagination']['page']) ? intval($datas['pagination']['page']): 1,
                        "pages"=> $total_pages,
                        "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 5,
                        "total"=> $countRow,
                        "sort"=> "asc",
                    ),
                    'data'=>$reqResult
                );

            }



            $response = new JsonModel($allDatas);
            $response->setTerminal(true);


        }
        elseif($request->isPost())
        {
            if($hayshack !== null && is_array($hayshack))
            {
                return $response = array();
            }
        }

        return $response;
    }

    public function fedapayTransactionAction()
    {
        $request = $this->getRequest();
        $view = null;

        if(!$request->isXmlHttpRequest())
        {
            $view = new JsonModel([
                'error'=>"Impossible d'effectuer le traitement demandé"
            ]);
            $view->setTerminal(true);

            return $view;
        }
        $datas = $request->getPost()->toArray();
        $tableJson = array();

        if(!in_array($datas['devise_source']['code_devise'],$this->serviceLocator->get('Config')['devises-fedapay']))
        {
            $view = new JsonModel([
                'error'=>"Impossible d'effectuer le traitement demandé"
            ]);
            $view->setTerminal(true);

            return $view;
        }


        \FedaPay\FedaPay::setApiKey($this->getFedaPayKey($this->serviceLocator->get('Config')['fedapayEnvironment']));
        \FedaPay\FedaPay::setEnvironment($this->serviceLocator->get('Config')['fedapayEnvironment']);


        $transaction = \FedaPay\Transaction::create([

            'description'=>'Transaction de: '.$datas['userDatas']['nom_complet'].'. Achat de '.$datas['devise_destination']['lib_devise'],
            'amount'=>floatval($datas['quantite_a_convertir']),
            'currency'=>[
                'iso'=>'XOF'
            ],
            "callback_url"=>$this->serviceLocator->get('Config')['lienSite'].'luni-api/check-fp-status-transaction',
            'customer'=>array(
                'firstname'=>isset($datas['userDatas']['prenom']) && $datas['userDatas']['prenom'] !== null && !empty($datas['userDatas']['prenom']) ? $datas['userDatas']['prenom'] : "",
                'lastname'=>isset($datas['userDatas']['nom']) && !empty($datas['userDatas']['nom']) && $datas['userDatas']['nom']!==null ? $datas['userDatas']['nom'] : $datas['userDatas']['nom_complet'],
                'email'=>$datas['userDatas']['email']
               /* 'phone_number'=>[
                   // 'number'=>'+'.$datas['userDatas']['dial_code'].$datas['userDatas']['telephone'],
                   // 'number'=>'+22966000001',
                    //'country'=>'bj'
                ]*/
            )

        ]);
       // die(var_dump($transaction));

        $this->sessionContainer->TransactionFedaPay = array(
            'ID'=>$transaction['id']
        );

        if(!empty($transaction))
        {
            
            $token =  $transaction->generateToken();
            $this->sessionContainer->TransactionDatas = $datas;
            $tableJson['success'] = $token->url;
        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }
    public function checkFpStatusTransactionAction()
    {

        $tableJson = [];
        $request = $this->getRequest();

       /* */
        if(!isset($this->sessionContainer->TransactionFedaPay) || empty($this->sessionContainer->TransactionFedaPay['ID']))
        {
            $view = new JsonModel([
                'error'=>"Une erreur s'est produite au cours du traitement."
            ]);
            $view->setTerminal(true);
            return $view;
        }
        $id = intval($this->sessionContainer->TransactionFedaPay['ID']);


        \FedaPay\FedaPay::setApiKey($this->getFedaPayKey($this->serviceLocator->get('Config')['fedapayEnvironment']));
        \FedaPay\FedaPay::setEnvironment($this->serviceLocator->get('Config')['fedapayEnvironment']);

        $transaction = \FedaPay\Transaction::retrieve($id);

        if(!$request->isXmlHttpRequest())
        {

            echo '<h1 style="text-align: center !important; margin-top: 50px !important">Vérification en cours ...</h1>';

            if($transaction->status === 'approved')
            {

               return  $this->redirect()->toRoute('customer',[
                   'action'=>'finish-transaction'
               ]);
            }
            else
            {
                echo 'La transaction n\'a pas pu aboutir';

                sleep(5);
                return $this->redirect()->toRoute('customer');


            }

        }

        if($request->isXmlHttpRequest())
        {
            if($transaction->status === 'approved')
                $tableJson['success'] = "success";
            elseif($transaction->status === 'pending')
            {
                $tableJson['pending'] = 'pending';
            }
            else
                $tableJson['error'] = "La transaction n'a pas pu aboutir";


            // unset($this->sessionContainer->TransactionFedaPay);

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);


            return $view;
        }
      return $this->getResponse();
    }

    private function getFedaPayKey($environment,$typeKey= 'secret')
    {

        if($typeKey !== 'secret')
        {
            $key = $this->serviceLocator->get('Config')['fedapayTestKey'];
            if($environment === 'live')
            {
                $key = $this->serviceLocator->get('Config')['fedapayLiveKey'];
            }

            return $key;
        }

        $key = $this->serviceLocator->get('Config')['fedaPaySecretKeys']['sandbox'];

        if($environment === 'live')
        {
            $key = $this->serviceLocator->get('Config')['fedaPaySecretKeys']['live'];
        }


        return $key;
    }


    public function checkProfilStatusAction()
    {

        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('dashboard');
        }
        if(isset($this->currentUser['codeProfile']) && $this->currentUser['codeProfile'] !== null)
        {
            if($this->currentUser['codeProfile'] !== '2')
            {
                echo 2;
                return $this->getResponse();
            }
            $getUserData = $this->postService->defaultSelect('luni_users',[],array('id_user'=>$this->sessionContainer->IdUser),null,null,'unique',null,null);

            if(!(bool)$getUserData)
            {
                echo 0;
                return $this->getResponse();
            }
            if($getUserData['telephone'] == null || $getUserData['profil_complet'] == '0')
            {
                echo 1;
            }
            else
            {
                echo 0;
            }
        }


      return $this->getResponse();
    }

    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        $datas = $request->getPost()->toArray();
        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('dashboard');
        }
        $dataGetTable = $this->params()->fromQuery('table');
        $dataGetType = $this->params()->fromQuery('type');
        $dataGetOrder = null;
        $dataConditions = [];
        if($this->params()->fromQuery('order') !== null || isset($_GET['order']))
        {
            $dataGetOrder = $this->params()->fromQuery('order');
        }
        if(!empty($datas))
        {
            if(isset($datas['update'],$datas['column']))
            {
                foreach($datas['column'] as $keys=>$values)
                {
                    $dataConditions[$keys] = $values;
                }
            }
            else
            {
                foreach ($datas as $keys=>$values)
                {

                    $dataConditions[$keys] = $values;
                }
            }

        }
        $request = $this->postService->defaultSelect ($this->postService->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
            $dataGetType,null,$dataGetOrder);
        if(isset($datas['update'], $datas['column']))
        {
            $this->postService->defaultUpdate($dataGetTable,array(
                $datas['update']['column']=>$datas['update']['value']
            ),$dataConditions);
        }

        if(!(bool)$request)
        {
            $tableJson['error'] = "Une erreur s'est produite au cours du traitement.";

            $view = new JsonModel($tableJson);
            return $view;
        }
        if((bool)$request)
        {
            $tableJson = $request;
            $this->sessionContainer->StdGetForUpdate = $request;
        }


        $view = new JsonModel($tableJson);
        $view->setTerminal (true);

      return $view;
    }


}