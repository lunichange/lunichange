<?php


namespace Dashboard\Controller;

use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
class CurrenciesManagementController extends AbstractActionController
{
    protected $postservice;

    protected $loginTable;
    protected $paysTable;
    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;



    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;

    protected $table;
    protected $serviceLocator;
    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();
        $this->table = 'luni_monnaies';


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');



        //footerView
        //$this->footerView = new ViewModel();
        // $this->footerView->setTemplate('layout/footer');

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }
    public function indexAction()
    {

        $this->checkAuth ();
        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')

            ->appendFile($this->getUriPath().'assets/plugins/sweet-alert/sweetalert.min.js')
            ->appendFile($this->getUriPath().'assets/plugins/sweet-alert/app.js')
            ->prependFile($this->getUriPath() . 'assets/plugins/datatables/datatables.bundle.js')
            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/datatables/search-options/advanced-search_currencies-management_new.js?'.uniqid())
            ->appendFile($this->getUriPath() . 'assets/pages/currencies-management/ajax_form_script_new.js')
            ->appendFile($this->getUriPath() . 'assets/pages/currencies-management/custom_script_new.js')

            //->appendFile($this->getUriPath() . '')

            ->appendFile($this->getUriPath() . 'assets/pages/global/Helpers.js')

        ;
        $this->renderPage ();
        $messagesNotification = array();

        if(isset($_POST['kt_btn_currencies_add_form']))
        {
            $messagesNotification = $this->addCurrencies ();
        }

        //$messagesNotification = array();

        return array(
            'monnaies'=>$this->getAll('luni_monnaies',array(),array('statut'=>'1'),null,null,null,'libelle_famille ASC'),
            'categories_devises'=>$this->getAll ('categorie_devise',array(),array('statut'=>'1'),null,null,null,'lib_categorie_devise ASC'),
            'pays'=>$this->postservice->defaultSelect("pays",[],[],null,null,'all',null, 'lib_pays ASC'),

            'messageNotification'=>$messagesNotification
        );
    }

    public function updateLogoAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();


        if($request->isXmlHttpRequest())
        {

            $getDataId = $this->params ()->fromQuery ('id_devise');


            $check = getimagesize($_FILES["file"]["tmp_name"]);
            $uploadOk = true;
            /***
             * Online folder
             * /home/u622323772/domains/lunichange.com/public_html/img/dashboard/devises/
             */
            $target_dir = $this->serviceLocator->get('Config')['devisesPath'];
            $target_file = $target_dir . basename($_FILES["file"]["name"]);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $fileTmpPath = $_FILES['file']['tmp_name'];
            $fileName = $_FILES['file']['name'];
            $new_width = 225;
            $new_height = 225;
            $sourceProperties = getimagesize($fileTmpPath);
            $resizeFileName = time();
            //$fileExt = pathinfo($_FILES['upload_image']['name'], PATHINFO_EXTENSION);
            $uploadImageType = $sourceProperties[2];
            $sourceImageWidth = $sourceProperties[0];
            $sourceImageHeight = $sourceProperties[1];


            $uploadOk = true;

            if($check !== false) {


                $uploadOk = true;
            }
            else
            {
                $tableJson['error'] = "Le fichier choisi n'est pas une image";

                $uploadOk = false;
            }

            if ($_FILES["file"]["size"] > 1000000) {
                $tableJson['error'] = "La taille du fichier est trop grande.";
                $uploadOk = false;
            }
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                $tableJson['error'] = "Seuls les fichiers JPG,JPEG,PNG et GIF sont autorisées";
                $uploadOk = false;
            }

            if(!isset($tableJson['error']) || empty($tableJson['error']))
            {
                switch ($uploadImageType) {
                    case IMAGETYPE_JPEG:
                        $resourceType = imagecreatefromjpeg($fileTmpPath);
                        $imageLayer = $this->resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
                        imagejpeg($imageLayer,$target_dir."thump_".$resizeFileName.'.'. $imageFileType);
                        break;

                    case IMAGETYPE_GIF:
                        $resourceType = imagecreatefromgif($fileTmpPath);
                        $imageLayer = $this->resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
                        imagegif($imageLayer,$target_dir."thump_".$resizeFileName.'.'. $imageFileType);
                        break;

                    case IMAGETYPE_PNG:
                        $resourceType = imagecreatefrompng($fileTmpPath);
                        $imageLayer = $this->resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
                        imagepng($imageLayer,$target_dir."thump_".$resizeFileName.'.'. $imageFileType);
                        break;

                    default:
                        $imageProcess = 0;
                        break;
                }
                $datasToUpdate = array();

                if(move_uploaded_file($fileTmpPath, $target_dir. $resizeFileName. ".". $imageFileType))
                {
                    $logoDevise =$this->convertImageToBase64($target_dir. $resizeFileName. ".". $imageFileType,$imageFileType);
                    $imageProcess = 1;

                    $reqUpdate = $this->postservice->defaultUpdate($this->postservice->getDbTables()['devises']['table'],array('image_devise'=>$logoDevise),array('id_devise'=>$getDataId));

                    if($reqUpdate)
                        $tableJson['success'] = "Enregistrement réussi.<br/>Veuillez ensuite faire le paramétrage concernant le type d'adresse de réception du client.";
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }

        return $view;
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }

    public function listCurrenciesAction()
    {
        $request = $this->getRequest ();
        $view = null;
        $limit = null;
        $order = null;

        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $arrayConditions = array();

            $joins [] = array(
                'table'=>array('m'=>$this->postservice->getDbTables ()['luni_monnaies']['table']),
                'condition'=>'m.'.$this->postservice->getDbTables ()['luni_monnaies']['columns']['id'].'=d.'.
                    $this->postservice->getDbTables ()['devises']['columns']['famille_devise']
            );

            $joins [] = array(
                'table'=>array('c'=>$this->postservice->getDbTables ()['categorie_devise']['table']),
                'condition'=>'c.'.$this->postservice->getDbTables ()['categorie_devise']['columns']['id_cat_devise'].'=d.'.
                    $this->postservice->getDbTables ()['devises']['columns']['cat_devise']
            );

            $arrayColumns = array(
                'id_devise'=>'id_devise',
                'libelle_devise'=>'lib_devise',
                'img_devise'=>'image_devise',
                'solde'=>'solde',
                'solde_min'=>'solde_minimum',
                'eligible_for_payment_method'=>'eligible_for_payment_method',
                'status'=>'statut'
            );
            if($data['length'] !== -1)
            {
                $limit['start'] = $data['length'];
                $limit['end'] = $data['start'];
            }
            if(isset($data['columns']) && !empty($data['columns']))
            {

                if(!empty($data['columns'][0]['search']['value']))
                {

                    array_push ($arrayConditions,array(
                        'lib_categorie_devise'=>$data['columns'][0]['search']['value']
                    ));
                }

                elseif(!empty($data['columns'][1]['search']['value']))
                {
                    array_push ($arrayConditions,array(
                        'libelle_famille '=>$data['columns'][1]['search']['value']
                    ));
                }
                elseif(!empty($data['columns'][2]['search']['value']))
                {
                    array_push ($arrayConditions,array(
                        'd.statut'=>$data['columns'][2]['search']['value']
                    ));
                }
                elseif(!empty($data['columns'][3]['search']['value']))
                {
                    array_push ($arrayConditions,array(
                        'lib_devise LIKE ?'=>'%'.$data['columns'][3]['search']['value'].'%'
                    ));
                }

            }


            $myRequest = $this->getAllDevises ($arrayColumns,$arrayConditions,$joins,$limit,null,'libelle_famille ASC, libelle_devise ASC');


            $countRow = $this->getAllDevises ($arrayColumns,$arrayConditions,$joins,array (),null,'libelle_famille ASC, libelle_devise ASC, libelle_devise ASC');

            $view = new JsonModel(
                [
                    "iTotalRecords"=>count($myRequest),
                    "iTotalDisplayRecords"=>count($countRow),
                    "sEcho"=>intval($data['draw']),
                    "sColumns"=> "",
                    "aaData"=>$myRequest

                ]
            );
            //$view = new JsonModel($data);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }

        return $view;
    }

    public function addCurrenciesAction()
    {
        $request = $this->getRequest ();
        $view = null;
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();

            $view = new JsonModel($_FILES["fileToUpload"]["tmp_name"]);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }
        return $view;
    }

    public function updateCurrenciesAction()
    {
        $request= $this->getRequest();
        $view = null;
        if($request->isXmlHttpRequest())
        {
            $datas = $request->getPost()->toArray();
            $checkDataEmpty = false;
            $messagesForm = array();
            $dispo_source = '0' ;
            $dispo_destination = '0';

            if(isset($datas['edit_libelle_devise'],$datas['edit_solde_minimum'],$datas['edit_solde'],$datas['edit_categorie_devise']))
            {

                foreach ($datas as $keys=>$values)
                {
                    if(empty($datas[$keys]))
                    {
                        $messagesForm['error'] = "Veuillez renseigner tous les champs";
                        $checkDataEmpty = true;
                        break;
                    }
                    else
                    {
                        $checkDataEmpty = false;
                        break;
                    }
                }

                if(isset($datas['lib_devise_edit']) && strlen($datas['lib_devise_edit']) < 3)
                {
                    $messagesForm['error'] = 'Veuillez vérifier le libellé de la devise.';
                }
                if(isset($datas['edit_solde_minimum'],$datas['edit_solde']) && ($datas['edit_solde'] <= 0 || $datas['edit_solde_minimum'] <= 0))
                {
                    $messagesForm['error'] = "Veuillez vérifier les soldes.";
                }
                if(isset($datas['edit_solde_minimum'],$datas['edit_solde']) && $datas['edit_solde_minimum'] > $datas['edit_solde'])
                {
                    $messagesForm['error'] = "Le solde minimum ne peut pas être supérieur au solde actuel";
                }
                if(isset($datas['edit_dispo_source']))
                {


                    if(!$datas['edit_dispo_source'])
                    {
                        $dispo_source = '0';
                    }
                    else
                    {
                        $dispo_source = '1';
                    }

                }
                if(isset($datas['edit_dispo_destination']))
                {


                    if(!$datas['edit_dispo_destination'])
                    {
                        $dispo_destination = '0';
                    }
                    else
                    {
                        $dispo_destination = '1';
                    }

                }
                if(!isset($datas['edit_dispo_source']) && !isset($datas['edit_dispo_destination']))
                {
                    $messagesForm['error'] = "Veuillez faire un choix sur la disponibilité de la devise.";
                }


                if($checkDataEmpty == false && (!isset($messagesForm['error']) || empty($messagesForm['error'])))
                {
                    $id_devise=null;
                    $datasToInsert = array();
                    if(isset($datas['hidden_id_devise']))
                    {
                        $id_devise = intval($datas['hidden_id_devise']);
                    }


                    $datasToInsert['categorie_devise'] = filter_var(intval($datas['edit_categorie_devise']), FILTER_SANITIZE_NUMBER_INT);
                    $datasToInsert['lib_devise'] = filter_var(ltrim($datas['edit_libelle_devise']), FILTER_SANITIZE_STRING);
                    $datasToInsert['solde'] = filter_var(ltrim($datas['edit_solde']),FILTER_SANITIZE_NUMBER_FLOAT);
                    $datasToInsert['solde_minimum'] = filter_var(ltrim($datas['edit_solde_minimum']),FILTER_SANITIZE_NUMBER_FLOAT);
                    $datasToInsert['famille_devise'] = intval($datas['edit_famille_devise']);
                    $datasToInsert['dispo_source'] = $dispo_source;
                    $datasToInsert['dispo_destination'] = $dispo_destination;


                    $this->postservice->defaultUpdate($this->postservice->getDbTables()['devises']['table'],$datasToInsert,array(
                        'id_devise'=>$id_devise
                    ));
                    $messagesForm['success'] = "Modification effectuée.";

                }
            }
            else
            {
                $messagesForm['error'] = "Impossible de traiter ces informations";
            }

            $view = new JsonModel($messagesForm);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }

        return $view;
    }

    private function addCurrencies()
    {
        $request = $this->getRequest();
        $messagesForm = array();
        $datas = [];

        if($request->isPost())
        {

            $datas = $request->getPost()->toArray();
            /*var_dump ($datas);
            die();*/

            $checkDataEmpty = false;
            $dispo_source = 0 ;
            $dispo_destination = 0;

            if(isset($datas['solde'],$datas['solde_minimum'],$datas['libelle_devise'],$datas['categorie_devise']))
            {
                if(!empty($_FILES["fileToUpload"]["tmp_name"]))
                {
                    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                    /***
                     * Online folder
                     * /home/u622323772/domains/lunichange.com/public_html/img/dashboard/devises/
                     */
                    $target_dir = $this->serviceLocator->get('Config')['devisesPath'];
                    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                    $fileTmpPath = $_FILES['fileToUpload']['tmp_name'];
                    $fileName = $_FILES['fileToUpload']['name'];
                    $new_width = 200;
                    $new_height = 200;
                    $sourceProperties = getimagesize($fileTmpPath);
                    $resizeFileName = time();
                    //$fileExt = pathinfo($_FILES['upload_image']['name'], PATHINFO_EXTENSION);
                    $uploadImageType = $sourceProperties[2];
                    $sourceImageWidth = $sourceProperties[0];
                    $sourceImageHeight = $sourceProperties[1];


                    $uploadOk = true;
                    foreach ($datas as $keys=>$values)
                    {
                        if(empty($datas[$keys]))
                        {
                            $messagesForm['error'] = "Veuillez remplir tous les champs";
                            $checkDataEmpty = true;
                            break;
                        }
                        else
                        {
                            $checkDataEmpty = false;
                            break;
                        }
                    }

                    if(!is_numeric($datas['solde']) || !is_numeric($datas['solde_minimum']))
                    {
                        $messagesForm['error'] = "Veuillez vérifier le format des soldes.";
                    }

                    if($datas['solde_minimum'] > $datas['solde'])
                    {
                        $messagesForm['error'] = "Le solde minimum doit être inférieur ou égal au solde de démarrage.";
                    }


                    if($check !== false) {


                        $uploadOk = true;
                    }
                    else
                    {
                        $messagesForm['error'] = "Le fichier choisi n'est pas une image";

                        $uploadOk = false;
                    }

                    if ($_FILES["fileToUpload"]["size"] > 1000000) {
                        $messagesForm['error'] = "La taille du fichier est trop grande.";
                        $uploadOk = false;
                    }
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                        $messagesForm['error'] = "Seuls les fichiers JPG,JPEG,PNG et GIF sont autorisées";
                        $uploadOk = false;
                    }

                    if(isset($datas['dispo_source']))
                    {


                        if(!$datas['dispo_source'])
                        {
                            $dispo_source = 0;
                        }
                        else
                        {
                            $dispo_source = 1;
                        }

                    }
                    if(isset($datas['dispo_destination']))
                    {


                        if(!$datas['dispo_destination'])
                        {
                            $dispo_destination = 0;
                        }
                        else
                        {
                            $dispo_destination = 1;
                        }

                    }
                    if(!isset($datas['dispo_source']) && !isset($datas['dispo_destination']))
                    {
                        $messagesForm['error'] = "Veuillez faire un choix sur la disponibilité de la devise.";
                    }

                    if($checkDataEmpty == false && $uploadOk == true)
                    {
                        if($this->getOneDevise(['lib_devise LIKE ?'=>$datas['libelle_devise'].'%','categorie_devise'=>$datas['categorie_devise']]))
                        {
                            $messagesForm['error'] = "Cet enregistrement existe déjà.";
                        }
                        else
                        {
                            if(!isset($messagesForm['error']) || empty($messagesForm['error']))
                            {
                                switch ($uploadImageType) {
                                    case IMAGETYPE_JPEG:
                                        $resourceType = imagecreatefromjpeg($fileTmpPath);
                                        $imageLayer = $this->resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
                                        imagejpeg($imageLayer,$target_dir."thump_".$resizeFileName.'.'. $imageFileType);
                                        break;

                                    case IMAGETYPE_GIF:
                                        $resourceType = imagecreatefromgif($fileTmpPath);
                                        $imageLayer = $this->resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
                                        imagegif($imageLayer,$target_dir."thump_".$resizeFileName.'.'. $imageFileType);
                                        break;

                                    case IMAGETYPE_PNG:
                                        $resourceType = imagecreatefrompng($fileTmpPath);
                                        $imageLayer = $this->resizeImage($resourceType,$sourceImageWidth,$sourceImageHeight,$new_width,$new_height);
                                        imagepng($imageLayer,$target_dir."thump_".$resizeFileName.'.'. $imageFileType);
                                        break;

                                    default:
                                        $imageProcess = 0;
                                        break;
                                }
                                $datasToInsert = array();

                                if(move_uploaded_file($fileTmpPath, $target_dir. $resizeFileName. ".". $imageFileType))
                                {
                                    $datasToInsert['image_devise']=$this->convertImageToBase64($target_dir. $resizeFileName. ".". $imageFileType,$imageFileType);
                                    $imageProcess = 1;

                                    $datasToInsert['lib_devise'] = ltrim($datas['libelle_devise']);
                                    $datasToInsert['categorie_devise'] = $datas['categorie_devise'];
                                    $datasToInsert['solde'] = $datas['solde'];
                                    $datasToInsert['solde_minimum'] = $datas['solde_minimum'];
                                    $datasToInsert['famille_devise'] = intval($datas['famille_devise']);
                                    $datasToInsert['dispo_source'] = filter_var($dispo_source,FILTER_SANITIZE_STRING);
                                    $datasToInsert['dispo_destination'] = filter_var($dispo_destination,FILTER_SANITIZE_STRING);


                                    $this->postservice->defaultInsert($this->postservice->getDbTables()['devises']['table'],$datasToInsert);
                                    $messagesForm['success'] = "Enregistrement réussi.<br/>Veuillez ensuite faire le paramétrage concernant le type d'adresse de réception du client.";
                                }
                            }



                        }
                    }


                }
                else
                {
                    $messagesForm['error'] = "Veuillez choisir un fichier";
                }




                /*else
                {
                    $messagesForm['error'] = "Une erreur s'est produite au cours de l'opération.";
                }*/

            }
            else
            {
                $messagesForm['error'] = "Impossible de traiter ces informations.";
            }


        }

        return $messagesForm;
    }

    public function updateStatusCurrenciesAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {

            $getDataId = $this->params ()->fromQuery ('id');
            $getStatusValue = $this->params ()->fromQuery ('valeur');
            $checkIfDeviseExist = $this->getOneDevise(array('id_devise'=>$getDataId));
            if((bool)$checkIfDeviseExist)
            {
                $myRequest = $this->updateCurrencies(array('statut'=>$getStatusValue),array('id_devise'=>$getDataId));
                if((bool)$myRequest)
                {
                    $tableJson['success'] = "Modification effectuée.";
                }
                else
                {
                    $tableJson['error'] = "Une erreur s'est produite au cours de l'opération.";
                }
            }
            else
            {
                $tableJson['error'] = "Impossible de traiter ces informations.";
            }
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }

        return $view;
    }

    public function switchEligibilityAction()
    {
        $request = $this->getRequest();

        if(!$request->isXmlHttpRequest())
        {
            die('Request must be asynchronous');
        }
        $data = $request->getPost()->toArray();
        $tableJson = null;

        if(!isset($data['devise']))
        {
            $tableJson['error'] = "Impossible de poursuivre le traitement demandé";
        }

        $getDevise = $this->postservice->defaultSelect('devises',[],[
            'id_devise'=>intval($data['devise'])
        ],null,null,'unique',null,null);
        if(empty($getDevise))
        {
            $tableJson['error'] = "Impossible de poursuivre le traitement demandé";
        }

        if(intval($getDevise['categorie_devise']) !== 2)
        {
            $tableJson['error'] = "Cette opération n'est possible que pour les devises de type compte mobile";
        }

        if(!isset($tableJson['error']) || $tableJson['error'] == null)
        {

            $setValues = [
                'eligible_for_payment_method'=>'1'
            ];
            if($getDevise['eligible_for_payment_method'] === '1')
            {
                $setValues = [
                    'eligible_for_payment_method'=>'0'
                ];
            }

            $this->postservice->defaultUpdate('devises',$setValues,[
                'id_devise'=>$getDevise['id_devise']
            ]);
            $tableJson['success'] = "Opération effectuée avec succès";
        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);
        return $view;

    }

    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttprequest())
        {
            $datas = $request->getPost()->toArray();
            $dataGetTable = $this->params ()->fromQuery ('table');
            $dataGetType = $this->params ()->fromQuery ('type');
            $dataConditions = array();
            // $this->sessionContainer->ElementForUpdate = array();
            foreach ($datas as $keys=>$values)
            {

                $dataConditions[$keys] = $values;
            }
            $request = $this->postservice->defaultSelect($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
                $dataGetType,null,null);
            if((bool)$request)
            {
                $tableJson = $request;
                $this->sessionContainer->StdGetForUpdate = $request;
            }


            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('currencies-management');
        }

        return $view;
    }



    private function getAllDevises($columns,$hayshack,$joins,$limit,$grpe,$sort)
    {

        return $this->postservice->defaultSelect (array('d'=>$this->postservice->getDbTables ()['devises']['table']),
            $columns,$hayshack,$joins,$limit,'all',$grpe,$sort);
    }

    private function getOneDevise($hayshack)
    {
        return $this->postservice->defaultSelect ($this->postservice->getDbTables ()['devises']['table'],
            [],$hayshack,null,null,'unique',null,null);
    }

    private function getAll($table,$columns,$hayshack,$joins,$limit,$grpe,$sort)
    {
        return  $this->postservice->defaultSelect ($table,$columns,$hayshack,$joins,$limit,'all',$grpe,$sort);
    }

    private function updateCurrencies($datas,$hayshack)
    {
        $request = $this->postservice->defaultUpdate($this->postservice->getDbTables()['devises']['table'],
            $datas,$hayshack);
        if((bool)$request)
        {
            return 1;
        }
        else
            return 0;
    }
    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }
    public function renderPage()
    {

        //header layout
        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        $this->headerView = new ViewModel();

        $user_values = array();

        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT),null);
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }



    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil == 2 || $this->sessionContainer->CodeProfil === 3)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }
        /*elseif($this->sessionContainer->CodeProfil == 2)
       {
           $this->redirect()->toRoute('customer');
       }
       elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
       {
           $this->redirect()->toRoute("administration");
       }*/
        /*
        elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
        {
            $this->redirect()->toRoute('customer');
        }
        else
        {
            $this->redirect()->toRoute('administration');
        }*/
    }

    private function resizeImage($resourceType,$image_width,$image_height,$resizeWidth,$resizeHeight) {
        // $resizeWidth = 100;
        // $resizeHeight = 100;
        $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);
        imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);
        return $imageLayer;
    }

    private function convertImageToBase64($image,$imageFileType)
    {
        $image_base64 = base64_encode(file_get_contents($image));
        $image = 'data:image/'.$imageFileType.';base64,'.filter_var($image_base64,FILTER_SANITIZE_STRING);

        return $image;
    }

}