<?php
	/**
	 * Created by PhpStorm.
	 * User: carlos
	 * Date: 13-Apr-20
	 * Time: 6:10 PM
	 */
	
	namespace Dashboard\Controller;



    use Application\Mapper\WebServiceDbSqlMapper;
    use Application\Service\PostServiceInterface;

    use Laminas\Mvc\Controller\AbstractActionController;
    use Laminas\ServiceManager\ServiceManager;
    use Laminas\Uri\Uri;
    use Laminas\View\Model\JsonModel;

    use Laminas\Session\Container;
    use Laminas\View\Model\ViewModel;

    class TransactionsController extends AbstractActionController
	{
        protected $postservice;


        protected $headerView;
        protected $menuView;
        protected $footerView;
        protected $layout;
        protected $headerVars;
        protected $menuVars;

        protected $footerVars;
        protected $contentVars;




        private  $_currentUser;
        protected $sessionManager;
        protected $sessionContainer;

        protected $serviceLocator;
        protected $table;
        public function __construct(PostServiceInterface $postService,$container)
        {
            $this->postservice = $postService;
            $this->serviceLocator = $container;
            $this->sessionContainer = new Container('lunichange');
            $this->sessionManager = $this->sessionContainer->getManager();
            //Layout
            $this->contentVars = array();
            $this->table = 'transactions';


            //menu layout
            $this->menuView = new ViewModel();
            $this->menuVars = array();
            $this->menuView->setTemplate('layout/menu');


            $this->headerVars = array();
            $this->footerVars = array();
            $this->contentVars = array();

            if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== null)
            {
                $this->_currentUser = [
                    'id'=>$this->sessionContainer->IdUser,
                    'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null ? $this->sessionContainer->CodeProfil:null,
                    'username'=>isset($this->sessionContainer->Login) && $this->sessionContainer->Login !== null ? $this->sessionContainer->Login : null,
                    'fullName'=>isset($this->sessionContainer->UserName) && $this->sessionContainer->UserName ? filter_var($this->sessionContainer->UserName, FILTER_SANITIZE_STRING) : null,
                    'timeStamp'=>$this->sessionContainer->LastLoginTimeStamp,
                    'profileName'=>isset($this->sessionContainer->ProfilName) && $this->sessionContainer->ProfilName !== null ? $this->sessionContainer->ProfilName : null
                ];
            }

        }
        public function indexAction()
        {
            $javascriptImport = null;

            $this->renderPage();
            $welcomeMessage = null;
            $join = array();
            $getManagersOfTransactions = array();



            $this->_getHelper('headScript', $this->serviceLocator)

                ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')
                ->appendFile($this->getUriPath() . 'assets/pages/global/Helpers_new.js')
                ->appendFile($this->getUriPath() . 'assets/pages/transactions/custom_script.min.js')
                ->appendFile($this->getUriPath() . 'assets/plugins/sweetAlert2/sweetAlert2.min.js')
                ->appendFile($this->getUriPath() . 'assets/plugins/easytimerjs/dist/easytimer.min.js')
                ->prependFile($this->getUriPath() . 'assets/plugins/tipped/dist/js/tipped.min.js')


            ;
            if(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null)
            {
                if($this->sessionContainer->CodeProfil == 2)

                {
                    $this->_getHelper('headScript', $this->serviceLocator)
                        ->appendFile($this->getUriPath() . 'assets/plugins/datatables/datatables.bundle.js')
                        ->appendFile($this->getUriPath() . 'assets/pages/transactions/transactions_customer.js?'.uniqid());

                    // $javascriptImport = "->appendFile(".$this->getUriPath()."'assets/pages/transactions/transactions_customer.js')";
                }
                else
                {
                    $this->_getHelper('headScript', $this->serviceLocator)
                        ->prependFile($this->getUriPath() . 'assets/plugins/datatables/datatables.bundle.js')
                        ->appendFile($this->getUriPath() . 'assets/pages/transactions/transactions_admin.js?'.uniqid());
                }
            }


            if(isset($this->sessionContainer->MessageTransaction['message']) && !empty($this->sessionContainer->MessageTransaction['message']))
               {
                   $welcomeMessage = $this->sessionContainer->MessageTransaction['message'];
                   unset($this->sessionContainer->MessageTransaction);
               }
               $join [] =array('table'=>array('t'=>'transactions'),'condition'=>'t.made_by=c.id_user');

               $getTransactionsDoneBy = $this->postservice->defaultSelect('transactions',[],array('done_by<>?'=>""),null,null,'all','done_by',null);

               $getCustomersOfTransactions = $this->postservice->defaultSelect(array('c'=>'luni_users'),[],array('code_profil'=>2),$join,null,'all','c.id_user','nom_complet ASC');


                    if(!empty($getTransactionsDoneBy))
                    {
                        foreach($getTransactionsDoneBy as $elements)
                        {
                                $getManagersOfTransactions[] = $this->postservice->defaultSelect('luni_users',[],
                                    array('id_user'=>intval($elements['done_by']),'code_profil >= ?'=>intval($this->sessionContainer->CodeProfil)),
                                    null,null,'unique','id_user',null);
                        }
                    }




            return array(
                'welcomeMessage'=>$welcomeMessage,
                'customersTransactions'=>$getCustomersOfTransactions,
                'gestionnairesTransactions'=>$getManagersOfTransactions,
                'currentUser'=>$this->_currentUser

            );
        }

        public function listTransactionsAction()
        {
            $request = $this->getRequest();
            $view = null;

            if(!$request->isXmlHttpRequest())
            {
               return $this->redirect()->toRoute('transactions');
            }
                $data = $request->getPost()->toArray();



                //$arrayConditions[] = array('status_transaction<>?'=>'3');
                $arrayConditions = [];


                $myArray = array();
                $arrayColumns = array(
                    'id_transaction'=>'id_transaction',
                    'transaction_id'=>'transaction_id',
                    'context'=>'context',
                    'date_transaction'=>'date_transaction',
                    'code_transaction'=>'code_transaction',
                    'devise_source'=>'devise_source',
                    'devise_cible'=>'devise_cible',
                    'quantite_source'=>'quantite_source',
                    'quantite_cible'=>'quantite_cible',
                    'date_start_expired'=>'date_start_expired',
                    'date_end_expired'=>'date_end_expired',
                    'status_transaction'=>'status_transaction',
                    'numero_envoi'=>'numero_envoi',
                    'adresse_reception'=>'adresse_reception',
                    'done_by'=>'done_by',
                    'made_by'=>'made_by',
                    'lastName'=>'nom',
                    'firstName'=>'prenom',
                    'country'=>'pays',
                    'city'=>'ville'
                );
                if(isset($this->_currentUser['codeProfile'],$this->_currentUser['id']) && !empty($this->_currentUser['id']) && !empty($this->_currentUser['codeProfile']) && intval($this->_currentUser['codeProfile']) === 2)

                {
                    $arrayConditions[] = array('t.made_by'=>$this->_currentUser['id']
                      );

                    $arrayConditions[] = array(  't.status_transaction'=>'0');
                    if(isset($data['query'],$data['query']['columns']) && !empty($data['query']) && !empty($data['query']['columns']))
                    {



                        if(!empty($data['query']['columns'][0]['search']['value']))
                        {

                            array_push ($arrayConditions,array(
                                't.transaction_id LIKE ?'=>'%'.trim($data['query']['columns'][0]['search']['value']).'%'
                            ));
                        }

                        if(!empty($data['query']['columns'][1]['search']['value']))
                        {
                            array_push ($arrayConditions,array(
                                'code_transaction LIKE ?'=>'%'.trim($data['query']['columns'][1]['search']['value']).'%'
                            ));
                        }
                        if(!empty($data['query']['columns'][2]['search']['value']))
                        {

                            unset($arrayConditions[1]);


                            array_push ($arrayConditions,array(
                                'status_transaction'=>$data['query']['columns'][2]['search']['value']
                            ));
                        }
                        if(!empty($data['query']['columns'][3]['search']['value']))
                        {

                            $formData = $data['query']['columns'][3]['search']['value'];

                            $tableDate = explode(' - ',ltrim($formData));

                            $firstDate = str_replace('/','-',$tableDate[0]);
                            $firstDate = trim(date('Y-m-d H:i:s',strtotime($firstDate)));

                            $lastDate = str_replace('/','-',$tableDate[1]);
                            $lastDate = trim(date('Y-m-d H:i:s',strtotime($lastDate)));

                            array_push($arrayConditions,array(
                                "DATE_FORMAT(date_transaction,'%Y-%m-%d') >= ?"=>$firstDate,
                                "DATE_FORMAT(date_transaction,'%Y-%m-%d') <= ?"=>$lastDate
                            ));
                        }


                    }

                }
                else
                {

                    $arrayConditions[] = array('t.status_transaction'=>'0');
                    if(isset($data['columns']) && !empty($data['columns']))
                    {


                        if(!empty($data['columns'][0]['search']['value']))
                        {

                            array_push ($arrayConditions,array(
                                't.transaction_id LIKE ?'=>'%'.trim($data['columns'][0]['search']['value']).'%'
                            ));
                        }

                        if(!empty($data['columns'][1]['search']['value']))
                        {

                            array_push ($arrayConditions,array(
                                'code_transaction LIKE ?'=>'%'.trim($data['columns'][1]['search']['value']).'%'
                            ));
                        }
                        if(!empty($data['columns'][2]['search']['value']))
                        {

                            unset($arrayConditions[0]);

                            array_push ($arrayConditions,array(
                                't.status_transaction = ?'=>$data['columns'][2]['search']['value']
                            ));

                        }
                        if(!empty($data['columns'][3]['search']['value']))
                        {

                            $formData = $data['columns'][3]['search']['value'];

                            $tableDate = explode(' - ',$formData);

                            $firstDate = str_replace('/','-',$tableDate[0]);
                            $firstDate = trim(date('Y-m-d',strtotime($firstDate)));

                            $lastDate = str_replace('/','-',$tableDate[1]);
                            $lastDate = trim(date('Y-m-d',strtotime($lastDate)));


                            array_push($arrayConditions,array(
                                "DATE_FORMAT(date_transaction,'%Y-%m-%d') >= ?"=>$firstDate,
                                "DATE_FORMAT(date_transaction,'%Y-%m-%d') <= ?"=>$lastDate
                            ));
                        }
                        if(!empty($data['columns'][4]['search']['value']))
                        {

                            array_push($arrayConditions,array(
                                'id_user'=>intval($data['columns'][4 ]['search']['value'])
                            ));
                        }
                        if(!empty($data['columns'][5]['search']['value']))
                        {


                            array_push($arrayConditions,array(
                                'done_by'=>$data['columns'][5]['search']['value']
                            ));
                        }

                    }
                }

            $limit = null;

            $joins [] =array('table'=>array('l'=>'luni_users'),'condition'=>'l.id_user=t.made_by');

            $rowCount = count($this->Transactions([],$arrayConditions,$joins,null,'all',null,'date_transaction DESC'));


            if(isset($this->_currentUser['codeProfile']) && intval($this->_currentUser['codeProfile']) == 2)
                {
                    if(isset($data['pagination']['page'],$data['pagination']['perpage']))
                    {
                        $offset = (intval($data['pagination']['page'])-1) * intval($data['pagination']['perpage']);

                        $limit['end'] = $offset;
                        $limit['start'] = $data['pagination']['perpage'];
                    }
                }
                else
                {
                    if(isset($data['length']) && $data['length'] !== -1)
                    {
                        $limit['start'] = $data['length'];
                        $limit['end'] = $data['start'];
                    }
                }

                 $myRequest = $this->Transactions($arrayColumns,$arrayConditions,$joins,$limit,'all',null,'date_transaction DESC');
                 foreach ($myRequest as $keys=>$values)
                {

                    foreach ($values as $j=>$elements)
                    {
                        $myArray[$keys][$j] = $elements;
                        if($j ==="devise_source" || $j === 'devise_cible')
                        {
                            $nn[] = array('table'=>array('i'=>'luni_monnaies'),'condition'=>'i.id_famille=devises.famille_devise');

                            $myArray[$keys][$j] = $this->postservice->defaultSelect('devises',[],array(
                                'id_devise'=>intval($elements)
                            ),null,null,'unique',null,null);
                        }
                        elseif($j === 'made_by')
                        {
                            $myArray[$keys][$j] = $this->postservice->defaultSelect('luni_users',[],array('statut'=>'1','id_user'=>intval($elements)),null,null,'unique',null,null);
                        }
                        elseif($j === 'done_by')
                        {
                            $myArray[$keys][$j] = $this->postservice->defaultSelect('luni_users',[],array('id_user'=>intval($elements)),null,null,'unique',null,null);
                        }
                    }

                }

            if(isset($this->_currentUser['codeProfile']) && intval($this->_currentUser['codeProfile']) == 2)
            {

                $total_pages = ceil($rowCount / intval($data['pagination']['perpage']));

                $tableJson = [

                    'meta'=>array(
                        "page"=> intval($data['pagination']['page']),
                        "pages"=> $total_pages,
                        "perpage"=> isset($data['pagination'])?intval($data['pagination']['perpage']):1,
                        "total"=> $rowCount,
                        "sort"=> "desc",
                        "field"=> "date_transaction"
                    ),
                    'data'=>$myArray,
                    'current_user'=>intval($this->_currentUser['codeProfile'])
                ];

                $view  = new JsonModel($tableJson);
                $view->setTerminal(true);


                return $view;
            }


            $tableJson['data'] = $myArray;
            $tableJson['iTotalRecords'] = count($myRequest);
            $tableJson['iTotalDisplayRecords'] = $rowCount;
            $tableJson['sColumns'] ="";
            $tableJson['sEchos'] = isset($data['draw']) && !empty($data['draw']) ? intval($data['draw']) : null;
            $tableJson['current_user'] = array(
                'code_profil'=>intval($this->sessionContainer->CodeProfil)

            );

            $view  = new JsonModel($tableJson);
            $view->setTerminal(true);


            return $view;
        }

        public function retraitBonusAction()
        {


            $this->_getHelper('headScript',$this->serviceLocator)

                ->appendFile($this->getUriPath(). 'assets/pages/transactions/retrait_bonus.js')
            ;
            $this->renderPage();

            return array();
        }

        public function updateTransactionAction()
        {
            $request = $this->getRequest();
            $view = null;
            $tableJson = null;

            if(!$request->isXmlHttpRequest())
            {
                return $this->redirect()->toRoute('transactions',['action'=>'retrait-bonus']);
            }

            $datas = $request->getPost()->toArray();
            if(isset($datas['transaction_id']) && empty($datas['transaction_id']))
            {
                $tableJson['error'] = "Veuillez renseigner l'ID de la transaction";

                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            elseif(isset($datas['transaction_id']) && !empty($datas['transaction_id']))
            {
                $datas['transaction_id'] = filter_var($datas['transaction_id'],FILTER_SANITIZE_STRING);
            }

            if(isset($datas['motif']) && empty($datas['motif']))
            {
                $tableJson['error'] = "Veuillez renseigner le motif du rejet";

                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            elseif(isset($datas['motif']) && !empty($datas['motif']))
            {
                $datas['motif'] = filter_var($datas['motif'],FILTER_SANITIZE_STRING);
            }

            $getTransaction = $this->postservice->defaultSelect('transactions_bonus',[],[
                'id_transaction_bonus'=>intval($datas['id'])
            ],null,null,'unique',null,null);

            if(empty($getTransaction))
            {
                $tableJson['error'] = "Impossible de poursuivre le traitement demandé.";
                $view = new JsonModel($tableJson);
                $view->setTerminal(true);

                return $view;
            }
            $getUser = $this->postservice->getOneUser($getTransaction['customer'],null);

            if($datas['action'] == '1')
            {
                $nouveau_solde = null;

                if($getUser['gain'] >= $getTransaction['montant'])
                {
                    $nouveau_solde = $getUser['gain']-$getTransaction['montant'];
                }
                else
                {
                    $tableJson['error'] = "Impossible de faire cette opération.Solde insuffisant";
                    $view = new JsonModel($tableJson);
                    $view->setTerminal(true);

                    return $view;

                }


                $reqUpdateUser = $this->postservice->defaultUpdate('luni_users',
                    [
                        'gain'=>$nouveau_solde
                    ],[
                        'id_user'=>$getUser['id_user']
                    ]);
                if($reqUpdateUser)
                {
                    $this->postservice->defaultUpdate('transactions_bonus',[
                        'statut_transaction_bonus'=>'1',
                        'commentaire'=>'ID de la transaction: <b>'.$datas['transaction_id'].'</b>'
                    ],[
                        'id_transaction_bonus'=>$getTransaction['id_transaction_bonus']
                    ]);

                    $salutations = "Bonjour";
                    $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                    $getFormat = $nowUtc->format('H');
                    $getFormat = intval($getFormat);

                    if($getFormat > 12)
                    {
                        $salutations = "Bonsoir";
                    }

                    $key = "";
                    $token ="";
                    $nom_complet = $this->sessionContainer->UserName;
                    $btn = '';
                    $url = '';
                    $titre = '';
                    $subject = 'Votre compte LUNICHANGE';
                    $email = $getUser['email'];
                    $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                    $f_id = WebServiceDbSqlMapper::FROM_ID;

                    $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                    $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre ticket de retrait: <b>'.$getTransaction['ticket_demande'].'</b> a été validée.Identifiant de la transaction: <b style="text-decoration: underline;">'.$datas['transaction_id'].'.</b></p><br><p> Veuillez nous contacter en cas de non réception de votre bonus</p>';
                    $msg ['nb'][]= '';
                    $msg ['nb'][]= '';
                    $msg ['base_url'] = "";
                    $msg ['btn_url'] = '';



                    $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                    $mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');

                    if($mailSend == null)
                    {
                        $tableJson['success'] = "Demande de retrait validée";
                        $this->sessionContainer->MessageWelcome =  $tableJson['success'];
                    }



                }
            }
            else
            {
                $this->postservice->defaultUpdate('transactions_bonus',[
                    'statut_transaction_bonus'=>'2',
                    'commentaire'=> isset($datas['motif']) && !empty($datas['motif']) ? $datas['motif'] : null
                ],[
                    'id_transaction_bonus'=>$getTransaction['id_transaction_bonus']
                ]);

                $salutations = "Bonjour";
                $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                $getFormat = $nowUtc->format('H');
                $getFormat = intval($getFormat);

                if($getFormat > 12)
                {
                    $salutations = "Bonsoir";
                }

                $key = "";
                $token ="";
                $nom_complet = $this->sessionContainer->UserName;
                $btn = '';
                $url = '';
                $titre = '';
                $subject = 'Votre compte LUNICHANGE';
                $email = $getUser['email'];
                $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                $f_id = WebServiceDbSqlMapper::FROM_ID;

                $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre ticket de retrait: <b>'.$getTransaction['ticket_demande'].'</b> a été rejetée.</p><br><p> Veuillez nous contacter en cas de non réception de votre bonus</p>';
                $msg ['nb'][]= '';
                $msg ['nb'][]= '';
                $msg ['base_url'] = "";
                $msg ['btn_url'] = '';



                $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                $mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');

                if($mailSend == null)
                {
                    $tableJson['error'] = "Demande de retrait rejetée";

                }

            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        public function listBonusAction()
        {
            $request = $this->getRequest();

            $view = null;
            $tableJson = null;
            if(!$request->isXmlHttpRequest())
            {
                return $this->redirect()->toRoute('customer',['action'=>'mentoring']);
            }

            $data = $request->getPost()->toArray();

            $joins = null;
            $arrayConditions= array();
            $myArray = array();
            $arrayColumns = array(
                'id_transaction_bonus'=>'id_transaction_bonus',
                'moyen_paiement'=>'moyen_paiement',
                'montant'=>'montant',
                'commentaire'=>'commentaire',
                'telephone'=>'telephone',
                'ticket_demande'=>'ticket_demande',
                'statut_transaction'=>'statut_transaction_bonus',
                'date_transaction'=>'date',
                'customer'=>'customer',
                'proprietaire'=>'proprietaire'

            );

            $joins[] = array(
                'table'=>['d'=>'devises'],
                'condition'=>'d.id_devise=t.moyen_paiement'
            );
            $joins[] = array(
                'table'=>['u'=>'luni_users'],
                'condition'=>'u.id_user=t.customer'
            );
            if(isset($data['query']) && !empty($data['query']))
            {

                if(!empty($data['query'][0]))
                {
                    array_push($arrayConditions,[

                            'ticket_demande LIKE ?'=>'%'.ltrim(filter_var($data['query'][0],FILTER_SANITIZE_STRING)).'%'

                        ]
                    );
                }

            }


            $limit = null;
            $sort = 'date DESC';

            if(isset($data['length']) && $data['length'] !== -1)
            {
                $limit['start'] = $data['length'];
                $limit['end'] = $data['start'];
            }
            if(isset($data['field'],$data['sort']) && $data['field'] != '')
            {
                $sort = $data['field'].' '.strtoupper($data['sort']);
            }



            $myRequest = $this->postservice->defaultSelect(array(
                't'=>'transactions_bonus'),$arrayColumns,$arrayConditions,$joins,$limit,'all',null,$sort);
            $rowCount = count($this->postservice->defaultSelect([
                't'=>'transactions_bonus'
            ],[],$arrayConditions,$joins,null,'all',null,$sort));
            foreach ($myRequest as $keys=>$values)
            {

                foreach ($values as $j=>$elements)
                {
                    $myArray[$keys][$j] = $elements;
                }

            }


            $total_pages = ceil(count($myRequest) / intval($data['pagination']['perpage']) );



            $tableJson = [

                'meta'=>array(
                    "page"=> isset($data['pagination']['page']) && !empty($data['pagination']['page']) ? intval($data['pagination']['page']): 1,
                    "pages"=> $total_pages,
                    "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 1,
                    "total"=> $rowCount,
                    "sort"=> "desc",
                    "field"=> "date_transaction"
                ),
                'data'=>$myArray,
                'current_user'=>intval($this->_currentUser['codeProfile'])
            ];




            $view = new JsonModel($tableJson);
            $view->setTerminal(true);


            return $view;
        }

        protected function _getHelper($helper, $serviceLocator)
        {
            return $serviceLocator
                ->get('ViewHelperManager')
                ->get($helper);
        }



        public function getSessionTimeAction()
        {
            $request = $this->getRequest();
            $view = null;
            $tableJson =array();

            if($request->isXmlHttpRequest())
            {
                $data = $request->getPost()->toArray();

                if(isset($this->sessionContainer->SessionTimeForDecount[$data['dataSession']]) && !empty($this->sessionContainer->SessionTimeForDecount[$data['dataSession']]))
                {
                    if(isset($data['valueUpdate']))
                    {
                        $this->sessionContainer->SessionTimeForDecount[$data['dataSession']] = $this->sessionContainer->SessionTimeForDecount[$data['dataSession']] + $data['valueUpdate'];
                    }

                    $tableJson['success'] = $this->sessionContainer->SessionTimeForDecount[$data['dataSession']];
                }


                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {
                $this->redirect()->toRoute('transactions');
            }

            return $view;
        }

        public function validateTransactionAction()
        {
            $request = $this->getRequest();
            $view = null;
            $tableJson = array();

            if($request->isXmlHttpRequest())
            {
                $data = $request->getPost()->toArray();



                if(isset($data['id_transaction']) && !empty($data['id_transaction']))
                {
                    $id_transaction = intval($data['id_transaction']);

                    $getTransaction = $this->postservice->defaultSelect('transactions',[],array('id_transaction'=>$id_transaction),null,null,'unique',null,null);


                    if(!empty($getTransaction))
                    {
                        if($getTransaction['status_transaction'] == '0')
                        {
                            $getDevise =$this->postservice->defaultSelect('devises',[],array('id_devise'=>intval($getTransaction['devise_cible'])),null,null,'unique',null,null);


                            if(!empty($getDevise))
                            {
                                if(floatval($getTransaction['quantite_cible']) < floatval($getDevise['solde']))
                                {
                                    $currentSolde = floatval($getDevise['solde']) - floatval($getTransaction['quantite_cible']);
                                    $reqUpdate = $this->postservice->defaultUpdate('devises',array('solde'=>$currentSolde),array('id_devise'=>intval($getTransaction['devise_cible'])));
                                    $updateTransaction = $this->postservice->defaultUpdate('transactions',['status_transaction'=>'1','done_by'=>intval($this->sessionContainer->IdUser)],['id_transaction'=>$data['id_transaction']]);

                                    if($reqUpdate && $updateTransaction)
                                    {
                                        $dataToInsert['message'] =  "La transaction ".$getTransaction['transaction_id']." effectuée à la date du ".date('Y-m-d H:i:s',strtotime($getTransaction['date_transaction'])).' a été validée.';

                                        $dataToInsert['objet'] = "Validation de transaction";
                                        $dataToInsert['date'] = date('Y-m-d H:i:s');
                                        $dataToInsert['user'] = intval($getTransaction['made_by']);



                                        $salutations = "Bonjour";
                                        $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                                        $getFormat = $nowUtc->format('H');
                                        $getFormat = intval($getFormat);

                                        if($getFormat > 12)
                                        {
                                            $salutations = "Bonsoir";
                                        }
                                        $getUser = $this->postservice->defaultSelect('luni_users',[],array('id_user'=>intval($getTransaction['made_by'])),null,null,'unique',null,null);

                                        $getParentUser = $this->postservice->getUser([
                                            'id_user'=>$getUser['parent']
                                        ]);


                                        if(!empty($getParentUser) && $getParentUser !== null)
                                        {
                                            $fjoin [] = array(
                                                'table'=>array(
                                                    'f'=>'luni_monnaies'
                                                ),
                                                'condition'=>'f.id_famille=d.famille_devise'
                                            );

                                            $fjoin [] = array(
                                                'table'=>array(
                                                    'c'=>'categorie_devise'
                                                ),
                                                'condition'=>'c.id_categorie_devise=d.categorie_devise'
                                            );

                                            $getD = $this->postservice->defaultSelect(array(
                                                'd'=>'devises'),[],[
                                                'id_devise'=>$getTransaction['devise_source']
                                            ],$fjoin,null,'unique',null,null);


                                            if(intval($getD['categorie_devise']) == 2 && $getD['libelle_famille'] == 'FCFA')
                                            {

                                                $somme = $getTransaction['quantite_source'] /100;
                                                $somme = floatval($somme);
                                                $nouveauSolde = $getParentUser['gain']+ $somme;
                                                $nouveauSolde = floatval($nouveauSolde);

                                                $this->postservice->defaultUpdate('luni_users',[
                                                    'gain'=>$nouveauSolde
                                                ],['id_user'=>$getParentUser['id_user']]);

                                            }

                                        }
                                        $key = "";
                                        $token ="";
                                        $nom_complet = $getUser['nom_complet'];
                                        $btn = '';
                                        $url = '';
                                        $titre = '';
                                        $subject = 'Votre compte LUNICHANGE';
                                        $email = $getUser['email'];
                                        $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                                        $f_id = WebServiceDbSqlMapper::FROM_ID;

                                        $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                                        $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre transaction ID:<b>&nbsp;'.$getTransaction['transaction_id'].'</b> | Ticket: <b>'.$getTransaction['code_transaction'].'</b>, effectuée le '.$getTransaction['date_transaction'].' a été validée.</p>';
                                        $msg ['nb'][]= '';
                                        $msg ['nb'][]= '';
                                        $msg ['base_url'] = "";
                                        $msg ['btn_url'] = '';



                                        $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                                        //$mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');

                                        $mailSend = null;

                                        if($mailSend == null)
                                        {
                                            $reqInsert = $this->postservice->defaultInsert('messages',$dataToInsert);
                                            if($reqInsert)
                                            {
                                                $tableJson['success'] = "success";

                                                $this->sessionContainer->MessageTransaction = "Transaction validée !";
                                            }
                                       }

                                    }
                                }
                                else
                                {
                                    $tableJson['error'] = "Le solde actuel est insuffisant. Cette transaction ne peut-être valider.";
                                }
                            }
                        }
                        else
                        {
                            $tableJson['error'] = "Cette opération ne peut aboutir.";
                        }

                    }


                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement.";
                }


                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {
                $this->redirect()->toRoute('transactions');
            }

            return $view;
        }
        public function cancelTransactionAction()
        {
            $request = $this->getRequest();
            $view = null;
            $tableJson = array();

            if($request->isXmlHttpRequest())
            {
                $data = $request->getPost()->toArray();



                if(isset($data['id_transaction']) && !empty($data['id_transaction']))
                {
                    $id_transaction = intval($data['id_transaction']);

                    $getTransaction = $this->postservice->defaultSelect('transactions',[],array('id_transaction'=>$id_transaction),null,null,'unique',null,null);


                    if(!empty($getTransaction))
                    {
                        if($getTransaction['status_transaction'] == "0")
                        {

                            $getDevise =$this->postservice->defaultSelect('devises',[],array('id_devise'=>intval($getTransaction['devise_cible'])),null,null,'unique',null,null);


                            if(!empty($getDevise))
                            {


                                $currentSolde = floatval($getDevise['solde']) - floatval($getTransaction['quantite_cible']);
                                $reqUpdate = (bool)$this->postservice->defaultUpdate('devises',array('solde'=>$currentSolde),array('id_devise'=>intval($getTransaction['devise_cible'])));


                                $updateTransaction = (bool)$this->postservice->defaultUpdate('transactions',['status_transaction'=>'2'],['id_transaction'=>$data['id_transaction']]);

                                if($reqUpdate && $updateTransaction)
                                {
                                    $dataToInsert['message'] =  "La transaction ".$getTransaction['transaction_id']." effectuée à la date du ".date('Y-m-d H:i:s',strtotime($getTransaction['date_transaction'])).' a été annulée.';

                                    $dataToInsert['objet'] = "Annulation de transaction";
                                    $dataToInsert['date'] = date('Y-m-d H:i:s');
                                    $dataToInsert['user'] = intval($getTransaction['made_by']);
                                    //$dataToInsert['done_by'] = $this->sessionContainer->IdUser;

                                    $salutations = "Bonjour";
                                    $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
                                    $getFormat = $nowUtc->format('H');
                                    $getFormat = intval($getFormat);

                                    if($getFormat > 12)
                                    {
                                        $salutations = "Bonsoir";
                                    }
                                    $getUser = $this->postservice->defaultSelect('luni_users',[],array('id_user'=>intval($getTransaction['made_by'])),null,null,'unique',null,null);

                                    $key = "";
                                    $token ="";
                                    $nom_complet = $getUser['nom_complet'];
                                    $btn = '';
                                    $url = '';
                                    $titre = '';
                                    $subject = 'Votre compte LUNICHANGE';
                                    $email = $getUser['email'];
                                    $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                                    $f_id = WebServiceDbSqlMapper::FROM_ID;

                                    $msg ['salutations']= "<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                                    $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important">Votre transaction ID:<b>&nbsp;'.$getTransaction['transaction_id'].'</b> | Code: <b>'.$getTransaction['code_transaction'].'</b>, effectuée le '.$getTransaction['date_transaction'].' a été annulée.</p>';
                                    $msg ['nb'][]= '';
                                    $msg ['nb'][]= '';
                                    $msg ['base_url'] = "";
                                    $msg ['btn_url'] = '';



                                    $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                                    //$mailSend = $this->postservice->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template_transactions');


                                    $mailSend = null;
                                    if($mailSend === null)
                                    {
                                        $reqInsert = $this->postservice->defaultInsert('messages',$dataToInsert);
                                        if($reqInsert)
                                        {
                                            $tableJson['success'] = "success";

                                            $this->sessionContainer->MessageTransaction = "Transaction annulée !";
                                        }
                                    }
                                    else
                                    {
                                        $tableJson['error'] = "Une erreur s'est produite au cours du traitement.";

                                    }

                                }

                            }
                            else
                            {
                               $delete = $this->postservice->defaultDelete('transactions',['id_transaction'=>$data['id_transaction']]);

                                if($delete)
                                {
                                    $tableJson['success'] = "success";

                                    $this->sessionContainer->MessageTransaction = "Transaction annulée !";
                                }

                            }
                        }
                        else
                        {
                            $tableJson['error'] = "Cette opération ne peut aboutir.";
                        }

                    }


                }
                else
                {
                    $tableJson['error'] = "Impossible d'effectuer le traitement.";
                }


                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {
                $this->redirect()->toRoute('transactions');
            }

            return $view;
        }

        public function showNumberDetailsAction()
        {
            $request = $this->getRequest();
            $view = null;
            $tableJson = array();

            if($request->isXmlHttpRequest())
            {
                $data = $request->getPost()->toArray();

                if (isset($data['id_transaction']) && !empty($data['id_transaction']))
                {
                    $getActiveTransaction = $this->postservice->defaultSelect($this->postservice->getDbTables()['transactions']['table'],array(),array(
                        'id_transaction'=>(int)$data['id_transaction']
                    ),null,null,'unique',null,null);

                    if(!empty($getActiveTransaction))
                    {
                        $workOnNumberFirstTry = explode(' ',$getActiveTransaction['numero_envoi']);
                        $getNumber = array_pop($workOnNumberFirstTry);

                        $getNumberAbout = $this->postservice->defaultSelect('numeros_transactions',array(),array(
                            'libelle_numero'=>$getNumber
                        ),null,null,'unique',null,null);

                        if(!empty($getNumberAbout))
                        {
                            $tableJson = $getNumberAbout;
                        }
                        else
                        {
                            $tableJson['error'] = "Votre demande ne peut aboutir";
                        }
                    }
                    else
                    {
                        $tableJson['error'] = "Votre demande ne peut aboutir";
                    }
                }

                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {
                $this->redirect()->toRoute('transactions');
            }
            return $view;
        }

        public function stdGetAction()
        {
            $request =$this->getRequest();
            $view = null;
            $tableJson = array();
            if($request->isXmlHttprequest())
            {
                $datas = $request->getPost()->toArray();
                $dataGetTable = $this->params ()->fromQuery ('table');
                $dataGetType = $this->params ()->fromQuery ('type');
                $dataConditions = array();
                // $this->sessionContainer->ElementForUpdate = array();
                foreach ($datas as $keys=>$values)
                {
                    //$this->sessionContainer->ElementForUpdate[$keys] = $values;
                    $dataConditions[$keys] = $values;
                }
                $request = $this->postservice->defaultSelect ($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
                    $dataGetType,null,null);
                if((bool)$request)
                {
                    $tableJson = $request;
                    $this->sessionContainer->StdGetForUpdate = $request;
                }


                $view = new JsonModel($tableJson);
                $view->setTerminal (true);
            }
            else
            {
                $this->redirect()->toRoute('transactions');
            }

            return $view;
        }

        public function updateSessionTimeAction()
        {
            $request = $this->getRequest();
            $view = null;
            $tableJson = array();

            if($request->isXmlHttpRequest())
            {
                $data = $request->getPost()->toArray();

                    $this->sessionContainer->SessionTimeForDecount[$data['dataSession']] = time()+$data['valueUpdate'];


                $tableJson['success'] = $this->sessionContainer->SessionTimeForDecount[$data['dataSession']];


                $view = new JsonModel($tableJson);
                $view->setTerminal(true);
            }
            else
            {
                $this->redirect()->toRoute('transactions');
            }
            return $view;
        }
        private function Transactions($columns,$conditions,$joins,$limit,$type,$grpe,$order)
        {
            return $this->postservice->defaultSelect(array('t'=>$this->table),$columns,$conditions,$joins,$limit,$type,$grpe,$order);

        }


        public function getUriPath()
        {
            $basePath = $this->getRequest()->getBasePath();
            $uri = new Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
            return $baseUrl;
        }

        public function renderPage()
        {
            $this->checkAuth();

            $this->contentVars['user_code_profil'] = isset($this->_currentUser['codeProfile']) && $this->_currentUser['codeProfile'] !== null ? $this->_currentUser['codeProfile'] : null;
            //header layout
            $this->headerView = new ViewModel();

            $user_values = array();

            $this->menuVars['user_name']= isset($this->_currentUser);
            $this->menuVars['user_id']= $this->sessionContainer->IdUser;

            $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));
            if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
            {
                $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
                // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
                $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


            }
            $this->menuVars['user_data'] = $user_values;

            //menu layout
            $this->menuView = new ViewModel();
            $this->menuView->setVariables($this->menuVars);
            $this->menuView->setTemplate('layout/menu');

            //footerView
            $this->footerView = new ViewModel();
            $this->footerView->setTemplate('layout/footer');

            $this->layout = $this->layout();

            $this->layout->addChild($this->menuView, 'menu');
            $this->layout->addChild($this->footerView, 'footer');
            $this->layout->setVariables($this->contentVars);


            $this->layout->setTemplate('layout/layout');


        }

        public function checkAuth()
        {

            if(!isset($this->_currentUser['id']) || $this->_currentUser['id'] == null)
            {
                return $this->redirect()->toRoute('logout');
            }

                return 0;

        }

        private function getOneUser($id,$join)
        {
            return $this->postservice->defaultSelect(array('u'=>$this->postservice->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
        }

        public function SendMail($message, $key,$token,$nom_complet,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template)
        {
            //Envoie des notifications mail
            $incMail = 0;


            try {
                //Les paramètres d'envoie
                $template_array = [];
                $template_array['key'] = $key;
                $template_array['token'] = $token;
                $template_array['btn_url'] = $btn_url;
                $template_array['btn_label'] = $btn_label;
                $template_array['message'] = $message;
                $template_array['nom_user'] = $nom_complet;
                $template_array['titre'] = $titre;
                //  $template_array['prenom_user'] = $prenom;

                //Envoie du mail de notification
                $mailService = $this->getServiceLocator()->get('MailMan\LUNI');
                $view       = new PhpRenderer();
                $resolver   = new TemplateMapResolver();
                $resolver->setMap(array(
                    $template => __DIR__ . '/../../../view/mails/'.$template.'.phtml'
                ));
                $view->setResolver($resolver);

                $content  = new ViewModel();
                $content->setTemplate($template)->setVariables($template_array);
                $content->setVariables($template_array);
                $message = new MailManMessage();
                $message->setSubject($subject);
                $message->addHtmlPart($view->render($content));
                $message->addTo($email);
                $message->addFrom($from_mail, $from_id);
                $res = $mailService->send($message);
                $logMessage = "Succès Envoi de mail à " . $nom_complet . "  au mail " . $email . " le" . date("d-m-Y H:m:s") . " Résultat : " . $res;
                $this->mailLog($logMessage);
            } catch (\Exception $ex) {

                $logMessage = "Erreur Envoi de mail à " .$nom_complet . " au mail " . $email . " le" . date("d-m-Y H:m:s") . " Résultat : " . $ex;
                $this->mailLog($logMessage);
            }

        }



        /**
         * Log des mails
         * @param null $message
         */
        private function mailLog($message = null)
        {
            if (isset($message)) {
                $config = $this->getServiceLocator()->get('Config');
                $log_path = $config['log_path'];
                $logger = new Logger();
                $write = new Stream($log_path . 'mail.log');
                $logger->addWriter($write);
                $logger->log(Logger::ALERT, 'MAIL MESSAGE');
                $message = $message . " \n";
                $logger->alert($message);
            }
        }

        private function sendMessageTelegram($message)
        {
          
              $telegrambot = "1477791644:AAGnbXBPsnBZ135rKH4kry7elLA0bYl60Ww"; 
              $telegramchatid = "-1001408210866";
              $url='https://api.telegram.org/bot'.$telegrambot.'/sendMessage';$data=array('chat_id'=>$telegramchatid,'text'=>$message);
              $options=array('http'=>array('method'=>'POST','header'=>"Content-Type:application/x-www-form-urlencoded\r\n",'content'=>http_build_query($data),),);
            $context=stream_context_create($options);
            $result=file_get_contents($url,false,$context);
          
        return $result;
        }



    }