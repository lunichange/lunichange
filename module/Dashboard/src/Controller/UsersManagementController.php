<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15-Apr-20
 * Time: 9:55 PM
 */

namespace Dashboard\Controller;



use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;


class UsersManagementController extends AbstractActionController
{
    protected $postservice;

    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;


    protected $_currentUser;
    protected $sessionManager;
    protected $sessionContainer;

    protected $table;
    protected $serviceLocator;


    public function __construct(PostServiceInterface $postservice,$container)
    {
        $this->postservice = $postservice;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();
        $this->table = 'luni_monnaies';

        //menu layout
        $this->menuView = new ViewModel();
        $this->menuVars = array();
        $this->menuView->setTemplate('layout/menu');


        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }

    public function indexAction()
    {

        $this->checkAuth ();

        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/js/pages/crud/forms/widgets/bootstrap-select.js')
            ->appendFile($this->getUriPath() . 'assets/pages/users-management/ajax_list_of_users.js')
            ->appendFile($this->getUriPath() . 'assets/pages/users-management/custom_script.js')
            ->appendFile($this->getUriPath() . 'assets/pages/users-management/form_add_user.js')

        ;

        $this->renderPage ();


        return array
        (
            'codeProfilConnecte'=>$this->sessionContainer->CodeProfil,
            'profils_user'=>$this->postservice->getProfilAccordingToProfilOnline ($this->sessionContainer->CodeProfil)

        );
    }

    public function generatePasswordAction()
    {
        $request = $this->getRequest ();
        $view = null;


        if($request->isXmlHttpRequest())
        {
            $view = new JsonModel(array('password'=>$this->postservice->generateRandom(6)));
            $view->setTerminal (true);
        }

        return $view;
    }
    public function listUsersAction()
    {
        $request = $this->getRequest ();
        $view = null;
        $join = null;
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $sort = null;
            $arrayConditions = array();

            // var_dump($data);

           /* if(isset($data['sort']))
            {
                if(!empty($data['sort']['field']) && $data['sort']['field']!== "libelle_famille" && !empty($data['sort']['sort']))
                {
                    $fieldData = $data['sort']['field'];
                    $sortData = $data['sort']['sort'];

                    $sort = $fieldData.' '.strtoupper($sortData);
                }
                else
                {
                    $sort = 'nom_complet ASC';
                }
            }*/
             $sort = 'nom_complet ASC';

            if(isset($data['query']) && !empty($data['query']))
            {

                if(isset($data['query']['Status']) && !empty($data['query']['Status']))
                {
                    array_push($arrayConditions,array(
                        'u.lock_u'=>$data['query']['Status']));
                }
                if(isset($data['query']['Type']) && !empty($data['query']['Type']))
                {
                    array_push($arrayConditions,array(
                        'online'=>$data['query']['Type']));
                }
            }
            /*if($this->sessionContainer->CodeProfil > 0 && $this->sessionContainer->CodeProfil !== 2)
            {*/
            $arrayConditions [] = array(
                'u.code_profil<>?'=>2,
                'u.code_profil>?'=>$this->sessionContainer->CodeProfil,
                'id_user<>?'=>$this->sessionContainer->IdUser
            );
           



            $join [] = array(
                'table'=>array('p'=>$this->postservice->getDbTables ()['profil_user']['table']),
                'condition'=>'p.'.$this->postservice->getDbTables ()['profil_user']['columns']['code_profil'].'=u.'.$this->postservice->getDbTables ()['customers']['columns']['code_profil']
            );
            $columnsArray = array(
                'id_user'=>'id_user',
                'nom_user'=>'nom_complet',
                'email_user'=>'email',
                'last_connexion'=>'derniere_connexion',
                'lock_user'=>'lock_u',
                'status'=>'statut',
                'online'=>'online',
                'profil_user'=>'code_profil'
            );
            $tableJson = $this->postservice->defaultSelect(array('u'=>'luni_users'),$columnsArray,$arrayConditions,$join,null,'all',null,$sort);

            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('users-management');
        }

        return $view;
    }


    public function updateStatusAccountAction()
    {
        $request = $this->getRequest ();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $getDataId = $this->params ()->fromQuery ('id');
            $getStatusValue = $this->params ()->fromQuery ('valeur');

            $checkIfExist = $this->getOneAccount (array(
                'id_user'=>filter_var ($getDataId,FILTER_SANITIZE_NUMBER_INT)
            ));

            if((bool)$checkIfExist)
            {
                if($this->updateAccount(array('lock_u'=>$getStatusValue),array('id_user'=>filter_var ($getDataId,FILTER_SANITIZE_NUMBER_INT))))
                {
                    $tableJson['success'] = "Status du compte mis à jour";
                }
                else
                {
                    $tableJson['error'] = "Une erreur s'est produite au cours de l'opération";
                }
            }
            else
            {
                $tableJson['error'] = "Une erreur s'est produite au cours de l'opération";
            }
            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('users-management');
        }

        return $view;
    }


    public function addUserAction()
    {
        $request = $this->getRequest ();
        $view = null;
        $tableJson = array();


        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost();
            $dataToInsert = array();
            $dataEmpty = false;

            foreach ($data as $keys=>$elements)
            {
                if(empty($data[$keys]) || $elements == "")
                {
                    $dataEmpty = true;
                    break;
                }
            }
            if($dataEmpty)
            {
                $tableJson['error'] = "Veuillez renseigner tous les champs";
            }
            else
            {
                $data['user_name'] = filter_var (ltrim ($data['user_name']),FILTER_SANITIZE_STRING);
                $data['user_firstName'] = filter_var (ltrim($data['user_firstName']),FILTER_SANITIZE_STRING);
                $data['email_user'] = trim($data['email_user']);

                if(strlen ($data['user_name']) < 3)
                {
                    $tableJson['error'] = "Nom trop court. Trois(03) caractères au minimum";
                }
                elseif(strlen ($data['user_firstName']) < 3)
                {
                    $tableJson['error'] = "Prénom(s) trop. Trois(03) caractères au minimum";
                }
                elseif(!filter_var($data['user_email'],FILTER_VALIDATE_EMAIL))
                {
                    $tableJson['error'] = "Adresse mail invalide";
                }
                elseif(strlen ($data['user_password'])< 6)
                {
                    $tableJson['error'] = "Mot de passe trop court. Six(06) caractères au minimum";
                }
                elseif($data['user_password'] !== $data['user_rpassword'])
                {
                    $tableJson['error'] =  "Les mot de passe doitvent-être identiques.";
                }
                elseif(!(bool)$this->getOneData ('profil_user',array('code_profil'=>intval ($data['profile_user']))))
                {
                    $tableJson['error'] = "Une erreur s'est produite au cours de l'opération. Veuillez réésayer avec des données correctes.";
                }
                else
                {
                    $checkifUserExist = $this->getOneAccount (array(
                        'email'=>htmlentities (filter_var (trim ($data['user_email']),FILTER_SANITIZE_EMAIL))
                    ));
                    if((bool)$checkifUserExist)
                    {
                        $tableJson['error'] = "Cet utilisateur existe déjà !";
                    }
                    else
                    {

                        /*$sendMail = $this->helpersService->sendMail (null,null,$data['email_user'],
                            array(
                                'profil'=>$this->getOneData ('profil_user',array('code_profil'=>intval ($data['profil_user'])))['lib_profil'],
                                'email'=>$data['email_user'],
                                'password'=>$data['user_password']

                            ),null,null,MainHelperManager::MAIL_SEND_FROM,'ADMIN LUNICHANGE','createUserTemplate','create_user_mail_template',$this->getUriPath ().'img/logo_site/full_logo.png'

                        );*/
                        /*if((bool) $sendMail)
                        {*/
                        $dataToInsert['email'] = htmlspecialchars (filter_var ($data['user_email'],FILTER_SANITIZE_EMAIL));
                        $dataToInsert['nom_complet'] = htmlspecialchars ($data['user_firstName']." ".$data['user_name']);
                        $dataToInsert['password'] = htmlentities (password_hash ($data['user_password'],PASSWORD_DEFAULT));
                        $dataToInsert['code_profil'] = filter_var ($data['profile_user'],FILTER_SANITIZE_NUMBER_INT);
                        $dataToInsert['date_creation'] = date('Y-m-d H:i:s');
                        $dataToInsert['statut'] = '1';
                        $dataToInsert['lock_u'] = '0';

                        $requestInsert = $this->addUser ($dataToInsert);
                        if((bool) $requestInsert)
                        {
                            $tableJson['success'] = "Enregistrement effectué.";
                        }
                        else
                        {
                            $tableJson['error'] = "Une erreur s'est produite au cours de l'enregsitrement";
                        }
                        //}

                    }
                }

            }

            $view  = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('users-management');
        }
        return $view;
    }


    private function getAllUsers($hayshack,$columns,$join,$limit,$grpe,$sort)
    {
        return $this->postservice->defaultSelect (
            array('u'=>'luni_users'),$columns,
            $hayshack,$join,$limit,'all',$grpe,$sort
        );
    }

    private function getOneAccount($hayshack)
    {
        return $this->postservice->defaultSelect(
            $this->postservice->getDbTables ()['customers']['table'],array(),
            $hayshack,
            null,null,'unique',
            null,null
        );
    }

    private function getOneData($table,$hayshack)
    {
        return $this->postservice->defaultSelect ($table,array(),$hayshack,null,null,'unique',null,null);
    }
    private function updateAccount($datas,$hayshack)
    {

        return (bool) $this->postservice->defaultUpdate ($this->postservice->getDbTables ()['customers']['table'],$datas,$hayshack);
    }
    private function addUser($datas)
    {
        return (bool) $this->postservice->defaultInsert ($this->postservice->getDbTables ()['customers']['table'],$datas);
    }

    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttprequest())
        {
            $datas = $request->getPost()->toArray();
            $dataGetTable = $this->params ()->fromQuery ('table');
            $dataConditions = array();
            foreach ($datas as $keys=>$values)
            {
                $dataConditions[$keys][] = $values;
            }
            $request = $this->postservice->defaultSelect ($this->postservice->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
                'unique',null,null);
            if((bool)$request)
            {
                $tableJson = $request;
                $this->sessionContainer->StdGetForUpdate = $request;
            }
            else
            {
                $tableJson['error'] = "Une erreur s'est produite. Veuillez rééssayer.";
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }
        else
        {
            $this->redirect()->toRoute('users-management');
        }
        return $view;
    }
    private function getOneUser($id,$join)
    {
        return $this->postservice->defaultSelect(array('u'=>'luni_users'),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


    public function renderPage()
    {

        $this->contentVars['user_code_profil'] = intval($this->sessionContainer->CodeProfil);
        //header layout
        $this->headerView = new ViewModel();



        $this->menuVars['user_name']= $this->sessionContainer->Login;
        $this->menuVars['user_id']= $this->sessionContainer->IdUser;

        $this->menuVars['themenu']= $this->postservice->retriveMenu (filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_NUMBER_INT));

        $user_values = array();
        if($this->sessionContainer->IdUser !== "" && $this->sessionContainer->IdUser !== null)
        {
            $joins [] = array('table'=>array('p'=>'profil_user'),'condition'=>'p.code_profil=u.code_profil');
            // $joins [] = array('table'=>array('c'=>'pays'),'condition'=>'c.id_pays=u.pays');
            $user_values  = $this->getOneUser(intval($this->sessionContainer->IdUser),$joins);


        }
        $this->menuVars['user_data'] = $user_values;


        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/menu');

        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->footerView, 'footer');
        $this->layout->setVariables($this->contentVars);


        $this->layout->setTemplate('layout/layout');


    }

    public function checkAuth()
    {

        if(!isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser == '' || $this->sessionContainer->IdUser === NULL)
        {
            $this->redirect()->toRoute('logout');
        }
        elseif(isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== NULL)
        {
            if($this->sessionContainer->CodeProfil === 2)
            {
                $this->redirect ()->toRoute ('logout');
            }

        }
        /*elseif($this->sessionContainer->CodeProfil == 2)
       {
           $this->redirect()->toRoute('customer');
       }
       elseif($this->sessionContainer->CodeProfil == 1 || $this->sessionContainer->CodeProfil == 0)
       {
           $this->redirect()->toRoute("administration");
       }*/
        /*
        elseif(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser >1)
        {
            $this->redirect()->toRoute('customer');
        }
        else
        {
            $this->redirect()->toRoute('administration');
        }*/
    }
}