<?php

namespace Dashboard\Controller;


use Application\Service\PostServiceInterface;
use DateTime;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\AbstractContainer;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class WebServicesController extends AbstractActionController
{
    private $currentUser = array();
    protected $postService;


    protected $sessionManager;
    protected $sessionContainer;
    protected $serviceLocator;

    public function __construct(PostServiceInterface $postService,$container)
    {
        $this->postService = $postService;
        $this->serviceLocator = $container;

        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) || $this->sessionContainer->IdUser !== null)
        {
            $this->currentUser = array(
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>$this->sessionContainer->CodeProfil,
                'username'=>$this->sessionContainer->Login,
                'fullname'=>isset($this->sessionContainer->UserName) && $this->sessionContainer->UserName !== null ? filter_var($this->sessionContainer->UserName,FILTER_SANITIZE_STRING) : null,
                'timeStamp'=>isset($this->sessionContainer->LastLoginTimeStamp) && $this->sessionContainer->LastLoginTimeStamp !== null ? $this->sessionContainer->LastLoginTimeStamp : null
            );
        }
    }

    public function showNotificationAction()
    {
        $request = $this->getRequest();
        $view =  null;
        $tableJson = array();

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('dashboard',['action'=>'tableau-de-bord']);
        }
        $tableJson['error'] = true;
        if(isset($this->sessionContainer->MessageWelcome) && !empty($this->sessionContainer->MessageWelcome))
        {
            $tableJson['success'] = $this->sessionContainer->MessageWelcome ;
            $tableJson['error'] = false;

        }

        unset($this->sessionContainer->MessageWelcome);
        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }


    public function cronTaskAction()
    {

        $getTransactionsTemp = $this->postService->defaultSelect('transactions_temp',[],[

                'type_transaction'=>"FEDAPAY",

        ],null,null,'all',null,null);

        //die(print_r($getTransactionsTemp));
        if(!empty($getTransactionsTemp))
        {
            foreach($getTransactionsTemp as $values)
            {

                $dateTransaction = date('d-m-Y',strtotime($values['date_transaction']));
                $dateToday = date('d-m-Y');



                if($dateTransaction == $dateToday)
                {

                    switch ($values['type_transaction'])
                    {
                        case "FEDAPAY":
                        case "Fedapay":

                            $getUser = $this->postService->defaultSelect('luni_users',[],[
                                'id_user'=>$values['made_by']
                            ],null,null,'unique',null,null);

                            $deviseSource = $this->postService->defaultSelect('devises',[],[
                                'id_devise'=>$values['devise_source']
                            ],null,null,'unique',null,null);

                            $deviseDestination = $this->postService->defaultSelect('devises',[],[
                                'id_devise'=>$values['devise_destination']
                            ],null,null,'unique',null,null);

                            $getTransaction = $this->postService->defaultSelect('transactions',[],[
                                'made_by'=>$values['made_by'],
                                'api_transaction_id'=>$values['transaction_identifier'],
                                'api_transaction_id<>?'=>NULL
                            ],null,null,'unique',null,null);

                            \FedaPay\FedaPay::setApiKey($this->getFedaPayKey($this->serviceLocator->get('Config')['fedapayEnvironment']));
                            \FedaPay\FedaPay::setEnvironment($this->serviceLocator->get('Config')['fedapayEnvironment']);

                            $transaction = \FedaPay\Transaction::retrieve($values['transaction_identifier']);


                            $id_transaction = $this->checkGenerateCode('id');
                            $code_transaction = $this->checkGenerateCode('code');

                            switch($transaction->status)
                            {
                                case 'approved':

                                    if(empty($getTransaction))
                                    {
                                        $reqInsert = $this->postService->defaultInsert('transactions',[
                                            'context'=>'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].'.',
                                            'code_transaction'=>$code_transaction,
                                            'date_transaction'=>$values['date_transaction'],
                                            'made_by'=>$values['made_by'],
                                            'transaction_id'=>$id_transaction,
                                            'devise_source'=>$deviseSource['id_devise'],
                                            'devise_cible'=>$deviseDestination['id_devise'],
                                            'quantite_source'=>$values['quantite_source'],
                                            'quantite_cible'=>$values['quantite_destination'],
                                            'numero_envoi'=>'Fedapay',
                                            'adresse_reception'=>$values['adresse_reception'],
                                            'type_transaction'=>"FEDAPAY",
                                            'api_transaction_id'=>$values['transaction_identifier']

                                        ]);

                                        if(!empty($reqInsert))
                                        {


                                            $messageTelegrame = "La transaction initiée par le client ".$getUser['nom_complet']. " a été traité par Fedapay.";
                                            $messageTelegrame .= " Informations de la transaction:  ";
                                            $messageTelegrame .= "Id transaction Fedapay : ".$values['transaction_identifier'].". ";
                                            $messageTelegrame .= "Context de la transaction:";
                                            $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                                            $messageTelegrame .= "Date transaction: ".$values['date_transaction'].".  ";
                                            $messageTelegrame .= "Montant donné : ".$values['quantite_source']." |  ";
                                            $messageTelegrame .= "Montant demandé : ".$values['quantite_destination'].". ";
                                            $messageTelegrame .= "Adresse réception : ".$values['adresse_reception'];



                                            $this->postService->sendMessageTelegram($messageTelegrame);



                                        }
                                    }

                                    $this->postService->defaultDelete('transactions_temp',[
                                        'transaction_identifier'=>$values['transaction_identifier'],
                                        'made_by'=>$values['made_by']
                                    ]);




                                    break;


                              /*  case 'pending':

                                    $messageTelegrame = "La transaction initiée par le client ".$getUser['nom_complet']. " est toujours en attente au niveau de Fedapay.";
                                    $messageTelegrame .= " Informations de la transaction:  ";
                                    $messageTelegrame .= "Id transaction Fedapay : ".$values['transaction_identifier'].". ";
                                    $messageTelegrame .= "Context de la transaction:";
                                    $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                                    $messageTelegrame .= "Date transaction: ".$values['date_transaction'].".  ";
                                    $messageTelegrame .= "Montant donné : ".$values['quantite_source']." |  ";
                                    $messageTelegrame .= "Montant demandé : ".$values['quantite_destination'].". ";
                                    $messageTelegrame .= "Adresse réception : ".$values['adresse_reception'];

                                    $this->postService->sendMessageTelegram($messageTelegrame);


                                    break;*/
                                case 'declined':
                                case 'canceled':

                                    $this->postService->defaultDelete('transactions_temp',[
                                        'transaction_identifier'=>$values['transaction_identifier'],
                                        'made_by'=>$values['made_by']
                                    ]);

                                    $messageTelegrame = "La transaction initiée par le client ".$getUser['nom_complet']. " a été décliné/annulé par Fedapay.";
                                    $messageTelegrame .= " Informations de la transaction:  ";
                                    $messageTelegrame .= "Id transaction Fedapay : ".$values['transaction_identifier'].". ";
                                    $messageTelegrame .= "Context de la transaction:";
                                    $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                                    $messageTelegrame .= "Date transaction: ".$values['date_transaction'].".  ";
                                    $messageTelegrame .= "Montant donné : ".$values['quantite_source']." |  ";
                                    $messageTelegrame .= "Montant demandé : ".$values['quantite_destination'].". ";
                                    $messageTelegrame .= "Adresse réception : ".$values['adresse_reception'];

                                    $this->postService->sendMessageTelegram($messageTelegrame);


                                    break;




                            }


                            break;

                        case "PAYDUNYA":
                        case "Paydunya":
                            break;
                    }
                }

            }
        }

      
        
        return $this->getResponse();
    }

    private function checkGenerateCode($typeCode)
    {
        $codeRetour = null;
        do{

            switch ($typeCode)
            {
                case 'ID':
                case 'Id':
                case 'id':
                    $codeRetour =   'LUNI_ID_'.$this->postService->generateRandom(strlen($this->random('6')));
                    break;

                case 'code':
                case 'CODE':
                    $codeRetour = $this->postService->generateRandom(strlen($this->random('6')));
                    break;
            }
        }while((bool)$this->postService->defaultSelect('transactions',[],['or'=>'or','results'=>['code_transaction'=>$codeRetour,'transaction_id'=>$codeRetour]],null,null,'unique',null,null));

        return $codeRetour;
    }
    private function random($car)
    {
        $string = "";
        $chaine = "1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    public function getRequestsAction($hayshack = null)
    {
        $request = $this->getRequest();
        $getTable = $this->params ()->fromQuery ('table');
        $getTypeRequest = $this->params()->fromQuery('type');

        $datas = $request->getPost()->toArray();
        $response = null;
        $join = null;
        $cond = array();
        $sort = null;

        if($request->isXmlHttpRequest())
        {

            $allDatas = null;
            if($getTable === 'documents')
            {
                $sort = 'statut_document ASC';
            }
            if(isset($datas['pagination']))
            {
                if($datas['pagination']['perpage'] !== null && intval($datas['pagination']['perpage']) < 20)
                {
                    $datas['pagination']['perpage'] = 20;
                }
                else
                {
                    $datas['pagination']['perpage'] = intval($datas['pagination']['perpage']);
                }
            }


            if(isset($_GET['join'],$_GET['cond_join'],$_GET['alias']) && $_GET['join'] !== null)
            {

                $getIfJoinTable = strpos($_GET['join'],'-');
                $getIfCondTable = strpos($_GET['cond_join'],'-');
                $getIfCond = strpos($_GET['cond'],'-');

                $joinTable = array();

                if($getIfJoinTable !== false)
                {
                    $getJoinTable = explode('-',filter_var($_GET['join'],FILTER_SANITIZE_STRING));
                    foreach($getJoinTable as $keys=>$values)
                    {
                        $getJoin = explode(':',$values);


                        $joinTable['alias'][] = $getJoin[0];
                        $joinTable['table'][] = $getJoin[1];

                    }
                }
                else
                {
                    $getJoin = explode(':',$_GET['join']);


                    $joinTable['alias'] = $getJoin[0];

                    $joinTable['table'] = $getJoin[1];



                }

                if($getIfCondTable !== false)
                {
                    $getCondTable = explode('-',$_GET['cond_join']);
                    $join = array();
                    foreach ($getCondTable as $keys=>$elements)
                    {
                        $getCond = explode(':',$elements);

                        for($k=0;$k<count($joinTable['alias']);$k++)
                        {
                            if($keys === $k)
                            {
                                array_push($join,array(
                                    'table'=>array_unique(array($joinTable['alias'][$k]=>$joinTable['table'][$k])),
                                    'condition'=>$getCond[0].'='.$getCond[1]

                                ));
                            }

                        }


                    }


                }
                else
                {
                    $getCond = explode(':',$_GET['cond_join']);

                    $join[] = array(
                        'table'=>array(
                            $joinTable['alias']=>$joinTable['table']
                        ),
                        'condition'=>$getCond[0].'='.$getCond[1]);


                }

                if($getIfCond !== false)
                {
                    foreach($getIfCond as $keys=>$values)
                    {
                        $getCond= explode(':',$values);


                        array_push($cond,array(
                            $getCond[0]=>$getCond[1]
                        ));

                    }
                }
                else
                {
                    $getCond = explode(':',$_GET['cond']);


                    array_push($cond,array(
                        $getCond[0]=>$getCond[1]
                    ));


                }

                $countRow = count($this->postService->defaultSelect(array(
                    $_GET['alias']=>$getTable),[],$cond,null,null,filter_var($getTypeRequest,FILTER_SANITIZE_STRING),null,null));
                $offset = (intval($datas['pagination']['page'])-1) * intval($datas['pagination']['perpage']);

                $limit['end'] = $offset;
                $limit['start'] = $datas['pagination']['perpage'];


                $total_pages = ceil($countRow / intval($datas['pagination']['perpage']) );



                $reqResult = $this->postService->defaultSelect(array(
                    $_GET['alias']=>$getTable),[],$cond,$join,$limit,filter_var($getTypeRequest,FILTER_SANITIZE_STRING),null,$sort);





                $allDatas = array(
                    'meta'=>array(
                        'page'=> isset($datas['pagination']['page']) && !empty($datas['pagination']['page']) ? intval($datas['pagination']['page']): 1,
                        "pages"=> $total_pages,
                        "perpage"=> isset($data['pagination']['perpage']) && !empty($data['pagination']['perpage']) ? intval($data['pagination']['perpage']) : 5,
                        "total"=> $countRow,
                        "sort"=> "asc",
                    ),
                    'data'=>$reqResult
                );

            }



            $response = new JsonModel($allDatas);
            $response->setTerminal(true);


        }
        elseif($request->isPost())
        {
            if($hayshack !== null && is_array($hayshack))
            {
                return $response = array();
            }
        }

        return $response;
    }

    public function fedapayTransactionAction()
    {
        $request = $this->getRequest();
        $view = null;

        if(!$request->isXmlHttpRequest())
        {
            $view = new JsonModel([
                'error'=>"Impossible d'effectuer le traitement demandé"
            ]);
            $view->setTerminal(true);

            return $view;
        }
        $datas = $request->getPost()->toArray();
        $tableJson = array();

        if(!in_array($datas['devise_source']['code_devise'],$this->serviceLocator->get('Config')['devises-fedapay']))
        {
            $view = new JsonModel([
                'error'=>"Impossible d'effectuer le traitement demandé"
            ]);
            $view->setTerminal(true);

            return $view;
        }


        \FedaPay\FedaPay::setApiKey($this->getFedaPayKey($this->serviceLocator->get('Config')['fedapayEnvironment']));
        \FedaPay\FedaPay::setEnvironment($this->serviceLocator->get('Config')['fedapayEnvironment']);


        $transaction = \FedaPay\Transaction::create([

            'description'=>'Transaction de: '.$datas['userDatas']['nom_complet'].'. Achat de '.$datas['devise_destination']['lib_devise'],
            'amount'=>floatval($datas['quantite_a_convertir']),
            'currency'=>[
                'iso'=>'XOF'
            ],
            "callback_url"=>$this->serviceLocator->get('Config')['lienSite'].'customer/checkout/',
            'customer'=>array(
                'firstname'=>isset($datas['userDatas']['prenom']) && $datas['userDatas']['prenom'] !== null && !empty($datas['userDatas']['prenom']) ? $datas['userDatas']['prenom'] : "",
                'lastname'=>isset($datas['userDatas']['nom']) && !empty($datas['userDatas']['nom']) && $datas['userDatas']['nom']!==null ? $datas['userDatas']['nom'] : $datas['userDatas']['nom_complet'],
                'email'=>$datas['userDatas']['email']
                /* 'phone_number'=>[
                    // 'number'=>'+'.$datas['userDatas']['dial_code'].$datas['userDatas']['telephone'],
                    // 'number'=>'+22966000001',
                     //'country'=>'bj'
                 ] */
            )
        ]);

        //die(var_dump($datas));



        if(!empty($transaction))
        {

            $this->sessionContainer->TransactionFedaPay = array(
                'ID'=>$transaction['id']
            );
            $token =  $transaction->generateToken();
            $datasToInsert = [
                'devise_source'=>$datas['devise_source']['id_devise'],
                'devise_destination'=>$datas['devise_destination']['id_devise'],
                'made_by'=>$this->sessionContainer->IdUser,
                'quantite_source'=>$datas['quantite_a_convertir'],
                'quantite_destination'=>$datas['quantite_a_recevoir'],
                'numero_depot'=>"Fedapay",
                'adresse_reception'=>$datas['adresse_reception'],
                'date_transaction'=>$this->DateTime(),
                'transaction_identifier'=>$transaction['id'],
                'type_transaction'=>'FEDAPAY'

            ];

            $reqSaveTransaction = $this->postService->defaultInsert('transactions_temp',$datasToInsert);
            if($reqSaveTransaction)
            {
                $messageTelegrame = "Le client ".$this->sessionContainer->UserName. " a initié une transaction Fedapay.";
                $messageTelegrame .= " Informations de la transaction:  ";
                $messageTelegrame .= "Id transaction Fedapay : ".$transaction['id'].". ";
                $messageTelegrame .= "Context de la transaction:";
                $messageTelegrame .= 'Conversion de '.$datas['devise_source']['lib_devise'].' vers '.$datas['devise_destination']['lib_devise'].".  ";
                $messageTelegrame .= "Date transaction: ".$this->DateTime().".  ";
                $messageTelegrame .= "Montant donné : ".$datas['quantite_a_convertir']." |  ";
                $messageTelegrame .= "Montant demandé : ".$datas['quantite_a_recevoir'].". ";


                $this->postService->sendMessageTelegram($messageTelegrame);

                $this->sessionContainer->TransactionDatas = $datas;
                $tableJson['success'] = $token->url;
            }

        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }

    public function checkoutAction()
    {

        return array();
    }
    public function checkStatusTransactionFedapayAction()
    {
        $request = $this->getRequest();
        $tableJson = null;
        if(!$request->isXmlHttpRequest())
        {
            echo "No action required";
        }

        $checkTransactionTemp = $this->postService->defaultSelect('transactions_temp',[],[
            'made_by'=>$this->currentUser['id'],
            'type_transaction'=>'FEDAPAY'
        ],null,null,'unique',null,null);

        if(!empty($checkTransactionTemp))
        {
            $deviseSource = $this->postService->defaultSelect('devises',[],[
                'id_devise'=>$checkTransactionTemp['devise_source']
            ],null,null,'unique',null,null);
            $deviseDestination = $this->postService->defaultSelect('devises',[],[
                'id_devise'=>$checkTransactionTemp['devise_destination']
            ],null,null,'unique',null,null);

            \FedaPay\FedaPay::setApiKey($this->getFedaPayKey($this->serviceLocator->get('Config')['fedapayEnvironment']));
            \FedaPay\FedaPay::setEnvironment($this->serviceLocator->get('Config')['fedapayEnvironment']);

            $transaction = \FedaPay\Transaction::retrieve($checkTransactionTemp['transaction_identifier']);


            switch($transaction->status)
            {
                case 'approved':

                    $id_transaction = "LUNI_ID_".$this->postService->generateRandom('6');
                    $code_transaction = $this->postService->generateRandom('6');

                    $reqInsert = $this->postService->defaultInsert('transactions',[
                        'context'=>'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].'.',
                        'code_transaction'=>$code_transaction,
                        'date_transaction'=>$this->DateTime(),
                        'made_by'=>$this->sessionContainer->IdUser,
                        'transaction_id'=>$id_transaction,
                        'devise_source'=>$deviseSource['id_devise'],
                        'devise_cible'=>$deviseDestination['id_devise'],
                        'quantite_source'=>$checkTransactionTemp['quantite_source'],
                        'quantite_cible'=>$checkTransactionTemp['quantite_destination'],
                        'numero_envoi'=>'Fedapay',
                        'adresse_reception'=>$checkTransactionTemp['adresse_reception'],
                        'api_transaction'=>$checkTransactionTemp['transaction_identifier'],
                        'type_transaction'=>"FEDAPAY"

                    ]);

                    if(!empty($reqInsert))
                    {


                        $messageTelegrame = "La transaction initiée par le client ".$this->sessionContainer->UserName. " a été traité par Fedapay.";
                        $messageTelegrame .= " Informations de la transaction:  ";
                        $messageTelegrame .= "Id transaction Fedapay : ".$checkTransactionTemp['transaction_identifier'].". ";
                        $messageTelegrame .= "Context de la transaction:";
                        $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                        $messageTelegrame .= "Date transaction: ".$this->DateTime().".  ";
                        $messageTelegrame .= "Montant donné : ".$checkTransactionTemp['quantite_source']." |  ";
                        $messageTelegrame .= "Montant demandé : ".$checkTransactionTemp['quantite_destination'].". ";
                        $messageTelegrame .= "Adresse réception : ".$checkTransactionTemp['adresse_reception'];



                        $this->postService->sendMessageTelegram($messageTelegrame);

                        $this->postService->defaultDelete('transactions_temp',[
                            'transaction_identifier'=>$checkTransactionTemp['transaction_identifier'],
                            'made_by'=>$this->sessionContainer->IdUser
                        ]);

                    }
                    $tableJson['success'] = "success";

                    break;


                case 'declined':
                case 'canceled':

                $this->postService->defaultDelete('transactions_temp',[
                    'transaction_identifier'=>$checkTransactionTemp['transaction_identifier'],
                    'made_by'=>$this->sessionContainer->IdUser,
                    'type_transaction'=>"FEDAPAY"
                ]);

                    $messageTelegrame = "La transaction initiée par le client ".$this->sessionContainer->UserName. " a été décliné/annulé par Fedapay.";
                    $messageTelegrame .= " Informations de la transaction:  ";
                    $messageTelegrame .= "Id transaction Fedapay : ".$checkTransactionTemp['transaction_identifier'].". ";
                    $messageTelegrame .= "Context de la transaction:";
                    $messageTelegrame .= 'Conversion de '.$deviseSource['lib_devise'].' vers '.$deviseDestination['lib_devise'].".  ";
                    $messageTelegrame .= "Date transaction: ".$this->DateTime().".  ";
                    $messageTelegrame .= "Montant donné : ".$checkTransactionTemp['quantite_source']." |  ";
                    $messageTelegrame .= "Montant demandé : ".$checkTransactionTemp['quantite_destination'].". ";
                    $messageTelegrame .= "Adresse réception : ".$checkTransactionTemp['adresse_reception'];



                    $this->postService->sendMessageTelegram($messageTelegrame);
                $tableJson['success'] = "success";

                    break;




            }


        }
        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;
    }
    private function getFedaPayKey($environment,$typeKey= 'secret')
    {

        if($typeKey !== 'secret')
        {
            $key = $this->serviceLocator->get('Config')['fedapayTestKey'];
            if($environment === 'live')
            {
                $key = $this->serviceLocator->get('Config')['fedapayLiveKey'];
            }

            return $key;
        }

        $key = $this->serviceLocator->get('Config')['fedaPaySecretKeys']['sandbox'];

        if($environment === 'live')
        {
            $key = $this->serviceLocator->get('Config')['fedaPaySecretKeys']['live'];
        }


        return $key;
    }

    private function DateTime()
    {
        //$oTimeZoneUtc = new \DateTimeZone('UTC');
        //date_default_timezone_set('UTC');
        $oTimeZoneLocal = new \DateTimeZone(date_default_timezone_get());
        $oDateTime = new DateTime('now', $oTimeZoneLocal);
        return $oDateTime->format('Y-m-d H:i:s');

    }



    public function checkProfilStatusAction()
    {

        $request = $this->getRequest();
        $view = null;
        $tableJson = null;

        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('dashboard');
        }
        if(isset($this->currentUser['codeProfile']) && $this->currentUser['codeProfile'] !== null)
        {
            if($this->currentUser['codeProfile'] !== '2')
            {
                echo 2;
                return $this->getResponse();
            }
            $getUserData = $this->postService->defaultSelect('luni_users',[],array('id_user'=>$this->sessionContainer->IdUser),null,null,'unique',null,null);

            if(!(bool)$getUserData)
            {
                echo 0;
                return $this->getResponse();
            }
            if($getUserData['telephone'] == null || $getUserData['profil_complet'] == '0')
            {
                echo 1;
            }
            else
            {
                echo 0;
            }
        }


        return $this->getResponse();
    }

    public function stdGetAction()
    {
        $request =$this->getRequest ();
        $view = null;
        $tableJson = array();
        $datas = $request->getPost()->toArray();
        if(!$request->isXmlHttpRequest())
        {
            return $this->redirect()->toRoute('dashboard');
        }
        $dataGetTable = $this->params()->fromQuery('table');
        $dataGetType = $this->params()->fromQuery('type');
        $dataGetOrder = null;
        $dataConditions = [];
        if($this->params()->fromQuery('order') !== null || isset($_GET['order']))
        {
            $dataGetOrder = $this->params()->fromQuery('order');
        }
        if(!empty($datas))
        {
            if(isset($datas['update'],$datas['column']))
            {
                foreach($datas['column'] as $keys=>$values)
                {
                    $dataConditions[$keys] = $values;
                }
            }
            else
            {
                foreach ($datas as $keys=>$values)
                {

                    $dataConditions[$keys] = $values;
                }
            }

        }
        $request = $this->postService->defaultSelect ($this->postService->getDbTables ()[$dataGetTable]['table'],[],$dataConditions,null,null,
            $dataGetType,null,$dataGetOrder);
        if(isset($datas['update'], $datas['column']))
        {
            $this->postService->defaultUpdate($dataGetTable,array(
                $datas['update']['column']=>$datas['update']['value']
            ),$dataConditions);
        }

        if(!(bool)$request)
        {
            $tableJson['error'] = "Une erreur s'est produite au cours du traitement.";

            $view = new JsonModel($tableJson);
            return $view;
        }
        if((bool)$request)
        {
            $tableJson = $request;
            $this->sessionContainer->StdGetForUpdate = $request;
        }


        $view = new JsonModel($tableJson);
        $view->setTerminal (true);

        return $view;
    }


}