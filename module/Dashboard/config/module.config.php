<?php

namespace Dashboard;



use Dashboard\Controller\CurrenciesManagementController;
use Dashboard\Controller\CurrencyManagementController;
use Dashboard\Controller\CustomerController;
use Dashboard\Controller\CustomersManagementController;
use Dashboard\Controller\EquivalencesSettingsController;
use Dashboard\Controller\MyProfileController;
use Dashboard\Controller\NumbersManagementController;
use Dashboard\Controller\StatisticsController;
use Dashboard\Controller\TermsOfUseSettingsController;
use Dashboard\Controller\UsersManagementController;
use Dashboard\Factory\MyProfileControllerFactory;
use Laminas\Mvc\Service\ViewHelperManagerFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [

    'router' => [
        'routes' => [
            'customer' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/customer[/:action[/:id]]',
                    'constraints'=>[

                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]*'

                    ],
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/website/home',
                    'defaults' => [
                        'controller' => \Application\Controller\HomeController::class,
                        'action'     => 'index',
                    ],
                ]

            ],
            'logout'=>[
                'type'=> Segment::class,
                'options'=>[
                    'route'=>'/logout',
                    'defaults'=>[
                        'controller'=> \Application\Controller\IndexController::class,
                        'action'=> 'logout'
                    ]
                ],
                'may_terminate'=>true
            ],
            'luni-api' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/luni-api[/:action[/:id]]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => [
                        'controller' => Controller\WebServicesController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'dashboard'=>[
                'type'=> Segment::class,
                'options'=>[
                    'route' => '/dashboard[/:action[/:id]]',
                    'constraints'=> [
                        'action'=>'[a-zA-Z][a-zA-Z0-9_]*',
                        'id'=>'[0-9]+'
                    ],
                    'defaults' =>[
                        'controller'=> Controller\DashboardController::class,
                        'action'=>'index'
                    ]
                ]
            ],
            'transactions'=>[
                'type'=> Segment::class,
                'options'=>[
                    'route'=>'/transactions[/:action[/:id]]',
                    'constraints'=>[
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]+'
                    ],
                    'defaults'=>[
                        'controller'=>Controller\TransactionsController::class,
                        'action'=>'index'
                    ]
                ]
            ],

            'payment-methods'=>[
                'type'=> Segment::class,
                'options'=>[
                    'route'=>'/administration/payment-methods[/:action[/:id]]',
                    'constraints'=>[
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]+'
                    ],
                    'defaults'=>[
                        'controller'=>Controller\PaymentMethodsController::class,
                        'action'=>'index'
                    ]
                ]
            ],

            'inbox'=>[
                'type'=>Segment::class,
                'options'=>[
                    'route'=>'/inbox[/:action[/:id]]',
                    'constraints'=>[
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]+',
                    ],
                    'defaults'=>[
                        'controller'=>Controller\InboxController::class,
                        'action'=>'index'
                    ]
                ],
                'verb'=>'get,post'
            ],
            'requests'=> [
                'type'=>Segment::class,
                'options'=>[
                    'route'=>'/administration/requests[/:action[/:id]]',
                    'constraints'=>[
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]+'
                    ],
                    'defaults'=>[
                        '__NAMESPACE'=>'Dashboard\Controller',
                        'controller'=>Controller\RequestsController::class,
                        'action'=>'index'
                    ]
                ]

            ],
            'users-management'=>[
                'type'=> Segment::class,
                'options'=>array(
                    'route'=>'/administration/users-management[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => Controller\UsersManagementController::class,
                        'action'       => 'index',
                    )
                ),
            ],

            'my-profile'=>[
                'type'=>Segment::class,
                'options'=>array(
                    'route'=>'/my-profile[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => Controller\MyProfileController::class,
                        'action'       => 'index',
                    )
                ),
            ],
            'tableau-de-bord'=>[
                'type'=>Segment::class,
                'options'=>[
                    'route'=>'/dashboard[/:action]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>null
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => Controller\DashboardController::class,
                        'action'       => 'tableau-de-bord',
                    )
                ],
                'may_terminate'=>true
            ],

            'customers-management'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/customers-management[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]*'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' =>'Dashboard\Controller',
                        'controller' => CustomersManagementController::class,
                        'action'  =>'index'
                    )
                ),
                'verb'=>'get,post',
                'may_terminate' =>true
            ),


            'profils-management'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/profils-management[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => UsersManagementController::class,
                        'action'       => 'index',
                    )
                ),
            ),


            'currency-management'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/currency-management[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => CurrencyManagementController::class,
                        'action'       => 'index',
                    )
                ),
            ),

            'currencies-management'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/currencies-management[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => CurrenciesManagementController::class,
                        'action'       => 'index',
                    )
                ),
                'verb'=>'get,post',
                'may_terminate'=>true
            ),

            'numbers-management'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/numbers-management[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => NumbersManagementController::class,
                        'action'       => 'index',
                    )
                ),
                'verb'=>'get,post',
                'may_terminate'=>true
            ),
            'equivalences-settings'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/equivalences-settings[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),
                    'defaults'=>array(
                        '__NAMESPACE__'=>'Dashboard\Controller',
                        'controller'   =>EquivalencesSettingsController::class,
                        'action'       =>'index'
                    )
                ),
                'verb'=>'get,post',
                'may_terminate'=>true
            ),
            'list-fieuls'=>array(
                'type'=>'Literal',
                'options'=>array(
                    'route'=>'/list-fieuls',

                    'defaults'=>array(
                        '__NAMESPACE__'=> 'Dashboard\Controller',
                        'controller'   => CustomerController::class,
                        'action'       => 'list-fieuls',
                    )
                ),
            ),


            'statistics'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/statistics[/:action][/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[a-zA-Z0-9_-]*'
                    ),
                    'defaults'=>array(
                        '__NAMESPACE__'=>'Dashboard\Controller',
                        'controller'=>StatisticsController::class,
                        'action'=>'index'
                    ),
                    'verb'=>'get,post',
                    'may_terminate'=>true
                ),
                'verb'=>'get,post',
                'may_terminate'=>'true'
            ),
            'terms-of-use-settings'=>array(
                'type'=>'segment',
                'options'=>array(
                    'route'=>'/administration/terms-of-use-settings[/:action]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>null
                    ),
                    'defaults'=>array(
                        '__NAMESPACE__'=>'Dashboard\Controller',
                        'controller'=>TermsOfUseSettingsController::class,
                        'action'=>'index'
                    ),
                    'verb'=>'get,post',
                    'may_terminate'=>true
                ),
                'verb'=>'get,post',
                'may_terminate'=>'true'
            ),
        ],
    ],


    'service_manager' => array(
        'factories' =>array(
            'Application\Mapper\PostMapperInterface' => 'Application\Factory\WebServiceDbSqlMapperFactory',
            'Application\Service\PostServiceInterface' => 'Application\Factory\PostServiceFactory',
            'Laminas\Db\Adapter\Adapter'           =>   'Laminas\Db\Adapter\AdapterServiceFactory',
        ),
        'view_helpers' => [
            'aliases' => [
                'viewhelpermanager' => ViewHelperManagerFactory::class,

            ],
            'factories' => [
                ViewHelperManagerFactory::class => InvokableFactory::class,
                MyProfileController::class =>MyProfileControllerFactory::class
            ],
        ],

    ),
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => [
            'dashboard' => __DIR__ . '/../view',
        ],

        'strategies' => array('ViewJsonStrategy'),
    ]

];