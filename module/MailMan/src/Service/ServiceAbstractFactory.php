<?php
/**
 * ZF2 Mail Manager
 *
 * @link        https://github.com/ripaclub/zf2-mailman
 * @copyright   Copyright (c) 2014, RipaClub
 * @license     http://opensource.org/licenses/BSD-2-Clause Simplified BSD License
 */
namespace MailMan\Service;


use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use MailMan\Transport\Factory;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Class ServiceAbstractFactory
 */
class ServiceAbstractFactory implements AbstractFactoryInterface
{
    /**
     * Config Key
     *
     * @var string
     */
    protected $configKey = 'mailman';

    /**
     * Config
     *
     * @var array
     */
    protected $config;

    /**
     * Determine if we can create a service with name
     *
     * @param ContainerInterface $serviceLocator
     * @param $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $serviceLocator, $requestedName)
    {
        $config = $this->getConfig($serviceLocator);


        if (empty($config)) {
            return false;
        }

        $serviceConfig = $this->checkHasRequestedNameConfig($config, $requestedName);
        $transportConfig = $this->checkHasTransportConfig($config, $requestedName);


        return $serviceConfig && $transportConfig;
    }

    /**
     * Create service with name
     *
     * @param ContainerInterface $serviceLocator
     * @param $requestedName
     * @return mixed
     */
    public function createServiceWithName(ContainerInterface $serviceLocator, $requestedName)
    {
        $config = $this->getConfig($serviceLocator)[$requestedName];


        $defaultSender = isset($config['default_sender']) ? $config['default_sender'] : null;
        $aliasSender = isset($config['alias_sender']) ? $config['alias_sender'] : null;
        $transport = Factory::create($config['transport']);

        $serviceClient = new MailService($transport, $defaultSender);
        $serviceClient->setAdditionalInfo(isset($config['additional_info']) ? $config['additional_info'] : []);

        return $serviceClient;
    }

    /**
     * Get model configuration, if any
     *
     * @param ContainerInterface $serviceLocator
     * @return array
     */
    protected function getConfig(ContainerInterface $serviceLocator)
    {
        if ($this->config !== null) {
            return $this->config;
        }

        if (!$serviceLocator->has('Config')) {
            $this->config = [];
            return $this->config;
        }

        $config = $serviceLocator->get('Config');
        if (!isset($config[$this->configKey]) || !is_array($config[$this->configKey])) {
            $this->config = [];
            return $this->config;
        }

        $this->config = $config[$this->configKey];
        return $this->config;
    }

    /**
     * Check if has node config
     *
     * @param $config
     * @param $requestedName
     * @return bool
     */
    protected function checkHasRequestedNameConfig($config, $requestedName)
    {
        if (isset($config[$requestedName]) && is_array($config[$requestedName]) && !empty($config[$requestedName])) {
            return true;
        }
        return false;
    }

    /**
     * Check if has node config
     *
     * @param $config
     * @param $requestedName
     * @return bool
     */
    protected function checkHasTransportConfig($config, $requestedName)
    {
        if (isset($config[$requestedName]['transport'])
            && is_array($config[$requestedName]['transport'])
            && !empty($config[$requestedName]['transport'])
            && isset($config[$requestedName]['transport']['type'])
            && !empty($config[$requestedName]['transport']['type'])
            && is_string($config[$requestedName]['transport']['type'])
        ) {
            return true;
        }
        return false;
    }

    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $this->getConfig($container)[$requestedName];


        $defaultSender = isset($config['default_sender']) ? $config['default_sender'] : null;
        $aliasSender = isset($config['alias_sender']) ? $config['alias_sender'] : null;
        $transport = Factory::create($config['transport']);

        $serviceClient = new MailService($transport, $defaultSender);
        $serviceClient->setAdditionalInfo(isset($config['additional_info']) ? $config['additional_info'] : []);

        return $serviceClient;
    }
}
