<?php
namespace MailMan;
use Laminas\ServiceManager\Factory\InvokableFactory;
use MailMan\Controller\MailManController;

return [

    'controllers' => [
        'factories' => [
            MailManController::class => InvokableFactory::class,
        ],
    ],
    'service_manager' => array(
        'abstract_factories' => array(
            'MailMan\Service\ServiceAbstractFactory',
        )
    ),
    'view_manager' => [
        'template_path_stack' => [
            'mailman' => __DIR__ . '/../view',
        ],
    ],
];