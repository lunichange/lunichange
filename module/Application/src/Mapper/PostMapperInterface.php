<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/11/2020
 * Time: 09:28
 */

namespace Application\Mapper;


interface PostMapperInterface
{

    public function defaultSelect ($table,$columns, $conditions, $joins, $limit, $type, $grpBy, $sort);
    public function defaultInsert($table, $data);
    public function defaultUpdate($table, $data, $condition);
    public function defaultDelete($table, $condition);

    public function userLogin ($login, $pwd);
    public function checkIfUserExist($login);

    public function loadMenuGroup($id_group);
    public function retriveMenu($id_group);

    public function getOneUser($id,$joins);
    public function getDbTables();


    public function listUsers($codeProfil,$idUser);
    public function listOfUsers($user_account,$user_account_search,$user_id,$limit,$order);
    public function listOfMonnaie($search,$limit,$order);
    public function listOfDevises($search,$limit,$order);
    public function listDevises($conditions);
    public function getUser($condition);
    public function listFamilleDevise();
    public function getSpecifiqueFamilleDevise($condition,$type);
    public function getProfilAccordingToProfilOnline($codeProfilOfUserActive);
    public function getAdminUsers($codeProfil);
    public function addUser($data);
    public function getCategorieDevise();
    public function random($car);
    public function generateRandom($car);

    #Fonction de conversion appelÃ©e dans la feuille principale
    public function Conversion($sasie);



    #Gestion des millions

        public  function Million($nombre,$unite,$dizaine,$centaine,$mille,$million);

    #Gestion des milles

        public function Mille($nombre,$unite,$dizaine,$centaine,$mille);

    #Gestion des centaines

    /**
     * @param $inmillier
     * @param $nombre
     * @param $unite
     * @param $dizaine
     * @param $centaine
     */
    public  function Centaine($inmillier, $nombre, $unite, $dizaine, $centaine);


#Gestion des dizaines

    public function Dizaine($inmillier,$nombre);
#Gestion des unitÃ©s

    public function Unite($unite);
    public function password_strength($password);
    public function sendMail($message, $key,$token,$destinataire,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template);

    public function sendMessageTelegram($message);



}