<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/11/2020
 * Time: 09:29
 */

namespace Application\Mapper;


use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Sql\Expression;
use Laminas\Hydrator\HydratorInterface;
use Laminas\Log\Logger;
use Laminas\Log\Writer\Stream;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplateMapResolver;
use MailMan\Message as MailManMessage;

class WebServiceDbSqlMapper extends AbstractServicesDbSqlMapper implements PostMapperInterface
{
    protected $dbAdapter;
    protected $hydrator;
    protected $dbTables;
    protected $leChiffreSaisi;
    protected $enLettre='';
    protected $chiffre=array(1=>"un",2=>"deux",3=>"trois",4=>"quatre",5=>"cinq",6=>"six",7=>"sept",8=>"huit",9=>"neuf",10=>"dix",11=>"onze",12=>"douze",13=>"treize",14=>"quatorze",15=>"quinze",16=>"seize",17=>"dix-sept",18=>"dix-huit",19=>"dix-neuf",20=>"vingt",30=>"trente",40=>"quarante",50=>"cinquante",60=>"soixante",70=>"soixante-dix",80=>"quatre-vingt",90=>"quatre-vingt-dix");

    protected $sql;

    protected $postPrototype;
    protected $serviceLocator;
    const PASSWORD_LENGTH_LETTER = "six";
    const PASSWORD_LENGTH_DIGIT = 6;
    const MESSAGE_FOR_PASSWORD_ERROR = "Le mot de passe doit comporter au moins six(06) caractères, contenir au moins un chiffre.";
    const MAIL_SEND_FROM = "contact@lunichange.com";
    const FROM_ID = "LUNICHANGE - ADMIN";
    public function __construct(AdapterInterface $dbAdapter, HydratorInterface $hydrator,$container)
    {
        $this->dbAdapter = $dbAdapter;
        $this->hydrator = $hydrator;
        $this->serviceLocator = $container;
        $this->dbTables = array(
        'customers'=>array(
            'table'=>'luni_users',
            'columns'=>array(
                'id_user'=>'id_user',
                'nom_user'=>'nom',
                'prenoms_user'=>'prenoms',
                'email_user'=>'email',
                'password_user'=>'password',
                'date_creation'=>'date_creation',
                'last_connected'=>'derniere_connexion',
                'online'=>'online',
                'lock_u'=>'lock_u',
                'code_profil'=>'code_profil',
                'statut'=>'statut',
                'derniere_deconnexion'=>'derniere_deconnexion'
            )
        ),
        'documents'=>array(
            'table'=>'documents',
            'columns'=>array(
                'id_document'=>'id_document',
                'type_document'=>'type_document',
                'chemin_document'=>'chemin_document',
                'numero_piece'=>'numero_piece',
                'document'=>'document',
                'id_user'=>'id_user',
                'lock_doc'=>'lock_doc',
                'statut'=>'statut'
            )
        ),
        'pays'=>array(
            'table'=>'pays',
            'columns'=>array(
                'id_pays'=>'id_pays',
                'lib_pays'=>'lib_pays',
                'code_pays'=>'code_pays',
                'statut'=>'statut'
            )
        ),
        'access_menu'=>array(
            'table'=>'luni_access_menu',
            'columns'=>array(
                'id_ac_menu'=>'id_ac_menu',
                'id_menu'=>'id_menu',
                'code_profil'=>'code_profil',
                'statut'=>'statut'
            )
        ),
        'menu'=>array(
            'table'=>'luni_menu',
            'columns'=>array(
                'id_menu'=>'id_menu',
                'label_menu'=>'label_menu',
                'link'=>'link',
                'controller'=>'controller',
                'icon'=>'icon',
                'ranking'=>'ranking'
            )
        ),

        'categorie_devise'=>array(
            'table'=>'categorie_devise',
            'columns'=>array(
                'id_cat_devise'=>'id_categorie_devise',
                'lib_cat_devise'=>'lib_categorie_devise',
                'statut'=>'statut'
            )
        ),

        'devises'=>array(
            'table'=>'devises',
            'columns'=>array(
                'id_devise'=>'id_devise',
                'lib_devise'=>'lib_devise',
                'img_devise'=>'image_devise',
                'cat_devise'=>'categorie_devise',
                'code_devise'=>'code_devise',
                'statut'=>'statut',
                'famille_devise'=>'famille_devise'
            )
        ),
        "luni_monnaies"=>array(

            'table'=>'luni_monnaies',
            'columns'=>array(
                'id'=>'id_famille',
                'libelle'=>'libelle_famille',
                'capitalize'=>'capitalize',
                'categorie_devise'=>'categorie_devise',
                'type_monnaie'=>'type_monnaie',
                'img_famille'=>'img_famille',
                'statut'=>'statut'
            )
        ),
        'type_monnaie'=>array(
            'table'=>'luni_type_monnaie',
            'columns'=>array(
                'id_type_monnaie'=>'id_type_monnaie',
                'lib_type_monnaie'=>'lib_type_monnaie',
                'code_type_monnaie'=>'code_type_monnaie'
            )
        ),
        'transactions_temp'=>array(
            'table'=>'transactions_temp',
            'columns'=>array(
                'id'=>'id_transaction',
                'devise_source'=>'devise_source',
                'devise_destination'=>'devise_destination',
                'quantite_source'=>'quantite_source',
                'quantite_destination'=>'quantite_destination',
                'numero_depot'=>'numero_depot',
                'adresse_reception'=>'adresse_reception',
                'made_by'=>'made_by'
            )
        ),
        'equivalences'=>array(
            'table'=>'equivalences',
            'columns'=>array(
                'id_equivalence'=>'id_equivalence',
                'source'=>'source',
                'destination'=>'destination',
                'taux'=>'taux',
                'solde_minimum'=>'quantite_minimum'
            )
        ),
        'equivalence_globale'=>array(
            'table'=>'equivalence_globale',
            'columns'=>array(
                'id_equiv'=>'id_equiv',
                'devise'=>'devise',
                'prix_achat'=>'prix_achat',
                'prix_vente'=>'prix_vente'
            )
        ),
        'profil_user'=>array(
            'table'=>'profil_user',
            'columns'=>array(
                'id_profil'=>'id_profil',
                'lib_profil'=>'lib_profil',
                'code_profil'=>'code_profil',
                'access_level'=>'access_level'
            )
        ),
        'devises_input'=>array(
            'table'=>'devises_input',
            'columns'=>array(
                'id_devise_input'=>'id_devise_input',
                'id_devise'=>'id_devise',
                'type_input'=>'type_input'
            )
        ),
        'numeros_transactions'=>array(
            'table'=>'numeros_transactions',
            'columns'=>array(
                "id_numero"=>'id_numero',
                'libelle_numero'=>'libelle_numero',
                'status'=>'status',
                'devise'=>'devise',
                'number_name'=>'number_name'
            )
        ),
        'transactions'=>array(
            'table'=>'transactions',
            'columns'=>array(
                'id_transaction'=>'id_transaction',
                'context'=>'context',
                'code_transaction'=>'code_transaction',
                'devise_source'=>'devise_source',
                'devise_cible'=>'devise_cible',
                'date_transaction'=>'date_transaction',
                'numero_envoi'=>'numero_envoi',
                'adresse_reception'=>'adresse_reception',
                'quantite_source'=>'quantite_source',
                'quantite_cible'=>'quantite_cible'
            )
        ),
        'messages_site'=>array(
            'table'=>'messages_site',
            'columns'=>array(
                'id_message'=>'id_message',
                'contenu'=>'contenu',
                'email'=>'email',
                'subject'=>'subject',
                'customer'=>'customer',
                'contact'=>'contact',
                'statut'=>'statut'
            )
        )
    );
        if($this->sql == null)
        {
            $this->sql = new Sql($this->dbAdapter);
        }
    }

    public function defaultSelect ($table, $columns, $conditions, $joins, $limit, $type, $grpBy, $sort)
    {
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($table);
        if(is_array ($columns) && !empty($columns) && $columns !== NULL)
        {
            $select->columns ($columns);
        }


        if($joins)
        {
            if(isset($joins['type'],$joins['data']))
            {

                foreach($joins['data'] as $join)
                {
                    $select->join($join['table'],$join['condition'],array(),$joins['type']);
                }

            }
            else
            {
                foreach($joins as $join)
                {
                    $select->join($join['table'],$join['condition']);
                }
            }

        }
        if($grpBy)
        {
            $select->group($grpBy);
        }
        if($conditions)
        {
            $cond = array();
            if(is_array ($conditions))
            {
                $elementIsArray = false;

                if(!isset($conditions['or']))
                {
                    foreach ($conditions as $keys=>$elements)
                    {
                        if(is_array ($conditions[$keys]))
                        {
                            $elementIsArray = true;
                        }
                    }
                }
                if($elementIsArray)
                {
                    foreach ($conditions as $keys=>$elements)
                    {
                        foreach ($elements as $kk=>$values)
                        {
                            $cond[$kk][] = $values;
                        }
                    }
                }
                else
                {
                    $cond = $conditions;
                }

                if(isset($cond['or'],$cond['results']) && !empty($cond['or']))
                {
                    $select->where($cond['results'],PredicateSet::OP_OR);
                }
                else
                {
                    $select->where($cond);
                }
                //$select->where($cond);
            }

        }
        if($sort)
        {
            $select->order($sort);
        }
        if($limit)
        {
            if(isset($limit['start'],$limit['end']))
            {
                //
                $select->limit(intval($limit['start']));
                $select->offset (intval ($limit['end']));
            }
            else
            {
                $select->limit(intval($limit));
            }




        }

        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($type == 'unique')
        {
            if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
                return $result->current();
            }
            return array();
        }
        else
        {
            if ($result instanceof ResultInterface && $result->isQueryResult()) {
                $resultSet = new ResultSet();
                $resultSet->initialize($result);
                return $resultSet->toArray();
            }
            return array();
        }
    }

    public function getDbTables()
    {
        return (array) $this->dbTables;
    }
    public function defaultUpdate ($table, $data, $conditions)
    {

        $sql = new Sql($this->dbAdapter);
        $update = $sql->update($table);
        $update->set($data);
        $update->where($conditions);
        $stmt= $sql->prepareStatementForSqlObject($update);
        $stmt->execute();
        return 1;
    }

    public function defaultInsert ($table, $data)
    {
        $sql = new Sql($this->dbAdapter);
        $data_in = filter_var_array($data, FILTER_SANITIZE_STRING);
        $insert = $sql->insert($table);
        $insert->values($data_in);
        $stmt= $sql->prepareStatementForSqlObject($insert);
        $stmt->execute();

        return 1;
    }

    public function defaultDelete ($table, $conditions)
    {
        // TODO: Implement defaultDelete() method.
        $sql = new Sql($this->dbAdapter);
        $delete = $sql->delete($table);
        $delete->where($conditions);
        $stmt = $sql->prepareStatementForSqlObject($delete);
        $stmt->execute();
        return 1;
    }


    public function userLogin ($login, $pwd)
    {

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['customers']['table']);
        $select->columns(array('num' => new Expression('COUNT(*)'), 'id_user'=>'id_user', 'password'=>'password'));
        $select->where(array("email=?" => $login));
        $select->where(array("lock_u=?"=>'0', "statut=?"=>'1'), PredicateSet::OP_AND);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();


        if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {

            $alldata = $result->current();

            if (intval($alldata['num'])) {

                $passcheck = password_verify($pwd, $alldata['password']);

                if ($passcheck) {
                    $select = $sql->select(array('u'=>$this->dbTables['customers']['table']));
                    $select->join(array('p'=>$this->dbTables['profil_user']['table']),'u.code_profil= p.code_profil');
                    $select->where(array('u.id_user=?' => $alldata['id_user']));
                    $stmt = $sql->prepareStatementForSqlObject($select);
                    $result = $stmt->execute();

                    if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
                        $r = $result->current();
                        return array(
                            'message'=>'',
                            'id_user'=>$r['id_user'],
                            'profil_name'=>$r['lib_profil'],
                            'code_profil'=>$r['code_profil'],
                            'login'=>$r['email'],
                            'user_name'=>$r['nom_complet']
                        );
                    }
                }
                return array(
                    'message'=>'D&eacute;sol&eacute; mot de passe incorrect.',
                    'user_data'=>array()
                );

            } else {
                return array(
                    'message'=>'Vos identifiants sonts incorrectes ou votre compte n\'est pas actif.',
                    'user_data'=>array()
                );
            }

        } else {
            return array(
                'message'=>'Vos identifiants sonts incorrectes ou votre compte n\'est pas actif.',
                'user_data'=>array()
            );
        }
    }

    public function checkIfUserExist($login)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select ($this->dbTables['customers']['table']);
        $select->where(array(

            $this->dbTables['customers']['columns']['email_user'].'=?'=>filter_var (trim($login),FILTER_VALIDATE_EMAIL),

        ));
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        $returnValue = false;
        if($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows())
        {
            $returnValue = (bool) $result->current ();
        }

        return $returnValue;
    }

    public function loadMenuGroup($id_group)
    {
        $temp_hold = array();
        $sql = new Sql($this->dbAdapter);

        $select2 = $sql->select($this->dbTables['access_menu']['table']);
        $select2->where(array(
            $this->dbTables['access_menu']['columns']['statut'] =>'1',
            $this->dbTables['access_menu']['columns']['code_profil'] =>intval($id_group)
        ));

        $stmt2 = $sql->prepareStatementForSqlObject($select2);
        $result2 = $stmt2->execute();
        if ($result2 instanceof ResultInterface && $result2->isQueryResult()) {
            $resultSet2 = new ResultSet();
            $resultSet2->initialize($result2);
            foreach ($resultSet2->toArray() as $eachto) {
                $temp_hold[$eachto['id_menu']] = intval($eachto['id_menu']);
            }
        }
        return $temp_hold;
    }
    public function retriveMenu($id_group)
    {
        $id_group = intval ($id_group);

        $outarray = array();

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select('luni_menu');
        $select->where(array('statut' => '1', 'level' => 0));
        $select->order('ranking');

        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $temp_hold = $this->loadMenuGroup($id_group);


        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);



            foreach ($resultSet->toArray() as $eachtopmenu) {

                $obj = new \stdClass();
                $obj->id = $eachtopmenu['id_menu'];
                $obj->label = $eachtopmenu['label_menu'];
                $obj->link = $eachtopmenu['link'];
                $obj->controller = $eachtopmenu['controller'];
                $obj->css_id = $eachtopmenu['css_id'];
                $obj->icon = $eachtopmenu['icon'];
                $obj->child = array();

                $eachtopmenu['id_menu'] = intval ($eachtopmenu['id_menu']);




                if (in_array(intval($eachtopmenu['id_menu']), $temp_hold))
                    $outarray[$eachtopmenu['id_menu']] = $obj;




                $select2 = $sql->select('luni_menu');
                $select2->where(array('statut' =>'1', 'level' => $eachtopmenu['id_menu']));
                $select2->order('ranking');

                $stmt2 = $sql->prepareStatementForSqlObject($select2);
                $result2 = $stmt2->execute();


                if ($result2 instanceof ResultInterface && $result2->isQueryResult()) {
                    $resultSet2 = new ResultSet();
                    $resultSet2->initialize($result2);
                    $arrt = $resultSet2->toArray();

                    foreach ($arrt as $eachchildmenu) {
                        if ($id_group) {
                            if (!in_array($eachchildmenu['id_menu'], $temp_hold))
                                continue;
                        }
                        $obj2 = new \stdClass();
                        $obj2->id = $eachchildmenu['id_menu'];
                        $obj2->label = $eachchildmenu['label_menu'];
                        $obj2->link = $eachchildmenu['link'];
                        $obj2->controller = $eachchildmenu['controller'];
                        $obj2->css_id = $eachchildmenu['css_id'];
                        $obj2->icon = $eachchildmenu['icon'];
                        $obj->child[] = $obj2;
                    }
                    if ((!empty($obj->child)))
                        $outarray[$eachtopmenu['id_menu']] = $obj;
                }

            }
            return $outarray;
        }
        return false;

    }

    public function getOneUser($id,$join)
    {
        return $this->defaultSelect(array('u'=>$this->getDbTables()['customers']['table']),[],array('id_user'=>$id),$join,null,'unique',null,null);
    }



    public function listUsers($codeProfil,$idUser)
    {

        $resultRequest = array();
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select($this->dbTables['customers']['table']);

        if($codeProfil != "all" && $codeProfil !== null)
        {
            $select->where(array($this->dbTables['customers']['columns']['code_profil']=>$codeProfil,$this->dbTables['customers']['columns']['id_user'].'<>?'=>$idUser));

        }
        elseif($codeProfil === null)
        {
            $select->where(array($this->dbTables['customers']['columns']['id_user'].'<>?'=>$idUser));


        }

        elseif($idUser== null)
        {

            $select->where(array($this->dbTables['customers']['columns']['code_profil'].'>=?'=>$codeProfil));

        }


        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }

        return $resultRequest;
    }

    public function listOfUsers($user_account,$user_account_search,$user_id,$limit,$order)
    {
        if($user_account !== null)
        {
            $resultRequest = array();
            $sql = new Sql($this->dbAdapter);
            $column = array('fullName', 'email', 'id', 'derniere_connexion', 'online', 'lock_u');


            $select = $sql->select($this->dbTables['customers']['table']);
            $select->columns(
                array(
                    'fullName'=>new Expression("CONCAT(nom,' ',prenoms)"),
                    'email'=>'email',
                    'id'=>'id_user',
                    'derniere_connexion'=>'derniere_connexion',
                    'online'=>'online',
                    'lock_u'=>'lock_u'

                )
            );
            if($user_account == 0 || $user_account == 1)
            {
                if($user_account_search == null || $user_account == 'all')
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>$user_account,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }
                else
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>$user_account_search,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }
            }
            else
            {
                if($user_account_search == null || $user_account == 'all')
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>2,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }
                else
                {
                    $select->where(
                        array(
                            $this->dbTables['customers']['columns']['id_user'].'<>?'=>$user_id,
                            $this->dbTables['customers']['columns']['code_profil'].'>=?'=>$user_account_search,
                            $this->dbTables['customers']['columns']['statut']=>'1'
                        )

                    );
                }


            }


            if($order !== null)
            {
                $select->order($column[$order['0']['column']].' '.$order['0']['dir'].' ');
            }
            else
            {
                $select->order('fullName ASC');
            }

            if($limit !== null)
            {
                $select->limit(intval($limit['end']))->offset(intval($limit['start']));
            }
            $stmt = $sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            if($result instanceof ResultInterface && $result->isQueryResult())
            {
                $resultSet = new ResultSet();
                $resultSet->initialize($result);
                $resultRequest = $resultSet->toArray();
            }

            return $resultRequest;
        }

    }

    public function listOfMonnaie($search,$limit,$order)
    {
        $resultRequest = array();
        $columns = array('id_famille','libelle_famille','categorie_devise','type_monnaie','statut');

        if(is_array($limit) && $limit !== null)
        {
            foreach ($limit as $keys=>$values)
            {
                $limit[$keys] = intval($values);
            }
        }
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['familles_devise']['table']);
        $select->columns(array(
            'id'=>'id_famille',
            'libelle'=>'libelle_famille',
            'capitalize'=>'capitalize',
            'categorie_devise'=>'categorie_devise',
            'type_monnaie'=>'type_monnaie',
            'statut'=>'statut'
        ));
        if($limit !== null)
        {
            $select->limit($limit['end'])->offset($limit['start']);
        }

        if($order !== null)
        {
            $select->order($columns[$order['0']['column']].' '.$order['0']['dir'].' ');
        }
        else
        {
            $select->order($this->dbTables['familles_devise']['columns']['libelle'].' ASC');
        }
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }
        return $resultRequest;

    }
    public function listOfDevises($search,$limit,$order)
    {
        $resultRequest = array();
        $column = array('lib_devise', 'image_devise', 'id_devise', 'categorie_devise', 'solde', 'solde_minimum','statut');
        if(is_array($limit) && $limit !== null)
        {
            foreach ($limit as $keys=>$values)
            {
                $limit[$keys] = intval($values);
            }
        }
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['devises']['table']);
        $select->columns(array(

            'id'=>'id_devise',
            'libelle'=>'lib_devise',
            'image'=>'image_devise',
            'categorie'=>'categorie_devise',
            'statut'=>'statut',
            'solde'=>'solde',
            'solde_min'=>'solde_minimum'
        ));

        if($limit !== null)
        {
            $select->limit($limit['end'])->offset($limit['start']);
        }

        if($order !== null)
        {
            $select->order($column[$order['0']['column']].' '.$order['0']['dir'].' ');
        }
        else
        {
            $select->order('lib_devise ASC');
        }


        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }

        return $resultRequest;


    }

    public function listDevises($conditions)
    {
        $resultRequest = array();

        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['devises']['table']);
        if(isset($conditions) && $conditions !== null)
        {
            $select->where($conditions);
        }
        $select->where(array(
            'statut'=>'1'
        ));
        $select->columns(array(

            'id'=>'id_devise',
            'libelle'=>'lib_devise',
            'image'=>'image_devise',
            'categorie'=>'categorie_devise',
            'statut'=>'statut',
            'solde'=>'solde',
            'solde_min'=>'solde_minimum'
        ));


        $select->order('lib_devise ASC');



        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }

        return $resultRequest;

    }

    public function getUser($condition)
    {

        $tableRequest = [];
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->getDbTables()['customers']['table']);

        if(is_array($condition) && !empty($condition))
        {

            $select->where($condition);

        }
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {
            $tableRequest = $result->current();
        }
        return $tableRequest;
    }

    public function listFamilleDevise()
    {
        $returnRequest = array();
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['familles_devise']['table']);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $returnRequest = $resultSet->toArray();
        }

        return $returnRequest;
    }

    public function getSpecifiqueFamilleDevise($condition,$type)
    {
        $returnRequest = array();
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['familles_devise']['table']);
        if(is_array($condition))
        {
            $select->where($condition);
        }
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof  ResultInterface && $result->isQueryResult())
        {

            if($type == "all")
            {
                $resultSet = new ResultSet();
                $resultSet->initialize($result);
                $returnRequest = $resultSet->toArray();
            }
            elseif($type == "unique")
            {
                $returnRequest = $result->current();
            }

        }

        return $returnRequest;
    }

    /**
     * @param $codeProfilOfUserActive
     * @return array
     * @throws \Exception
     */
    public function getProfilAccordingToProfilOnline($codeProfilOfUserActive)
    {
        $codeProfilOfUserActive = intval($codeProfilOfUserActive);
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['profil_user']['table']);
        $resultRequest = array();
        switch ($codeProfilOfUserActive)
        {
            case 0:
                $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'>?'=>0));
                break;
            case 1:
                $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'>?'=>1));
                break;
            default:
                $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'<>?'=>2));

        }
        $select->where(array($this->dbTables['profil_user']['columns']['code_profil'].'<>?'=>2));
        $select->order($this->dbTables['profil_user']['columns']['lib_profil']);
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if ($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }
        return $resultRequest;
    }


    public function getAdminUsers($codeProfil)
    {
        $codeProfilOfUserActive = intval($codeProfil);
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['customers']['table']);
        $resultRequest = array();
        switch ($codeProfilOfUserActive)
        {
            case 0:
                $select->where(array($this->dbTables['customers']['columns']['code_profil'].'>?'=>0));
                break;
            case 1:
                $select->where(array($this->dbTables['customers']['columns']['code_profil'].'>?'=>1));
                break;
            default:
                $select->where(array($this->dbTables['customers']['columns']['code_profil'].'<>?'=>2));

        }


        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();
        if ($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $resultRequest = $resultSet->toArray();
        }
        return $resultRequest;
    }

    public function addUser($data)
    {
        $sql = new Sql($this->dbAdapter);
        $dataToInsert = filter_var_array($data,FILTER_SANITIZE_STRING);

        $insert = $sql->insert($this->dbTables['customers']['table']);
        $insert->values($dataToInsert);
        $stmt = $sql->prepareStatementForSqlObject($insert);
        $stmt->execute();


        return 1;
    }

    public function getCategorieDevise()
    {
        $tableReturn = array();
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select($this->dbTables['categorie_devise']['table']);
        $select->where([
            $this->dbTables['categorie_devise']['columns']['statut']=>"1"
        ]);
        $select->order($this->dbTables['categorie_devise']['columns']['lib_cat_devise'].' ASC');
        $stmt = $sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if($result instanceof ResultInterface && $result->isQueryResult())
        {
            $resultSet = new ResultSet();
            $resultSet->initialize($result);
            $tableReturn = $resultSet->toArray();
        }
        return $tableReturn;
    }


    public function password_strength($password)
    {
        $password_length = 6;

        $returnVal = True;

        if ( strlen($password) < $password_length ) {
            $returnVal = False;
        }

        if ( !preg_match("#[0-9]+#", $password) ) {
            $returnVal = False;
        }

        if ( !preg_match("#[a-z]+#", $password) ) {
            $returnVal = False;
        }

        /*if ( !preg_match("#[A-Z]+#", $password) ) {
            $returnVal = False;
        }*/

        /*if ( !preg_match("/[\'^£$%&*()}{@#~?><>,|=_+!-]/", $password) ) {
            $returnVal = False;
        }*/

        return $returnVal;

    }


    public function sendMail($message, $key,$token,$destinataire,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template)
    {
        //Envoie des notifications mail
        $incMail = 0;


        try {
            //Les paramètres d'envoie
            $template_array = [];
            $template_array['key'] = $key;
            $template_array['token'] = $token;
            $template_array['btn_url'] = $btn_url;
            $template_array['btn_label'] = $btn_label;
            $template_array['message'] = $message;
            $template_array['nom_user'] = $destinataire;
            $template_array['titre'] = $titre;


            //Envoie du mail de notification
            $mailService = $this->serviceLocator->get('MailMan\LUNI');
            $view       = new PhpRenderer();
            $resolver   = new TemplateMapResolver();
           // $include  = include_once(__DIR__ . '\..\..\view\mails\\'.$template.'.phtml');

            $resolver->setMap(array(
                 $template =>$this->serviceLocator->get('Config')["main_path"].'module/Application/view/mails/'.$template.'.phtml'
                 
            ));
            $view->setResolver($resolver);

            $content  = new ViewModel();
            $content->setTemplate($template)->setVariables($template_array);
            $content->setVariables($template_array);
            $message = new MailManMessage();
            $message->setSubject($subject);
            $message->addHtmlPart($view->render($content));
            $message->addTo($email);
            $message->addFrom($from_mail, $from_id);
            $res = $mailService->send($message);
           /* $logMessage = "Succès Envoi de mail à " . $destinataire . "  au mail " . $email . " le" . date("d-m-Y H:m:s") . " Résultat : " . $res;
            $this->mailLog($logMessage);*/
        } catch (\Exception $ex) {

           /* $logMessage = "Erreur Envoi de mail à " .$destinataire . " au mail " . $email . " le" . date("d-m-Y H:m:s") . " Résultat : " . $ex;*/
           // $this->mailLog($logMessage);
        }

    }

    public function sendMessageTelegram($message)
    {
      
        
         
              $telegrambot = "1477791644:AAGnbXBPsnBZ135rKH4kry7elLA0bYl60Ww"; 
              $telegramchatid = "-1001408210866";
              $url='https://api.telegram.org/bot'.$telegrambot.'/sendMessage';$data=array('chat_id'=>$telegramchatid,'text'=>$message);
              $options=array('http'=>array('method'=>'POST','header'=>"Content-Type:application/x-www-form-urlencoded\r\n",'content'=>http_build_query($data),),);
            $context=stream_context_create($options);
            $result=file_get_contents($url,false,$context);
          
        return $result;
    }
  


    public function mailRapport($messageMail, $destinateurMail, $destinateurNom, $destinataireMail)
    {
        $mailService = $this->serviceLocator->get('MailMan\HBD');
        $message = new MailManMessage();
        $message->addTextPart($messageMail);
        $message->setSubject('RAPPORT HBD MTN DU ' . date('d-m-Y'));
        $message->addFrom($destinateurMail, $destinateurNom);
        $message->addTo($destinataireMail);
        $mailService->send($message);
        $message = null;
    }

    /**
     * Log des mails
     * @param null $message
     */
    private function mailLog($message = null)
    {
        if (isset($message)) {
            $config = $this->serviceLocator->get('Config');
            $log_path = $config['log_path'];
            $logger = new Logger();
            $write = new Stream($log_path . 'mail.log');
            $logger->addWriter($write);
            $logger->log(Logger::ALERT, 'MAIL MESSAGE');
            $message = $message . " \n";
            $logger->alert($message);
        }
    }

    public function generateRandom($car)
    {
        $string = "";
        $chaine = "1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        $stringChaine = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $myString ="";
        srand((double)microtime()*1000000);
        for($i=0; $i<4; $i++) {
            $myString .= $stringChaine[rand()%strlen($stringChaine)];
        }

        return $myString."".$string;
    }



    public function random($car)
    {
        $string = "";
        $chaine = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }










    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
       return $id;
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */




#Fonction de conversion appelÃ©e dans la feuille principale
    public function Conversion($sasie)
    {
        $this->enLettre='';
        $sasie=trim($sasie);

#suppression des espaces qui pourraient exister dans la saisie
        $nombre='';
        $laSsasie=explode(' ',$sasie);
        foreach ($laSsasie as $partie)
            $nombre.=$partie;

#suppression des zÃ©ros qui prÃ©cÃ©deraient la saisie
        $nb=strlen($nombre);
        for ($i=0;$i<=$nb;)
        {
            if(substr($nombre,$i,1)==0)
            {
                $nombre=substr($nombre,$i+1);
                $nb=$nb-1;
            }
            elseif(substr($nombre,$i,1)<>0)
            {
                $nombre=substr($nombre,$i);
                break;
            }
        }
#echo $nombre;
#$this->SupZero($nombre);
#le nombre de caract que comporte le nombre saisi de sa forme sans espace et sans 0 au dÃ©but
        $nb=strlen($nombre);

#conversion du chiffre saisi en lettre selon les cas
        switch ($nb)
        {
            case 0:
                $this->enLettre='zÃ©ro';
                break;
            case 1:
                if ($nombre==0)
                {
                    $this->enLettre='zÃ©ro';

                }
                else
                {
                    $this->Unite($nombre);

                }
                break;

            case 2:
                $unite=substr($nombre,1);
                $dizaine=substr($nombre,0,1);
                $this->Dizaine(0,$nombre);
                break;

            case 3:
                $unite=substr($nombre,2);
                $dizaine=substr($nombre,1,1);
                $centaine=substr($nombre,0,1);
                $this->Centaine(0,$nombre,$unite,$dizaine,$centaine);
                break;

#cas des milles
            case ($nb>3 and $nb<=6):
                $unite=substr($nombre,$nb-1);
                $dizaine=substr($nombre,($nb-2),1);
                $centaine=substr($nombre,($nb-3),1);
                $mille=substr($nombre,0,($nb-3));
                $this->Mille($nombre,$unite,$dizaine,$centaine,$mille);
                break;

#cas des millions
            case ($nb>6 and $nb<=9):
                $unite=substr($nombre,$nb-1);
                $dizaine=substr($nombre,($nb-2),1);
                $centaine=substr($nombre,($nb-3),1);
                $mille=substr($nombre,-6);
                $million=substr($nombre,0,$nb-6);
                $this->Million($nombre,$unite,$dizaine,$centaine,$mille,$million);
                break;

#cas des milliards
            /*case ($nb>9 and $nb<=12):
            $unite=substr($nombre,$nb-1);
            $dizaine=substr($nombre,($nb-2),1);
            $centaine=substr($nombre,($nb-3),1);
            $mille=substr($nombre,-6);
            $million=substr($nombre,-9);
            $milliard=substr($nombre,0,$nb-9);
            Milliard($nombre,$unite,$dizaine,$centaine,$mille,$million,$milliard);
            break;*/

        }

        return !empty($this->enLettre)? $this->enLettre : null;


    }

#Gestion des miiliards
    /*
    function Milliard($nombre,$unite,$dizaine,$centaine,$mille,$million,$milliard)
    {

    }
    */

#Gestion des millions

    public  function Million($nombre,$unite,$dizaine,$centaine,$mille,$million)
    {
#si les mille comportent un seul chiffre
#$cent represente les 3 premiers chiffres du chiffre ex: 321 dans 12502321
#$mille represente les 3 chiffres qui suivent les cents ex: 502 dans 12502321
#reste represente les 6 premiers chiffres du chiffre ex: 502321 dans 12502321

        $cent=substr($nombre,-3);
        $reste=substr($nombre,-6);

        if (strlen($million)==1)
        {
            $mille=substr($nombre,1,3);
            $this->enLettre.=$this->chiffre[$million];
            if ($million == 1){
                $this->enLettre.=' million ';
            }else{
                $this->enLettre.=' millions ';
            }
        }
        elseif (strlen($million)==2)
        {
            $mille=substr($nombre,2,3);
            $nombre=substr($nombre,0,2);
//echo $nombre;
            $this->Dizaine(0,$nombre);
            $this->enLettre.='millions ';
        }
        elseif (strlen($million)==3)
        {
            $mille=substr($nombre,3,3);
            $nombre=substr($nombre,0,3);
            $this->Centaine(0,$nombre,$unite,$dizaine,$centaine);
            $this->enLettre.='millions ';
        }

#recuperation des cens dans nombre

#suppression des zÃ©ros qui prÃ©cÃ©deraient le $reste
        $nb=strlen($reste);
        for ($i=0;$i<=$nb;)
        {
            if(substr($reste,$i,1)==0)
            {
                $reste=substr($reste,$i+1);
                $nb=$nb-1;
            }
            elseif(substr($reste,$i,1)<>0)
            {
                $reste=substr($reste,$i);
                break;
            }
        }
        $nb=strlen($reste);
#si tous les chiffres apres les milions =000000 on affiche x million
        if ($nb==0)
            ;
        else
        {
#Gestion des milles
#suppression des zÃ©ros qui prÃ©cÃ©deraient les milles dans $mille
            $nb=strlen($mille);
            for ($i=0;$i<=$nb;)
            {
                if(substr($mille,$i,1)==0)
                {
                    $mille=substr($mille,$i+1);
                    $nb=$nb-1;
                }
                elseif(substr($mille,$i,1)<>0)
                {
                    $mille=substr($mille,$i);
                    break;
                }
            }
#le nombre de caract que comporte le nombre saisi de sa forme sans espace et sans 0 au dÃ©but
            $nb=strlen($mille);
#echo '<br />nb='.$nb.'<br />';
            if ($nb==0)
                ;
#AffichageResultat($enLettre);
            elseif ($nb==1)
            {
                if ($mille==1)
                    $this->enLettre.='mille ';
                else
                {
                    $this->Unite($mille);
                    $this->enLettre.='mille ';
                }
            }
            elseif ($nb==2)
            {
                $this->Dizaine(1,$mille);
                $this->enLettre.='mille ';
            }
            elseif ($nb==3)
            {
                $this->Centaine(1,$mille,$unite,$dizaine,$centaine);
                $this->enLettre.='mille ';
            }
#Gestion des cents
#suppression des zÃ©ros qui prÃ©cÃ©deraient les cents dans $cent
            $nb=strlen($cent);
            for ($i=0;$i<=$nb;)
            {
                if(substr($cent,$i,1)==0)
                {
                    $cent=substr($cent,$i+1);
                    $nb=$nb-1;
                }
                elseif(substr($cent,$i,1)<>0)
                {
                    $cent=substr($cent,$i);
                    break;
                }
            }
#le nombre de caract que comporte le nombre saisi de sa forme sans espace et sans 0 au dÃ©but
            $nb=strlen($cent);
#echo '<br />nb='.$nb.'<br />';
            if ($nb==0)
                ;
#AffichageResultat($enLettre);
            elseif ($nb==1)
                $this->Unite($cent);
            elseif ($nb==2)
                $this->Dizaine(0,$cent);
            elseif ($nb==3)
                $this->Centaine(0,$cent,$unite,$dizaine,$centaine);
        }
    }

#Gestion des milles

    public function Mille($nombre,$unite,$dizaine,$centaine,$mille)
    {
#si les mille comportent un seul chiffre
#$cent represente les 3 premiers chiffres du chiffre ex: 321 dans 12321
        if (strlen($mille)==1)
        {
            $cent=substr($nombre,1);
#si ce chiffre=1
            if ($mille==1)
                $this->enLettre.='';
#si ce chiffre<>1
            elseif($mille<>1)
                $this->enLettre.=$this->chiffre[$mille];
        }
        elseif (strlen($mille)>1)
        {
            if (strlen($mille)==2)
            {
                $cent=substr($nombre,2);
                $nombre=substr($nombre,0,2);
#echo $nombre;
                $this->Dizaine(1,$nombre);
            }
            if (strlen($mille)==3)
            {
                $cent=substr($nombre,3);
                $nombre=substr($nombre,0,3);
#echo $nombre;
                $this->Centaine(1,$nombre,$unite,$dizaine,$centaine);
            }
        }

        $this->enLettre.='mille ';
#recuperation des cens dans nombre
#suppression des zÃ©ros qui prÃ©cÃ©deraient la saisie
        $nb=strlen($cent);
        for ($i=0;$i<=$nb;)
        {
            if(substr($cent,$i,1)==0)
            {
                $cent=substr($cent,$i+1);
                $nb=$nb-1;
            }
            elseif(substr($cent,$i,1)<>0)
            {
                $cent=substr($cent,$i);
                break;
            }
        }
#le nombre de caract que comporte le nombre saisi de sa forme sans espace et sans 0 au dÃ©but
        $nb=strlen($cent);
#echo '<br />nb='.$nb.'<br />';
        if ($nb==0)
            ;//AffichageResultat($enLettre);
        elseif ($nb==1)
            $this->Unite($cent);
        elseif ($nb==2)
            $this->Dizaine(0,$cent);
        elseif ($nb==3)
            $this->Centaine(0,$cent,$unite,$dizaine,$centaine);

    }

#Gestion des centaines

    /**
     * @param $inmillier
     * @param $nombre
     * @param $unite
     * @param $dizaine
     * @param $centaine
     */
    public  function Centaine($inmillier, $nombre, $unite, $dizaine, $centaine)
    {

        $unite=substr($nombre,2);
        $dizaine=substr($nombre,1,1);
        $centaine=substr($nombre,0,1);
#comme 700
        if ($unite==0 and $dizaine==0)
        {
            if ($centaine==1)
                $this->enLettre.='cent';
            elseif ($centaine<>1)
            {
                if ($inmillier == 0)
                    $this->enLettre.=($this->chiffre[$centaine].' cents').' ';
                if ($inmillier == 1)
                    $this->enLettre.=($this->chiffre[$centaine].' cent').' ';
            }
        }
#comme 705
        elseif ($unite<>0 and $dizaine==0)
        {
            if ($centaine==1)
                $this->enLettre.=('cent '.$this->chiffre[$unite]).' ';
            elseif ($centaine<>1)
                $this->enLettre.=($this->chiffre[$centaine].' cent '.$this->chiffre[$unite]).' ';
        }
//comme 750
        elseif ($unite==0 and $dizaine<>0)
        {
#recupÃ©ration des dizaines
            $nombre=substr($nombre,1);
//echo '<br />nombre='.$nombre.'<br />';
            if ($centaine==1)
            {
                $this->enLettre.='cent ';
                $this->Dizaine(0,$nombre).' ';
            }
            elseif ($centaine<>1)
            {
                $this->enLettre.=$this->chiffre[$centaine].' cent ';
                $this->Dizaine(0,$nombre).' ';

            }

        }
#comme 695
        elseif ($unite<>0 and $dizaine<>0)
        {
            $nombre=substr($nombre,1);

            if ($centaine==1)
            {
                $this->enLettre.='cent ';
                $this->Dizaine(0,$nombre).' ';
            }

            elseif ($centaine<>1)
            {
                $this->enLettre.=($this->chiffre[$centaine].' cent ');
                $this->Dizaine(0,$nombre).' ';
            }
        }

    }


#Gestion des dizaines

    public function Dizaine($inmillier,$nombre)
    {
        $unite=substr($nombre,1);
        $dizaine=substr($nombre,0,1);

#comme 70
        if ($unite==0)
        {
            $val=$dizaine.'0';
            $this->enLettre.=$this->chiffre[$val];
            if ($inmillier == 0 && $val == 80){
                $this->enLettre.='s ';
            }
            $this->enLettre.=' ';
        }
#comme 71
        elseif ($unite<>0)
#dizaine different de 9
            if ($dizaine<>9 and $dizaine<>7)
            {
                if ($dizaine==1)
                {
                    $val=$dizaine.$unite;
                    $this->enLettre.=$this->chiffre[$val].' ';
                }
                else
                {
                    $val=$dizaine.'0';
                    if ($unite == 1 && $dizaine <> 8){
                        $this->enLettre.=($this->chiffre[$val].' et '.$this->chiffre[$unite]).' ';
                    }else{
                        $this->enLettre.=($this->chiffre[$val].'-'.$this->chiffre[$unite]).' ';
                    }
                }
            }
#dizaine =9
            elseif ($dizaine==9)
                $this->enLettre.=($this->chiffre[80].'-'.$this->chiffre['1'.$unite]).' ';
            elseif ($dizaine==7)
            {
                if ($unite == 1){
                    $this->enLettre.=($this->chiffre[60].' et '.$this->chiffre['1'.$unite]).' ';
                }else{
                    $this->enLettre.=($this->chiffre[60].'-'.$this->chiffre['1'.$unite]).' ';
                }
            }
    }
#Gestion des unitÃ©s

    public function Unite($unite)
    {
        $this->enLettre.=($this->chiffre[$unite]).' ';
    }

}