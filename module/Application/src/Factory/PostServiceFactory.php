<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 12/11/2020
 * Time: 03:31
 */

namespace Application\Factory;



 use Application\Service\PostService;
 use Interop\Container\ContainerInterface;
 use Interop\Container\Exception\ContainerException;
 use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
 use Laminas\ServiceManager\Exception\ServiceNotFoundException;

 use Laminas\ServiceManager\Factory\FactoryInterface;

 class PostServiceFactory implements FactoryInterface
    {


     /**
      * Create an object
      *
      * @param  ContainerInterface $container
      * @param  string $requestedName
      * @param  null|array $options
      * @return object
      * @throws ServiceNotFoundException if unable to resolve the service.
      * @throws ServiceNotCreatedException if an exception is raised when
      *     creating a service.
      * @throws ContainerException if any other error occurs
      */
     public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
     {
         // TODO: Implement __invoke() method.
         $dependency = $container->get('Application\Mapper\PostMapperInterface');

         return new PostService($dependency);
     }
 }