<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/11/2020
 * Time: 09:27
 */

namespace Application\Service;


interface PostServiceInterface
{
    public function defaultSelect($table,$columns, $condition, $joins,$limit, $type, $grpe, $sort);
    public function defaultInsert($table, $data);
    public function defaultUpdate($table, $data, $condition);
    public function defaultDelete($table, $condition);

    public function userLogin ($login, $pwd);
    public function checkIfUserExist($login);
    public function getOneUser($id,$joins);


    public function getDbTables();
    public function loadMenuGroup($id_group);
    public function retriveMenu($id_group);

    public function listUsers($codeProfil,$idUser);
    public function listOfUsers($user_account,$user_account_search,$user_id,$limit,$order);
    public function listOfMonnaie($search,$limit,$order);
    public function listOfDevises($search,$limit,$order);
    public function listDevises($conditions);
    public function getUser($condition);
    public function listFamilleDevise();
    public function getSpecifiqueFamilleDevise($condition,$type);
    public function getProfilAccordingToProfilOnline($codeProfilOfUserActive);
    public function getAdminUsers($codeProfil);
    public function addUser($data);
    public function getCategorieDevise();
    public function random($car);
    public function generateRandom($car);

    public function password_strength($password);
    public function sendMail($message, $key,$token,$destinataire,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template);

    public function sendMessageTelegram($message);




}