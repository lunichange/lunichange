<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/11/2020
 * Time: 09:25
 */

namespace Application\Service;



use Application\Mapper\PostMapperInterface;

class PostService implements PostServiceInterface
{

    protected $postMapper;

    /**
     * PostService constructor.
     * @param PostMapperInterface $postMapper
     */
    public function __construct(PostMapperInterface $postMapper)
    {
        $this->postMapper = $postMapper;
    }

    public function defaultSelect($table,$columns, $condition, $joins,$limit,$type, $grpe, $sort)
    {
        return $this->postMapper->defaultSelect($table,$columns, $condition, $joins,$limit, $type, $grpe, $sort);
    }

    public function defaultInsert($table, $data)
    {
        return $this->postMapper->defaultInsert($table, $data);
    }

    public function defaultUpdate($table, $data, $condition)
    {
        return $this->postMapper->defaultUpdate($table, $data, $condition);
    }

    public function defaultDelete($table, $condition)
    {
        return $this->postMapper->defaultDelete($table, $condition);
    }


    public function loadMenuGroup($id_group)
    {
        return $this->postMapper->loadMenuGroup($id_group);
    }

    public function retriveMenu($id_group)
    {
        return $this->postMapper->retriveMenu($id_group);
    }

    public function getOnerUser($id,$joins)
    {
        return $this->postMapper->getOneUser($id,$joins);
    }

    #Fonction de conversion appelÃ©e dans la feuille principale
    public function Conversion($sasie)
    {
        return $this->postMapper->Conversion($sasie);
    }

    #Gestion des millions

    public  function Million($nombre,$unite,$dizaine,$centaine,$mille,$million)
    {

        return $this->postMapper->Million($nombre,$unite,$dizaine,$centaine,$mille,$million);
    }

#Gestion des milles

    public function Mille($nombre,$unite,$dizaine,$centaine,$mille)
    {
        return $this->postMapper->Mille($nombre,$unite,$dizaine,$centaine,$mille);
    }

#Gestion des centaines

    /**
     * @param $inmillier
     * @param $nombre
     * @param $unite
     * @param $dizaine
     * @param $centaine
     */
    public  function Centaine($inmillier, $nombre, $unite, $dizaine, $centaine)
    {
        return $this->postMapper->Centaine($inmillier,$nombre,$unite,$dizaine,$centaine);
    }


    #Gestion des dizaines
    public function Dizaine($inmillier,$nombre)
    {
        return $this->postMapper->Dizaine($inmillier,$nombre);
    }

    #Gestion des unitÃ©s
    public function Unite($unite)
    {
        return $this->postMapper->Unite($unite);
    }


    public function userLogin($login, $pwd)
    {
        return $this->postMapper->userLogin($login,$pwd);
    }

    public function checkIfUserExist($login)
    {
        return $this->postMapper->checkIfUserExist($login);
    }

    public function getOneUser($id, $joins)
    {
       return $this->postMapper->getOneUser($id,$joins);
    }

    public function getDbTables()
    {
        return $this->postMapper->getDbTables();
    }

    public function listUsers($codeProfile,$idUser)
    {
        return $this->postMapper->listUsers($codeProfile,$idUser);
    }
    public function listOfUsers($user_account,$user_account_search,$user_id,$limit,$order)
    {
        return $this->postMapper->listOfUsers($user_account,$user_account_search,$user_id,$limit,$order);
    }
    public function listOfMonnaie($search,$limit,$order)
    {
        return $this->postMapper->listOfMonnaie($search,$limit,$order);
    }
    public function listOfDevises($search,$limit,$order)
    {
        return $this->postMapper->listOfDevises($search,$limit,$order);
    }
    public function listDevises($conditions)
    {
        return $this->postMapper->listDevises($conditions);
    }
    public function getUser($condition)
    {
        return $this->postMapper->getUser($condition);
    }
    public function listFamilleDevise()
    {
        return $this->postMapper->listFamilleDevise();
    }
    public function getSpecifiqueFamilleDevise($condition,$type)
    {
        return $this->postMapper->getSpecifiqueFamilleDevise($condition,$type);
    }
    public function getProfilAccordingToProfilOnline($codeProfilOfUserActive)
    {
        return $this->postMapper->getProfilAccordingToProfilOnline($codeProfilOfUserActive);
    }
    public function getAdminUsers($codeProfil)
    {
        return $this->postMapper->getAdminUsers($codeProfil);
    }
    public function addUser($data)
    {
        return $this->postMapper->addUser($data);
    }
    public function getCategorieDevise()
    {
        return $this->postMapper->getCategorieDevise();
    }
    public function random($car)
    {
        return $this->postMapper->random($car);
    }
    public function generateRandom($car)
    {
        return $this->postMapper->generateRandom($car);
    }

    public function password_strength($password)
    {
        return $this->postMapper->password_strength($password);
    }
    public function sendMail($message, $key,$token,$destinataire,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template)
    {
        return $this->postMapper->sendMail($message, $key,$token,$destinataire,$btn_url,$btn_label, $titre, $subject, $email, $from_mail, $from_id,$template);
    }

    public function sendMessageTelegram($message)
    {
        return $this->postMapper->sendMessageTelegram($message);
    }
}