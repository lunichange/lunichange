<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */



namespace Application;

class Module
{
    public function getConfig() : array
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => array(
                Controller\HomeController::class => function($container) {

                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\HomeController($postService);
                },
                Controller\IndexController::class =>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');


                    return new Controller\IndexController($postService);
                },
                Controller\TreatmentController::class =>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');


                    return new Controller\TreatmentController($postService);
                },

                Controller\ConfirmRegistrationController::class =>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');

                    return new Controller\ConfirmRegistrationController($postService,$container);
                },
                Controller\TermsOfUseController::class=>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\TermsOfUseController($postService,$container);
                },
                 Controller\HowItWorkController::class=>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\HowItWorkController($postService,$container);
                },
                Controller\FaqController::class=>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\FaqController($postService,$container);
                },
                Controller\RecoveringPasswordController::class=>function($container)
                {
                    $postService         = $container->get('Application\Service\PostServiceInterface');
                    return new Controller\RecoveringPasswordController($postService,$container);
                }

            ),
        ];
    }
}
