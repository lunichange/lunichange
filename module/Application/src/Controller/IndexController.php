<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */



namespace Application\Controller;

use Application\Mapper\WebServiceDbSqlMapper;
use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;


class IndexController extends AbstractActionController
{


    protected $contentVars = array();
    protected $layout;

    protected $postService;
    protected $sessionContainer;
    protected $sessionManager;

    private $_currentUser = array();

    public function __construct(PostServiceInterface $postService)
    {
        $this->postService = $postService;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();



        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser != null)
        {
            $this->_currentUser = [
                'id'=>$this->sessionContainer->IdUser,
                'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null,
            ];
        }

    }

    public function indexAction()
    {
        $this->renderPage();
        $messageConfirmRegistration = array();

        //$request = $this->getRequest();
        $this->sessionContainer->GetRef = isset($_GET['ref']) && $_GET['ref'] !== null ? intval($_GET['ref']) : null;

        if(isset($_GET['ref']) && $_GET['ref'] !==0 && $_GET['ref'] !== null)
        {
            $this->redirect()->toRoute('register',['id'=>$_GET['ref']]);
        }



        if(isset($this->sessionContainer->MessageConfirmRegistration) && $this->sessionContainer->MessageConfirmRegistration !== null)
        {

            $messageConfirmRegistration = $this->sessionContainer->MessageConfirmRegistration;
            unset($this->sessionContainer->MessageConfirmRegistration);

        }

        return array(
            'messagesConfirmRegistration'=>$messageConfirmRegistration,
            'getRef'=>$this->sessionContainer->GetRef

        );
    }
    
    public function registerTestAction()
    {
        $this->renderPage();
        $request = $this->getRequest();
        $msgNotification = null;
        $post = null;

        $id = $this->params()->fromRoute('id',0);

        if($id !== 0)
        {
            $id = intval($id);
            $this->sessionContainer->ReferalKey = $id;
        }



        if($request->isPost())
        {
            $data = $request->getPost()->toArray();



            $arrayFieldsAllowed = ['firstName','lastName','sexe','email','password','rpassword','agree'];
            foreach($data as $keys=>$elements)
            {

                if(empty($elements))
                {
                    $msgNotification['type'] = "danger";
                    $msgNotification['message'] = "Veuillez remplir tous les champs.";

                }

                $data[$keys] = filter_var($elements,FILTER_SANITIZE_STRING);

            }


            $getParent   = null;
            if(isset($this->sessionContainer->ReferalKey) && $this->sessionContainer->ReferalKey !== null)
            {
                $identifiant = $this->sessionContainer->ReferalKey;
                $getParent = $this->postService->getUser([
                    'parent_key'=>$identifiant,
                    'email<>?'=>$data['email']
                ]);
                unset($this->sessionContainer->ReferalKey);
            }

            $dataTempToInsert['email_temp'] = $data['email'];
            $dataTempToInsert['key_temp'] = $this->getToken (8);
            $dataTempToInsert['token_temp'] = $this->postService->random(30);
            $dataTempToInsert['time_expired'] = time() ;

            $salutations = "Bonjour";
            if(intval(date('H')) > 12)
            {
                $salutations = "Bonsoir";

            }
            $nom_complet = ltrim($data['lastName'].' '.$data['firstName']);
            $btn = 'Se connecter';
            $url = $this->getBaseUrl().'/confirm-registration/';
            $titre = 'Mail de confirmation d\'inscription';
            $subject = 'Votre compte LUNICHANGE';
            $email = $data['email'];
            $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
            $f_id = WebServiceDbSqlMapper::FROM_ID;


            //setlocale (LC_ALL, 'fr_FR.utf8','fra');
            date_default_timezone_set('UTC');
            setlocale (LC_ALL, 'fr_FR.utf8','fra');

            $dateNow = strftime('%A %d %B %Y, %H:%M');

            $date_et_heure = "<small style='font-size:0.65em !important;'>Date et heure: ".$dateNow."</small><br><br>";

            $msg ['salutations']= $date_et_heure."<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
            $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important"> Une demande de confirmation d\'inscription a été lancée.</p>';
            $msg ['paragraphe'][]= '<p style="color:#000 !important;font-size: 0.9em !important ; line-height: 20px ">Si ce mail vous es destiné, veuillez cliquer sur le bouton bleue <b>"'.$btn.'"</b> ci-dessous. </p>';
            $msg ['nb'][]= '<b style="font-size: 0.9em;text-decoration: underline !important; color: red !important;">NB: Le mail est valide pour une durée d\'une heure</b>.<br>';
            $msg ['nb'][]= '<span style="font-size: 0.9em !important">Si cela ne vous concerne pas, veuillez ignorer ce message.</span><br>';
            $msg ['base_url'] = $this->getBaseUrl();
            $msg ['btn_url'] = 'confirm-registration';

            $msg['politesse'] = '<b style="font-size: 0.8em !important">Coordialement l\'administration LUNICHANGE</b>';

            $mailSend = $this->postService->sendMail($msg,$dataTempToInsert['key_temp'],$dataTempToInsert['token_temp'],$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template');

            if($mailSend !== null)
            {
                return array(
                    'msgNotification'=>[
                        'type'=>"danger",
                        'message'=>"Une erreur s'est produite lors de l'envoi du mail"
                    ],
                    'post'=>$data
                );
            }


            if($mailSend == null)
            {
                $rekk = (bool)$this->postService->defaultInsert ('luni_users_temp',$dataTempToInsert);
                if($rekk)
                {
                    $arrayInsertDatasUser = [
                        'email'=>trim(strtolower ($data['email'])),
                        'nom_complet' => ltrim ($data['firstName'].' '.$data['lastName']),
                        'nom'=>$data['lastName'],
                        'prenom'=>$data['firstName'],
                        'sexe'=>$data['sexe'],
                        'password'=>password_hash ($data['password'], PASSWORD_DEFAULT),
                        'code_profil'=>2,
                        'statut'=>'0',
                        'date_creation'=>date('Y-m-d H:i:s'),
                        'lock_u'=>'0'
                    ];
                    if($getParent !== null)
                    {
                        $arrayInsertDatasUser['reference_parent'] = $getParent['parent_key'];
                        $arrayInsertDatasUser['parent'] = $getParent['id_user'];
                    }

                    $reqInsert = (bool)$this->postService->defaultInsert('luni_users',$arrayInsertDatasUser);


                    if($reqInsert)
                    {
                        $this->sessionContainer->MessageConfirmRegistration = [];
                        $this->sessionContainer->MessageConfirmRegistration['message'] = "Merci de consulter votre adresse électronique afin de finaliser votre inscription. Veuillez également consulter vos spams";
                        $this->sessionContainer->MessageConfirmRegistration['type'] = "success";

                        $this->redirect()->toRoute('login');
                    }
                }

            }

        }

        return array(
            'msgNotification'=>$msgNotification,
            'post'=>$post
        );
    }
    
    public function testAction()
    {
        
        return $this->getResponse();
    }


    public function registerAction()
    {
        $this->renderPage();
        $request = $this->getRequest();
        $msgNotification = null;
        $post = null;

        $id = $this->params()->fromRoute('id',0);

        if($id !== 0)
        {
            $id = intval($id);
            $this->sessionContainer->ReferalKey = $id;
        }



        if($request->isPost())
        {
            $data = $request->getPost()->toArray();

           
            $arrayFieldsAllowed = ['firstName','lastName','sexe','email','password','rpassword','agree'];
            
            $myPostArray = ["lastName"=>"Nom","firstName"=>"Prénom(s)","email"=>"Email","sexe"=>"Sexe","password"=>"Mot de passe","rpassword"=>"Mot de passe","agree"=>"Conditions d'utilisation"];
            
            $isOkay = true;


            foreach($data as $keys=>$elements)
            {

                if(count($data) !== count($myPostArray))
                {
                    return [
                        'post'=>$data,
                        'msgNotification'=>[
                            'type'=>'danger',
                            'message'=>'Impossible de poursuivre le traitement demandé. Les données fournies ne sont pas conformes.'
                        ]
                    ];
                }
                if(empty($elements))
                {
                    return [
                        'post'=>$data,
                        'msgNotification'=>[
                            'type'=>'danger',
                            'message'=>'Veuillez remplir tous les champs. Problème(s) au niveau du champs suivant: <b>'.$myPostArray[$keys].'</b>'
                        ]
                    ];


                }
                switch ($keys)
                {
                    case 'firstName':
                    case 'lastName':
                        if(strlen($elements) < 2 || strlen($elements) > 100)
                        {


                            return [
                                'post'=>$data,
                                'msgNotification'=>[

                                    'type'=>"danger",
                                    'message'=>"Le nom et Le(s) prénom(s) doivent faire entre deux(02) et cent(100) caractères."

                                ]

                            ];
                        }
                        break;
                    case 'sexe':
                        if(!in_array($elements,['F','M']))
                        {
                            $msgNotification['type'] = "danger";
                            $msgNotification['message'] = "Oops,une erreur s'est poduite. Le sexe renseigné n'est pas correcte";

                            return [
                                'post'=>$data,
                                'msgNotification'=>$msgNotfication

                            ];
                        }

                        break;
                    case 'email':
                        if(!filter_var($elements,FILTER_VALIDATE_EMAIL))
                        {

                            return [

                                'post'=>$data,
                                'msgNotification'=>[
                                    'type'=>"danger",
                                    'message'=>  "Oops, une erreur s'est produite. L'adresse mail renseigné est incorrecte"
                                ]
                            ];
                        }

                        $checkEmail = $this->postService->defaultSelect('luni_users',[],[
                            'email'=>filter_var($data['email'],FILTER_SANITIZE_STRING)
                        ],null,null,'unique',null,null);


                        $checkIfMailTempExist = $this->postService->defaultSelect('luni_users_temp',[],
                            array(
                                'email_temp'=>$data['email']
                            ),null,null,'unique',null,null);


                        if(!empty($checkEmail) || !empty($checkIfMailTempExist))
                        {

                            $msgNotification['type'] = "danger";
                            $msgNotification['message'] = "Cette adresse mail est déjà utilisée <b class='text-decoration-underline'>OU</b> Vous n'avez pas finalisé votre inscription,veuillez consulter votre boîte mail.";

                        }

                        if($msgNotfication !== null && isset($msgNotfication['type']) && $msgNotfication['type'] === "danger")
                        {
                            return [

                                'post'=>$data,
                                'msgNotification'=>$msgNotification

                            ];
                        }

                        break;

                    case 'password':
                    case 'rpassword':
                        if($elements == $data['email'])
                        {
                            $msgNotification['type'] = "danger";
                            $msgNotification['message'] = "Oops, une erreur s'est produite.Le mot de passe ne doit identique ni au nom ni à votre adresse mail.";
                        }
                        if($this->postService->password_strength($data['password']) == false)
                        {
                            $msgNotification['type'] = "danger";
                            $msgNotification['message'] = WebServiceDbSqlMapper::MESSAGE_FOR_PASSWORD_ERROR;
                        }
                        if($data['password'] !== $data['rpassword'])
                        {
                            $msgNotification['type'] = "danger";
                            $msgNotification['message'] = "Les mots de passe doivent-être identiques";
                        }

                        if($msgNotfication !== null && isset($msgNotfication['type']) && $msgNotfication['type'] === "danger")
                        {

                            return [

                                'post'=>$data,
                                'msgNotification'=>$msgNotification

                            ];
                        }



                }
                $data[$keys] = filter_var($elements,FILTER_SANITIZE_STRING);

            }

            $getParent   = null;
            if(isset($this->sessionContainer->ReferalKey) && $this->sessionContainer->ReferalKey !== null)
            {
                $identifiant = $this->sessionContainer->ReferalKey;
                $getParent = $this->postService->getUser([
                    'parent_key'=>$identifiant,
                    'email<>?'=>$data['email']
                ]);
                unset($this->sessionContainer->ReferalKey);
            }


            $dataTempToInsert['email_temp'] = $data['email'];
            $dataTempToInsert['key_temp'] = $this->getToken (8);
            $dataTempToInsert['token_temp'] = $this->postService->random(30);
            $dataTempToInsert['time_expired'] = time() ;

            $salutations = "Bonjour";
            if(intval(date('H')) > 12)
            {
                $salutations = "Bonsoir";

            }
            $nom_complet = ltrim($data['lastName'].' '.$data['firstName']);
            $btn = 'Se connecter';
            $url = $this->getBaseUrl().'/confirm-registration/';
            $titre = 'Mail de confirmation d\'inscription';
            $subject = 'Votre compte LUNICHANGE';
            $email = $data['email'];
            $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
            $f_id = WebServiceDbSqlMapper::FROM_ID;


            //setlocale (LC_ALL, 'fr_FR.utf8','fra');
            date_default_timezone_set('UTC');
            setlocale (LC_ALL, 'fr_FR.utf8','fra');

            $dateNow = strftime('%A %d %B %Y, %H:%M');

            $date_et_heure = "<small style='font-size:0.65em !important;'>Date et heure: ".$dateNow."</small><br><br>";

            $msg ['salutations']= $date_et_heure."<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
            $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important"> Une demande de confirmation d\'inscription a été lancée.</p>';
            $msg ['paragraphe'][]= '<p style="color:#000 !important;font-size: 0.9em !important ; line-height: 20px ">Si ce mail vous es destiné, veuillez cliquer sur le bouton bleue <b>"'.$btn.'"</b> ci-dessous. </p>';
            $msg ['nb'][]= '<b style="font-size: 0.9em;text-decoration: underline !important; color: red !important;">NB: Le mail est valide pour une durée d\'une heure</b>.<br>';
            $msg ['nb'][]= '<span style="font-size: 0.9em !important">Si cela ne vous concerne pas, veuillez ignorer ce message.</span><br>';
            $msg ['base_url'] = $this->getBaseUrl();
            $msg ['btn_url'] = 'confirm-registration';

            $msg['politesse'] = '<b style="font-size: 0.8em !important">Coordialement l\'administration LUNICHANGE</b>';

            $mailSend = $this->postService->sendMail($msg,$dataTempToInsert['key_temp'],$dataTempToInsert['token_temp'],$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template');


            if($mailSend !== null)
            {
                return array(
                    'msgNotification'=>[
                        'type'=>"danger",
                        'message'=>"Une erreur s'est produite lors de l'envoi du mail"
                    ],
                    'post'=>$data
                );
            }
            
           
            if($mailSend == null && $msgNotfication == null)
            {
                $rekk = (bool)$this->postService->defaultInsert ('luni_users_temp',$dataTempToInsert);
                if($rekk)
                {
                    $arrayInsertDatasUser = [
                        'email'=>trim(strtolower ($data['email'])),
                        'nom_complet' => ltrim ($data['firstName'].' '.$data['lastName']),
                        'nom'=>$data['lastName'],
                        'prenom'=>$data['firstName'],
                        'sexe'=>$data['sexe'],
                        'password'=>password_hash ($data['password'], PASSWORD_DEFAULT),
                        'code_profil'=>2,
                        'statut'=>'0',
                        'date_creation'=>date('Y-m-d H:i:s'),
                        'lock_u'=>'0'
                    ];
                    if($getParent !== null)
                    {
                        $arrayInsertDatasUser['reference_parent'] = $getParent['parent_key'];
                        $arrayInsertDatasUser['parent'] = $getParent['id_user'];
                    }

                    try
                    {
                        $reqInsert = (bool)$this->postService->defaultInsert('luni_users',$arrayInsertDatasUser);


                        if($reqInsert)
                        {
                            $this->sessionContainer->MessageConfirmRegistration = [];
                            $this->sessionContainer->MessageConfirmRegistration['message'] = "Merci de consulter votre adresse électronique afin de finaliser votre inscription. Veuillez également consulter vos spams";
                            $this->sessionContainer->MessageConfirmRegistration['type'] = "success";
    
                            $this->redirect()->toRoute('login');
                        } 
                    }catch(Exception $e)
                    {
                         $msgNotification = 
                           [
                                'type'=>'danger',
                                'message'=>"Une erreur s'est produite au cours du traitement.Vos informations existent déjà !"
                            
                            
                            ];
                    }
                   
                }

            }

        }

        return array(
            'msgNotification'=>$msgNotification,
            'post'=>$post
        );
    }


    public function deleteUsersTemp()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            if(isset($data['delete']) && $data['delete'] === true)
            {
                $getUsersTemp = $this->postService->defaultSelect('luni_users_temp',[],[],null,null,'all',null,null);

                foreach ($getUsersTemp as $keys=>$values)
                {
                    $checkIfUserExist = $this->postService->defaultSelect('luni_users',[],array('statut'=>'0','email'=>$values['email_temp']),null,null,'unique',null,null);
                    if(!empty($checkIfUserExist))
                    {
                        if(time() - $values['time_expired'] >= 60*720)
                        {
                            $this->postService->defaultDelete('luni_users_temp',array('email_temp'=>$values['email_temp']));
                            $this->postService->defaultDelete('luni_users',array('email'=>$checkIfUserExist['email']));
                            $tableJson['success'] = "success";
                        }
                    }
                    else
                    {
                        if(time() - $values['time_expired'] >= 60*60)
                        {
                            $this->postService->defaultDelete('luni_users_temp',array('email_temp'=>$values['email_temp']));
                            $tableJson['success'] = "success";
                        }
                    }
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

        }
        else
        {
            $this->redirect()->toRoute('login');
        }
        return $view;
    }

    public function passwordRecoverAction()
    {
        $request = $this->getRequest ();
        $tableJson = array();
        $view = null;

        if($request->isXmlHttpRequest())
        {
            $this->connCheckAction();
            $data = $request->getPost()->toArray();

            $data['email'] = trim(strtolower($data['email']));
            if(empty($data['email']))
            {
                $tableJson['error'] = "Veuillez renseigner votre adresse mail";
            }
            elseif(!filter_var($data['email'],FILTER_VALIDATE_EMAIL))
            {
                $tableJson['error'] = 'Adresse mail incorrecte';
            }
            else
            {
                $checkIfUserExist =  $this->dbHelperService->defaultSelect ($this->dbHelperService->getDbTables ()['customers']['table'],
                    array(
                        $this->dbHelperService->getDbTables ()['customers']['columns']['email'] => filter_var ($data['email'],FILTER_VALIDATE_EMAIL)
                    ),
                    null,null,'unique',null,null
                );

                if(count($checkIfUserExist) > 0 || !empty($checkIfUserExist))
                {
                    if($checkIfUserExist['statut'] === '0')
                    {
                        $tableJson['error'] = "Vous ne pouvez pas effectuer cette opération. Votre compte n'est pas actif";
                    }
                    else
                    {
                        $checkIfTempExist = (bool)$this->dbHelperService->defaultSelect ('luni_recup_password_user',array('email_recup'=>$data['email']),null,null,'unique',null,null);

                        $salutations = "Bonjour";
                        if(date('H') > 12)
                        {
                            $salutations = "Bonsoir";
                        }


                        $key = $this->getToken (10);
                        $token = $this->random (30,'all');
                        $nom_complet = filter_var ($checkIfUserExist['nom_complet'],FILTER_SANITIZE_STRING);
                        $btn = 'Changer mot de passe';
                        $url = $this->getBaseUrl().'/recovering-password/';
                        $titre = 'Mail de réinitialisation de mot de passe';
                        $subject = 'Votre compte LUNICHANGE';
                        $email = $data['email'];
                        $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                        $f_id = WebServiceDbSqlMapper::FROM_ID;

                        date_default_timezone_set('UTC');
                        setlocale (LC_ALL, 'fr_FR.utf8','fra');

                        $dateNow = strftime('%A %d %B %Y, %H:%M');

                        $date_et_heure = "<small style='font-size:0.8em !important;'>Date et heure: ".$dateNow."</small><br><br>";

                        $msg ['salutations']= $date_et_heure."<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                        $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important"> Une demande de réinitialisation de mot de passe a été lancée.</p>';
                        $msg ['paragraphe'][]= '<p style="color:#000 !important;font-size: 0.9em !important ; line-height: 20px ">Si ce mail vous es destiné, veuillez cliquer sur le bouton bleue <b>"'.$btn.'"</b> ci-dessous. </p>';
                        $msg ['nb'][]= '<b style="font-size: 0.9em;text-decoration: underline !important; color: red !important;">NB: Le mail est valide pour une durée d\'une heure</b>.<br>';
                        $msg ['nb'][]= '<span style="font-size: 0.9em !important">Veuillez ignorer ce message s\'il ne vous concerne pas.</span><br>';
                        $msg ['base_url'] = $this->getBaseUrl();
                        $msg ['btn_url'] = 'customer/recovering-password';


                        $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                        $mailSend = $this->postService->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template');


                        if($mailSend === null)
                        {   if($checkIfTempExist)
                        {
                            $this->dbHelperService->defaultUpdate ('luni_recup_password_user',array(
                                'key_recup'=>$key,
                                'token_recup'=>$token,
                                'time_expired_recup'=>time()
                            ),array(
                                'email_recup'=>filter_var ($data['email'],FILTER_VALIDATE_EMAIL)
                            ));
                        }
                        else
                        {
                            $this->dbHelperService->defaultInsert ('luni_recup_password_user',array (
                                'email_recup'=>filter_var ($data['email'],FILTER_VALIDATE_EMAIL),
                                'key_recup'=>$key,
                                'token_recup'=>$token,
                                'time_expired_recup'=>time()
                            ));

                        }

                            $tableJson['success'] = "Merci pour l'attente! L'instruction de récupération du mot de passe a été envoyée à votre adresse électronique.";
                        }
                        else
                        {
                            $tableJson['error'] = "Une erreur s'est produite lors de l'envoi du mail. Veuillez vérifier votre connexion et rééssayer. Merci!!";
                        }



                    }
                }
                else
                {
                    $tableJson['error'] = "Désolé cette adresse mail n'existe pas.";
                }

            }



            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }else
        {
            $this->redirect()->toRoute('login');
        }

        return $view;
    }

    public function getBaseUrl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Laminas\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }



    private function renderPage()
    {
        $this->checkAuth();
        $this->layout = $this->layout();
        $this->layout->setVariables($this->contentVars);
        $this->layout->setTemplate('layout/login/layout');

    }

    public function logoutAction()
    {
        if(!empty($this->_currentUser))
        {
            $this->postService->defaultUpdate(
                'luni_users',[
                'derniere_connexion'=>date('Y-m-d H:i:s'),
                'online'=>'0'
            ],[
                    'id_user'=>$this->_currentUser['id']
                ]
            );
            $this->sessionManager->getStorage()->clear('lunichange');
            return $this->redirect()->toRoute('login');
        }
        if(empty($this->_currentUser))
        {
            return $this->redirect()->toRoute('login');
        }

        return 0;
    }

    private function checkAuth()
    {
        if(!empty($this->_currentUser) && isset($this->_currentUser['id']))
        {
            /*if(isset($this->_currentUser['codeProfile']) && $this->_currentUser['codeProfile'] !== null)
            {
                if($this->_currentUser['codeProfile'] !== 2)
                {
                    return $this->redirect()->toRoute('home');
                }
            }*/
            return $this->redirect()->toRoute('dashboard');
        }

        return 0;
    }

    function getToken($len=32)
    {
        return substr(md5(openssl_random_pseudo_bytes(20)), -$len);
    }
}
