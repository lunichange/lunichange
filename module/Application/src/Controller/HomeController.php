<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 25/11/2020
 * Time: 15:32
 */

namespace Application\Controller;


use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\ViewModel;

class HomeController extends AbstractActionController
{

    protected $headerView;
    protected $menuView;
    protected $sliderView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $sliderVars;
    protected $footerVars;
    protected $contentVars;

    protected $postService;

    protected $currentUser = array();
    protected $sessionManager;
    protected $sessionContainer;

    public function __construct(PostServiceInterface $postService)
    {
        $this->postService = $postService;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();


        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== null)
        {
            $this->currentUser = [
                'id'=>  intval($this->sessionContainer->IdUser),
                'profile'=>$this->sessionContainer->ProfilName,
                'codeProfile'=>$this->sessionContainer->CodeProfil,
                'timeStamp'=> $this->sessionContainer->LastLoginTimeStamp = time(),
                'username'=>$this->sessionContainer->Login,
                'fullName'=> isset($this->sessionContainer->UserName) && $this->sessionContainer->UserName !== null ? filter_var($this->sessionContainer->UserName,FILTER_SANITIZE_STRING) : null
            ];
        }


        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();
    }


    public function indexAction()
    {
        $this->renderPage();

        return new ViewModel();
    }

    private function renderPage()
    {
        $this->menuVars['sessionExist'] = $this->headerVars['sessionExist']= false;


        if($this->sessionContainer->IdUser !== null && !empty($this->sessionContainer->IdUser))
        {
            $this->headerVars['sessionExist'] = true;
            $this->menuVars['sessionExist'] = true;
        }
        //header layout
        $this->headerView = new ViewModel();

        $this->headerView->setVariables($this->headerVars);
        $this->headerView->setTemplate('layout/website/header');
        //menu layout
        $this->menuView = new ViewModel();
        $this->menuView->setVariables($this->menuVars);
        $this->menuView->setTemplate('layout/website/menu');


        //Sliders
        $this->sliderView = new ViewModel();
        $this->sliderView->setVariables($this->menuVars);
        $this->sliderView->setTemplate('layout/website/slider');


        //footerView
        $this->footerView = new ViewModel();
        $this->footerView->setTemplate('layout/website/footer');

        $this->layout = $this->layout();

        $this->layout->addChild($this->headerView, 'header');
        $this->layout->addChild($this->menuView, 'menu');
        $this->layout->addChild($this->sliderView, 'slider');
        $this->layout->setVariables($this->contentVars);
        $this->layout->addChild($this->footerView, 'footer');

        $this->layout->setTemplate('layout/website/layout');


    }
}