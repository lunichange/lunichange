<?php


namespace Application\Controller;

use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Application\Mapper\WebServiceDbSqlMapper;

use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;
use Laminas\Session\Container;

class RecoveringPasswordController extends AbstractActionController
{
    protected $postService;
    protected $serviceLocator;


    protected $headerView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $footerVars;
    protected $contentVars;


    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;


    public function __construct(PostServiceInterface $postService, $container)
    {
        $this->postService = $postService;
        $this->serviceLocator = $container;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }
    public function indexAction()
    {
        
        $request = $this->getRequest()->getRequestUri();
        $slashRequest = explode('/',$request);
        
        //var_dump($slashRequest);
        //die();

        $messageError = null;
        $keysToConnect = null;
        $this->connCheckAction();

        $this->_getHelper('headScript', $this->serviceLocator)

            ->appendFile($this->getUriPath() . 'assets/pages/recovering_password/ajax-form_recover_password.js')
            ;

        if(!isset($slashRequest[2],$slashRequest[3]) )
        {
            $this->redirect()->toRoute('login');
        }
        else
        {
            if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== "")
            {
                $this->redirect()->toRoute('dashboard');
            }
            $get_user_recup_password = $this->postService->defaultSelect('luni_recup_password_user',[],array(
                'key_recup'=>$slashRequest[2],
                'token_recup'=>$slashRequest[3]
            ),null,null,'unique',null,null);

            $keysToConnect['key_recup'] = $slashRequest[2];
            $keysToConnect['token_recup'] = $slashRequest[3];

            if(!empty($get_user_recup_password))
            {
                if(time() - $get_user_recup_password['time_expired_recup'] >= 60*60)
                {
                    $this->sessionContainer->MessageConfirmRegistration = array('type'=>'error','message'=>'Le lien a expiré.');
                    $this->postService->defaultDelete('luni_recup_password_user',array(
                        'key_recup'=>$slashRequest[2],
                        'token_recup'=>$slashRequest[3]
                    ));
                    $this->redirect()->toRoute('login');
                }
            }
            else
            {
                $this->sessionContainer->MessageConfirmRegistration = array('type'=>'error','message'=>'Votre demande ne peut aboutir.');
                $this->redirect()->toRoute('login');
            }


        }


        $this->renderPage();
        return array(
            'keysToConnect'=>$keysToConnect
        );
    }

    public function treatFormAction()
    {
        $request = $this->getRequest();
        $tableJson = array();
        $view = null;


        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $dataEmpty = false;
            if(!empty($data))
            {
                foreach ($data as $keys=>$values)
                {
                    if(empty($data[$keys]))
                    {
                        $tableJson['error'] = 'Veuillez remplir tous les champs.';
                        $dataEmpty = true;

                    }
                }
            }


            if($this->postService->password_strength (filter_var($data['password'],FILTER_SANITIZE_STRING)) === false)
            {
                $tableJson['error'] = WebServiceDbSqlMapper::MESSAGE_FOR_PASSWORD_ERROR;
                //var_dump($messageError);
            }
            elseif($data['password'] !== $data['rpassword'])
            {
                $tableJson['error'] = "Les mots de passe doivent-être identiques.";
            }
            else
            {

                if(!$dataEmpty)
                {
                    $get_user_recup_password = $this->postService->defaultSelect('luni_recup_password_user',[],array(
                        'key_recup'=>$data['key_recup'],
                        'token_recup'=>$data['token_recup']
                    ),null,null,'unique',null,null);
                    $dataToUpdate['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                    $reqUpdate = $this->postService->defaultUpdate('luni_users',$dataToUpdate,array(
                        'email'=>$get_user_recup_password['email_recup']
                    ));
                    if($reqUpdate)
                    {
                        $this->postService->defaultDelete('luni_recup_password_user',array(
                            'key_recup'=>$data['key_recup'],
                            'token_recup'=>$data['token_recup']
                        ));
                        $this->sessionContainer->MessageConfirmRegistration = array('type'=>'success','message'=>'Mot de passe changé avec succès.');

                        $tableJson['success'] = "success";
                        //$this->redirect()->toRoute('login');
                    }
                    else
                    {
                        $tableJson['error'] = "Une erreur s'est produite au cours du traitement.";
                    }


                }
                else
                {
                    $tableJson['error'] = "Veuillez vérifier les informations.";
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('login');
        }
        return $view;
    }

    private function connCheckAction()
    {
        if ($this->sessionContainer->IdUser !== '' && $this->sessionContainer->IdUser !== NULL)
        {
            //$this->sessionManager->getStorage()->clear('luni');
            return $this->redirect ()->toRoute ('dashboard');

        }
        elseif (isset($this->sessionContainer->CodeProfil) && !empty($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil != 2)
        {
            $this->redirect()->toRoute('home');
        }

        return 0;
    }


    public function getBaseUrl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost();
        return $baseUrl;
    }

    public function renderPage()
    {
        $this->layout = $this->layout();
        $this->layout->setVariables($this->contentVars);
        $this->layout->setTemplate('layout/recovering_password/layout');
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }
}