<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */



namespace Application\Controller;

use Application\Mapper\WebServiceDbSqlMapper;
use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;
use Laminas\View\Model\JsonModel;


class IndexController extends AbstractActionController
{


    protected $contentVars = array();
    protected $layout;

    protected $postService;
    protected $sessionContainer;
    protected $sessionManager;

    private $_currentUser = array();

    public function __construct(PostServiceInterface $postService)
    {
        $this->postService = $postService;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();



        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser != null)
        {
           $this->_currentUser = [
               'id'=>$this->sessionContainer->IdUser,
               'codeProfile'=>isset($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil !== null,
           ];
        }

    }

    public function indexAction()
    {
        $this->renderPage();
        $messageConfirmRegistration = array();

        $request = $this->getRequest();
        $this->sessionContainer->GetRef = isset($_GET['ref']) && $_GET['ref'] !== null ? intval($_GET['ref']) : null;



        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $view = null;
            $getRef = isset($_GET['ref']) && $_GET['ref'] !== null ? intval($_GET['ref']) : null;


            foreach ($data as $keys=>$values)
            {
                if(empty($data[$keys]))
                {
                    if($keys === "agree")
                    {
                        $tableJson['error'] = "Veuillez accepter les conditions d'utilisation.";
                        break;
                    }
                    else
                    {
                        $tableJson['error'] = "Veuillez remplir tous les champs !";
                        break;
                    }
                }

            }
            if(!filter_var(ltrim (strtolower ($data['email'])),FILTER_VALIDATE_EMAIL))
            {
                $tableJson['error'] = "Adresse mail invalide.";
            }
            elseif (strlen($data['fullname']) > 45)
            {
                $tableJson['error'] = "Nom trop long. Nous avons juste besoin de votre nom de famille et d'un prénom";
            }
            elseif (($data['password'] == $data['email']) || $data['password'] == $data['fullname'])
            {
                $tableJson['error'] = "Veuillez changer de mot passe. Le mot de passe ne doit identique ni au nom ni à votre adresse mail.";
            }
            elseif(strlen($data['fullname']) > 40)
            {
                $tableJson['error'] = "Nom trop long. Nous avons juste besoin du nom de famille ,d'un prénom et au maximum de deux prénoms.";
            }
            elseif($this->postService->password_strength (filter_var($data['password'],FILTER_SANITIZE_STRING)) === false)
            {
                $tableJson['error'] = WebServiceDbSqlMapper::MESSAGE_FOR_PASSWORD_ERROR;
            }
            elseif($data['password'] !== $data['rpassword'])
                $tableJson['error'] = "Les mots de passe doivent-être identiques";
            else
            {


                if($this->postService->checkIfUserExist (strtolower($data['email'])))
                {

                    $tableJson['error'] = "Vous avez déjà un compte chez nous.";
                    $view = new JsonModel($tableJson);
                    $view->setTerminal(true);

                    return $view;
                }
                else
                {


                    $getParent = [];
                   if($getRef !== null)
                   {
                      $getParent = $this->postService->getUser([
                          'parent_key'=>$getRef,
                          'email<>?'=>$data['email']
                      ]);
                   }
                    $key = $this->getToken (8);
                    $token = $this->postService->random(30);
                    $email = filter_var (trim(strtolower ($data['email']),FILTER_VALIDATE_EMAIL));
                    $dataTemp['email'] = filter_var (trim(strtolower ($data['email']),FILTER_VALIDATE_EMAIL));

                    $dataTemp['key'] = $key;
                    $dataTemp['token'] = $token;
                    $dataTemp['time_expired'] = time();

                    $dataTempToInsert['email_temp'] = $dataTemp['email'];
                    $dataTempToInsert['key_temp'] = $dataTemp['key'];
                    $dataTempToInsert['token_temp'] = $dataTemp['token'];
                    $dataTempToInsert['time_expired'] = $dataTemp['time_expired'] ;
                    $checkIfMailTempExist = (bool)$this->postService->defaultSelect('luni_users_temp',[],
                        array(
                            'email_temp=?'=>$dataTemp['email']
                        ),null,null,'unique',null,null);
                    if($checkIfMailTempExist)
                    {
                        $tableJson['error'] = "Veuillez vérifier vos mails pour activer votre compte";
                    }
                    else
                    {
                      
                      
                        $salutations = "";
                        $getHourNow = date('H');
                        $getHourNow = intval($getHourNow);

                        if($getHourNow > 12)
                        {
                            $salutations = "Bonsoir";

                        }
                        else
                        {
                            $salutations = "Bonjour";
                        }

                        $nom_complet = filter_var (ltrim ($data['fullname']),FILTER_SANITIZE_STRING);
                        $btn = 'Se connecter';
                        $url = $this->getBaseUrl().'/confirm-registration/';
                        $titre = 'Mail de confirmation d\'inscription';
                        $subject = 'Votre compte LUNICHANGE';
                        $email = $dataTemp['email'];
                        $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                        $f_id = WebServiceDbSqlMapper::FROM_ID;
                     

                        //setlocale (LC_ALL, 'fr_FR.utf8','fra');
                        date_default_timezone_set('UTC');
                        setlocale (LC_ALL, 'fr_FR.utf8','fra');

                        $dateNow = strftime('%A %d %B %Y, %H:%M');

                        $date_et_heure = "<small style='font-size:0.65em !important;'>Date et heure: ".$dateNow."</small><br><br>";

                        $msg ['salutations']= $date_et_heure."<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                        $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important"> Une demande de confirmation d\'inscription a été lancée.</p>';
                        $msg ['paragraphe'][]= '<p style="color:#000 !important;font-size: 0.9em !important ; line-height: 20px ">Si ce mail vous es destiné, veuillez cliquer sur le bouton bleue <b>"'.$btn.'"</b> ci-dessous. </p>';
                        $msg ['nb'][]= '<b style="font-size: 0.9em;text-decoration: underline !important; color: red !important;">NB: Le mail est valide pour une durée d\'une heure</b>.<br>';
                        $msg ['nb'][]= '<span style="font-size: 0.9em !important">Si cela ne vous concerne pas, veuillez ignorer ce message.</span><br>';
                        $msg ['base_url'] = $this->getBaseUrl();
                        $msg ['btn_url'] = 'confirm-registration';



                        $msg['politesse'] = '<b style="font-size: 0.8em !important">Coordialement l\'administration LUNICHANGE</b>';

                        $mailSend = $this->postService->sendMail($msg,$dataTempToInsert['key_temp'],$dataTempToInsert['token_temp'],$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template');



                        if($mailSend == null)
                        {
                            $this->postService->defaultInsert ('luni_users_temp',$dataTempToInsert);



                            $this->postService->defaultInsert ('luni_users',
                                array(
                                    'email'=>filter_var (trim(strtolower ($data['email'])),FILTER_VALIDATE_EMAIL),
                                    'nom_complet' => filter_var (ltrim ($data['fullname']),FILTER_SANITIZE_STRING),
                                    'password'=>password_hash ($data['password'], PASSWORD_DEFAULT),
                                    'code_profil'=>2,
                                    'statut'=>'0',
                                    'reference_parent'=>!empty($getParent) ? $getParent['parent_key'] : "",
                                    'parent'=>!empty($getParent) ? $getParent['id_user'] : null,
                                    'date_creation'=>date('Y-m-d H:i:s'),
                                    'lock_u'=>'0'
                                ));
                            $getUser = $this->postService->getUser([
                                'email'=>filter_var (trim(strtolower ($data['email'])),FILTER_VALIDATE_EMAIL)
                            ]);

                           /* $this->postService->defaultUpdate('luni_users',[
                                'gain'=>$gain,
                                'gain_fcfa'=>$gain_fcfa
                            ],[
                                'id_user'=>$getUser['id_user']
                            ]);*/

                            $tableJson['success'] = "Merci de consulter votre adresse électronique afin de finaliser votre inscription. Veuillez également consulter vos spams";

                        }
                        else
                        {
                            $tableJson['error'] = "Une erreur s'est produite au cours de l'opération. Veuillez rééssayer.";
                        }

                    }

                }
            }




            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }


        if(isset($this->sessionContainer->MessageConfirmRegistration) && $this->sessionContainer->MessageConfirmRegistration !== null)
        {
            $messageConfirmRegistration = $this->sessionContainer->MessageConfirmRegistration;

            unset($this->sessionContainer->MessageConfirmRegistration);
        }

        return array(
            'messagesConfirmRegistration'=>$messageConfirmRegistration,
            'getRef'=>$this->sessionContainer->GetRef

        );
    }
    public function deleteUsersTemp()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            if(isset($data['delete']) && $data['delete'] === true)
            {
                $getUsersTemp = $this->postService->defaultSelect('luni_users_temp',[],[],null,null,'all',null,null);

                foreach ($getUsersTemp as $keys=>$values)
                {
                    $checkIfUserExist = $this->postService->defaultSelect('luni_users',[],array('statut'=>'0','email'=>$values['email_temp']),null,null,'unique',null,null);
                    if(!empty($checkIfUserExist))
                    {
                        if(time() - $values['time_expired'] >= 60*720)
                        {
                            $this->postService->defaultDelete('luni_users_temp',array('email_temp'=>$values['email_temp']));
                            $this->postService->defaultDelete('luni_users',array('email'=>$checkIfUserExist['email']));
                            $tableJson['success'] = "success";
                        }
                    }
                    else
                    {
                        if(time() - $values['time_expired'] >= 60*60)
                        {
                            $this->postService->defaultDelete('luni_users_temp',array('email_temp'=>$values['email_temp']));
                            $tableJson['success'] = "success";
                        }
                    }
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

        }
        else
        {
            $this->redirect()->toRoute('login');
        }
        return $view;
    }
    
     public function passwordRecoverAction()
      {
        $request = $this->getRequest ();
        $tableJson = array();
        $view = null;

        if($request->isXmlHttpRequest())
        {
            $this->connCheckAction();
            $data = $request->getPost()->toArray();

            $data['email'] = trim(strtolower ($data['email']));
            if(empty($data['email']))
            {
                $tableJson['error'] = "Veuillez renseigner votre adresse mail";
            }
            elseif(!filter_var($data['email'],FILTER_VALIDATE_EMAIL))
            {
                $tableJson['error'] = 'Adresse mail incorrecte';
            }
            else
            {
                $checkIfUserExist =  $this->dbHelperService->defaultSelect ($this->dbHelperService->getDbTables ()['customers']['table'],
                    array(
                        $this->dbHelperService->getDbTables ()['customers']['columns']['email'] => filter_var ($data['email'],FILTER_VALIDATE_EMAIL)
                    ),
                    null,null,'unique',null,null
                );

                if(count($checkIfUserExist) > 0 || !empty($checkIfUserExist))
                {
                    if($checkIfUserExist['statut'] === '0')
                    {
                        $tableJson['error'] = "Vous ne pouvez pas effectuer cette opération. Votre compte n'est pas actif";
                    }
                    else
                    {
                        $checkIfTempExist = (bool)$this->dbHelperService->defaultSelect ('luni_recup_password_user',array('email_recup'=>$data['email']),null,null,'unique',null,null);

                        $salutations = "Bonjour";
                        if(date('H') > 12)
                        {
                            $salutations = "Bonsoir";
                        }


                        $key = $this->getToken (10);
                        $token = $this->random (30,'all');
                        $nom_complet = filter_var ($checkIfUserExist['nom_complet'],FILTER_SANITIZE_STRING);
                        $btn = 'Changer mot de passe';
                        $url = $this->getBaseUrl().'/recovering-password/';
                        $titre = 'Mail de réinitialisation de mot de passe';
                        $subject = 'Votre compte LUNICHANGE';
                        $email = $data['email'];
                        $from = WebServiceDbSqlMapper::MAIL_SEND_FROM;
                        $f_id = WebServiceDbSqlMapper::FROM_ID;
                      
                        date_default_timezone_set('UTC');
                        setlocale (LC_ALL, 'fr_FR.utf8','fra');
                        
                        $dateNow = strftime('%A %d %B %Y, %H:%M');
                        
                        $date_et_heure = "<small style='font-size:0.8em !important;'>Date et heure: ".$dateNow."</small><br><br>";

                        $msg ['salutations']= $date_et_heure."<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                        $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important"> Une demande de réinitialisation de mot de passe a été lancée.</p>';
                        $msg ['paragraphe'][]= '<p style="color:#000 !important;font-size: 0.9em !important ; line-height: 20px ">Si ce mail vous es destiné, veuillez cliquer sur le bouton bleue <b>"'.$btn.'"</b> ci-dessous. </p>';
                        $msg ['nb'][]= '<b style="font-size: 0.9em;text-decoration: underline !important; color: red !important;">NB: Le mail est valide pour une durée d\'une heure</b>.<br>';
                        $msg ['nb'][]= '<span style="font-size: 0.9em !important">Veuillez ignorer ce message s\'il ne vous concerne pas.</span><br>';
                        $msg ['base_url'] = $this->getBaseUrl();
                        $msg ['btn_url'] = 'customer/recovering-password';


                        $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                        $mailSend = $this->postService->sendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template');


                        if($mailSend === null)
                        {   if($checkIfTempExist)
                        {
                            $this->dbHelperService->defaultUpdate ('luni_recup_password_user',array(
                                'key_recup'=>$key,
                                'token_recup'=>$token,
                                'time_expired_recup'=>time()
                            ),array(
                                'email_recup'=>filter_var ($data['email'],FILTER_VALIDATE_EMAIL)
                            ));
                        }
                        else
                        {
                            $this->dbHelperService->defaultInsert ('luni_recup_password_user',array (
                                'email_recup'=>filter_var ($data['email'],FILTER_VALIDATE_EMAIL),
                                'key_recup'=>$key,
                                'token_recup'=>$token,
                                'time_expired_recup'=>time()
                            ));

                        }

                            $tableJson['success'] = "Merci pour l'attente! L'instruction de récupération du mot de passe a été envoyée à votre adresse électronique.";
                        }
                        else
                        {
                            $tableJson['error'] = "Une erreur s'est produite lors de l'envoi du mail. Veuillez vérifier votre connexion et rééssayer. Merci!!";
                        }



                    }
                }
                else
                {
                    $tableJson['error'] = "Désolé cette adresse mail n'existe pas.";
                }

            }



            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }else
        {
            $this->redirect()->toRoute('login');
        }

        return $view;
    }

    public function getBaseUrl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Laminas\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }



    private function renderPage()
    {
        $this->checkAuth();
        $this->layout = $this->layout();
        $this->layout->setVariables($this->contentVars);
        $this->layout->setTemplate('layout/login/layout');

    }

    public function logoutAction()
    {
        if(!empty($this->_currentUser))
        {
            $this->postService->defaultUpdate(
                'luni_users',[
                'derniere_connexion'=>date('Y-m-d H:i:s'),
                 'online'=>'0'
            ],[
                    'id_user'=>$this->_currentUser['id']
                ]
            );
            $this->sessionManager->getStorage()->clear('lunichange');
            return $this->redirect()->toRoute('login');
        }
        if(empty($this->_currentUser))
        {
            return $this->redirect()->toRoute('login');
        }

        return 0;
    }

    private function checkAuth()
    {
        if(!empty($this->_currentUser) && isset($this->_currentUser['id']))
        {
            /*if(isset($this->_currentUser['codeProfile']) && $this->_currentUser['codeProfile'] !== null)
            {
                if($this->_currentUser['codeProfile'] !== 2)
                {
                    return $this->redirect()->toRoute('home');
                }
            }*/
            return $this->redirect()->toRoute('dashboard');
        }

        return 0;
    }

    function getToken($len=32)
    {
        return substr(md5(openssl_random_pseudo_bytes(20)), -$len);
    }
}
