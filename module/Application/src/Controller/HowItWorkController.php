<?php

namespace Application\Controller;

use Application\Service\PostServiceInterface;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Uri\Uri;
use Laminas\Session\Container;


class HowItWorkController extends AbstractActionController
{

    protected $postService;
    protected $serviceLocator;

    protected $loginTable;
    protected $headerView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $footerVars;
    protected $contentVars;

    protected $table;

    protected $cur_USer;
    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;

    public function __construct(PostServiceInterface $postService,$container)
    {

        $this->postService = $postService;
        $this->serviceLocator = $container;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array(
            'howItWork'=>'how-it-work'
        );
       /* $this->table = array(
            'terms_contenu'=>'terms_contenu',
            'terms_of_use_en_tete'=>'terms_of_use_en_tete',
        );*/

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }

    public function indexAction()
    {

        $this->contentVars = array(
            'how_it_work'=>'how-it-work'
        );

        $this->renderPage();
  


        return array(
            
            
            );
    }



    public function renderPage()
    {
        $this->layout = $this->layout();
        $this->contentVars['termsOfUse'] = true;
        $this->layout->setVariables($this->contentVars);
      //  $this->layout->setTemplate('layout/login/layout');
    }

    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


}