<?php


namespace Application\Controller;


use Application\Service\PostServiceInterface;
use Laminas\Json\Json;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;
use Laminas\Session\Container;
use Laminas\View\Model\ViewModel;

class TreatmentController extends AbstractActionController
{



    protected $layout;
    protected $contentVars;

    protected $postService;

    protected $currentUser = array();
    protected $sessionManager;
    protected $sessionContainer;

    public function __construct(PostServiceInterface $postService)
    {
        $this->postService = $postService;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();

        if(isset($this->sessionContainer->IdUser) && $this->sessionContainer->IdUser !== null)
        {
            $this->currentUser = [

                'id'=>intval($this->sessionContainer->IdUser),
                'profile'=>filter_var($this->sessionContainer->ProfilName,FILTER_SANITIZE_STRING),
                'codeProfile'=>filter_var($this->sessionContainer->CodeProfil,FILTER_SANITIZE_STRING),
                'userName' => $this->sessionContainer->Login,


            ];
        }


        $this->contentVars = array();
    }


    public function indexAction()
    {

        echo "No action required";

        return $this->getResponse();
    }

    public function treatFormLoginAction()
    {
        $request = $this->getRequest();
        $view = null;
        $tableJson = array();
        $datas = $request->getPost()->toArray();

        if(!$request->isXmlHttpRequest())
        {
           return $this->redirect()->toRoute('login');
        }


        $datas['email'] = filter_var($datas['email'],FILTER_SANITIZE_STRING);
        $datas['password'] = filter_var($datas['password'],FILTER_SANITIZE_STRING);
        $userLog = $this->postService->userLogin($datas['email'],$datas['password']);
        foreach($datas as $keys=>$values)
        {
            if(empty(ltrim($datas[$keys])))
            {
                $tableJson['error'] = "Veuillez remplir tous les champs";
            }
        }
        if(!filter_var($datas['email'],FILTER_SANITIZE_EMAIL))
        {
            $tableJson['error'] = "Adresse mail incorrecte, veuillez saisir une adresse mail valide";
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if(!empty($userLog['message']) || $userLog['message'] !== "")
        {
            $tableJson['error'] = filter_var($userLog['message'],FILTER_SANITIZE_STRING);
            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }
        if(isset($this->currentUser['id']) && $this->currentUser['id'] !== NULL)
        {
            $tableJson['error'] = "Vous avez déja une session en cours.";

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);

            return $view;
        }

        if($userLog['message'] === "" || empty($userLog['message']))
        {
            $this->sessionContainer->IdUser = intval($userLog['id_user']);
            $this->sessionContainer->ProfilName = $userLog['profil_name'];
            $this->sessionContainer->CodeProfil = intval($userLog['code_profil']);
            $this->sessionContainer->Login = $userLog['login'];
            $this->sessionContainer->LastLoginTimeStamp = time();
            $this->postService->defaultUpdate('luni_users',[
                'online'=>'1'
            ],[
                'id_user'=>intval($userLog['id_user'])
            ]);
            $getUser = $this->postService->defaultSelect('luni_users',[],[
                'id_user'=>filter_var($userLog['id_user'],FILTER_SANITIZE_NUMBER_INT)
            ],null,null,'unique',null,null);

            if(intval($userLog['code_profil']) === 2)
            {
                $this->sessionContainer->MessageWelcome = 'Nous sommes ravi de vous revoir '.$getUser['nom_complet'].'.';
            }
            else
            {
                $this->sessionContainer->MessageWelcome = 'Nous sommes ravi de vous revoir '.$getUser['email'].'.';
            }


            if($getUser['telephone'] === null || $getUser['telephone'] === '')
            {
                $checkifNotifExist = $this->postService->defaultSelect('notifications',[],array('id_user'=>$getUser['id_user'],
                    'motif'=>'compléter profil'),null,null,'unique',null,null);
                if(empty($checkifNotifExist))
                {
                    $this->postService->defaultInsert('notifications',array('contenu_notification'=>'Veuillez compléter votre profil. Compléter votre profil vous donnera un accès complet à la plateforme',
                            'motif'=>'compléter profil',
                            'type_notification'=>'Message',
                            'id_user'=>$getUser['id_user'])
                    );
                }
            }

            if($userLog['code_profil'] == 2)
            {
                $this->sessionContainer->UserName = $userLog['user_name'];

            }

            if($getUser['first_connect'] === '0')
            {
                $this->postService->defaultUpdate('luni_users',
                    array(

                        'first_connect'=>'1'
                    ),

                    array(
                        'id_user=?'=>$userLog['id_user']
                    ));
                $this->sessionContainer->MessageWelcome = "Bienvenue ".$getUser['email']." sur la plateforme d'échange LUNICHANGE. Merci de nous avoir choisi.";

            }
            $tableJson['success'] = "success";
        }

        $view = new JsonModel($tableJson);
        $view->setTerminal(true);

        return $view;

    }
    public function treatFormAction()
    {
        $request = $this->getRequest();
        $tableJson = array();
        $view = null;


        if($request->isXmlHttpRequest())
        {
            $data = $request->getPost()->toArray();
            $dataEmpty = false;
            if(!empty($data))
            {
                foreach ($data as $keys=>$values)
                {
                    if(empty($data[$keys]))
                    {
                        $tableJson['error'] = 'Veuillez remplir tous les champs.';
                        $dataEmpty = true;

                    }
                }
            }


            if($this->postService->password_strength (filter_var($data['password'],FILTER_SANITIZE_STRING)) === false)
            {
                $tableJson['error'] = WebServiceDbSqlMapper::MESSAGE_FOR_PASSWORD_ERROR;
                //var_dump($messageError);
            }
            elseif($data['password'] !== $data['rpassword'])
            {
                $tableJson['error'] = "Les mots de passe doivent-être identiques.";
            }
            else
            {

                if(!$dataEmpty)
                {
                    $get_user_recup_password = $this->postService->defaultSelect('luni_recup_password_user',[],array(
                        'key_recup'=>$data['key_recup'],
                        'token_recup'=>$data['token_recup']
                    ),null,null,'unique',null,null);
                    $dataToUpdate['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                    $reqUpdate = $this->postService->defaultUpdate('luni_users',$dataToUpdate,array(
                        'email'=>$get_user_recup_password['email_recup']
                    ));
                    if($reqUpdate)
                    {
                        $this->postService->defaultDelete('luni_recup_password_user',array(
                            'key_recup'=>$data['key_recup'],
                            'token_recup'=>$data['token_recup']
                        ));
                        $this->sessionContainer->MessageConfirmRegistration = array('type'=>'success','message'=>'Mot de passe changé avec succès.');

                        $tableJson['success'] = "success";
                        //$this->redirect()->toRoute('login');
                    }
                    else
                    {
                        $tableJson['error'] = "Une erreur s'est produite au cours du traitement.";
                    }


                }
                else
                {
                    $tableJson['error'] = "Veuillez vérifier les informations.";
                }
            }

            $view = new JsonModel($tableJson);
            $view->setTerminal(true);
        }
        else
        {
            $this->redirect()->toRoute('login');
        }
        return $view;
    }

    
     public function passwordRecoverAction()
      {
        $request = $this->getRequest ();
        $tableJson = array();
        $view = null;

        if($request->isXmlHttpRequest())
        {
            //$this->connCheckAction();
            $data = $request->getPost()->toArray();

            $data['email'] = trim(strtolower ($data['email']));
            if(empty($data['email']))
            {
                $tableJson['error'] = "Veuillez renseigner votre adresse mail";
            }
            elseif(!filter_var($data['email'],FILTER_VALIDATE_EMAIL))
            {
                $tableJson['error'] = 'Adresse mail incorrecte';
            }
            else
            {
                $checkIfUserExist =  $this->postService->defaultSelect ($this->postService->getDbTables ()['customers']['table'],[],
                    array(
                        $this->postService->getDbTables ()['customers']['columns']['email_user'] => filter_var ($data['email'],FILTER_SANITIZE_STRING)
                    ),
                    null,null,'unique',null,null
                );

                if(count($checkIfUserExist) > 0 || !empty($checkIfUserExist))
                {
                    if($checkIfUserExist['statut'] === '0')
                    {
                        $tableJson['error'] = "Vous ne pouvez pas effectuer cette opération. Votre compte n'est pas actif";
                    }
                    else
                    {
                        $checkIfTempExist = (bool)$this->postService->defaultSelect ('luni_recup_password_user',[],array('email_recup'=>$data['email']),null,null,'unique',null,null);

                        $salutations = "Bonjour";
                        if(date('H') > 12)
                        {
                            $salutations = "Bonsoir";
                        }


                        $key = $this->getToken (10);
                        $token = $this->random (30,'all');
                        $nom_complet = filter_var ($checkIfUserExist['nom_complet'],FILTER_SANITIZE_STRING);
                        $btn = 'Changer mot de passe';
                        $url = $this->getBaseUrl().'/recovering-password/';
                        $titre = 'Mail de réinitialisation de mot de passe';
                        $subject = 'Votre compte LUNICHANGE';
                        $email = $data['email'];
                        $from = "contact@lunichange.com";
                        $f_id = "LUNICHANGE - ADMIN";
                      
                        date_default_timezone_set('UTC');
                        setlocale (LC_ALL, 'fr_FR.utf8','fra');
                        
                        $dateNow = strftime('%A %d %B %Y, %H:%M');
                        
                        $date_et_heure = "<small style='font-size:0.8em !important;'>Date et heure: ".$dateNow."</small><br><br>";

                        $msg ['salutations']= $date_et_heure."<h7 style='color: #000 !important;font-size: 1.0em !important; padding-top: 50px'>".$salutations." cher client, </h7>";
                        $msg ['paragraphe'] []='<p style="color:#000 !important;font-size: 0.9em !important;padding:0 !important;margin-top: 10px !important; text-align: justify-all !important"> Une demande de réinitialisation de mot de passe a été lancée.</p>';
                        $msg ['paragraphe'][]= '<p style="color:#000 !important;font-size: 0.9em !important ; line-height: 20px ">Si ce mail vous es destiné, veuillez cliquer sur le bouton bleue <b>"'.$btn.'"</b> ci-dessous. </p>';
                        $msg ['nb'][]= '<b style="font-size: 0.9em;text-decoration: underline !important; color: red !important;">NB: Le mail est valide pour une durée d\'une heure</b>.<br>';
                        $msg ['nb'][]= '<span style="font-size: 0.9em !important">Veuillez ignorer ce message s\'il ne vous concerne pas.</span><br>';
                        $msg ['base_url'] = $this->getBaseUrl();
                        $msg ['btn_url'] = 'recovering-password';


                        $msg['politesse'] = '<b style="font-size: 0.85em !important">Coordialement l\'administration LUNICHANGE.</b>';

                        $mailSend = $this->postService->SendMail($msg,$key,$token,$nom_complet,$url,$btn,$titre,$subject,$email,$from,$f_id,'mail_template');


                        if($mailSend === null)
                        {   if($checkIfTempExist)
                        {
                            $this->postService->defaultUpdate ('luni_recup_password_user',array(
                                'key_recup'=>$key,
                                'token_recup'=>$token,
                                'time_expired_recup'=>time()
                            ),array(
                                'email_recup'=>filter_var ($data['email'],FILTER_VALIDATE_EMAIL)
                            ));
                        }
                        else
                        {
                            $this->postService->defaultInsert ('luni_recup_password_user',array (
                                'email_recup'=>filter_var ($data['email'],FILTER_VALIDATE_EMAIL),
                                'key_recup'=>$key,
                                'token_recup'=>$token,
                                'time_expired_recup'=>time()
                            ));

                        }

                            $tableJson['success'] = "Merci pour l'attente! L'instruction de récupération du mot de passe a été envoyée à votre adresse électronique.";
                        }
                        else
                        {
                            $tableJson['error'] = "Une erreur s'est produite lors de l'envoi du mail. Veuillez vérifier votre connexion et rééssayer. Merci!!";
                        }



                    }
                }
                else
                {
                    $tableJson['error'] = "Désolé cette adresse mail n'existe pas.";
                }

            }



            $view = new JsonModel($tableJson);
            $view->setTerminal (true);
        }else
        {
            $this->redirect()->toRoute('login');
        }

        return $view;
    }
    
     private function getToken($len=32)
      {
            return substr(md5(openssl_random_pseudo_bytes(20)), -$len);
      }
    public function random($car)
     {
        $string = "";
        $chaine = "1234567890";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }
    
    public function getBaseUrl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Laminas\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }



}
