<?php


namespace Application\Controller;

use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Uri\Uri;
use Laminas\Session\Container;


class TermsOfUseController extends AbstractActionController
{

    protected $postService;
    protected $serviceLocator;

    protected $loginTable;
    protected $headerView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $footerVars;
    protected $contentVars;

    protected $table;

    protected $cur_USer;
    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;

    public function __construct(PostServiceInterface $postService, $container)
    {

        $this->postService = $postService;
        $this->serviceLocator = $container;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array(
            'termsOfUse'=>'terms-of-use'
        );
        $this->table = array(
            'terms_contenu'=>'terms_contenu',
            'terms_of_use_en_tete'=>'terms_of_use_en_tete',
        );

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }

    public function indexAction()
    {

        $this->contentVars = array(
            'terms_of_use'=>'terms-of-use'
        );

        $this->renderPage();
        $join = null;



        $join[] = array(
                'table'=>array(
                    'c'=>$this->table['terms_contenu']
                ),
            'condition'=>'c.en_tete=e.id_en_tete'
        );


        $myArray = array();

        $getEnTete = $this->postService->defaultSelect(array('e'=>'terms_of_use_en_tete'),[],array(
            'e.statut'=>'1',
        ),$join,null,'all','id_en_tete','order ASC');

        foreach($getEnTete as $keys=>$values)
        {
            foreach($values as $key=>$element)
            {
                if($key === 'contenu')
                    $myArray[$keys][$key] = $this->postService->defaultSelect('terms_contenu',[],array(
                        'statut'=>'1',
                        'en_tete'=>intval($values['id_en_tete'])
                    ),null,null,'all',null,'id_contenu ASC');
                else
                {
                    $myArray[$keys][$key] = $element;
                }
            }
        }





        return array(
            'terms_of_use'=>$myArray
        );
    }




    private function renderPage()
    {
        $this->layout = $this->layout();
        $this->contentVars['termsOfUse'] = true;
        $this->checkAuth();
        $this->layout = $this->layout();
        $this->layout->setVariables($this->contentVars);
        $this->layout->setTemplate('layout/login/layout');

    }

    private function checkAuth()
    {
        if(!empty($this->_currentUser) && isset($this->_currentUser['id']))
        {

            return $this->redirect()->toRoute('dashboard');
        }

        return 0;
    }
    protected function _getHelper($helper, $serviceLocator)
    {
        return $serviceLocator
            ->get('ViewHelperManager')
            ->get($helper);
    }

    public function getUriPath()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost() . '/' . $uri->getPath();
        return $baseUrl;
    }


}