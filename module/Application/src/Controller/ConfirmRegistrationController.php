<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 07/12/2020
 * Time: 17:28
 */

namespace Application\Controller;


use Application\Service\PostServiceInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Container;

class ConfirmRegistrationController extends AbstractActionController
{

    protected $postservice;

    protected $loginTable;
    protected $headerView;
    protected $footerView;
    protected $layout;
    protected $headerVars;
    protected $menuVars;
    protected $footerVars;
    protected $contentVars;

    protected $cur_USer;
    protected $grp_access;
    protected $currentUser;
    protected $sessionManager;
    protected $sessionContainer;
    protected $pusher;

    protected  $serviceLocator;

    public function __construct(PostServiceInterface $postService,$container)
    {
        $this->postservice = $postService;
        $this->sessionContainer = $container;
        $this->sessionContainer = new Container('lunichange');
        $this->sessionManager = $this->sessionContainer->getManager();
        //Layout
        $this->contentVars = array();

        $this->headerVars = array();
        $this->footerVars = array();
        $this->contentVars = array();

    }



    public function indexAction()
    {
        $this->connCheckAction();
        $request = $this->getRequest()->getRequestUri();
        $slashRequest = explode(' /',trim($request));
        $slashRequest = explode('/',$slashRequest[0]);

        if(!isset($slashRequest[2],$slashRequest[3]) )
        {
            $this->redirect()->toRoute('login');
        }

        $id = $slashRequest[2];
        $params = array_pop($slashRequest);

        $myRequest = $this->postservice->defaultSelect('luni_users_temp',[],array('key_temp'=>$id,'token_temp'=>$params),null,null,'unique',null,null);

        if(!empty($myRequest))
        {
            if(time() - intval($myRequest['time_expired']) <= 60 *60)
            {
                $checkIfUserExist = $this->postservice->defaultSelect('luni_users',[],array(
                    'email'=>$myRequest['email_temp']),null,null,'unique',null,null);
                if(!empty($checkIfUserExist))
                {

                    $gain = 5.00;
                    $gain = floatval($gain);

                    $updating = (bool) $this->postservice->defaultUpdate('luni_users',array('date_creation'=>date('Y-m-d H:i:s'),'statut'=>'1',
                        'gain'=>$gain,
                        ),array('email'=>$checkIfUserExist['email']));

                    if($updating)
                    {
                        $this->postservice->defaultDelete('luni_users_temp',array('email_temp'=>$checkIfUserExist['email']));
                        $this->sessionContainer->MessageConfirmRegistration = array(
                            'type'=>'success',
                            'message'=>"Inscription réussie. Vous pouvez maintenant vous connecter"
                        );
                        $this->redirect()->toRoute('login');
                    }
                    else
                    {
                        $this->postservice->defaultDelete('luni_users',array('email'=>$myRequest['email_temp']));
                        $this->postservice->defaultDelete('luni_users_temp',array('email_temp'=>$myRequest['email_temp']));
                        $this->sessionContainer->MessageConfirmRegistration = array('type'=>'error','message'=>"Une erreur s'est produite au cours de la validation");
                        $this->redirect()->toRoute('login');
                    }
                }
                else
                {
                    $this->postservice->defaultDelete('luni_users',array('email'=>$myRequest['email_temp']));
                    $this->postservice->defaultDelete('luni_users_temp',array('email_temp'=>$myRequest['email_temp']));
                    $this->sessionContainer->MessageConfirmRegistration = array('type'=>'error','message'=>'Cette inscription ne peut-être validées');
                    $this->redirect()->toRoute('login');
                }
            }
            else
            {
                $this->postservice->defaultDelete('luni_users',array('email'=>$myRequest['email_temp']));
                $this->postservice->defaultDelete('luni_users_temp',array('email_temp'=>$myRequest['email_temp']));
                $this->sessionContainer->MessageConfirmRegistration = array(
                    'type'=>'error',
                    'message'=>'Le lien a expiré.'
                );
                $this->redirect()->toRoute('login');
            }
        }
        else
        {
            $this->sessionContainer->MessageConfirmRegistration = array(
                'type'=>'error',
                'message'=>'Le lien est incorrecte.'
            );
            $this->redirect()->toRoute('login');
        }

    }

    private function connCheckAction()
    {
        if ($this->sessionContainer->IdUser !== '' && $this->sessionContainer->IdUser !== NULL)
        {
            //$this->sessionManager->getStorage()->clear('luni');
            return $this->redirect ()->toRoute ('dashboard');

        }
        elseif (isset($this->sessionContainer->CodeProfil) && !empty($this->sessionContainer->CodeProfil) && $this->sessionContainer->CodeProfil != 2)
        {
            $this->redirect()->toRoute('home');
        }

        return 0;
    }
}