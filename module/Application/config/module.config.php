<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */

namespace Application;



use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Regex;
use Laminas\Router\Http\Segment;


return
    [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/website[/:action]',
                    'defaults' => [
                        'controller' => Controller\HomeController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'login' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'=>'[0-9]+'
                    ),
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate'=>true,

            ],
            'register'=>[
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/:action[/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z]+',
                        'id'=>'[0-9]+'

                    ),
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'register',
                    ],
                ]
            ],
            'register-test'=>[
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/:action[/:id]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z]+',
                        'id'=>'[0-9]+'

                    ),
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'register-test',
                    ],
                ]
            ],
            'login-api' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/login-api[/:action[/:id]]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => [
                        'controller' => Controller\TreatmentController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'faq' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/faq[/:action[/:id]]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => [
                        'controller' => Controller\FaqController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            
            'how-it-work' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/how-it-work[/:action[/:id]]',
                    'constraints'=>array(
                        'action'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => [
                        'controller' => Controller\HowItWorkController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'confirm-registration'=>array(
                'type'=>Regex::class,
                'options'=>array(
                    'regex' => '/confirm-registration/(?<id>[a-zA-Z0-9_-]+)/(?<params>[a-zA-Z0-9_-]+)?',
                    'defaults'=>array(
                        '__NAMESPACE__'=>'Application\Controller',
                        'controller'=>Controller\ConfirmRegistrationController::class,
                        'action'=>'index'
                    ),
                    'spec' => '/confirm-registration/%id%/%params%',
                ),
                'verb'=>'get,post',
                'may_terminate'=>true
            ),

            'recovering-password'=>array(
                 'type'=>Regex::class,
                'options'=>array(
                    'regex' => '/recovering-password/(?<id>[a-zA-Z0-9_-]+)/(?<params>[a-zA-Z0-9_-]+)?',
                    'defaults'=>array(
                        '__NAMESPACE__'=>'Application\Controller',
                        'controller'=>Controller\RecoveringPasswordController::class,
                        'action'=>'index'
                    ),
                    'spec' => '/recovering-password/%id%/%params%',
                ),
                'verb'=>'get,post',
                'may_terminate'=>true
            ),

            'logout'=>[
                'type'=> Segment::class,
                'options'=>[
                    'route'=>'/logout',
                    'defaults'=>[
                        'controller'=> Controller\IndexController::class,
                        'action'=> 'logout'
                    ]
                ],
                'may_terminate'=>true
            ],
            'terms-of-use'=>[
                'type' => Literal::class,
                'options' => array(
                    'route' => '/terms-of-use',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => Controller\TermsOfUseController::class,
                        'action'        => 'index',
                    ),
                ),
                'verb'=>'get,post',
                'may_terminate'=>true
            ],
            'dashboard'=>[
                'type'=> Segment::class,
                'options'=>[
                    'route' => '/dashboard[/:action[/:id]]',
                    'constraints'=> [
                        'action'=>'[a-zA-Z][a-zA-Z0-9_]*',
                        'id'=>'[0-9]+'
                    ],
                    'defaults' =>[
                        'controller'=> \Dashboard\Controller\DashboardController::class,
                        'action'=>'index'
                    ]
                ]
            ]

        ],
    ],

    'service_manager' => array(
        'factories' =>array(
            'Application\Mapper\PostMapperInterface' => 'Application\Factory\WebServiceDbSqlMapperFactory',
            'Application\Service\PostServiceInterface' => 'Application\Factory\PostServiceFactory',
            'Laminas\Db\Adapter\Adapter'           =>   'Laminas\Db\Adapter\AdapterServiceFactory',
        ),
    ),


    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/header'           => __DIR__ . '/../view/layout/header.phtml',
            'layout/menu'           => __DIR__ . '/../view/layout/menu.phtml',
            'layout/footer'           => __DIR__ . '/../view/layout/footer.phtml',
            'layout/website/layout'   => __DIR__ . '/../view/layout/website/layout.phtml',
            'layout/website/header'   => __DIR__.'/../view/layout/website/header.phtml',
            'layout/website/footer'   => __DIR__.'/../view/layout/website/footer.phtml',
            'layout/website/slider'   => __DIR__.'/../view/layout/website/slider.phtml',
            'layout/website/menu'   => __DIR__.'/../view/layout/website/menu.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'layout/recovering_password/layout'  => __DIR__ . '/../view/layout/recovering_password/layout.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array('ViewJsonStrategy'),
    ],
];
