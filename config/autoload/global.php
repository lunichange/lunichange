<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'db'=>array(
        'driver' => 'PDO',
        'dsn'=> 'mysql:dbname=u622323772_luni_final;host=localhost',
        'driver_options'=>[PDO::MYSQL_ATTR_INIT_COMMAND =>'SET NAMES \'UTF8\'']
    ),
    'devises-payment-methods'=>[
        'MTN-CI',
        'MOOV-TG',
        'ORM-CI',
        'ORM-SEN',
        'MTN-BJ',
        'MOOV-BJ'
    ],
    'devises-fedapay'=>[
        'MTN-CI',
        'MOOV-TG',
        'ORM-CI',
        'MOOV-IN-NI-BU-CI-MAL',
        'ORM-SEN',
        'MTN-BJ'
    ],
    'devises-kkiapay'=>[
        'MTN-BJ',
        'MOOV-BJ'
    ],
    'paydunyaEnvironment'=>'live',
    'paydunyaPublicTestKey'=>'test_public_1h8y0Q5eT6uMkNkKqHYDnz2fR5n',
    'paydunyaPrivateTestKey'=>'test_private_QVxtQfgY0XTQPOsnjouy0rQ0ZwX',

    'paydunyaPublicProductionKey'=>'live_public_6gzwaigHO2CIDEBX0C2kt4joi3k',
    'paydunyaPrivateProductionKey'=>'live_private_w88petk0EShls9Kj2P2wgM2ABtG',
    'paydunyaProductionToken'=>'giMOeeLqWzlJxJLDdVVJ',

    'paydunyaToken'=>'jnGq60QxMeCWmZcQD35Q',
    'paydunyaMasterKey'=>"savNVdVk-JuVl-py4B-OA36-oTLjMs6kZ6gn",

    'fedapayEnvironment'=>'live',
    'fedapayTestKey'=>'pk_sandbox_HLp3Q8Ley1EHRBrCO9MGeMPO',
    'fedapayLiveKey'=>'pk_live_dfhvGkS156G_JEF2MIDr99j-',

    'fedaPaySecretKeys'=>[
        'sandbox'=>'sk_sandbox_nQPuEygf-ngch2JwwlkQXDNG',
        'live'=>'sk_live_6IwmiHnkWDqyuxnJ_uZ-pFBZ'
    ],
    //'log_path' => 'C:/xampp_7_4/htdocs/lunichange.com/public/data/log/',
    'main_path'=>'/home/u622323772/domains/lunichange.com/',
     'log_path' => '/home/u622323772/domains/lunichange.com/public_html/data/log/',
    'document_path'=>  '/home/u622323772/domains/lunichange.com/public_html/data/documents/',
    //'document_path'=>  'C:/xampp_7_4/htdocs/lunichange.com/public/data/documents/',
    'lienSite'=>'https://www.lunichange.com/',
    //'lienSite'=>'https://lunichange.lan/',
    'devisesPath'=>'/home/u622323772/domains/lunichange.com/public_html/img/dashboard/devises/',
    //'devisesPath'=>'/xampp/htdocs/lunichange.com/public_html/img/dashboard/devises/',
    'mailman' => array(
        'MailMan\LUNI' => [
            'default_sender' => 'contact@lunichange.com',
            'transport' => [
                'type' => 'smtp',
                'options' => [
                    'host' => 'smtp.hostinger.fr',
                    'port' => '465',
                    'connection_class' => 'login',
                    'connection_config' => [
                        'ssl' => 'ssl',
                        'username' => 'contact@lunichange.com',
                        'password' => 'Lun1ch@ng3@2020',
                    ]
                ]
            ],
        ],
    ),
];
