"use strict";
var KTDatatablesSearchOptionsAdvancedSearch=function() {
    $.fn.dataTable.Api.register("column().title()", function() {
            return $(this.header()).text().trim()
        }
    );
    return {
        init:function() {
            var t;
            t=$("#kt_table_currencies").DataTable( {
                    responsive:!0, dom:"<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", lengthMenu:[5, 10, 25, 50], pageLength:5, language: {
                        lengthMenu: "Display _MENU_"
                    }
                    , searchDelay:500, processing:!0, serverSide:!0,
                ajax: {
                        url:basePath+"administration/currencies-management/list-currencies",
                    type:"POST",
                    data: {
                            columnsDef: ["img_devise", "libelle_devise", "lib_categorie_devise", "libelle_famille", "solde_min", "solde", "status"]
                        }
                    },
                    searching:true
                    , columns:[ {
                        data: "img_devise"
                    }
                        , {
                            data: "libelle_devise"
                        }
                        , {
                            data: "lib_categorie_devise"
                        }
                        , {
                            data: "libelle_famille"
                        }
                        , {
                            data: "solde_min"
                        }
                        , {
                            data: "solde"
                        }
                        , {
                            data: "status"
                        }
                        , {
                            data: "Actions", responsivePriority: -1
                        }
                    ], initComplete:function() {
                        this.api().columns().every(function() {
                                switch(this.title()) {
                                    case"lib_categorie_devise":this.data().unique().sort().each(function(t, a) {
                                            $('.kt-input[data-col-index="2"]').append('<option value="'+t+'">'+t+"</option>")
                                        }
                                    );
                                        break;
                                    case"status":var t= {
                                            '1': {
                                                title: "Actif", class: " kt-badge--success"
                                            }
                                            , '0': {
                                                    title: "Inactif", class: " kt-badge--warning"
                                            }

                                        }
                                    ;
                                        this.data().unique().sort().each(function(a, e) {
                                                $('.kt-input[data-col-index="6"]').append('<option value="'+a+'">'+t[a].title+"</option>")
                                            }
                                        );
                                        break;

                                }
                            }
                        )
                    }
                    , columnDefs:[
                        {
                        targets:-1, title:"Actions", width:'115px', orderable:!1, render:function(t, a, e, n) {

                        var p = e.id_devise;
                        let link_update_status;

                        if(e.status === '1')
                        {
                            link_update_status = '<a title="Modifier le status" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="updateStatusForCurrencies('+p+',\'0\');" href="javascript:void(0);" data-id="\'+t+\'"><i class="la la-leaf"></i> </a>';

                        }
                        else
                        {
                            link_update_status = '<a title="Modifier le status" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="updateStatusForCurrencies('+p+',\'1\');" href="javascript:void(0);" data-id="\'+t+\'"><i class="la la-leaf"></i></a>';

                        }


                        return link_update_status +
                            '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="updateDevise('+e.id_devise+')" title="View"> <i class="la la-edit"></i></a>' +
                            '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="callModalSettings('+e.id_devise+','+e.categorie_devise+')"><i class="flaticon-cogwheel"></i> </a>'
                        }
                    }
                    ,  {
                        targets:4, render:function(t, a, e, n) {

                            return '<span class="kt-badge kt-badge--primary font-size-0_9 font-weight-600 kt-badge--inline kt-badge--pill">'+e.solde_min+'&nbsp;('+e.libelle_famille.toLowerCase()+')</span>'
                        }
                    }
                    ,
                    {
                        targets:5, render:function(t, a, e, n)
                        {

                            return '<span class="kt-badge kt-badge--info font-size-0_9 font-weight-600 kt-badge--inline kt-badge--pill">'+e.solde+'&nbsp;('+e.libelle_famille.toLowerCase()+')</span>'
                        }
                    },
                    {
                        targets:6, render:function(t, a, e, n) {
                            var i= {
                                    '1': {
                                        title: "Actif", class: " kt-badge--success"
                                    }
                                    , '0': {
                                        title: "Non actif", class: " kt-badge--danger"
                                    }

                                }
                            ;
                            return void 0===i[t]?t:'<span class="kt-badge '+i[t].class+' kt-badge--inline kt-badge--pill">'+i[t].title+"</span>"
                        }
                    }
                        ,{
                        targets:0,render:function(t,a,e,n)
                        {
                            return '<a href="javsacript:void(0);" onclick="updateLogoDevise(\''+e.id_devise+'\')"> <img src="'+e.img_devise+'" class="img-circle img_rounded img-fluid img-reflection img-responsive" style="width: 50px !important; height: 50px !important"/></a>'
                        }

                    }
                    ]
                }
            ),
                $("#kt_search").on("click", function(a) {
                        a.preventDefault();
                        var e= {}
                        ;
                        $(".kt-input").each(function() {
                                var t=$(this).data("col-index");
                                e[t]?e[t]+="|"+$(this).val(): e[t]=$(this).val()
                            }
                        ), $.each(e, function(a, e) {
                                t.column(a).search(e||"", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_reset").on("click", function(a) {
                        a.preventDefault(), $(".kt-input").each(function() {
                                $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_datepicker").datepicker( {
                        todayHighlight:!0, templates: {
                            leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    }
                )
        },

        refresh:function(){

            $("#kt_table_currencies").DataTable( {
                    responsive:!0, dom:"<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", lengthMenu:[5, 10, 25, 50], pageLength:5, language: {
                        lengthMenu: "Display _MENU_"
                    }
                    , searchDelay:500, processing:!0, serverSide:!0,
                    ajax: {
                        url:basePath+"administration/currencies-management/list-currencies",
                        type:"POST",
                        data: {
                            columnsDef: ["img_devise", "libelle_devise", "lib_categorie_devise", "libelle_famille", "solde_min", "solde", "status"]
                        }
                    }
                    , columns:[ {
                        data: "img_devise"
                    }
                        , {
                            data: "libelle_devise"
                        }
                        , {
                            data: "lib_categorie_devise"
                        }
                        , {
                            data: "libelle_famille"
                        }
                        , {
                            data: "solde_min"
                        }
                        , {
                            data: "solde"
                        }
                        , {
                            data: "status"
                        }
                        , {
                            data: "Actions", responsivePriority: -1
                        }
                    ], initComplete:function() {
                        this.api().columns().every(function() {
                                switch(this.title()) {
                                    case"lib_categorie_devise":this.data().unique().sort().each(function(t, a) {
                                            $('.kt-input[data-col-index="2"]').append('<option value="'+t+'">'+t+"</option>")
                                        }
                                    );
                                        break;
                                    case"status":var t= {
                                            '1': {
                                                title: "Actif", class: " kt-badge--success"
                                            }
                                            , '0': {
                                                title: "Inactif", class: " kt-badge--warning"
                                            }

                                        }
                                    ;
                                        this.data().unique().sort().each(function(a, e) {
                                                $('.kt-input[data-col-index="6"]').append('<option value="'+a+'">'+t[a].title+"</option>")
                                            }
                                        );
                                        break;

                                }
                            }
                        )
                    }
                    , columnDefs:[ {
                        targets:-1, title:"Actions", orderable:!1, render:function(t, a, e, n) {

                            var p = e.id_devise;
                            let link_update_status;

                            if(e.status === '1')
                            {
                                link_update_status = '<a class="dropdown-item link-for-update-status" onclick="updateStatusForCurrencies('+p+',\'0\');" href="#" data-id="\'+t+\'"><i class="la la-leaf"></i> Modifier Status</a>';

                            }
                            else
                            {
                                link_update_status = '<a class="dropdown-item link-for-update-status" onclick="updateStatusForCurrencies('+p+',\'1\');" href="#" data-id="\'+t+\'"><i class="la la-leaf"></i> Modifier Status</a>';

                            }


                            return'                        <span class="dropdown">                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">                              <i class="la la-ellipsis-h"></i>                            </a>                            <div class="dropdown-menu dropdown-menu-right">                                <a class="dropdown-item" href="javascript:void(0);"><i class="la la-edit"></i> Edit Details</a>'+link_update_status+'                             <!--<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a> -->                           </div>                        </span>                        ' +
                                '<a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="updateDevise('+e.id_devise+')" title="View"><i class="la la-edit"></i></a>'




                        }
                    }
                        ,  {
                            targets:6, render:function(t, a, e, n) {
                                var i= {
                                        '1': {
                                            title: "Actif", class: " kt-badge--success"
                                        }
                                        , '0': {
                                            title: "Non actif", class: " kt-badge--danger"
                                        }

                                    }
                                ;
                                return void 0===i[t]?t:'<span class="kt-badge '+i[t].class+' kt-badge--inline kt-badge--pill">'+i[t].title+"</span>"
                            }
                        }
                        ,{
                            targets:0,render:function(t,a,e,n)
                            {
                                return '<img src="'+e.img_devise+'" class="img-circle img-fluid img-reflection img-responsive" width="50" height="50"/>'
                            }

                        }
                    ]
                }
            ),
                $("#kt_search").on("click", function(a) {
                        a.preventDefault();
                        var e= {}
                        ;
                        $(".kt-input").each(function() {
                                var t=$(this).data("col-index");
                                e[t]?e[t]+="|"+$(this).val(): e[t]=$(this).val()
                            }
                        ), $.each(e, function(a, e) {
                                t.column(a).search(e||"", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_reset").on("click", function(a) {
                        a.preventDefault(), $(".kt-input").each(function() {
                                $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_datepicker").datepicker( {
                        todayHighlight:!0, templates: {
                            leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    }
                )
        }
    }
}

();
jQuery(document).ready(function() {
    KTDatatablesSearchOptionsAdvancedSearch.init();


    }

);