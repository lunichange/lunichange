"use strict";
var KTDatatablesSearchOptionsAdvancedSearch=function() {
    $.fn.dataTable.Api.register("column().title()", function() {
            return $(this.header()).text().trim()
        }
    );
    return {
        init:function() {
            var t;
            t=$("#kt_table_numbers-management").DataTable( {
                    responsive:!0, dom:"<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", lengthMenu:[5,10,15,20,30,40,50], pageLength:10, language: {
                        lengthMenu: "Display _MENU_"
                    }
                    ,
                        searchDelay:500,
                        processing:!0,
                        serverSide:!0,
                        searching:true,
                        ajax: {
                            url:basePath+"administration/numbers-management/list-numbers",
                            type:"POST",
                            data:
                            {
                                columnsDef: ["id_numero", "libelle_numero","number_name", "lib_categorie_devise","lib_devise","status", "Actions"]
                            }
                        }
                    , columns:[ {
                        data: "id_numero"
                    }
                        , {
                            data: "libelle_numero"
                        }
                        , {
                            data: "number_name"
                        }
                        , {
                            data: "lib_categorie_devise"
                        }
                        ,
                        {
                            data: "lib_devise"
                        }
                        ,
                    {
                            data: "status"
                        }

                        , {
                            data: "Actions", responsivePriority: -1
                        }
                    ], initComplete:function() {
                        this.api().columns().every(function() {
                                switch(this.title()) {

                                    case"status":var t= {
                                            '0': {
                                                title: "Non actif", class: "kt-badge--danger"
                                            }
                                            , '1': {
                                                title: "Actif", class: " kt-badge--success"
                                            }
                                        }
                                    ;
                                        this.data().unique().sort().each(function(a, e) {
                                                $('.kt-input[data-col-index="6"]').append('<option value="'+a+'">'+t[a].title+"</option>")
                                            }
                                        );
                                        break;

                                }
                            }
                        )
                    }
                    , columnDefs:[ {
                        targets:-1, title:"Actions", orderable:!1, render:function(t, a, e, n) {

                            let typeAction = null;
                            let imageAction = null;
                            let titleAction = null;
                            let link = null;

                                if(e.status === '0')
                                {

                                    imageAction = '<svg xmlns="http://www.w3.org/2000/svg" style="width: 21px !important; height: 21px !important" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                        '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                        '        <mask fill="white">\n' +
                                        '            <use xlink:href="#path-1"/>\n' +
                                        '        </mask>\n' +
                                        '        <g/>\n' +
                                        '        <path d="M15.6274517,4.55882251 L14.4693753,6.2959371 C13.9280401,5.51296885 13.0239252,5 12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L14,10 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C13.4280904,3 14.7163444,3.59871093 15.6274517,4.55882251 Z" fill="#000000"/>\n' +
                                        '    </g>\n' +
                                        '</svg>';


                                    link = '<a href="javascript:void(0);" title="Activer le numéro" onclick="updateStatusNumber('+e.id_numero+',\'activate\')">'+imageAction+'</a>';

                                }
                                else
                                {


                                    imageAction = '<svg xmlns="http://www.w3.org/2000/svg"  style="width: 21px !important; height: 21px !important" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                        '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                        '        <mask fill="white">\n' +
                                        '            <use xlink:href="#path-1"/>\n' +
                                        '        </mask>\n' +
                                        '        <g/>\n' +
                                        '        <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"/>\n' +
                                        '    </g>\n' +
                                        '</svg>';

                                    link = '<a href="javascript:void(0);" title="Désactiver le numéro" onclick="updateStatusNumber('+e.id_numero+',\'disabled\')">'+imageAction+'</a>';

                                }

                            return ''+link +
                                '<a href="javascript:void(0);" onclick="deleteNumber('+e.id_numero+')"><i class="flaticon-delete text-danger"></i> </a>'
                        }
                    }
                    ,
                    {

                        targets:1, render:function(t, a, e, n) {

                            var span = null;

                            if(e.libelle_numero === null)
                            {
                                span = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>";
                            }
                            else
                            {
                                span = "<span class='kt-badge kt-badge--unified-brand kt-badge--inline badge_hover' data-toggle='kt-tooltip' data-html=true title='Cliquer pour modifier' style='font-size: 1.0em !important; font-weight: bold !important' title='' onclick='updateLibelleNumber("+e.id_numero+")'>"+e.libelle_numero+"</span>";
                            }

                            return span;
                        }
                    },

                    {

                        targets:2, render:function(t, a, e, n) {

                            var span = null;

                            if(e.number_name === null)
                            {
                                span = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>";
                            }
                            else
                            {
                                span = "<span class='kt-badge kt-badge--unified-primary kt-badge--inline badge_hover' data-toggle='kt-tooltip' data-html=true title='Cliquer pour modifier' style='font-size: 1.0em !important; font-weight: bold !important' title='' onclick='updateNumberName("+e.id_numero+")'>"+e.number_name+"</span>";
                            }

                            return span;
                        }
                    },
                    {

                        targets:4, render:function(t, a, e, n) {

                            var span = null;

                                if(e.devise.length === 0)
                                {
                                    span = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>";
                                }
                                else
                                {
                                    span = "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill'>"+e.devise.lib_devise+"</span>";
                                }

                            return span;
                        }
                    },
                    {

                            targets:5, render:function(t, a, e, n) {
                                var i= {
                                        "0": {
                                            title: "Non actif", class: "kt-badge--danger"
                                        }

                                        , "1": {
                                            title: "Actif", class: " kt-badge--success"
                                        }

                                    }
                                ;
                                return void 0===i[t]?t:'<span class="kt-badge '+i[t].class+' kt-badge--inline kt-badge--pill">'+i[t].title+"</span>"
                            }
                        }

                    ]
                }
            ),
                $("#kt_search").on("click", function(a) {
                        a.preventDefault();
                        var e= {}
                        ;
                        $(".kt-input").each(function() {
                                var t=$(this).data("col-index");
                                e[t]?e[t]+="|"+$(this).val(): e[t]=$(this).val()
                            }
                        ), $.each(e, function(a, e) {
                                t.column(a).search(e||"", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_reset").on("click", function(a) {
                        a.preventDefault(), $(".kt-input").each(function() {
                                $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_datepicker").datepicker( {
                        todayHighlight:!0, templates: {
                            leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    }
                )
        }
    }
}

();
jQuery(document).ready(function() {
        KTDatatablesSearchOptionsAdvancedSearch.init()
    }

);

function updateNumberName(id)
{
  id= parseInt(id);

  $.ajax({
      type:'POST',
      dataType:'json',
      url:basePath+'administration/numbers-management/std-get?table=numeros_transactions&type=unique',
      data:
          {
              id_numero:id
          },
      success:function(response)
      {



          Swal.fire({
              title: 'Modifier le nom',
              input: 'text',

              inputAttributes: {

                  name:'edit_number_name',
                  id:'edit_number_name',
                  placeholder:response.number_name,
                  value:response.number_name,
                  innerHTML: response.number_name,
                  required:true
              },
              showCancelButton: true,
              confirmButtonText: 'Modifier',
              showLoaderOnConfirm: true,
              preConfirm: (login) => {
                  return fetch(`/dashboard/administration/numbers-management/update-number-name?id_numero=${id}&data=${login}`)
                      .then(response => {
                          if (!response.ok) {
                              throw new Error(response.statusText)
                          }
                          console.log(response);

                          return response
                      })
                      .catch(error => {
                          Swal.showValidationMessage(
                              `Request failed: ${error}`
                          )
                      })
              },
              allowOutsideClick: () => !Swal.isLoading()
          }).then((result) => {
              if (result.value) {
                  console.log(response.value);
                  Swal.fire({
                      position: 'top-end',
                      type: 'success',
                      title: 'Modification effectuée !',
                      showConfirmButton: true,
                     // timer: 1500
                  });
                  setTimeout(function () {
                      document.location.href= "";
                  },2e3)
              }
          })

      },
      error:function()
      {

      }
  })
}

function updateLibelleNumber(id)
{
    id = parseInt(id);
    $.ajax({
        type:'POST',
        dataType:'json',
        url:basePath+'administration/numbers-management/std-get?table=numeros_transactions&type=unique',
        data:
            {
                id_numero:id
            },
        success:function(response)
        {


            let type_compte;

            switch (parseInt(response.type_numero))
            {
                case 1:
                    type_compte = "BANQUE EN LIGNE";
                    break;
                case 2:
                    type_compte = "COMPTE MOBILE";
                    break;

                case 4:
                    type_compte = "CRYPTOMONNAIES";
            }

            Swal.fire({
                title: 'Modifier le libelle du numéro | Type d Compte :'+type_compte,
                input: 'text',

                inputAttributes: {

                    name:'edit_number_name',
                    id:'edit_number_name',
                    placeholder:response.libelle_numero,
                    value:response.libelle_numero,
                    innerHTML: response.libelle_numero,
                    required:true,
                    minLength:8
                },
                showCancelButton: true,
                confirmButtonText: 'Modifier',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    return fetch(`/dashboard/administration/numbers-management/update-libelle-number?id_numero=${id}&data=${login}`)
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(response.statusText)
                            }
                            //console.log(response);

                            return response
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {

                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Modification effectuée !',
                        showConfirmButton: true
                    });
                    setTimeout(function () {
                        document.location.href= "";
                    },2e3)
                }
            })

        },
        error:function()
        {

        }
    })
}