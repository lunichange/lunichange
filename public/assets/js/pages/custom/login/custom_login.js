$.noConflict();

$(document).ready(function(){

    var images = ["backgrounds/background_login_2.jpg", "backgrounds/salle-gym-flou-abstrait.jpg"];
    var i = 0;
    $("#login_container_background").css("background-image", "url(assets/images/" + images[i] + ")");
    setInterval(function () {
        i++;
        if (i == images.length) {
            i = 0;
            $('.kt-login__subtitle').css('color','black');
        }
        $("#login_container_background").fadeOut("slow", function () {
            $(this).css("background-image", "url(assets/images/" + images[i] + ")");
            $(this).fadeIn("slow");
        });
    }, 20000);

});