"use strict";
var KTSessionTimeoutDemo= {
        init:function() {
            $.sessionTimeout( {
                    title:"Notification de fin de session",
                message:"Votre session est sur le point d'expirer.",
                keepAliveUrl:""+location.origin+'/dashboard/keep-alive',
                redirUrl:""+location.origin+'/customer/login/logout',
                logoutUrl:""+location.origin+'/customer/login/logout',
                warnAfter:35e3,
                redirAfter:35e3,
                ignoreUserActivity:!0,
                countdownMessage:"Redirection dans {timer} secondes.",
                countdownBar: !0,
                logoutButton:'Se déconnecter',
                keepAliveButton:"Rester connecter"


                }
            )
        }
    }

;
jQuery(document).ready(function() {
        KTSessionTimeoutDemo.init()
    }

);