"use strict";
var KTDatatablesSearchOptionsAdvancedSearch=function() {
    $.fn.dataTable.Api.register("column().title()", function() {
            return $(this.header()).text().trim()
        }
    );
    return {
        init:function() {
            var t;
            t=$("#kt_table_statistics").DataTable( {
                    responsive:!0, dom:"<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", lengthMenu:[1], pageLength:1, language: {
                        lengthMenu: "Display _MENU_"
                    }
                    , searchDelay:500, processing:!0, serverSide:!0, ajax: {
                        url:basePath+"administration/statistics/list-statistics",
                        type:"POST",
                        data: {
                            columnsDef: ["period", "transactionsValidate", "transactionsCancel", "allTransactions"]
                        }
                    }
                    , columns:[
                        {
                            data: "period"
                        }
                        , {
                            data: "transactionsValidate"
                        }

                        , {
                            data: "transactionsCancel"
                        }

                        , {
                            data: "allTransactions"
                        }
                    ], initComplete:function() {
                        this.api().columns().every(function() {
                                switch(this.title()) {



                                }
                            }
                        )
                    }
                    , columnDefs:[ {
                        targets:3, title:"Total transactions", orderable:!1, render:function(t, a, e, n) {


                            return '<span class="kt-badge kt-badge--info font-size-1_1 kt-badge--xl">'+e.allTransactions+'</span>';
                        }
                    }
                        , {
                            targets:0, width:'170px', render:function(t, a, e, n) {






                                return '<span class="font-size-1_2 font-weight-bold">'+e.period+'</span>';
                            }
                        },
                        {
                            targets:1, render:function(t, a, e, n) {






                                return '<span class="kt-badge kt-badge--success font-size-1_1 kt-badge--xl">'+e.transactionsValidate+'</span>';
                            }
                        },
                        {
                            targets:2,  render:function(t, a, e, n) {



                                return '<span class="kt-badge kt-badge--dark font-size-1_1 kt-badge--xl">'+e.transactionsCancel+'</span>';
                            }
                        }


                    ]
                }
            ),
                $("#kt_search").on("click", function(a) {
                        a.preventDefault();
                        var e= {}
                        ;
                        blockUI('Recherche en cours...');
                        $(".kt-input").each(function() {
                                var t=$(this).data("col-index");
                                e[t]?e[t]+="|"+$(this).val(): e[t]=$(this).val()
                            }
                        ), $.each(e, function(a, e) {
                                t.column(a).search(e||"", !1, !1)
                            }
                        ), t.table().draw(),unblockUI()
                    }
                ),
                $("#kt_reset").on("click", function(a) {
                        blockUI('Chargement en cours...');
                        a.preventDefault(), $(".kt-input").each(function() {
                                $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1);
                                $('input[data-col-index="1"]').val("");
                            }
                        ), t.table().draw(),unblockUI(), selectOptionOfSelect('kt_list_customers',"")

                    }
                ),
                $('#kt_daterangepicker_transactions .form-control').daterangepicker({
                        "locale": {
                            "format": "DD/MM/YYYY",
                            "separator": " - ",
                            "applyLabel": "Appliquer",
                            "cancelLabel": "Annuler",
                            "fromLabel": "De",
                            "toLabel": "A",
                            "customRangeLabel": "Personnaliser",
                            "daysOfWeek": [
                                "Di",
                                "Lu",
                                "Ma",
                                "Me",
                                "Je",
                                "Ve",
                                "Sa"
                            ],
                            "monthNames": [
                                "Janvier",
                                "Février",
                                "Mars",
                                "Avril",
                                "Mai",
                                "Juin",
                                "Juillet",
                                "Août",
                                "Septembre",
                                "Octobre",
                                "Novembre",
                                "Décembre"
                            ],
                            "firstDay": 1
                        },

                        ranges: {
                            "Aujourd'hui": [moment(), moment()],
                            "Hier": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                            "Ces 07 derniers jours": [moment().subtract(6, "days"), moment()],
                            "Ces 30 derniers jours": [moment().subtract(29, "days"), moment()],
                            "Ce mois": [moment().startOf("month"), moment().endOf("month")],
                            "Le mois dernier": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
                        },
                    },
                    function (a, t, e) {
                        $("#kt_daterangepicker_transactions .form-control").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"));
                    }

                )
        }
    }
}

();
jQuery(document).ready(function() {
    KTDatatablesSearchOptionsAdvancedSearch.init();
    $('.title_devise').tooltip();

    let kt_btn_validate = $('.kt-btn_validate');

    kt_btn_validate.on('click',function(){
        let current_value = $(this).val();

        //  console.log(current_value);
    });






});



function ValidateTransaction(id)
{
    let current_value = parseInt(id);


    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Voulez-vous valider cette transaction ?",
        imageUrl: location.origin+'/img/vector_check.jpg',
        imageWidth: 300,
        imageHeight: 200,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#aaa',
        confirmButtonText: 'Oui, confirmer',
        cancelButtonText:"Annuler"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'transactions/validate-transaction',
                data:
                    {

                        id_transaction:current_value
                    },
                beforeSend:function()
                {
                    blockUI('Traitement en cours...')
                },
                success:function(response)
                {
                    unblockUI();
                    if(response.error !== undefined)
                    {


                        toastr.error(response.error);
                    }
                    else
                    {
                        toastr.success('Transaction validée !');
                        setTimeout(function(){
                            document.location.href = basePath+'transactions';
                        },2e2);

                    }
                },
                error:function()
                {

                }
            });
        }
    })

    /*const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: 'Signed in successfully'
    })*/

}

function CancelTransaction(id)
{
    let current_value = parseInt(id);


    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Voulez-vous annuler cette transaction ?",
        imageUrl: location.origin+'/img/transactions/transaction_cancel.png',
        imageWidth: 200,
        imageHeight: 200,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#aaa',
        confirmButtonText: 'Oui, confirmer',
        cancelButtonText:"Annuler"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'transactions/cancel-transaction',
                data:
                    {

                        id_transaction:current_value
                    },
                beforeSend:function()
                {
                    blockUI('Traitement en cours...')
                },
                success:function(response)
                {
                    unblockUI();
                    if(response.error !== undefined)
                    {


                        toastr.error(response.error);
                    }
                    else
                    {
                        toastr.success('Transaction annulée !');
                        setTimeout(function(){
                            document.location.href = basePath+'/transactions';
                        },2e2);

                    }
                },
                error:function()
                {

                }
            });
        }
    })
}

function RefreshStateTransaction(id)
{
    //alert(id);
}
function ArchiveTransaction(id)
{
    // alert(id);
}
function showDateTransaction(id_transaction)
{
    id_transaction = parseInt(id_transaction);

    if(id_transaction !== "")
    {
        $.ajax({

            type:'POST',
            url:basePath+'transactions/std-get?type=unique&table=transactions',
            dataType:'JSON',
            data:
                {
                    id_transaction:id_transaction
                },
            success:function(response)
            {
                $('#kt_modal_date_transaction .modal-body').html(response.date_transaction);
                $('#kt_modal_date_transaction').modal('show');
            },
            error:function()
            {
                swal.fire('Oops',"Une erreur s'est produite. Veuillez vérifier votre connexion internet et rééssayer","error");
            }
        });
    }
}
function showUsername(id_user,element)
{
    id_user = parseInt(id_user);

    if(id_user !== "")
    {
        $.ajax({

            type:'POST',
            url:basePath+'transactions/std-get?type=unique&table=customers',
            dataType:'JSON',
            data:
                {
                    id_user:id_user
                },
            success:function(response)
            {
                $('#kt_modal_info_customer .modal-body').html('<h5> <b style="text-decoration: underline !important">Nom Complet:</b>&nbsp;'+response.nom_complet+'</h5><br><h5><b style="text-decoration: underline !important">Téléphone:</b> +'+response.dial_code+'&nbsp;'+response.telephone+'</h5><br><h5><b style="text-decoration: underline !important">Email:</b>&nbsp;'+response.email+'</h5>');
                $('#kt_modal_info_customer').modal('show');

            },
            error:function()
            {
                swal.fire('Oops',"Une erreur s'est produite. Veuillez vérifier votre connexion internet et rééssayer","error");
            }
        });
    }
}
