"use strict";
var KTDatatableAutoColumnHideDemo= {
        init:function() {
            var t;
            t=$(".kt-datatable").KTDatatable( {
                    data: {
                        type:"remote", source: {
                            read: {
                                url: basePath+"administration/customers-management/list-customers"
                            }
                        }
                        , pageSize:5, saveState:!1, serverPaging:!0, serverFiltering:!0, serverSorting:!0
                    }
                    , layout: {
                        scroll: !0, height: 550
                    }
                    , sortable:!0, pagination:!0, search: {
                        input: $("#generalSearch")
                    }
                    , columns:[ {
                        field: "nom_user", title: "Nom Complet"
                    }
                        , {
                            field:"email_user", title:"Email",align:"center", width:"auto", template:function(t) {
                                return '<span class="kt-badge kt-badge--dark kt-badge--inline kt-badge--pill bg-cyan color-white font-size-0_9" style="width: 180px !important">'+t.email_user+'</span>';


                            }
                        }
                        , {
                            field: "telephone", title: "Téléphone", width: "auto",template:function(t) {

                                var e;
                                    if(t.telephone ===""  || t.telephone == null)
                                    {
                                      e = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>"
                                    }
                                    else
                                    {
                                        e = "<span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>+"+t.dial_code+"&nbsp;"+t.telephone+"</span>";
                                    }

                                return e;
                            }

                        }
                        , {
                            field: "pays", title: "Pays", width: "auto",template:function(t) {

                            var e;
                            if(t.pays.length === 0  || t.pays == null || t.pays.lib_pays === undefined)
                            {
                                e = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>"
                            }
                            else
                            {
                                e = "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill'>"+t.pays.lib_pays+"</span>";
                            }

                            return e;
                         }
                        }
                        , {
                            field: "ville", title: "Ville", width: "auto",template:function(t) {

                            var e;
                            if(t.ville ===""  || t.ville == null)
                            {
                                e = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>"
                            }
                            else
                            {
                                e = "<span class='kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill'>"+t.ville+"</span>";
                            }

                            return e;
                            }
                        }
                        , {
                            field: "region", title: "Région", width: "auto",template:function(t) {

                            var e;
                            if(t.region ===""  || t.region == null)
                            {
                                e = "<span class='kt-badge  kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>"
                            }
                            else
                            {
                                e = "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill'>"+t.region+"</span>";
                            }

                            return e;
                        }
                        }
                        , {
                            field: "adresse", title: "Adresse", width:"auto",template:function(t) {

                            var e;
                            if(t.adresse ===""  || t.telephone == null || t.adresse===null)
                            {
                                e = "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill'>Non disponible</span>"
                            }
                            else
                            {
                                e = "<span class='kt-badge kt-badge--primary kt-badge--inline kt-badge--pill'>"+t.adresse+"</span>";
                            }

                            return e;
                            }
                        },
                    {
                        field:"online", title:"Status en ligne", template:function(t) {
                            var e= {
                                    "1": {
                                        title: "En ligne", class: "kt-badge--success"
                                    }
                                    , "0": {
                                        title: "Déconnecté", class: "kt-badge--danger"
                                    }

                                }
                            ;
                            let span;
                            if(t.online === '0')
                            {
                               span ='<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill"> Déconnecté(e)</span>';
                            }
                            else
                            {
                                span ='<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">En ligne</span>';

                            }
                            return span;

                        }
                    },
                        {
                            field:"status", title:"Compte atif", template:function(t) {
                                var e= {
                                        '1': {
                                            title: "Oui", class: " kt-badge--success"
                                        }
                                        , '0': {
                                            title: "Non", class: " kt-badge--danger"
                                        }

                                    }
                                ;
                                return'<span class="kt-badge '+e[t.status].class+' kt-badge--inline kt-badge--pill">'+e[t.status].title+"</span>"
                            }
                        },

                        {
                            field: "derniere_connexion", title: "Dernière connexion", type: "date", format: "Y-M-D H:i:s"
                        }, {
                            field: "date_creation", title: "Présence sur la plateforme depuis", type: "date", format: "Y-M-D"
                        },

                        {
                            field:"lock_u", title:"Status", template:function(t) {
                                var e= {
                                        '0': {
                                            title: "Actif", class: " kt-badge--success"
                                        }
                                        , '1': {
                                            title: "Bloqué", class: " kt-badge--danger"
                                        }

                                    }
                                ;

                                return'<span class="kt-badge '+e[t.lock_u].class+' kt-badge--inline kt-badge--pill">'+e[t.lock_u].title+"</span>"
                            }
                        },
                         {
                            field:"Actions", title:"Actions", sortable:!1, width:110, overflow:"visible", autoHide:!1, template:function(t,n,e) {

                                let lock_user;
                                let title;
                                let typeAction;

                                if(t.status === '0')
                                {
                                   lock_user = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                       '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                       '        <mask fill="white">\n' +
                                       '            <use xlink:href="#path-1"/>\n' +
                                       '        </mask>\n' +
                                       '        <g/>\n' +
                                       '        <path d="M15.6274517,4.55882251 L14.4693753,6.2959371 C13.9280401,5.51296885 13.0239252,5 12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L14,10 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C13.4280904,3 14.7163444,3.59871093 15.6274517,4.55882251 Z" fill="#000000"/>\n' +
                                       '    </g>\n' +
                                       '</svg>';
                                   title = "Activer le compte";
                                   typeAction = 'unlock';
                                }
                                else
                                {
                                    lock_user = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                        '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                        '        <mask fill="white">\n' +
                                        '            <use xlink:href="#path-1"/>\n' +
                                        '        </mask>\n' +
                                        '        <g/>\n' +
                                        '        <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"/>\n' +
                                        '    </g>\n' +
                                        '</svg>';
                                    title = "Désactiver le compte";
                                    typeAction = 'lock';
                                }
                                let details_customer = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <rect x="0" y="0" width="24" height="24"/>\n' +
                                    '        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>\n' +
                                    '        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" x="7" y="10" width="5" height="2" rx="1"/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" x="7" y="14" width="9" height="2" rx="1"/>\n' +
                                    '    </g>\n' +
                                    '</svg>';
                                let delet_false_customer = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <rect x="0" y="0" width="24" height="24"/>\n' +
                                    '        <path d="M6,8 L18,8 L17.106535,19.6150447 C17.04642,20.3965405 16.3947578,21 15.6109533,21 L8.38904671,21 C7.60524225,21 6.95358004,20.3965405 6.89346498,19.6150447 L6,8 Z M8,10 L8.45438229,14.0894406 L15.5517885,14.0339036 L16,10 L8,10 Z" fill="#000000" fill-rule="nonzero"/>\n' +
                                    '        <path d="M14,4.5 L14,3.5 C14,3.22385763 13.7761424,3 13.5,3 L10.5,3 C10.2238576,3 10,3.22385763 10,3.5 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\n' +
                                    '    </g>\n' +
                                    '</svg>';

                               // console.log(t);
                                return '<a href="javascript:void(0);" onclick="updateStatusCustomer('+t.id_user+',\''+typeAction+'\')" title="'+title+'">'+lock_user+'</a>&nbsp;&nbsp;<a href="javascript:void(0);" title="Détails utilisateur">'+details_customer+'</a>&nbsp;<a href="javascript:void(0);" title="Supprimer le compte" onclick="deleteUser('+t.id_user+')">'+delet_false_customer+'</a>';
                            }
                        }
                    ]
                }
            ),
                $("#kt_form_status").on("change", function() {
                        t.search($(this).val().toLowerCase(), "Status")
                    }
                ),
                $("#kt_form_type").on("change", function() {
                        t.search($(this).val().toLowerCase(), "Type")
                    }
                ),
                $('#btn_validate_period').on('click',function(){
                    let periodValue = $('#kt_form_period').val();
                    if(periodValue !== "")
                    {
                        t.search(periodValue.toLowerCase(), "periodDate")
                    }
                    else
                    {
                        toastr('Veuillez choisir une période','error');
                    }
                }),
                $('#btn_cancel_period').on('click',function(){

                    let value = true;
                    $('#kt_form_period').val('');
                    t.search(value, "destroyPeriodDate");
                    $(".kt-datatable").KTDatatable().destroy();
                    KTDatatableAutoColumnHideDemo.init();

                }),
                $("#kt_form_status,#kt_form_type").selectpicker()
        }
    }

;
jQuery(document).ready(function() {
        KTDatatableAutoColumnHideDemo.init();


        $('#kt_form_period').daterangepicker({
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Appliquer",
                "cancelLabel": "Annuler",
                "fromLabel": "De",
                "toLabel": "A",
                "customRangeLabel": "Personnaliser",
                "daysOfWeek": [
                    "Di",
                    "Lu",
                    "Ma",
                    "Me",
                    "Je",
                    "Ve",
                    "Sa"
                ],
                "monthNames": [
                    "Janvier",
                    "Février",
                    "Mars",
                    "Avril",
                    "Mai",
                    "Juin",
                    "Juillet",
                    "Août",
                    "Septembre",
                    "Octobre",
                    "Novembre",
                    "Décembre"
                ],
                "firstDay": 1
            },
            minDate:$('#start_date').val(),
          //  maxDate:$("#end_date").val(),
            ranges: {
                "Aujourd'hui": [moment(), moment()],
                "Hier": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "Ces 07 derniers jours": [moment().subtract(6, "days"), moment()],
                "Ces 30 derniers jours": [moment().subtract(29, "days"), moment()],
                "Ce mois": [moment().startOf("month"), moment().endOf("month")],
                "Le mois dernier": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
            },
        },
            function (a, t, e) {
                $("#kt_form_period").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"));
            }

        );


}


);

function updateStatusCustomer(id,action)
{
   id = parseInt(id);

   if(id !== "" && (typeof id === 'number' || typeof id === 'bigint'))
   {
       Swal.fire({
           title: 'Êtes-vous sûr ?',
           text: "Modifier le status de ce compte ?",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Confirmer',
           cancelButtonText:'Annuler'
       }).then((result) => {
           if (result.value) {
               //console.log(result.value);
               $.ajax({
                   type:'POST',
                   url:basePath+'administration/customers-management/update-status',
                   dataType:'JSON',
                   data:
                       {
                           id_user:id,
                           action:action
                       },
                   success:function(response)
                   {
                       if(response.success !== undefined)
                       {
                           swal.fire('Terminé',response.success,'success');
                           $(".kt-datatable").KTDatatable().destroy();


                               KTDatatableAutoColumnHideDemo.init();

                       }
                       else
                       {
                           swal.fire('Oops',response.error,'error');
                       }
                   },
                   error:function()
                   {
                       swal.fire('Oops',"Une erreur s'est produite avec Ajax !",'error');
                   }

               })
           }
       })

   }
   else
   {
       swal.fire('Oops',"Impossible de traiter ces informations",'error');
   }
}

function deleteUser(id)
{
    id = parseInt(id);

    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Voulez-vous supprimer ce compte ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, confirmer',
        cancelButtonText: 'Annuler'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                type:'POST',
                dataType: 'JSON',
                url:basePath+'administration/customers-management/delete-customer',
                data:
                    {
                        id_user:id
                    },
                success:function(response)
                {
                    if(response.success !== undefined)
                    {
                        swal.fire('Terminé',response.success,'success');
                        $(".kt-datatable").KTDatatable().destroy();


                            KTDatatableAutoColumnHideDemo.init();

                    }
                    else
                    {
                        swal.fire('Oops',response.error,'error');
                    }
                },
                error:function()
                {

                }
            })
        }
    })
}