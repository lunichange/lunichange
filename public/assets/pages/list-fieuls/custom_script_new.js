"use strict";
var KTDatatableRemoteAjaxDemo = {
    init: function () {
        var t;
        (t = $("#kt-datatable-fieuls").KTDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: basePath+"customer/fieuls",
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0,
            },
            layout: { scroll: !1, footer: !1 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },
            columns: [
                { field: "nom_complet", title: "Nom & Prénom(s)", textAlign: "center" ,template:function(e){

                    return '<span class="kt-badge  kt-badge--primary  kt-badge--inline kt-badge--pill kt-badge--square font-size-1_0 kt-padding-10">'+e.nom_complet+'</span>';
                    }},
                {
                    field: "email",
                    title: "Adresse mail",
                    template: function (t) {
                        return '<i class=""></i>'+t.email;
                    },
                },
                { field: "date_creation", title: "Date d'inscription", type: "date", format: "MM/DD/YYYY" }


            ],
        }));
    },
};
jQuery(document).ready(function () {
    KTDatatableRemoteAjaxDemo.init();
});
