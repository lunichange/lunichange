$(document).ready(function(){

    var baseUrl = location.origin;
    $('.btn-actions').on({

        click:function()
        {
          let currentValue = $(this).attr('data-id');
          let currentAction = $(this).attr('data-action');

          let Message = "Voulez-vous activer ce mode de paiement ?";

          if(currentAction === "disabled")
          {
              Message = "Voulez-vous désactiver ce mode de paiement ?";
          }
          customConfirmSweetAlert('warning',Message,function(){

              $.ajax({

                  type:'POST',
                  dataType:'JSON',
                  url:'/administration/payment-methods/operation-on-payment',
                  data:
                      {
                          'action':currentAction,
                          'id':currentValue,
                          'configuration':'standard'
                      },
                  beforeSend:function(){
                      blockUI('Traitement en cours...');
                  },
                  success:function(response)
                  {
                     unblockUI();
                     if(response.success !== undefined)
                     {
                         customSweetAlert('success',"Terminé",response.success);
                         setTimeout(function(){
                             document.location.href = "";
                         },2e2);

                     }
                     else if(response.error !== undefined)
                     {
                        customSweetAlert('error',"Erreur",response.error)
                     }
                  },
                  error:function()
                  {
                      unblockUI();

                      console.log("Une erreur s'est produite au cours du traitement...");
                  }

              })

          })

        }
    });

    $('.btn-activation-globale').on({

        click:function()
        {
            let currentValue = $(this).attr('data-id');

            customConfirmSweetAlert('warning',"Voulez-vous activez ce mode de paiement de façon globale ?",function(){
                $.ajax({

                    type:'POST',
                    dataType:'JSON',
                    data:
                        {
                            id:currentValue,
                            configuration: 'global'
                        },
                    beforeSend:function()
                    {
                        blockUI('Traitement en cours...');
                    },
                    success:function(response)
                    {
                        if(response.success !== undefined)
                        {
                            customSweetAlert('success',"Terminé",response.success);
                            setTimeout(function(){
                                document.location.href = "";
                            },2e2);

                        }
                        else if(response.error !== undefined)
                        {
                            customSweetAlert('error',"Erreur",response.error)
                        }
                    },
                    error:function()
                    {
                        unblockUI();
                        console.log("Une erreur s'est produite au cours du traitement")
                    }


                })
            })
        }
    })


    let paymentMethods = $('#payment-methods');

    paymentMethods.on('change',function(){

        let currentValue = $(this).val();

        if(currentValue === "")
        {
            $('#list_payment_methods').empty();
            $('#btn-submit-container').empty();
        }
        if(currentValue !== "")
        {
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:'/administration/payment-methods/list-devises',
                beforeSend:function()
                {
                    blockUI('Traitement en cours...');
                },
                success:function(response)
                {
                    unblockUI();

                    if(response.success !== undefined)
                    {
                        let result = response.success;
                        let html="";
                        result.forEach(function(elements){
                            html+=' <div>\n' +
                                '    <label class="checkbox">\n' +
                                '     <input type="checkbox" value="'+elements.id_devise+'" name="checkbox_devise[]">\n' +
                                '     <img class="img-responsive img-circle me-card-1"   src="'+elements.image_devise+'" style="width: 40px !important; border-radius: 50% !important; height: 40px !important; margin-left:10px !important"><small class="padding-left-10 font-weight-bold font-size-0_9">'+elements.lib_devise+'</small> \n' +
                                '    </label>\n' +
                                '   </div>'
                        });

                        $('#list_payment_methods').html(html);

                        $('#btn-submit-container').html('<button class="btn btn-primary btn-xs pull-right" type="submit">Soumettre</button>');


                    }
                    else if(response.error !== undefined)
                    {
                        customSweetAlert("error","Erreur",response.error);
                    }

                },
                error:function()
                {
                    unblockUI();
                    customSweetAlert('error',"Erreur","Oops une erreur s'est produite au cours du traitement...")
                }

            });
        }

    });

    let formConfigPpaymentMethod = $('#form-config-payment-method');

    formConfigPpaymentMethod.submit(function(e){

        e.preventDefault();

        let form = $(this).serialize();
        $.ajax({
            type:'POST',
            dataType:'JSON',
            url:'/administration/payment-methods/treat-form',
            data:form,
            beforeSend:function()
            {
                blockUI("Traitement en cours...")
            },
            success:function(response)
            {
                unblockUI();

                if(response.error !== undefined)
                {
                    customSweetAlert('error',"Erreur",response.error);
                }

                if(response.success !== undefined)
                {
                    customSweetAlert('success',"Terminé",response.success);
                    setTimeout(function(){
                        document.location.href = "";
                    },2e3)
                }

            },
            error:function()
            {
                unblockUI();

                customSweetAlert('error',"Error","Une erreur s'est produite au cours du traitement");
            }
        })

    })

    let btn_edit_settings = $('.btn-edit-settings');

    btn_edit_settings.click(function(){

        let currentId = $(this).attr('data-id');
        let currentMethodPayment = $(this).attr('data-method');

        $.ajax({

            type:'POST',
            dataType:'JSON',
            url:'/administration/payment-methods/index',
            data:
                {
                    devise:currentMethodPayment
                },
            beforeSend:function()
            {
                blockUI('Traitement en cours...');
            },
            success:function(response)
            {
                unblockUI();
                let html = "";

                if(response.success !== undefined)
                {
                    let result = response.success;

                    result.forEach(function(elements){
                        html +=  '<label class="radio">\n' +
                            '     <input type="radio" value="'+elements.method+'" name="method_payment">\n' +
                            '     <img class="img-responsive img-circle me-card-1"   src="'+baseUrl+'/img/payment-methods/'+elements.image+'"  style="width: 40px !important; border-radius: 50% !important; height: 40px !important; margin-left:10px !important"><small class="padding-left-10 font-weight-bold font-size-0_9">'+elements.method+'</small> \n' +
                            '    </label>' ;
                    });
                    html +='<input type="hidden" value="'+currentId+'" name="devise_update"><input type="hidden" value="'+currentMethodPayment+'" name="old-method-payment">';


                    $('#container_devise').html(html);
                    $('#kt_modal_show_devise').modal('show');
                }
                else if(response.error !== undefined)
                {
                    customSweetAlert('error',"Erreur","Oops, "+response.error);
                }
            },
            error:function()
            {
                unblockUI();
                customSweetAlert('error',"Erreur","Oops, une erreur s'est produite au cours du traitement !");
            }

        });

    });


    $('#update-settings').submit(function(e){
        e.preventDefault();

        $.ajax({
            type:'POST',
            dataType:'JSON',
            data:$(this).serialize(),
            url:'/administration/payment-methods/index',
            beforeSend:function()
            {
                blockUI("Traitement en cours...")
            },
            success:function(response)
            {
                unblockUI();
                if(response.success !== undefined)
                {
                    customSweetAlert('success',"Terminé",response.success);
                    setTimeout(function(){
                        document.location.href = "/administration/payment-methods";
                    },2e1);
                }
                else if(response.error !== undefined)
                {
                    customSweetAlert('error',"Erreur",response.error);
                }
            },
            error:function()
            {
                unblockUI();
            }
        });
    });



});