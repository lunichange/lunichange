"use strict";
var KTLoginGeneral = function() {
    var
        t = function(i, t, e) {
            var n = $('<div class="alert alert-' + t + ' alert-dismissible" role="alert">\t\t\t<div class="alert-text">' + e + '</div>\t\t\t<div class="alert-close">                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>            </div>\t\t</div>');
            i.find(".alert").remove(), n.prependTo(i), KTUtil.animateClass(n[0], "fadeIn animated"), n.find("span").html(e)
        };
    var alertNotification = function(cible,type,text){
        var n = '<div class="alert alert-'+type+'"><div class="alert-text">'+text+'</div><div class="alert-close">   <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>  </div> </div>';

           $(cible).fadeIn(4000).html(n)
    };

    return {
        init: function() {
           $("#kt-form_add_currency").click(function(i) {
                i.preventDefault();
                var e = $(this),
                    n = $(this).closest("form");
                n.validate({
                    rules: {
                        text: {
                            required: !0,
                            minLength:3

                        },
                       select: {
                            required: !0
                        }
                    }
                }), n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                        n.ajaxSubmit({
                            url: "/dashboard/administration/currency-management/add-currency",
                            dataType:'JSON',
                            success: function(response, s, r, a) {
                                /*setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), t(n, "danger", "Incorrect username or password. Please try again.")
                                }, 2e3)*/



                                if(response.error !== undefined)
                                {

                                    setTimeout(function(){
                                        e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), alertNotification('.alertNotification','danger',response.error)

                                    },2e3)


                                }
                                else if(response.success !== undefined)
                                {

                                    setTimeout(function(){
                                        document.location.href = "";
                                    },2e3);
                                }

                            },
                            error:function()
                            {
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), t(n, "danger", "Une erreur s'est produite au cours de l'opération.Veuillez rééssayer.")
                                }, 2e3)
                            }
                        })
                )
            }), $("#kt-form_update_currency").click(function(n) {
                n.preventDefault();
                var s = $(this),
                    r = $(this).closest("form");
                r.validate({
                    rules: {
                        input_update_designation_monnai: {
                            required: !0,
                            minLength:3
                        },
                        select: {
                            required: !0,

                        },

                    }
                }), r.valid() && (s.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                    r.ajaxSubmit({
                        url: "/dashboard/administration/currency-management/update-currency",
                        method:'POST',
                        dataType:'JSON',
                        success: function(response, a, l, o)
                        {



                            if(response.error !== undefined)
                            {

                                s.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1),/* r.clearForm(), r.validate().resetForm(),*/ e();


                            }
                            else if(response.success !== undefined)
                            {
                                var container_form_update_currency = $('#container_form_update_currency');
                                setTimeout(function(){
                                    swal.fire({
                                        title:"Terminé",
                                        text:response.success,
                                        type:"success",
                                        buttonsStyling:!1,
                                        confirmButtonText:"OK",
                                        confirmButtonClass:"btn btn-brand"
                                    });
                                    container_form_update_currency.addClass('hide')
                                    selectOptionOfSelect('select_to_update_monnaie',"");
                                    s.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1),s.clearForm(), s.validate().resetForm();

                                },2e3);

                            }

                        },
                        error:function()
                        {

                        }
                    }))
            }), $("#kt_login_forgot_submit").click(function(n) {
                n.preventDefault();
                var s = $(this),
                    r = $(this).closest("form");
                r.validate({
                    rules: {
                        email: {
                            required: !0,
                            email: !0
                        }
                    }
                }), r.valid() && (s.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                    r.ajaxSubmit({
                        url: "/customer/login/password-recover",
                        method:'POST',
                        success: function(response, a, l, o) {
                            //console.log(response);
                            if(response.success !== undefined)
                            {

                                s.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), e();
                                var n = i.find(".kt-login__signin form");
                                n.clearForm(), n.validate().resetForm(), t(n,"success", response.success)

                            }
                            else if(response.error !== undefined)
                            {

                                s.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1)/* r.clearForm(), r.validate().resetForm()*/, e();
                                var n = i.find(".kt-login__signin form");
                                n.clearForm(), n.validate().resetForm(), t(n, "danger", response.error)

                            }

                        }
                    }))
            })
        }
    }
}();

jQuery(document).ready(function() {
    KTLoginGeneral.init()
});