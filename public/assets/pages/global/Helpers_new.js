var baseUrl = location.origin;
//var dataPath = '/data/documents/';
var dataPath ='/data/documents/';


    $(document).ready(function()
    {

       checkProfilStatus();

    });
function selectOptionOfSelect(id_select,value)
{

    var x = document.getElementById(id_select);
    var i;

    for (i=0; i<x.length;i++)
    {


        if(x.options[i].value == value)
        {

            $('#'+id_select).selectpicker('destroy');
            document.getElementById(id_select).selectedIndex = i;
            $('#'+id_select).selectpicker('refresh');

        }
    }
}
function disableform(formId) {
    var f = document.forms[formId].getElementsByTagName('input');
    for (var i=0;i<f.length;i++)
        f[i].disabled=true
    var f = document.forms[0].getElementsByTagName('textarea');
    for (var i=0;i<f.length;i++)
        f[i].disabled=true

    /* var f = document.forms[0].getElementsByTagName('select');
     for (var i=0;i<f.length;i++)
         f[i].disabled=true*/

    // $('#edit_categorie_devise').disabled = true;

}

function enableform(formId) {
    var f = document.forms[formId].getElementsByTagName('input');
    for (var i=0;i<f.length;i++)
        f[i].disabled=false
    var f = document.forms[0].getElementsByTagName('textarea');
    for (var i=0;i<f.length;i++)
        f[i].disabled=false
    var f = document.forms[0].getElementsByTagName('select');
    for (var i=0;i<f.length;i++)
        f[i].disabled=false

    $('#edit_categorie_devise').disabled = false;
}

function blockUI(text)
{
    KTApp.blockPage({overlayColor:"#000000",type:"v2",state:"primary",message:text});

}

function unblockUI()
{
    KTApp.unblockPage();
   // setTimeout(function(){},50);
}

function removeSpaces(string) {
    return string.split(' ').join('');
}

function custom_std(type,msg,swalType=null)
{
    let reponse;
    let title;
    if(type === 'success')
    {
        reponse= "Op\351ration \351ffectu\351e avec succ\350s.";

        title = "Termin\351!";
    }
    else if(type === 'error' || type === 'failed')
    {
        reponse = "Une erreur s'est produite. Veuillez réésayer.";
        title = "Erreur";
    }

    if(msg !== "")
    {
        reponse = msg;
    }

    if(swalType !== null)
        swal.fire(title,reponse+'.',type);
    else
        swal(title, reponse+'.', type);

}

function CustomSpinner(id,action)
{
    if(action === 'enable')
    {
        $('#'+id).addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
    }
    else if(action === 'disabled')
    {
        $('#'+id).removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled',!1);
    }
}
function checkProfilStatus()
{
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:basePath+'luni-api/check-profil-status',

        success:function(response)
        {

            if(response === 1)
            {
                Swal.fire({
                    position: 'top-end',
                    type: 'info',
                    title: '<span style="font-weight:600 !important;text-align:center !important;font-size:1.15em !important">Votre profil est incomplet. Veuillez complèter votre profil afin de bénéficier pleinement des fonctionnalités de la plateforme</span>',
                    showCloseButton: true,
                    showConfirmButton: true,

                })
            }



        },
        error:function(state)
        {
            if(state.status === 404)
                swal.fire("Erreur","Une erreur s'est produite","error");
        }
    });
}