var KTFormControls= {
        init:function() {
            $("#kt_user_add_form").validate( {
                    rules: {
                        user_name: {
                            required: !0, minlength: 3
                        }
                        , user_firstName: {
                            required: !0, minlength: 3
                        }
                        , user_password: {
                            required: !0, minlength:6
                        },
                        user_rpassword: {
                            required: !0, minlength:6,
                        },
                        user_email: {
                            required: !0, email:!0,minlength:10
                        }
                        , profile_user: {
                            required: !0,
                        }

                    }
                    , errorPlacement:function(e, r) {
                        var i=r.closest(".input-group");
                        i.length?i.after(e.addClass("invalid-feedback")): r.after(e.addClass("invalid-feedback"))
                    }
                    , invalidHandler:function(e, r) {
                        swal.fire({
                            title:"", text:"Quelques erreurs se sont produites veuillez les corriger.", type:"error", confirmButtonClass:"btn btn-secondary", onClose:function(e) {

                            }
                        }),  e.preventDefault();
                    }
                    , submitHandler:function(e) {

                    $('#kt_btn_user_add_form').addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),

                        $.ajax({

                            type:'POST',
                            url:basePath+'administration/users-management/add-user',
                            dataType:'json',
                            data:$('#kt_user_add_form').serialize(),
                            success:function(response)
                            {
                                console.log(response);
                                if(response.error !== undefined)
                                {
                                    $('#kt_btn_user_add_form').removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", false),

                                        swal.fire('Erreur',response.error,'error');
                                }
                                else if(response.success !== undefined)
                                {


                                    swal.fire('Terminé',response.success,'success');
                                    setTimeout(function(){
                                        $('#kt_modal_4').modal('hide');
                                        $(".kt-datatable").KTDatatable().destroy();
                                        setTimeout(function(){
                                            KTDatatableRemoteAjaxDemo.init();
                                        },200) ;

                                    },2e2);
                                }
                            },
                            error:function()
                            {
                                swal.fire('Erreur','Une erreur s\'est produite lors du traitement','error');
                            }

                        })
                    }
                }
            ),
                $("#kt_form_2").validate( {
                        rules: {
                            billing_card_name: {
                                required: !0
                            }
                            , billing_card_number: {
                                required: !0, creditcard: !0
                            }
                            , billing_card_exp_month: {
                                required: !0
                            }
                            , billing_card_exp_year: {
                                required: !0
                            }
                            , billing_card_cvv: {
                                required: !0, minlength: 2, maxlength: 3
                            }
                            , billing_address_1: {
                                required: !0
                            }
                            , billing_address_2: {}
                            , billing_city: {
                                required: !0
                            }
                            , billing_state: {
                                required: !0
                            }
                            , billing_zip: {
                                required: !0, number: !0
                            }
                            , billing_delivery: {
                                required: !0
                            }
                        }
                        , invalidHandler:function(e, r) {
                            swal.fire( {
                                    title:"", text:"Quelques erreurs se sont produites veuillez les corriger.", type:"error", confirmButtonClass:"btn btn-secondary", onClose:function(e) {

                                    }
                                }
                            ), e.preventDefault();
                        }
                        , submitHandler:function(e) {

                        }
                    }
                )
        }
    }

;
jQuery(document).ready(function() {
        KTFormControls.init()
    }

);