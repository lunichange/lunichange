$(document).ready(function(){

    let btn_generate_password = $('#btn-generate_password');

    btn_generate_password.click(function(){

        $.ajax({
            url:basePath+'administration/users-management/generate-password',
            type:'post',
            success:function(response)
            {
               let kt_form_password = $('#kt-form-password')
               let kt_form_rpassword = $('#kt-form-rpassword')

                kt_form_password.val(response.password);
                kt_form_rpassword.val(response.password);
                $('#receive_password').html(response.password);
            },
            error:function () {
                swal.fire('Erreur','Une erreur s\'est produite lors du traitement !','error');
            }
        })
    });
});

function updateStatusForUserAccount(id,value)
{
    let id_user = parseInt(id);

    swal.fire(
        {
            title:"Mis à jour du status",
            text:"Voulez-vous mettre à jour le status de ce compte utilisateur ?",
            type:"info",
            showCancelButton:!0,
            confirmButtonText:"Oui, confirmer"}).
    then(function(e){

        if(e.value === true)
        {
            $.ajax({
                type:'get',
                dataType:'JSON',
                url:basePath+'administration/users-management/update-status-account?id='+id_user+'&valeur='+value,
                success:function(response)
                {

                    console.log(response);
                    if(response.error !== undefined)
                    {
                        swal.fire('Erreur',response.error,'error');
                    }
                    else if(response.success !== undefined)
                    {
                        swal.fire('Terminé',response.success,'success');
                        setTimeout(function(){
                            $(".kt-datatable").KTDatatable().destroy();
                            setTimeout(function(){
                                KTDatatableRemoteAjaxDemo.init();
                            },200) ;
                        },2e1)
                    }
                },
                error:function()
                {
                    swal.fire('Erreur','Une erreur s\'est produite au cours de l\'opération.Veuillez vérifier votre connexion','error');
                }

            });
        }
        //swal.fire("Deleted!","Your file has been deleted.","success")
    });

}