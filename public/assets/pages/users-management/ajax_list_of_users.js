"use strict";
var KTDatatableRemoteAjaxDemo= {
        init:function() {
            var t;
            t=$(".kt-datatable").KTDatatable( {
                    data: {
                        type:"remote", source: {
                            read: {
                                url:basePath+"administration/users-management/list-users", map:function(t) {
                                    var e=t;
                                    return void 0!==t.data&&(e=t.data), e
                                }
                            }
                        }
                        , pageSize:5, serverPaging:!0, serverFiltering:!0, serverSorting:!0
                    }
                    , layout: {
                        scroll: !1, footer: !1
                    }
                    , sortable:!0, pagination:!0, search: {
                        input: $("#generalSearch")
                    }
                    , columns:[
                        {
                            field:"nom_user", title:"Nom complet", width:180, sortable:!0,template:function(t) {
                                return t.nom_user
                            }
                        },
                        {
                            field:"lib_profil", title:"Profil", width:134, sortable:!0,template:function(t) {
                                return t.lib_profil
                            }
                        },
                        {
                            field:"email_user", title:"Adresse mail", width:150, sortable:!0,template:function(t) {
                                return t.email_user
                            }
                        },
                        {
                            field:"online", title:"Status en ligne", width:134, sortable:!0,template:function(t) {

                                var e= {

                                        "1": {
                                            title: "En ligne", class: " kt-badge--success forest-green color-white"

                                        }

                                        , "0": {
                                            title: "Pas en ligne", class: " kt-badge--warning color-white"
                                        }
                                    }
                                ;
                                return'<span class="kt-badge '+e[t.online].class+' kt-badge--inline kt-badge--pill">'+e[t.online].title+"</span>"
                            }
                        },
                        {
                            field:"lock_user", title:"Status du compte", width:134, sortable:"asc",template:function(t) {

                                var e= {

                                        "1": {
                                            title: "Bloqué", class: " kt-badge--danger forest-green color-white"

                                        }

                                        , "0": {
                                            title: "Actif", class: " kt-badge--success color-white"
                                        }
                                    }
                                ;
                                return'<span class="kt-badge '+e[t.lock_user].class+' kt-badge--inline kt-badge--pill">'+e[t.lock_user].title+"</span>"
                            }
                        },

                        {
                            field:"last_connexion", title:"Dernière connexion",type:"date",format:"YYYY-MM-DD H:i:s", width:134, sortable:"asc",template:function(t) {


                                if(t.last_connexion === null)
                                {
                                    return'<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill" style="color: #fff !important">Non disponible</span>';

                                }
                                else
                                {
                                    return'<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill" style="color: #000 !important">'+t.last_connexion+"</span>"

                                }


                             }
                        }

                        , {
                            field:"Actions", title:"Actions", sortable:!1, width:110, overflow:"visible", autoHide:!1, template:function(e) {
                                var t = e.id_user;
                                let link_update_status;

                                if(e.lock_user === '1')
                                {
                                    link_update_status = '<a class="dropdown-item link-for-update-status" onclick="updateStatusForUserAccount('+t+',\'0\');" href="#" data-id="\'+t+\'"><i class="la la-leaf"></i> Modifier Status</a>';

                                }
                                else
                                {
                                    link_update_status = '<a class="dropdown-item link-for-update-status" onclick="updateStatusForUserAccount('+t+',\'1\');" href="#" data-id="\'+t+\'"><i class="la la-leaf"></i> Modifier Status</a>';

                                }

                                return'\t\t\t\t\t\t<div class="dropdown">\t\t\t\t\t\t\t<a href="javascript:;"  class="btn btn-sm btn-clean btn-icon btn-icon-sm" data-toggle="dropdown">                                <i class="flaticon2-gear"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"  ><i class="la la-edit"></i> Modifier Details</a>\t\t\t\t\t\t    \t'+link_update_status+'\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-print"></i> Générer Raport</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm link-for-update-status" data-id="'+t+'" title="Modifier details">\t\t\t\t\t\t\t<i class="flaticon2-paper"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm link-for-delete-currency" onclick="rebootUserAccount('+t+');" data-id="'+t+'" title="Réinitialiser le compte">\t\t\t\t\t\t\t<i class="flaticon2-reload-1"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
                            }
                        }
                    ]
                }
            ),
                $(".kt_form_search").on("input", function() {
                        t.search($(this).val().toLowerCase(), "Search")
                    }
                ), $("#kt_form_status").on("change", function() {
                        t.search($(this).val().toLowerCase(), "Status")
                    }
                ),
                $("#kt_form_type").on("change", function() {
                        t.search($(this).val().toLowerCase(), "Type")
                    }
                ),
                $("#kt_form_status,#kt_form_type").selectpicker()
        }
    }

;
jQuery(document).ready(function() {


            KTDatatableRemoteAjaxDemo.init();

    }

);