$(document).ready(function(){


   let selectUpdateMonnaie = $('#select_to_update_monnaie');

   selectUpdateMonnaie.on('change',function(){

       let value = $(this).val();
       if(value !== '')
       {
           $.ajax({
               type:'POST',
               dataType:'JSON',
               url:basePath+'administration/currency-management/std-get?table=luni_monnaies&use_request=monnaie',
               data:
                   {
                       'id_famille':value,

                   },
               async:true,
               success:function(response,status)
               {
                   $('#container_form_update_currency').removeClass('hide');
                   if(response.error !== undefined)
                   {
                       swal.fire({
                           title:"Erreur",
                           text:response.error,
                           type:"error",
                           buttonsStyling:!1,
                           confirmButtonText:"OK",
                           confirmButtonClass:"btn btn-brand"
                       });
                   }
                   else
                   {
                       KTApp.blockPage(),setTimeout(function(){
                           KTApp.unblockPage();
                           $('#input_update_designation_curency').val(response.libelle_famille);
                           $('#input_hidden_update_currency').val(response.id_famille);
                           selectOptionOfSelect('select_update_categorie_monnaie',parseInt(response.categorie_devise)),
                           selectOptionOfSelect('select_update_type_monnaie',response.type_monnaie);
                       },2e3)

                   }
               },
               error:function()
               {
                   swal.fire({
                       title:"Erreur",
                       text:"Une erreur s'est produite avec lors du traitement !",
                       type:"error",
                       buttonsStyling:!1,
                       confirmButtonText:"OK",
                       confirmButtonClass:"btn btn-brand"
                   });
               }
           })
       }
       else
       {
           let container_form_update_currency = $('#container_form_update_currency');

           setTimeout(function(){
               container_form_update_currency.addClass('hide')
               selectOptionOfSelect('select_to_update_monnaie',"");
           },800)
       }


   });

   $('#kt-form_cancel_update_currency').click(function(){
       let container_form_update_currency = $('#container_form_update_currency');

       setTimeout(function(){
           container_form_update_currency.addClass('hide')
           selectOptionOfSelect('select_to_update_monnaie',"");
       },800)

   });

});

function updateStatusForCurrency(id,valeur)
{
    let id_currency = parseInt(id);

    swal.fire(
        {
            title:"Mis à jour du status",
            text:"Voulez-vous mettre à jour le status de cette monnaie ?",
            type:"info",
            showCancelButton:!0,
            confirmButtonText:"Oui, confirmer"}).
    then(function(e){

        if(e.value === true)
        {
            $.ajax({
                type:'get',
                dataType:'JSON',
                url:basePath+'administration/currency-management/update-status-currency?id='+id_currency+'&valeur='+valeur,
                success:function(response)
                {

                        if(response.error !== undefined)
                        {
                            swal.fire('Erreur',response.error,'error');
                        }
                        else if(response.success !== undefined)
                        {
                            swal.fire('Terminé',response.success,'success');
                            setTimeout(function(){
                                $(".kt-datatable").KTDatatable().destroy();
                              setTimeout(function(){
                                  KTDatatableRemoteAjaxDemo.init();
                              },200) ;
                            },2e1)
                        }
                },
                error:function()
                {
                    swal.fire('Erreur','Une erreur s\'est produite au cours de l\'opération.Veuillez vérifier votre connexion','error');
                }

            });
        }
        //swal.fire("Deleted!","Your file has been deleted.","success")
    });

}