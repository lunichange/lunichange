"use strict";
var KTDatatableRemoteAjaxDemo= {
        init:function() {
            var t;
            t=$(".kt-datatable").KTDatatable( {
                    data: {
                        type:"remote", source: {
                            read: {
                                url:basePath+"administration/currency-management/list-currency", map:function(t) {
                                    var e=t;
                                    return void 0!==t.data&&(e=t.data), e
                                }
                            }
                        }
                        , pageSize:5, serverPaging:!0, serverFiltering:!0, serverSorting:!0
                    }
                    , layout: {
                        scroll: !1, footer: !1
                    }
                    , sortable:!0, pagination:!0, search: {
                        input: $("#generalSearch")
                    }
                    , columns:[
                        {
                            field:"libelle_famille", title:"Désignation", width:134, sortable:"asc",template:function(t) {
                                return t.libelle_famille
                            }
                        }
                        , {
                            field: "lib_categorie_devise", title: "Categorie",width: 134
                        }
                        , {
                            field: "lib_type_monnaie", title: "Type", width:134
                        }
                        , {
                            field:"status", title:"Status", width:134, template:function(t) {

                                var e= {

                                        "1": {
                                            title: "Actif", class: " kt-badge--success"

                                        }

                                        , "0": {
                                            title: "Inactif", class: " kt-badge--warning"
                                        }
                                    }
                                ;
                                return'<span class="kt-badge '+e[t.status].class+' kt-badge--inline kt-badge--pill">'+e[t.status].title+"</span>"
                            }
                        }

                        , {
                            field:"Actions", title:"Actions", sortable:!1, width:110, overflow:"visible", autoHide:!1, template:function(e) {
                                var t = e.id_famille;
                                let link_update_status;

                                if(e.status === '1')
                                {
                                    link_update_status = '<a class="dropdown-item link-for-update-status" onclick="updateStatusForCurrency('+t+',\'0\');" href="#" data-id="\'+t+\'"><i class="la la-leaf"></i> Modifier Status</a>';

                                }
                                else
                                {
                                    link_update_status = '<a class="dropdown-item link-for-update-status" onclick="updateStatusForCurrency('+t+',\'1\');" href="#" data-id="\'+t+\'"><i class="la la-leaf"></i> Modifier Status</a>';

                                }
                                //console.log(e);
                                return'\t\t\t\t\t\t<div class="dropdown">\t\t\t\t\t\t\t<a href="javascript:;"  class="btn btn-sm btn-clean btn-icon btn-icon-sm" data-toggle="dropdown">                                <i class="flaticon2-gear"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"  ><i class="la la-edit"></i> Modifier Details</a>\t\t\t\t\t\t    \t'+link_update_status+'\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-print"></i> Générer Raport</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm link-for-update-status" data-id="'+t+'" title="Modifier details">\t\t\t\t\t\t\t<i class="flaticon2-paper"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm link-for-delete-currency" onclick="deleteCurrency('+t+');" data-id="'+t+'" title="Supprimer">\t\t\t\t\t\t\t<i class="flaticon2-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
                            }
                        }
                    ]
                }
            ),
                $("#kt_form_status").on("change", function() {
                        t.search($(this).val().toLowerCase(), "Status")
                    }
                ),
                $("#kt_form_type").on("change", function() {
                        t.search($(this).val().toLowerCase(), "Type")
                    }
                ),
                $("#kt_form_status,#kt_form_type").selectpicker()
        }
    }

;
jQuery(document).ready(function() {

    setTimeout(function(){
        KTDatatableRemoteAjaxDemo.init();
    },2e3) ;
    }

);