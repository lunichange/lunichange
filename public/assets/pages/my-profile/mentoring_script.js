//tinymce.init({selector:'#referal-link-container'});
$(document).ready(function()
{

    let generate_link = $('#generate_link');

    $("#btn-referal-link").click(function(){
        $("textarea").select();
        document.execCommand('copy');
        toastr.success('Lien copié avec succès')
    });

    generate_link.on('click',function(){

        Swal.fire({
            title: 'Génération de lien de parrainage',
            text: "Voulez-vous générer votre lien de parrainage ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, confirmer',
            cancelButtonText: 'Annuler'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type:'POST',
                    dataType: 'JSON',
                    url:basePath+'my-profile/mentoring',
                    beforeSend:function()
                    {
                        blockUI('Traitement en cours...');
                    },
                    success:function(response)
                    {
                        unblockUI();

                        if(response.success !== undefined)
                        {
                            location.reload();
                        }
                        else if(response.error !== undefined)
                        {
                            toastr.error(response.error);
                        }
                    },
                    error:function()
                    {
                        unblockUI();
                    }
                })
            }
        })
    });


});
/*

 */