$(document).ready(function(){

    let country_edit  = $('#country');
    let hidden_pays = $('#hidden_pays');

    if(hidden_pays.length !== 0 && country_edit.length !== 0)
    {
        $.ajax({
            type:'POST',
            url:basePath+'luni-api/std-get?table=pays&type=all&order=lib_pays ASC',
            dataType:'JSON',
            beforeSend:function()
            {
                blockUI('Chargement des pays en cours...');
                country_edit.selectpicker('destroy');
            },
            success:function(response)
            {

                let options = '';

                response.forEach(function(elements){
                    options+='<option value="'+elements.id_pays+'" data-code="'+elements.code_pays+'">'+elements.lib_pays+'</option>';

                });
                country_edit.empty();
                country_edit.html(options);


                if(hidden_pays.val() !== "" || hidden_pays.val() !== null)
                {


                    selectOptionOfSelect('country',hidden_pays.val());
                    $('#country').selectpicker('refresh');
                }
                else
                {
                    $('#country').selectpicker('refresh');
                }

                unblockUI();

            },
            error:function()
            {
                swal.fire('Erreur','Une erreur s\'est produite. Impossible de charger les pays',"error");
            }
        });
    }


});