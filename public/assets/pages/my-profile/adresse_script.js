$(document).ready(function(){

    let country_edit  = $('#pays');
    let hidden_pays = $('#hidden_pays');


    if(hidden_pays.length !== 0 && country_edit.length !== 0)
    {
        $.ajax({
            type:'POST',
            url:basePath+'luni-api/std-get?table=pays&type=all&order=lib_pays ASC',
            dataType:'JSON',
            beforeSend:function()
            {
                blockUI('Chargement des pays en cours...');
                country_edit.selectpicker('destroy');
            },
            success:function(response)
            {
                let options = '';

                response.forEach(function(elements){
                    options+='<option value="'+elements.id_pays+'" data-code="'+elements.code_pays+'">'+elements.lib_pays+'</option>';

                });
                country_edit.empty();
                country_edit.html(options);

                if(hidden_pays.val() !== "" || hidden_pays.val() !== null)
                {

                    selectOptionOfSelect('pays',hidden_pays.val());
                    country_edit.selectpicker('refresh');
                }
                else
                {
                    country_edit.selectpicker('refresh');
                }

                unblockUI();

            },
            error:function()
            {
                swal.fire('Erreur','Une erreur s\'est produite. Impossible de charger les pays',"error");
            }
        });
    }

    loadDatas();

    $("#btn_update_adresse").click(function(i)
    {
        i.preventDefault();
        var e = $(this),
            n = $(this).closest("form");
        let itiValidate = false;
        let getMessage = null;

        n.validate({
            rules: {
                pays: {
                    required: !0,

                },
                ville: {
                    required: !0
                },
                region:{
                    required: !0
                },
                adresse:
                    {
                        required: !0
                    }

            }
        }),n.valid();

        if(n.valid())
        {


            (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),

                    n.ajaxSubmit({
                        url: basePath+"my-profile/adresse",
                        dataType:'JSON',
                        success: function(i, s, r, a) {


                            if(i.error !== undefined)
                            {

                                e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                swal.fire('Erreur',i.error,'error');

                            }
                            else if(i.success !== undefined)
                            {

                                setTimeout(function(){
                                    document.location.href=basePath+'my-profile/adresse';
                                },2e3)
                            }

                        },
                        error:function()
                        {
                            setTimeout(function() {
                                e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                            }, 2e3)
                            swal.fire('Erreur',"Une erreur s'est produite lors de l'opération","error");
                        }
                    })
            )
        }
        else
        {
            if(getMessage !== null)
            {
                toastr.error(getMessage);
            }
        }

    });

});

function loadDatas()
{

    let ville = $('#ville');
    let region = $('#region');
    let adresse = $('#adresse');
    $.ajax({

        type:'POST',
        dataType:'JSON',
        url:basePath+'luni-api/std-get?table=customers&type=unique',
        data:
            {
                id_user:$('#user_info').val()
            },

        success:function(response)
        {
            //unblockUI();
            if(response.length !== 0)
            {
               ville.val(response['ville']);
               region.val(response['region']);
               adresse.val(response['adresse']);
            }

        },
        error:function()
        {
            //unblockUI();
            custom_std('error',"Une erreur s'est produite lors du traitement");
        }

    })
}