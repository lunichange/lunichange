"use strict";
var KTUpdatePassword= function() {

    return {
        update: function() {
            $("#btn_update_password").click(function(i) {
                i.preventDefault();
                var e = $(this),
                    n = $(this).closest("form");
                n.validate({
                    rules: {
                        password: {
                            required: !0
                        },
                        rpassword:
                            {
                                required: !0
                            },
                        confirm_password:
                            {
                                required: !0
                            }
                    }
                });
                n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                        n.ajaxSubmit({
                            url: basePath+"my-profile/update-password",
                            dataType:'JSON',
                            success: function(response) {

                                if(response.error !== undefined)
                                {
                                    toastr.error(response.error);

                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);


                                }
                               if(response.success !== undefined)
                                {

                                    setTimeout(function(){
                                        document.location.href=basePath+'my-profile/edit-password';
                                    },2e3);
                                }

                            },
                            error:function()
                            {
                                swal.fire('Error',"Une erreur s'est produite au cours du traitement",'error');
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                }, 2e3)
                            }
                        })
                )
            })
        }
    }
}();
jQuery(document).ready(function() {
    KTUpdatePassword.update()
});