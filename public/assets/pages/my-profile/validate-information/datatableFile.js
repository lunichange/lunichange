"use strict";
var KTDatatableAutoColumnHideDemo = {
    init: function () {
        var t;
        (t = $("#kt_table_demande").KTDatatable({

            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },

        })),
            $("#kt_form_status").on("change", function () {
                t.search($(this).val().toLowerCase(), "Status");
            }),
            $("#kt_form_type").on("change", function () {
                t.search($(this).val().toLowerCase(), "Type");
            })

    }


};
jQuery(document).ready(function () {
    KTDatatableAutoColumnHideDemo.init();

});
