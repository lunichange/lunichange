var kt_form_update_identity = $('#kt_form_update_identity');

var kt_form_update_selfie  = $('#kt_form_update_selfie');

$(document).ready(function(){


    let hidden_msg_demande = $('#hidden_msg_demande');


    if(hidden_msg_demande.val() !== '' && hidden_msg_demande.val() !== null)
    {
        toastr.options.timeOut = 0;
        toastr.success(hidden_msg_demande.val())
    }


    let add_info_identite = $('#add_info_identite');
    let add_photo_address = $('#add_photo_address');


    add_info_identite.submit(function(e){

        e.preventDefault();

        $.ajax({

            type:'POST',
            dataType:'JSON',
            url:'/dashboard/my-profile/add-information',
            data:new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend:function()
            {
                CustomSpinner('btn_add_id_document','enable');
                CustomSpinner('btn_cancel_id_document','enable');
            },
            success:function(response)
            {

                if(response.success !== undefined)
                {
                    custom_std('success',response.success);

                    setTimeout(function(){
                        location.reload();
                    },2e3);
                }
                else if(response.error !== undefined)
                {
                    CustomSpinner('btn_add_id_document','disabled');
                    CustomSpinner('btn_cancel_id_document','disabled');

                    //add_info_identite.reset();
                    custom_std('error',response.error);
                }

            },
            error:function(state)
            {
                CustomSpinner('btn_add_id_document','disabled');
                CustomSpinner('btn_cancel_id_document','disabled');

                if(state.status === 404)
                {
                    toastr.error("Page non trouvée");
                }
                else
                {
                    custom_std('error',"Une erreur s'est produite au cours du traitement");
                }
            }

        });

    });


    add_photo_address.submit(function(e){

        e.preventDefault();

        $.ajax({

            type:'POST',
            dataType:'JSON',
            url:'/dashboard/my-profile/add-selfie',
            data:new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend:function()
            {
                CustomSpinner('btn_cancel_selfie','enable');
                CustomSpinner('btn_add_selfie','enable');
            },
            success:function(response)
            {

                if(response.success !== undefined)
                {
                    custom_std('success',response.success);

                    setTimeout(function(){
                        location.reload();
                    },2e3);
                }
                else if(response.error !== undefined)
                {
                    CustomSpinner('btn_cancel_selfie','disabled');
                    CustomSpinner('btn_add_selfie','disabled');

                    //add_info_identite.reset();
                    custom_std('error',response.error);
                }

            },
            error:function(state)
            {
                CustomSpinner('btn_cancel_selfie','disabled');
                CustomSpinner('btn_add_selfie','disabled');

                if(state.status === 404)
                {
                    toastr.error("Page non trouvée");
                }
                else
                {
                    custom_std('error',"Une erreur s'est produite au cours du traitement");
                }
            }

        });

    });




    kt_form_update_identity.submit(function(e){

            e.preventDefault();


            $.ajax({

                type:'POST',
                dataType:'JSON',
                data:new FormData(this),
                url:'/dashboard/my-profile/update-information',
                contentType:false,
                cache:false,
                processData:false,
                beforeSend:function()
                {
                    CustomSpinner('cancel_update_identity','enable');
                    CustomSpinner('update_identity','enable');

                },
                success:function(response)
                {
                    CustomSpinner('cancel_update_identity','disabled');
                    CustomSpinner('update_identity','disabled');

                    if(response.success !== undefined)
                    {
                        $('#kt_table_demande').KTDatatable('destroy');
                        KTDatatableAutoColumnHideDemo.init();

                        custom_std('success',response.success);
                        $('#kt_modal_update_identity').modal('hide');

                        setTimeout(function(){
                            location.reload();
                        },2e2);
                    }
                    else if(response.error !== undefined)
                    {
                        toastr.error(response.error);
                    }

                },
                error:function()
                {
                    CustomSpinner('cancel_update_identity','disabled');
                    CustomSpinner('update_identity','disabled');
                }

            });

    });


    kt_form_update_selfie.submit(function(e){

            e.preventDefault();


            $.ajax({

                type:'POST',
                dataType:'JSON',
                data:new FormData(this),
                url:'/dashboard/my-profile/update-selfie',
                contentType:false,
                cache:false,
                processData:false,
                beforeSend:function()
                {
                    CustomSpinner('cancel_update_selfie','enable');
                    CustomSpinner('update_selfie','enable');

                },
                success:function(response)
                {
                    CustomSpinner('cancel_update_selfie','disabled');
                    CustomSpinner('update_selfie','disabled');

                    if(response.success !== undefined)
                    {

                        custom_std('success',response.success);
                        $('#kt_modal_update_selfie').modal('hide');

                        setTimeout(function(){
                            location.reload();
                        },2e2);
                    }
                    else if(response.error !== undefined)
                    {
                        toastr.error(response.error);
                    }

                },
                error:function()
                {
                    CustomSpinner('cancel_update_selfie','disabled');
                    CustomSpinner('update_selfie','disabled');
                }

            });

    });




});


function updateIdentify(id)
{
    let Id = parseInt(id);

    let kt_modal_update_identity = $('#kt_modal_update_identity');

    $.ajax({

        type:'POST',
        url:'/dashboard/my-profile/std-get?type=unique&table=documents',
        dataType:'JSON',
        data:
            {
                id_document:Id
            },
        beforeSend:function()
        {
          blockUI('Patientez...')  ;
        },
        success:function(response)
        {
                if(response.length !== 0)
                {

                    unblockUI();
                    selectOptionOfSelect('kt_select_update_type_piece',response.type_document);
                    $('#update_numero_piece').val(response.numero_piece);

                    setTimeout(function(){

                        kt_modal_update_identity.modal('show');

                    },2e1);

                }
        },
        error:function()
        {
            unblockUI();
            toastr.error('Une erreur s\'est produite. Le traitement ne peut aboutir')
        }


    })
}
function updateSelfie(id)
{
    let Id = parseInt(id);

    let kt_modal_update_selfie = $('#kt_modal_update_selfie');

    $.ajax({

        type:'POST',
        url:'/dashboard/my-profile/std-get?type=unique&table=document_selfie',
        dataType:'JSON',
        data:
            {
                id_selfie:Id
            },
        beforeSend:function()
        {
            blockUI('Patientez...')  ;
        },
        success:function(response)
        {
            if(response.length !== 0)
            {

                unblockUI();


                setTimeout(function(){

                    kt_modal_update_selfie.modal('show');

                },2e1);

            }
        },
        error:function()
        {
            unblockUI();
            toastr.error('Une erreur s\'est produite. Le traitement ne peut aboutir')
        }


    });
}

function showMotif(id)
{
    let Id = parseInt(id);

    $.ajax({

        type:'POST',
        dataType:'JSON',
        url:'/dashboard/luni-api/std-get?type=unique&table=documents',
        data:
            {
                id_document:Id
            },
        beforeSend:function()
        {
            blockUI('Veuillez patienter...')
        },
        success:function(response)
        {
            unblockUI();


            if(response.length !== 0)
            {
                let motif = ""+response.motif+"";
                swal('Motif', motif,'info');
            }
            else
            {
                custom_std('error','Une erreur s\'est produite au cours du traitement');
            }
        },
        error:function()
        {
            unblockUI();
            toastr.error('Oops','Une erreur s\'est produite');
        }

    })
}

function CustomSpinner(id,action)
{
    if(action === 'enable')
    {
        $('#'+id).addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
    }
    else if(action === 'disabled')
    {
        $('#'+id).removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled',!1);
    }
}