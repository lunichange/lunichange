"use strict";

let country_code = $('#hidden_codeCountry');
let dialCode = $('#hidden_dialCode');
let hidden_telephone = $('#hidden_telephone')
let initialCountry = "auto";

//alert(country_code.val());
if(country_code.val() === "" || country_code.val() === null)
{
    initialCountry = "auto";
}
else
{

    initialCountry = country_code.val()
}
if(hidden_telephone.val() !== "" || hidden_telephone.val() !== null)
{
    //removeSpaces(this.value);
    $('#telephone').val(hidden_telephone.val());
    /*console.log($('#telephone_edit').val());
    console.log(initialCountry);*/
}


var input = document.querySelector("#telephone"),
    errorMsg = document.querySelector("#error-msg"),
    validMsg = document.querySelector("#valid-msg");


// here, the index maps to the error code returned from getValidationError - see readme
var errorMap = ["Numéro de téléphone invalide", "Code du pays invalide", "Numéro de téléphone trop court", "Numéro de téléphone trop long", "Numéro invalide"];

// initialise plugin
var iti = window.intlTelInput(input, {
    initialCountry: initialCountry,
    geoIpLookup: function(callback) {
        $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
        });
    },
    separateDialCode:true,
    utilsScript: location.origin+'/assets/plugins/intl-tel-input/build/js/utils.js'
});

var reset = function() {
    input.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
};
let getCode =null;

var KTUpdateProfil = function() {

    return {
        init: function() {
            $("#btn_update_profil").click(function(i) {
                i.preventDefault();
                var e = $(this),
                    n = $(this).closest("form");
                let itiValidate = false;
                let getMessage = null;
                if (input.value.trim()) {
                    if (iti.isValidNumber()) {
                        itiValidate = true;
                        validMsg.classList.remove("hide");
                        country_code.val(iti.selectedCountryData.iso2);
                        dialCode.val(iti.selectedCountryData.dialCode);
                        // console.log(iti);
                        $('#telephone').val($('#telephone').val().trim());

                        $('#telephone').val(removeSpaces($('#telephone').val()));
                    } else {
                        input.classList.add("error");
                        var errorCode = iti.getValidationError();

                        if(errorMap[errorCode] === undefined)
                        {

                            errorMsg.innerHTML = "Numéro de téléphone invalide";
                            getMessage = "Numéro de téléphone invalide";
                        }
                        else
                        {
                            errorMsg.innerHTML = errorMap[errorCode];
                            getMessage = errorMap[errorCode];
                        }
                        itiValidate = false;

                        errorMsg.classList.remove("hide");
                    }
                }


                n.validate({
                    rules: {
                        email: {
                            required: !0,
                            email: !0
                        },
                        fname: {
                            required: !0
                        },
                        lname: {
                            required: !0
                        },
                        telephone:
                            {
                                required: !0
                            },
                        sexe: {
                            required: !0
                        },

                        date_naissance: {
                            required: !0,
                            date: !0
                        },
                    }
                }),n.valid();

                if(n.valid() && itiValidate === true)
                {

                    let telephone= $('#telephone');


                    telephone.val();

                    (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),

                            n.ajaxSubmit({
                                url: basePath+"my-profile/update-info-perso",
                                dataType:'JSON',
                                success: function(i) {


                                    if(i.error !== undefined)
                                    {

                                        e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                        swal.fire('Erreur',i.error,'error');

                                    }
                                    else if(i.success !== undefined)
                                    {

                                        setTimeout(function(){
                                            document.location.href=basePath+'my-profile/identification';
                                        },2e3)
                                    }

                                },
                                error:function()
                                {
                                    setTimeout(function() {
                                        e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                    }, 2e3)
                                    swal.fire('Erreur',"Une erreur s'est produite lors de l'opération","error");
                                }
                            })
                    )
                }
                else
                {
                    if(getMessage !== null)
                    {
                        toastr.error(getMessage);
                    }
                }

            });
        },
        loadData: function()
        {
            let sexe = $('#sexe');
            $.ajax({

                type:'POST',
                url:basePath+'luni-api/std-get?table=customers&type=unique',
                dataType:'JSON',
                data:{id_user:$('#id_user').val()},
                success:function(response)
                {
                    if(response.length !== 0)
                    {
                       if(response['sexe'] !== '' && response['sexe'] !== null)
                       {
                           sexe.selectpicker('destroy');
                           selectOptionOfSelect('sexe',response['sexe']);
                           setTimeout(function(){
                               sexe.selectpicker('refresh');
                           },2e1)
                       }
                    }
                },
                error:function()
                {
                    custom_std('error',"Une erreur s'est produite au cours du traitement...",'fire');
                }

            })
        }
    }
}();
jQuery(document).ready(function() {
    KTUpdateProfil.loadData();
    KTUpdateProfil.init();

});