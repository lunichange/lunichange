$(document).ready(function()
{
   // alert('okay');
    let hiddenMsg = $('#hiddenMsg');


    if(hiddenMsg.val() !== "" && hiddenMsg.val() !== null && hiddenMsg.val() !== undefined)
    {
        custom_std('error',hiddenMsg.val(),'fire');
    }

    let hidden_type_piece = $('#hidden_type_piece');

    if(hidden_type_piece.val() !== "" && hidden_type_piece.val() !== null && hidden_type_piece.val() !== undefined)
    {
        $('#kt_select_type_piece').selectpicker('destroy');
        selectOptionOfSelect('kt_select_type_piece',$('#kt_select_type_piece').val());
        setTimeout(function(){
            $('#kt_select_type_piece').selectpicker('refresh');
        },2e0);
    }



    $('#kt_form_update_id').submit(function(e){
        e.preventDefault();

        $.ajax({

            type:'POST',
            dataType:'JSON',
            data:new FormData(this),
            url:basePath+'my-profile/update-id',
            contentType:false,
            cache:false,
            processData:false,
            beforeSend:function()
            {
                CustomSpinner('btn_cancel_update_id','enable');
                CustomSpinner('btn_update_id','enable');
                blockUI('Traitement en cours...');

            },
            success:function(response)
            {
                CustomSpinner('btn_cancel_update_id','disabled');
                CustomSpinner('btn_update_id','disabled');
                unblockUI();

                if(response.success !== undefined)
                {
                    $('#kt_table_demande').KTDatatable('destroy');
                    KTDatatableAutoColumnHideDemo.init();

                    $('#kt_modal_update_id').modal('hide');

                    setTimeout(function(){
                        location.reload();
                    },2e1);
                }
                else if(response.error !== undefined)
                {
                    toastr.error(response.error);
                }

            },
            error:function()
            {
                CustomSpinner('btn_cancel_update_id','disabled');
                CustomSpinner('btn_update_id','disabled');
                unblockUI();
            }

        });

    })


    $('#kt_form_update_selfie').submit(function(e){
        e.preventDefault();

        $.ajax({

            type:'POST',
            dataType:'JSON',
            data:new FormData(this),
            url:basePath+'my-profile/update-selfie',
            contentType:false,
            cache:false,
            processData:false,
            beforeSend:function()
            {
                CustomSpinner('btn_cancel_update_selfie','enable');
                CustomSpinner('btn_update_selfie','enable');
                blockUI('Traitement en cours...');

            },
            success:function(response)
            {
                CustomSpinner('btn_cancel_update_selfie','disabled');
                CustomSpinner('btn_update_selfie','disabled');
                unblockUI();

                if(response.success !== undefined)
                {
                    $('#kt_table_demande').KTDatatable('destroy');
                    KTDatatableAutoColumnHideDemo.init();

                    $('#kt_modal_update_id').modal('hide');

                    setTimeout(function(){
                        location.reload();
                    },2e1);
                }
                else if(response.error !== undefined)
                {
                    toastr.error(response.error);
                }
            },
            error:function()
            {
                CustomSpinner('btn_cancel_update_selfie','disabled');
                CustomSpinner('btn_update_selfie','disabled');
                unblockUI();
            }

        });

    })

});

function updateIdentity(idDocument)
{
    let modal_choice = $('#kt_modal_make_choice');

    modal_choice.modal('show');
    let btn_update_id = $('.call_update_id');
    let btn_update_selfie = $('.call_update_selfie');

    /**
     * Update Modals
     */
    let kt_modal_update_id = $('#kt_modal_update_id');
    let kt_modal_update_selfie = $('#kt_modal_update_selfie');

    btn_update_id.on('click',function(){

        modal_choice.modal('hide');
        setTimeout(function(){

            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'luni-api/std-get?table=documents&type=unique',
                data:
                    {
                        id_document:parseInt(idDocument)
                    },
                success:function(response)
                {
                    if(response.length !== 0)
                    {
                        $('#id_document').val(response['id_document']);
                        $('#update_numero_piece').val(response['numero_piece']);
                        $('#kt_select_update_type_piece').selectpicker('destroy');

                        selectOptionOfSelect('kt_select_update_type_piece',response['type_document']);
                        $('#kt_select_update_type_piece').selectpicker('refresh');

                        kt_modal_update_id.modal('show');

                    }
                },
                error:function()
                {
                    toastr.error('Une erreur s\'est produite au cours du traitement')
                }
            })


        },2e0);
    });
    btn_update_selfie.on('click',function()
    {
        modal_choice.modal('hide');
        setTimeout(function(){

            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'luni-api/std-get?table=documents&type=unique',
                data:
                    {
                        id_document:parseInt(idDocument)
                    },
                success:function(response)
                {
                    if(response.length !== 0)
                    {


                        kt_modal_update_selfie.modal('show');
                    }
                },
                error:function()
                {
                    toastr.error('Une erreur s\'est produite au cours du traitement')
                }
            });

        },2e0);
    })
}