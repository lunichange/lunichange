$(document).ready(function(){

    let form_send_message = $('#form_send_message');
    let phone = $('#phone');
    let fullname = $('#fullname');
    let email = $('#email');
    let subject = $('#subject');
    let message = $('#message');
    let btn_submit_send_message = $('#btn_submit_send_message');

    form_send_message.submit(function(e){
        e.preventDefault();


        if(phone.val().trim() === "" || fullname.val().trim()=== "" || email.val().trim() === "" || subject.val().trim() === "" || message.val().trim() === "")
        {
            Swal.fire('Oops',"Veuillez remplir tous les champs");
            //alert("Veuillez remplir tous les champs");
        }
        else
        {
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    validMsg.classList.remove("hide");
                    $.ajax({
                        type:"POST",
                        url:"/customer/send-message",
                        dataType:'json',
                        data:$(this).serialize(),
                        beforeSend:function()
                        {
                            btn_submit_send_message.attr('disabled',!0);
                            btn_submit_send_message.html('<span class="font-18 inline-block text-uppercase p-lr15">' +
                                'Traitement en cours...' +
                                '</span>');

                        },
                        success:function(response)
                        {
                            btn_submit_send_message.attr('disabled',!1);
                            btn_submit_send_message.html('<span class="font-18 inline-block text-uppercase p-lr15">' +
                                'Envoyer' +
                                '</span>');

                            if(response.success !== undefined)
                            {
                                Swal.fire('Terminé',response.success,'success');

                                setTimeout(function(){
                                    document.location.href = location.origin+'/customer/home';
                                },2e1);
                            }
                            else if(response.error !== undefined)
                            {
                                Swal.fire('Oops',response.error,'error');
                            }
                        },
                        error:function(state)
                        {
                            if(state.status === 404)
                            {

                            }
                        }
                    })
                }

                else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    if(errorCode === -99)
                    {
                        errorCode = 0;
                    }

                    errorMsg.innerHTML = errorMap[errorCode];
                    Swal.fire('Oops',errorMsg.innerHTML,'error');
                    errorMsg.classList.remove("hide");
                }
            }

        }


    }) ;
});