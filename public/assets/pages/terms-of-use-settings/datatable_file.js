"use strict";
var KTDatatableAutoColumnHideDemo = {
    init: function () {
        var t;
        (t = $("#kt_table_en_tete").KTDatatable({
            data: { type: "remote", source: { read:
                        { url: basePath+"administration/terms-of-use-settings/list-of-terms-of-use" }
                        }, pageSize: 10, saveState: !1, serverPaging: !0, serverFiltering: !0, serverSorting: !0 },
            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },
            columns: [

                { field: "order", title: "Numéro ordre", width: "auto" },
                { field: "lib_en_tete", title: "En-tête", width: "480" },
                {
                    field: "status",
                    title: "Status",
                    width:'80',
                    template: function (t) {
                        var e = {
                            '1': { title: "Actif", class: "kt-badge--success" },
                            '0': { title: "Désactivé", class: " kt-badge--warning" },

                        };
                        return '<span class="kt-badge ' + e[t.status].class + ' kt-badge--inline kt-badge--pill">' + e[t.status].title + "</span>";
                    },
                },
                {
                    field: "Actions",
                    title: "Actions",
                    sortable: !1,
                    width: 100,
                    overflow: "visible",
                    autoHide: !1,
                    template: function (row) {

                        let status,
                        title = 'Mise à jour du statut de l\'en-tête';
                        if(row.status === '1')
                        {
                            status = '<a href="javascript:void(0);" onclick="updateStatus('+row.id+',\'lock\',\'id_en_tete\',\'terms_of_use_en_tete\')" class="btn btn-sm btn-clean btn-icon-md" title="'+title+'">' +
                                        '<i class="la la-trash text-danger"></i> ' +
                                    '</a>'
                        }
                        else
                        {
                            status = '<a href="javascript:void(0);" onclick="updateStatus('+row.id+',\'unlock\',\'id_en_tete\',\'terms_of_use_en_tete\')" class="btn btn-sm btn-clean btn-icon-md" title="'+title+'"><i class="la la-unlock-alt text-success"></i> </a>'
                        }
                        return '<a href="javascript:;" onclick="editEnTete('+row.id+')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Modifier l\'en-tête"><i class="la la-edit"></i></a>' +
                                ''+status;
                    },
                },
            ],
        })),
            $("#kt_form_status").on("change", function () {
                t.search($(this).val().toLowerCase(), "Status");
            }),
            $("#kt_form_type").on("change", function () {
                t.search($(this).val().toLowerCase(), "Type");
            })

    },

    initContain :function()
    {
        var t;
        (t = $("#kt_table_contenus").KTDatatable({
            data: { type: "remote", source: { read:
                        { url: basePath+"administration/terms-of-use-settings/list-of-contenus" }
                }, pageSize: 10, saveState: !1, serverPaging: !0, serverFiltering: !0, serverSorting: !0 },
            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },
            columns: [
                { field: "en_tete", title: "En-tête", width: "500",template:function(row){


                        return row.en_tete.name_en_tete;
                    } },
                { field: "contenu", title: "Contenu", width: "500",

                    template:function(row)
                    {

                        return '<p class="text-align-justify">'+decodeHTMLEntities(row.contenu)+'</p>';
                    }
                },

                {
                    field: "status",
                    title: "Status",
                    width:'80',
                    template: function (t)
                    {
                        var e = {
                            '1': { title: "Actif", class: "kt-badge--success" },
                            '0': { title: "Désactivé", class: " kt-badge--warning" },

                        };
                        return '<span class="kt-badge ' + e[t.status].class + ' kt-badge--inline kt-badge--pill">' + e[t.status].title + "</span>";
                    },
                },
                {
                    field: "Actions",
                    title: "Actions",
                    sortable: !1,
                    width: 90,
                    overflow: "visible",
                    autoHide: !1,
                    template: function (row) {

                        let status,
                            title = 'Mise à jour du statut de l\'en-tête';
                        if(row.status === '1')
                        {
                            status = '<a href="javascript:void(0);" onclick="updateStatus('+row.id+',\'lock\',\'id_contenu\',\'terms_contenu\')" class="btn btn-sm btn-clean btn-icon-md" title="'+title+'">' +
                                '<i class="la la-trash text-danger"></i> ' +
                                '</a>'
                        }
                        else
                        {
                            status = '<a href="javascript:void(0);" onclick="updateStatus('+row.id+',\'unlock\',\'id_contenu\',\'terms_contenu\')" class="btn btn-sm btn-clean btn-icon-md" title="'+title+'"><i class="la la-unlock-alt text-success"></i> </a>'
                        }
                        return '<a href="javascript:;" onclick="editContenu('+row.id+')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Modifier l\'en-tête"><i class="la la-edit"></i></a>' +
                            ''+status;
                    },
                },
            ],
        })),
            $("#kt_form_status").on("change", function () {
                t.search($(this).val().toLowerCase(), "Status");
            }),
            $("#kt_form_type").on("change", function () {
                t.search($(this).val().toLowerCase(), "Type");
            })
    }
};
jQuery(document).ready(function () {
    KTDatatableAutoColumnHideDemo.init();
    KTDatatableAutoColumnHideDemo.initContain();
});
