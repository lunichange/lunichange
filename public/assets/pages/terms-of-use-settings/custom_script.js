$(document).ready(function(){

    loadRubrique();
    $('#terms_content').summernote();
    var kt_add_rubrique_form = $('#kt_add_rubrique_form'),
        kt_form_add_content_to_rubric = $('#kt_form_add_content_to_rubric'),
        kt_modal_update_rubrique = $('#kt_modal_update_rubrique'),
        kt_update_contenu_form = $('#kt_update_contenu_form'),
        kt_modal_update_contenu = $('#kt_modal_update_contenu')
    ;


    kt_add_rubrique_form.submit(function(e){
        e.preventDefault();

        let rubrique_name = $('input[name="rubrique_name"]');
        let rubrique_order = $('select[name="rubrique_order"]');

        let t = $('#btn_add_rubrique');
        let n = $('#btn_cancel_rubrique');

        if(rubrique_name.val().trim() === "" || rubrique_order.val() === "")
        {
            custom_std('error','Veuillez remplir tous les champs');
        }
        else
        {
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'administration/terms-of-use-settings/add-rubric',
                data:$(this).serialize(),
                beforeSend:function()
                {
                    t.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0);
                    n.attr("disabled", !0);
                },
                success:function(response)
                {
                   t.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                   n.attr("disabled", !1);
                   if(response.success !== undefined)
                   {
                       custom_std('success',response.success);

                       $('#kt_modal_add_rubrique').modal('hide');
                       setTimeout(function(){
                           document.location.href = "";
                       },2e2);

                   }
                   else if(response.error !== undefined)
                   {
                      custom_std('error',response.error);
                   }
                },
                error:function(state)
                {
                    t.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                    if(state.status === 404)
                    {
                        custom_std('error',"Page non trouvée");
                    }
                    else
                    {
                        custom_std('error',"Une erreur s'est produite au cours du traitement")
                    }
                }
            })
        }
    })

    kt_form_add_content_to_rubric.submit(function(e){

            e.preventDefault();
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'administration/terms-of-use-settings/add-content-by-rubric',
                data:$(this).serialize(),
                beforeSend:function()
                {

                },
                success:function(response)
                {
                    if(response.success!== undefined)
                    {
                        custom_std('success',response.success);

                        setTimeout(function(){

                            $('#kt_summernote_modal').modal('hide');

                            setTimeout(function(){
                                document.location.href = "";
                            },2e2);
                        },2e0);
                    }
                    else if(response.error !== undefined)
                    {
                        custom_std('error',response.error);

                    }
                },
                error:function(state)
                {

                }
            });


    });

    $('#kt_update_rubrique_form').submit(function(e){

        e.preventDefault();

        console.log($(this));

        $.ajax({
            type:'POST',
            dataType:'JSON',
            url:basePath+'administration/terms-of-use-settings/update-terms',
            data:$(this).serialize(),
            beforeSend:function()
            {
                $('#btn_update_cancel_rubrique').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
                $('#btn_update_rubrique').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
            },
            success:function(response)
            {

                  $('#btn_update_cancel_rubrique').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                  $('#btn_update_rubrique').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);



                if(response.success !== undefined)
                {
                    custom_std('success',response.success);

                    setTimeout(function(){
                        location.reload();
                    },2e2)
                }
                else if(response.error !== undefined)
                {
                    custom_std('error',response.error);
                }


            },
            error:function()
            {
                $('#btn_update_cancel_rubrique').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                $('#btn_update_rubrique').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);

                custom_std('error',"Une erreur s'est produite au cours du traitement");
            }

        })

    });

    kt_update_contenu_form.submit(function(e){

        e.preventDefault();
        let btn_update_cancel_contenu = $('#btn_update_cancel_contenu'),
            btn_update_contenu        = $('#btn_update_contenu');


        $.ajax({

            type:'POST',
            dataType:'JSON',
            url:basePath+'administration/terms-of-use-settings/update-contenu',
            data:$(this).serialize(),
            beforeSend:function()
            {
                btn_update_contenu.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
                btn_update_cancel_contenu.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
            },
            success:function(response)
            {
                kt_modal_update_contenu.modal('hide');

                btn_update_contenu.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                btn_update_cancel_contenu.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);

                if(response.success !== undefined)
                {
                    custom_std('success',response.success);
                    setTimeout(function(){
                        location.reload();
                    });
                }
                else if(response.error !== undefined)
                {
                    custom_std('error',response.error);
                }
            },
            error:function()
            {
                btn_update_contenu.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                btn_update_cancel_contenu.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                custom_std('error',"Une erreur s'est produite au cours du traitement")
            }

        })

    });

});

function loadRubrique()
{
    let kt_select_rubric_order = $("#kt_select_rubric_order");
    $.ajax({
        url:basePath+'administration/terms-of-use-settings/load-rubric-order',
        type:'POST',
        dataType:'json',
        success:function(response)
        {
            let options = "";

            response.forEach(function(elements){

                options += "<option value='"+elements+"'>"+elements+"</option>";
            });
            kt_select_rubric_order.html(options);

        },
        error:function(state)
        {
            if(state.status === 404)
            {
                custom_std('error',"Page non trouvée");

            }
        }
    })
}

function decodeHTMLEntities (str)
{

    if(str && typeof str === 'string') {

        str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
        str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');


    }

    return str;
}

function updateStatus(id,action,column=null,table=null)
{
    let Id = parseInt(id);

    swal({
            title: "Mettre à jour ?",
            text: "Mettre à jour le statut de cet élément",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirmer",
            confirmButtonColor:"#1d53b0",
            cancelButtonColor:"#e8e8e8",
            cancelButtonText: "Annuler",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm)
            {

                $.ajax({
                    type:'POST',
                    dataType:'JSON',
                    url:basePath+'administration/terms-of-use-settings/update-status?column='+column+'&table='+table+'',
                    data:
                        {
                            action:action,
                            conditions:Id
                        },
                    success:function(response)
                    {
                        if(response.success !== undefined)
                        {
                            custom_std('success',response.success);

                            setTimeout(function(){
                                location.reload();
                            },2e3);
                        }
                        else if(response.error !== undefined)
                        {
                            custom_std('error',response.error);
                        }
                    },
                    error:function()
                    {
                        custom_std('error',"Une erreur s'est produite au cours du traitement");
                    }


                })
            }
        });

}

function editEnTete(id)
{
    let Id = parseInt(id);

    $.ajax({

        type:'POST',
        url:basePath+'administration/terms-of-use-settings/std-get?type=unique&table=terms_of_use_en_tete',
        dataType:'JSON',
        data:
            {
                id_en_tete:Id
            },

        success:function(response)
        {


           let kt_modal_update_rubrique = $('#kt_modal_update_rubrique');
           if(response.length !== 0)
           {
               $('#update_rubrique').val(response.name_en_tete);
               selectOptionOfSelect('ed_kt_select_rubric_order',response.order);
               setTimeout(function()
               {
                   kt_modal_update_rubrique.modal('show');

               },2e0);

           }
        },
        error:function()
        {
            custom_std('error',"Une erreur s'est produite au cours du traitement");
        }

    })
}

function editContenu(id)
{
   let Id = parseInt(id);

   let kt_modal_update_contenu = $('#kt_modal_update_contenu');
   $.ajax({
       type:'POST',
       dataType:'JSON',
       url:basePath+'administration/terms-of-use-settings/std-get?table=terms_contenu&type=unique',
       data:
           {
               id_contenu:Id
           },
       success:function(response)
       {

           let html = $('<textarea />').html(response.contenu).text();
           if(response.length !== 0)
           {
               $('#terms_update_content').summernote('code',html);
               selectOptionOfSelect('kt_update_select_load_rubric',response.en_tete);
               kt_modal_update_contenu.modal('show');
           }

       },
       error:function(state)
       {
           if(state.status === 404)
           {
               toastr.error('Impossible de contacter le server')
           }
           else
           {
               toastr.error('Une erreur s\'est produite au cours du traitement');
           }
       }
   })
}