$(document).ready(function(){

    let kt_inbox__item = $('.kt-inbox__item');

    kt_inbox__item.on('click',function(){

        let Id = parseInt($(this).attr('data-id'));
        $.ajax({

            type:'POST',
            url:'/dashboard/administration/inbox/std-get?table=messages_site&type=unique',
            data:
                {
                    column:
                        {
                            id_message:Id
                        },
                    update:
                        {
                            column:'statut',
                            value:'1'
                        }
                },
            beforeSend:function()
            {
                blockUI('Veuillez patienter...')
            },
            success:function(response)
            {
                unblockUI();

                if(response.length !== 0)
                {

                    $('#kt-inbox__subject').html(response.subject);
                    $('#kt-inbox__customer').html(response.customer);
                    $('#kt-inbox_details_msg').html('<tr>' +
                        '<td>de</td>' +
                        '<td>'+response.customer+'</td>' +
                        '</tr>' +
                        '' +
                        '<tr>' +
                        '<td>date:</td>' +
                        '<td>'+response.date_message+'</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Sujet:</td>' +
                        '<td>'+response.subject+'</td>' +
                        '</tr>');
                    $('#kt-inbox_substr_text').html(response.customer.substring(0,200));
                    $('#kt-inbox_datetime').html(response.date_message);
                    $('#kt-inbox_text_msg').html('<p style="font-size:1.0em !important;line-height: 50px !important; text-align: justify-all !important">' +
                        ''+response.contenu +
                        '</p>')
                }
            },
            error:function(state)
            {
                unblockUI();

                if(state.status === 404)
                {
                    toastr.error('Oops','Page non trouvée');
                }
                else
                {
                    custom_std('error','Une erreur s\'est produite au cours du traitement');
                }

            }


        })
    });

});