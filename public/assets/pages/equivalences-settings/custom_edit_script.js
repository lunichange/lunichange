//jQuery.noConflict();
(function(){

    let select_edit_source = $('#select_edit_source');
    let select_edit_destination = $('#select_edit_destination');

    select_edit_source.on('change',function(){
        let selectValue = parseInt($(this).val());

        $.ajax({

            type:'post',
            url:basePath+'administration/equivalences-settings/std-get?table=devises&type=all',
            dataType:'JSON',
            data:
                {
                    'statut':'1',
                    'id_devise<>?':selectValue
                },
            beforeSend:function()
            {
              blockUI('Traitement en cours...');
            },
            success:function(response)
            {
                let tailleResponse = response.length;
                unblockUI();

               if(tailleResponse > 1)
               {
                    select_edit_destination.selectpicker('destroy');
                    let options = '';
                    response.forEach(function (elements) {
                        options += '<option value="'+elements.id_devise+'">'+elements.lib_devise+'</option>';
                    });
                    select_edit_destination.html(options);

                    select_edit_destination.selectpicker();
                    $('#container_btn_confirm_edit').removeClass('hide');

               }

            },
            error:function(state)
            {
                if(state.status === 404)
                swal.fire('Erreur',"Une s'est produite au cours de l'opération",'error');
            }

        });


    })

    $('#btn_confirm_edit').click(function(){

        let source = parseInt(select_edit_source.val());
        let destination = parseInt(select_edit_destination.val());

        if(source !== "" && destination !== "")
        {

            $.ajax({
                type:'POST',
                dataType: 'JSON',
                url:basePath+'administration/equivalences-settings/load-elements-input',
                data:
                    {
                        'source':source,
                        'destination':destination,
                        'edit':true
                    },
                beforeSend:function()
                {
                    blockUI('Traitement en cours...');
                },
                success:function(response)
                {
                    unblockUI();
                   if(response.error !== undefined)
                   {
                       swal.fire('Erreur',response.error,'error');
                       $('#container_result_for_edit_equivalences').empty();
                   }
                   else
                   {
                       let inputs = '';
                       inputs+= '<form method="post" id="form_edit_equiv_settings">' +
                           '<div class="row width-100 clearfix margin-0-auto">' +
                           '<div class="col-xl-3 col-lg-2 padding-l-0 padding-r-0">' +
                           '<img src="'+response.source.image_devise+'" class="img_rounded img_settings_equiv" style=""/>' +
                           '<center><b style="margin-top: 8px !important" class="text-center">'+response.source.lib_devise+'</b></center>' +
                           '<input type="hidden" name="hidden_edit_source" value="'+response.source.id_devise+'"/>'+
                           '</div>' +
                           '<div class="col-xl-7 col-lg-8 padding-r-0 padding-l-0">' +
                           '<div class="row kt-padding-0">' +
                           '<span class="col-md-6"><label for="taux_achat" class="form-control-label font-size-0_9">Prix ou Taux d\'achat de: '+response.source.lib_devise+'</label><input type="number" step="any" min="0" value="'+response.source.taux_achat+'" class="form-control col-md-12 margin-top-10" id="taux_achat" placeholder="Indiquer le taux d\'achat" required name="taux_achat"/> </span> ' +
                           '<span class="col-md-6"><label for="taux_achat" class="form-control-label font-size-0_9">Quantité minimum de <b>'+response.destination.lib_devise+'</b></label><input type="number" class="form-control margin-top-10" value="'+response.source.quantite_min_achat+'" placeholder="Quantité minimum" step="any" min="0" id="quantite_min_achat" required name="quantite_min_achat"/> </span> ' +
                           '</div>'+
                           '<div class="row margin-top-30">' +
                           '<span class="col-md-6"><label class="form-control-label col-md-12 width-100 font-size-0_9" for="taux_vente">Prix ou Taux de vente de: '+response.source.lib_devise+'</label><input placeholder="Indiquer le taux de vente" value="'+response.source.taux_vente+'" class="form-control col-md-12 margin-top-10" type="number" step="any" min="0" id="taux_vente" required name="taux_vente"/> </span> ' +
                           '<span class="col-md-6"><label for="quantite_min_vente" class="form-control-label col-md-12 font-size-0_9">Quantité minimum de <b>'+response.source.lib_devise+'</b> </label><input type="number" placeholder="Quantité minimum" value="'+response.source.quantite_min_vente+'" class="form-control col-md-12 margin-top-10" step="any" min="0" id="quantite_min_vente" required name="quantite_min_vente"/> </span> ' +
                           '</div>'+
                           '</div> ' +
                           '<div class="col-lg-2 padding-l-0 padding-r-0">' +
                           '<img src="'+response.destination.image_devise+'" class="img_rounded img_settings_equiv" style=""/>' +
                           '<center><b style="margin-top: 8px !important" class="text-center">'+response.destination.lib_devise+'</b></center>' +

                           '<input type="hidden" name="hidden_edit_destination" value="'+response.destination.id_devise+'"/>'+
                           '</div>' +
                           '</div>' +
                           '<div class="col-md-12 kt-margin-t-50">' +
                           '<center>' +
                           '<button style="" class="btn btn-outline-primary btn-hover-primary btn-elevate-air" id="btn_submit_edit_equiv_settings" type="submit"><i class="flaticon-edit"></i>&nbsp;Modifier </button>' +
                           '</center> ' +
                           '</div> ' +
                           '</form>';

                             $('#container_result_for_edit_equivalences').html(inputs);


                       $("#btn_submit_edit_equiv_settings").click(function(i) {
                           i.preventDefault();
                           var e = $(this),
                               n = $(this).closest("form");
                           n.validate({
                               rules: {
                                   text: {
                                       required: !0,
                                       min:0

                                   },
                                   select: {
                                       required: !0
                                   }
                               }
                           }), n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                                   n.ajaxSubmit({
                                       url: basePath+"administration/equivalences-settings/edit-equivalences-settings",
                                       dataType:'JSON',
                                       success: function(response)
                                       {

                                           if(response.error !== undefined)
                                           {
                                               setTimeout(function(){
                                                   e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), alertNotification('.alertNotification','danger',response.error)

                                               },2e3);
                                               swal.fire('Erreur',response.error,'error');

                                           }
                                           else if(response.success !== undefined)
                                           {
                                               swal.fire('Terminé',response.success,'success');
                                               setTimeout(function(){
                                                   document.location.href = "";
                                               },2e3);
                                           }

                                       },
                                       error:function()
                                       {
                                           setTimeout(function() {
                                               e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                           }, 2e3)
                                       }
                                   })
                           )
                       })


                   }
                },
                error:function()
                {
                    unblockUI();
                    swal.fire('Erreur','Une erreur s\'est produite au cours du traitement','error');
                }
            })
        }
    });
})();