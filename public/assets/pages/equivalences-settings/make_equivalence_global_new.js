var KTFormControls= {
        init:function() {

            $("#kt_form_add_global_settings").validate( {
                    rules: {
                        electronic_currency: {
                            required: !0
                        }
                        , purchase_price: {
                            required: !0, minlength: 1,min: 0,maxlength:5
                        }
                        , sale_price: {
                            required: !0, minlength:1, min:0,maxlength: 5
                        }
                    }
                    , invalidHandler:function(e, r) {
                        swal.fire( {
                                title:"", text:"Il y a quelques erreurs dans votre soumission. Veuillez les corriger.", type:"error", confirmButtonClass:"btn btn-secondary", onClose:function(e) {

                                }
                            }
                        ), e.preventDefault()
                    }
                    , submitHandler:function(e) {

                        $.ajax({
                            type:'POST',
                            url:basePath+'administration/equivalences-settings/add-global-settings',
                            data:$('#kt_form_add_global_settings').serialize(),
                            beforeSend:function()
                            {
                                $('#btn_add_global_equivalence').addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0);
                                $('#btn_cancel_global_equivalence').addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0);


                            },
                            success:function(response)
                            {
                                $('#btn_add_global_equivalence').removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                $('#btn_cancel_global_equivalence').removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);

                                if(response.success !== undefined)
                                {
                                    $('#modal_global_settings').modal('hide');
                                    Swal.fire('Terminé',response.success,"success")
                                    setTimeout(function(){
                                        document.location.href= location.origin+'/dashboard/administration/equivalences-settings'
                                    },2e1)
                                }
                                else
                                {
                                    Swal.fire('Oops',response.error,'error');
                                }
                            },
                            error:function(state)
                            {
                                if(state.status === 404)
                                swal.fire('Erreur',"Une erreur s'est produite lors du traitement","erreur");
                            }

                        })
                    }
                }
            ),

            $("#kt_form_update_global_settings").validate( {
                    rules: {
                        update_electronic_currency: {
                            required: !0
                        }
                        , update_purchase_price: {
                            required: !0, minlength: 1,min: 0,maxlength:12
                        }
                        , update_sale_price: {
                            required: !0, minlength:1, min:0,maxlength: 12
                        },
                         update_quantite_min:
                             {
                                 required: !0, minlength:1, min:0, maxlength:12
                             }
                    }
                    , invalidHandler:function(e, r) {
                        swal.fire( {
                                title:"", text:"Il y a quelques erreurs dans votre soumission. Veuillez les corriger.", type:"error", confirmButtonClass:"btn btn-secondary", onClose:function(e) {

                                }
                            }
                        ), e.preventDefault()
                    }
                    , submitHandler:function(e) {

                        $.ajax({
                            type:'POST',
                            url:basePath+'administration/equivalences-settings/update-global-settings',
                            data:$('#kt_form_update_global_settings').serialize(),
                            beforeSend:function()
                            {
                                $('#btn_update_global_equivalence').addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0);
                                $('#btn_cancel_update_global_equivalence').addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0);


                            },
                            success:function(response)
                            {
                                $('#btn_update_global_equivalence').removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                $('#btn_cancel_update_global_equivalence').removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);

                                if(response.success !== undefined)
                                {
                                    $('#modal_update_global_settings').modal('hide');
                                    Swal.fire('Terminé',response.success,"success");
                                    setTimeout(function(){
                                        document.location.href= location.origin+'/dashboard/administration/equivalences-settings'
                                    },2e1);
                                }
                                else
                                {
                                    Swal.fire('Oops',response.error,'error');
                                }
                            },
                            error:function(state)
                            {
                                if(state.status === 404)
                                swal.fire('Erreur',"Une erreur s'est produite lors du traitement","erreur");
                            }

                        })
                    }
                }
            )

        }
    }

;

jQuery(document).ready(function() {
        KTFormControls.init();


        let update_electronic_currency = $('#update_electronic_currency');

        update_electronic_currency.on('change',function(){

            let currentValue = $(this).val();
            if(currentValue !== "")
            {
                currentValue = parseInt(currentValue);

                $.ajax({
                    type:'post',
                    url:basePath+"administration/equivalences-settings/std-get?type=unique&table=equivalence_globale",
                    data:
                        {
                            devise:currentValue
                        },
                    beforeSend:function()
                    {
                        blockUI('Veuillez patienter...')
                    },
                    success:function(response)
                    {
                        unblockUI();
                       $('#update_purchase_price').val(response.prix_achat);
                       $('#update_sale_price').val(response.prix_vente);
                       $('#update_quantite_min').val(response.quantite_min);
                    },
                    error:function (state)
                    {
                        if(state.status === 404)
                        {
                            Swal.fire('Oops',state.status,'error');
                        }
                    }


                });
            }





        })


}

);