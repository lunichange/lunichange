$(document).ready(function(){

   let select_add_equivalence_monnaie_source = $('#select_add_equivalence_monnaie_source');
   //select_add_equivalence_monnaie_source.selectpicker('destroy');
    let select_add_equivalence_monnaie_destination = $('#select_add_equivalence_monnaie_destination');
    let select_add_equivalence_devise_source = $("#select_add_equivalence_devise_source");
    let select_add_equivalence_devise_destination = $("#select_add_equivalence_devise_destination");

    select_add_equivalence_monnaie_source.on('change',function(){

        let tableSplit = split('-',$(this).val().trim());

       let valueSource = parseInt(tableSplit.value);

       let typeCurrency = tableSplit.typeCurrency;

       let typeCurrencyArray = ["forte","crypto","faible"];

       let isInArray = inArray(typeCurrency,typeCurrencyArray);

       if(isInArray)
       {
           if(typeCurrency === "crypto")
           {
               arrayConditions = {
                   'statut':'1',
                   'id_famille<>?':valueSource,
                   'libelle_famille':'FCFA'
               }
           }
           else if(typeCurrency === "faible")
           {
               arrayConditions = {
                   'statut':'1',
                   'id_famille<>?':valueSource
               }
           }
           else
           {
               arrayConditions = {
                   'statut':'1',
                   'libelle_famille':'FCFA'
               }
           }
            $.ajax({

                type:'POST',
                dataType:'JSON',
                url:basePath+'administration/equivalences-settings/std-get?table=luni_monnaies&type=all',
                data:arrayConditions,
                beforeSend:function()
                {
                    blockUI('Traitement en cours...');
                    select_add_equivalence_monnaie_destination.selectpicker('destroy');
                },
                success:function(response)
                {
                    let options = '';
                    response.forEach(function(elements){
                        options +='<option value="'+elements.id_famille+'">'+elements.libelle_famille+'</option>';
                    });


                    select_add_equivalence_monnaie_destination.html(options);
                    select_add_equivalence_monnaie_destination.selectpicker();
                    let source_to_load_devise = valueSource;
                    let destination_to_load_devise = parseInt(select_add_equivalence_monnaie_destination.val());
                    $.ajax({
                        type:'POST',
                        dataType: 'JSON',
                        url:basePath+'administration/equivalences-settings/load-currencies-from-currency',
                        data:
                            {
                                source:      source_to_load_devise,
                                destination: destination_to_load_devise
                            },
                        success:function(r)
                        {
                            select_add_equivalence_devise_source.selectpicker('destroy'),
                            select_add_equivalence_devise_destination.selectpicker('destroy');
                            if(r.source !== undefined && r.destination !== undefined)
                            {

                                let source_devise = r.source;
                                let destination_devise = r.destination;
                                let optionsSource = '';
                                let optionsDestination = '';


                               if((source_devise !== "" || source_devise.length < 1) || (destination_devise !== "" || destination_devise.length < 1))
                               {
                                   source_devise.forEach(function(t){
                                       optionsSource +='<option value="'+t.id_devise+'">'+t.lib_devise+'</option>';

                                   });

                                   select_add_equivalence_devise_source.html(optionsSource);

                                   let getValueDeviseSource = parseInt(select_add_equivalence_devise_source.val());

                                   destination_devise.forEach(function(e) {
                                       if (parseInt(select_add_equivalence_monnaie_destination.val()) === valueSource)
                                       {
                                           if(getValueDeviseSource !== parseInt(e.id_devise))
                                           {
                                               optionsDestination += '<option value="'+e.id_devise+'">'+e.lib_devise+'</option>';
                                           }

                                       }
                                       else
                                       {
                                           optionsDestination += '<option value="'+e.id_devise+'">'+e.lib_devise+'</option>';
                                       }
                                   });

                                   select_add_equivalence_devise_destination.html(optionsDestination);
                                   select_add_equivalence_devise_source.selectpicker();
                                   select_add_equivalence_devise_destination.selectpicker();
                                   $('#confirm_btn_container').removeClass('hide');
                                   unblockUI();
                               }
                               else
                               {

                               }



                            }
                            else if(r.error !== undefined)
                            {
                                swal.fire('Erreur',r.error,'error');
                            }
                        },
                        error:function()
                        {
                            swal.fire('Erreur','Error s\'est produite','error');
                        }
                    })

                },
                error:function()
                {
                    swal.fire('',"Une erreur s'est produite. Vérifier votre connexion ou sinon veuillez nous contacter","error");
                }

            });
       }
       else
       {
           $('#confirm_btn_container').addClass('hide');
           swal.fire('Message','Impossible de traiter ces informations. Veuillez rééssayer','warning');
       }


    });


    /**
     * Application de onChange sur la monnaie de destination
     */

    select_add_equivalence_monnaie_destination.on('change',function(){
        let getValue = parseInt($(this).val());
        //alert('ok');
        arrayConditions = {
            'famille_devise':getValue,
            'statut':'1'
        };

        $.ajax({
            type:'POST',
            dataType:'JSON',
            url:basePath+'administration/equivalences-settings/std-get?table=devises&type=all',
            data:arrayConditions,
            beforeSend:function()
            {
                blockUI('Traitement en cours...');
                select_add_equivalence_devise_destination.selectpicker('destroy');
                select_add_equivalence_devise_destination.empty();
            },
            success:function (response)
            {
               // console.log(response);
                let options = '';
                response.forEach(function(elements)
                {
                    if(parseInt(select_add_equivalence_devise_source.val()) !== parseInt(elements.id_devise))
                    options +='<option value="'+elements.id_devise+'">'+elements.lib_devise+'</option>';
                });

                select_add_equivalence_devise_destination.html(options);
                select_add_equivalence_devise_destination.selectpicker();
                unblockUI();
            },
            error:function () {
                swal.fire('Erreur',"Une erreur s'est produite au cours de l'opération","error");
            }
        })

    });

    /**
     * Application de onChange sur la devise source
     */

    select_add_equivalence_devise_source.on('change',function(){
        //alert('dfd');
        let getValue = parseInt($(this).val());
        let getValueCurrencyDestination = parseInt(select_add_equivalence_monnaie_destination.val());
        //alert('ok');
        arrayConditions = {
            'id_devise<>?':getValue,
            'famille_devise':getValueCurrencyDestination,
            'statut':'1'
        };

        $.ajax({
            type:'POST',
            dataType:'JSON',
            url:basePath+'administration/equivalences-settings/std-get?table=devises&type=all',
            data:arrayConditions,
            beforeSend:function()
            {
                blockUI('Traitement en cours...');
                select_add_equivalence_devise_destination.selectpicker('destroy');
                select_add_equivalence_devise_destination.empty();
            },
            success:function (response)
            {
                // console.log(response);
                let options = '';
                response.forEach(function(elements)
                {
                    if(parseInt(select_add_equivalence_devise_source.val()) !== parseInt(elements.id_devise))
                        options +='<option value="'+elements.id_devise+'">'+elements.lib_devise+'</option>';
                });

                select_add_equivalence_devise_destination.html(options);
                select_add_equivalence_devise_destination.selectpicker();
                unblockUI();
            },
            error:function () {
                swal.fire('Erreur',"Une erreur s'est produite au cours de l'opération","error");
            }
        })

    });

    $('#btn_confirm_devise_to_add').click(function(){

        let source = select_add_equivalence_devise_source
        let destination  = select_add_equivalence_devise_destination;

        if(source !=="" && destination !== "")
        {
            $.ajax({

                type:'POST',
                url:basePath+'administration/equivalences-settings/load-elements-input',
                dataType:'JSON',
                data:
                    {
                        'source':parseInt(source.val()),
                        'destination':parseInt(destination.val())
                    },
                beforeSend:function()
                {
                    blockUI('Traitement en cours...');
                },
                success:function(response)
                {
                   if(response.error !== undefined)
                   {
                       unblockUI();
                       swal.fire('Erreur',response.error,'error');
                   }
                   else
                   {
                       let inputs = '';
                       inputs+= '<form method="post" id="form_add_equiv_settings">' +
                           '<div class="row width-100 clearfix margin-0-auto">' +
                           '<div class="col-xl-3 col-lg-2 padding-l-0 padding-r-0">' +
                           '<img src="'+response.source.image_devise+'" class="img_rounded img_settings_equiv" style=""/>' +
                           '<center><b style="margin-top: 8px !important" class="text-center">'+response.source.lib_devise+'</b></center>' +
                           '<input type="hidden" name="hidden_add_source" value="'+response.source.id_devise+'"/>'+
                           '</div>' +
                           '<div class="col-xl-7 col-lg-8 padding-r-0 padding-l-0">' +
                           '<div class="row kt-padding-0">' +
                           '<div class="col-md-12 text-center padding-20">' +
                           '<h5> A l\'achat: <b>1 </b> <b style="color: forestgreen">'+response.source.lib_devise+'</b> vaut combien de <b style="color: red !important;">'+response.destination.lib_devise+'</b> </h5>' +
                           '</div> ' +
                           '<span class="col-md-6"><label for="taux_achat" class="form-control-label font-size-0_9">Prix  d\'achat de: '+response.source.lib_devise+'</label><input type="number" step="any" min="0" class="form-control col-md-12 margin-top-10" id="taux_achat" placeholder="Indiquer le taux d\'achat" required name="taux_achat"/> </span> ' +
                           '<span class="col-md-6"><label for="taux_achat" class="form-control-label font-size-0_9">Quantité minimum de <b>'+response.destination.lib_devise+'</b></label><input type="number" class="form-control margin-top-10" placeholder="Quantité minimum" step="any" min="0" id="quantite_min_achat" required name="quantite_min_achat"/> </span> ' +
                           '</div>'+
                           '<div class="row margin-top-30">' +
                           '<div class="col-md-12 text-center padding-20">' +
                           '<h5> A la vente: <b>1 </b> <b style="color: forestgreen">'+response.source.lib_devise+'</b> vaut combien de <b style="color: red !important;">'+response.destination.lib_devise+'</b> </h5>' +
                           '</div> ' +
                           '<span class="col-md-6"><label class="form-control-label col-md-12 width-100 font-size-0_9" for="taux_vente">Prix de vente de: '+response.source.lib_devise+'</label><input placeholder="Indiquer le taux de vente" class="form-control col-md-12 margin-top-10" type="number" step="any" min="0" id="taux_vente" required name="taux_vente"/> </span> ' +
                           '<span class="col-md-6"><label for="quantite_min_vente" class="form-control-label col-md-12 font-size-0_9">Quantité minimum de <b>'+response.source.lib_devise+'</b> </label><input type="number" placeholder="Quantité minimum" class="form-control col-md-12 margin-top-10" step="any" min="0" id="quantite_min_vente" required name="quantite_min_vente"/> </span> ' +
                           '</div>'+
                           '</div> ' +
                           '<div class="col-lg-2 padding-l-0 padding-r-0">' +
                           '<img src="'+response.destination.image_devise+'" class="img_rounded img_settings_equiv" style=""/>' +
                           '<center><b style="margin-top: 8px !important" class="text-center">'+response.destination.lib_devise+'</b></center>' +

                           '<input type="hidden" name="hidden_add_destination" value="'+response.destination.id_devise+'"/>'+
                           '</div>' +
                           '</div>' +
                           '<div class="col-md-12 kt-margin-t-50">' +
                           '<center>' +
                           '<button style="" class="btn btn-success btn-elevate-air btn-hover-success" id="btn_submit_add_equiv_settings" type="submit">Sauvegarder</button>' +
                           '</center> ' +
                           '</div> ' +
                           '</form>';

                       $('#container_results_for_add_equivalence').html(inputs);

                       unblockUI();

                       $("#btn_submit_add_equiv_settings").click(function(i) {
                           i.preventDefault();
                           var e = $(this),
                               n = $(this).closest("form");
                           n.validate({
                               rules: {
                                   text: {
                                       required: !0,
                                       min:0

                                   },
                                   select: {
                                       required: !0
                                   }
                               }
                           }), n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0))
                           if(($('input[name="hidden_add_source"]').val() !== select_add_equivalence_devise_source.val()) || ($('input[name="hidden_add_destination"]').val() !== select_add_equivalence_devise_destination.val()))
                           {
                               swal.fire('Erreur','Veuillez vérifier les informations. Les devises à paramétrer ne sont pas en conformité avec les devises sélectionnées.',"error");
                           }
                           else
                           {
                               n.ajaxSubmit({

                                   url: basePath+"administration/equivalences-settings/add-equivalences-settings",
                                   dataType:'JSON',
                                   success: function(answer) {
                                       setTimeout(function() {
                                           e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), t(n, "danger", "Incorrect username or password. Please try again.")
                                       }, 2e3);



                                       if(answer.error !== undefined)
                                       {
                                           e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);

                                           swal.fire('Erreur',answer.error,'error')

                                       }
                                       else if(answer.success !== undefined)
                                       {
                                           swal.fire('Succès!',answer.success,"success");

                                           setTimeout(function(){
                                               document.location.href = "";
                                           },2e1);
                                       }

                                   },
                                   error:function()
                                   {
                                       setTimeout(function() {
                                           e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                           swal.fire('','Une erreur s\'est produite avec Ajax !','error');
                                       }, 2e1);

                                   }
                               })
                           }

                       })
                   }

                },
                error:function()
                {

                }
            })
        }
        else
        {
            swalNotification('Erreur','Veuillez renseigner les devises intervenants dans le paramètrage','error');
        }
    });

});


function inArray(hayshack,array)
{
    let returnValue = false;
    if(Array.isArray(array))
    {
        let i =0;

        for(i;i< array.length;i++)
        {
           // console.log(array[i]);
            if(array[i] === hayshack)
            {
                returnValue = true;
            }

        }

    }
    else if(array instanceof Object)
    {
        for(let j in array)
        {
            if(array[j] === hayshack)
            {
                returnValue = true;
            }
        }
    }

    return returnValue;
}
function split(delimiter,elementToSplit)
{
    let tableSplit = elementToSplit.split(delimiter)

    return {
            typeCurrency: tableSplit[1],
            value:tableSplit[0]
    }
}