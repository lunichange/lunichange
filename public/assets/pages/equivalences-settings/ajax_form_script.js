"use strict";
var KTLoginGeneral = function() {
    var
        t = function(i, t, e) {
            var n = $('<div class="alert alert-' + t + ' alert-dismissible" role="alert">\t\t\t<div class="alert-text">' + e + '</div>\t\t\t<div class="alert-close">                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>            </div>\t\t</div>');
            i.find(".alert").remove(), n.prependTo(i), KTUtil.animateClass(n[0], "fadeIn animated"), n.find("span").html(e)
        };
    var alertNotification = function(cible,type,text){
        var n = '<div class="alert alert-'+type+'"><div class="alert-text">'+text+'</div><div class="alert-close">   <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>  </div> </div>';

        $(cible).fadeIn(4000).html(n)
    };

    return {
        init: function() {
            $("#btn_submit_add_equiv_settings").click(function(i) {
                i.preventDefault();
                var e = $(this),
                    n = $(this).closest("form");
                n.validate({
                    rules: {
                       input_taux: {
                            required: !0,
                            minLength:1,
                            min:0

                        },
                        select: {
                            required: !0
                        }
                    }
                }), n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                        n.ajaxSubmit({
                            url: basePath+"administration/equivalences-settings/add-equivalences-settings",
                            dataType:'JSON',
                            success: function(response, s, r, a) {
                                /*setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), t(n, "danger", "Incorrect username or password. Please try again.")
                                }, 2e3)*/



                                if(response.error !== undefined)
                                {

                                    setTimeout(function(){
                                        e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), alertNotification('.alertNotification','danger',response.error)

                                    },2e3)


                                }
                                else if(response.success !== undefined)
                                {

                                    setTimeout(function(){
                                        document.location.href = "";
                                    },2e3);
                                }

                            },
                            error:function(state)
                            {
                                if(state.status === 404)
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), t(n, "danger", "Une erreur s'est produite au cours de l'opération.Veuillez rééssayer.")
                                }, 2e3)
                            }
                        })
                )
            })

        }
    }
}();

jQuery(document).ready(function() {
    KTLoginGeneral.init()
});