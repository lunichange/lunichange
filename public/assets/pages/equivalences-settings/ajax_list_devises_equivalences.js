"use strict";
var KTDatatablesDataSourceAjaxServer= {
        init:function() {
            $("#kt_table_1").DataTable( {
                    responsive:!0, searchDelay:500, processing:!0, serverSide:!0,lengthMenu:[5, 10, 25, 50,"All"], pageLength:5,
                ajax:basePath+"administration/equivalences-settings/load-devises-equivalences",
                type:'POST',
                columns:[

                        {
                            data: "image_devise"
                        },{
                            data: "lib_devise"
                        }
                        , {
                            data: "taux_achat"
                        }
                        , {
                            data: "taux_vente"
                        }
                        , {
                            data: "quantite_min_achat"
                        }
                        , {
                            data: "quantite_min_vente"
                        }
                        , {
                            data: "destination"
                        }
                        , {
                            data: "id_devise", responsivePriority: -1
                        }
                    ], columnDefs:[ {
                        targets:-1, title:"Actions", orderable:!1, render:function(a, t, e, s) {


                            let devise_source = e.source;
                            let devise_destination = e.destination;
                            return'' +
                                '<a title="Permuter les devises" href="javascript:void(0);" onclick="switchDevise(\''+devise_source.id_devise+'\',\''+devise_destination.id_devise+'\')">' +
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <polygon points="0 0 24 0 24 24 0 24"/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" transform="translate(13.000000, 6.000000) rotate(-450.000000) translate(-13.000000, -6.000000) " x="12" y="8.8817842e-16" width="2" height="12" rx="1"/>\n' +
                                    '        <path d="M9.79289322,3.79289322 C10.1834175,3.40236893 10.8165825,3.40236893 11.2071068,3.79289322 C11.5976311,4.18341751 11.5976311,4.81658249 11.2071068,5.20710678 L8.20710678,8.20710678 C7.81658249,8.59763107 7.18341751,8.59763107 6.79289322,8.20710678 L3.79289322,5.20710678 C3.40236893,4.81658249 3.40236893,4.18341751 3.79289322,3.79289322 C4.18341751,3.40236893 4.81658249,3.40236893 5.20710678,3.79289322 L7.5,6.08578644 L9.79289322,3.79289322 Z" fill="#000000" fill-rule="nonzero" transform="translate(7.500000, 6.000000) rotate(-270.000000) translate(-7.500000, -6.000000) "/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" transform="translate(11.000000, 18.000000) scale(1, -1) rotate(90.000000) translate(-11.000000, -18.000000) " x="10" y="12" width="2" height="12" rx="1"/>\n' +
                                    '        <path d="M18.7928932,15.7928932 C19.1834175,15.4023689 19.8165825,15.4023689 20.2071068,15.7928932 C20.5976311,16.1834175 20.5976311,16.8165825 20.2071068,17.2071068 L17.2071068,20.2071068 C16.8165825,20.5976311 16.1834175,20.5976311 15.7928932,20.2071068 L12.7928932,17.2071068 C12.4023689,16.8165825 12.4023689,16.1834175 12.7928932,15.7928932 C13.1834175,15.4023689 13.8165825,15.4023689 14.2071068,15.7928932 L16.5,18.0857864 L18.7928932,15.7928932 Z" fill="#000000" fill-rule="nonzero" transform="translate(16.500000, 18.000000) scale(1, -1) rotate(270.000000) translate(-16.500000, -18.000000) "/>\n' +
                                    '    </g>\n' +
                                    '</svg>' +
                                '</a>';
                        }
                    },{
                        targets:-2, title:"Devises destination", orderable:!1, render:function(a, t, e, s) {
                           // console.log(a.lib_devise);
                            return '<span><div class="col-md-12"><img src="'+a.image_devise+'" class="img_rounded" width="50" height="50"/> </div><div class="col-md-12 text-center">'+a.lib_devise+'</div></span>'
                        }
                    },{
                        targets:-8, title:"#", orderable:!1, render:function(a, t, e, s) {

                            return '<span class="text-center fon1_0"><img src="'+a+'" class="img_rounded" width="50" height="50"/> </span>'
                        }
                    },
                    {
                        targets:-3, title:"Quantité minimum vente", orderable:!1, render:function(a, t, e, s) {
                            // console.log(a.lib_devise);

                            return '<div class="col-md-12 text-center"><span  class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill text-center font-size-1_0 kt-badge--rounded" style="font-weight: bold !important; text-align: center !important">'+a+'</span></div>'
                        }
                    },{
                        targets:-4, title:"Quantité minimum achat", orderable:!1, render:function(a, t, e, s) {
                            // console.log(a.lib_devise);

                            return '<div class="col-md-12 text-center"><span  class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill text-center font-size-1_0 kt-badge--rounded" style="font-weight: bold !important; text-align: center !important">'+a+'</span></div>'
                        }
                    },{
                        targets:-5, title:"Taux/Prix de vente", orderable:!1, render:function(a, t, e, s) {
                            // console.log(a.lib_devise);

                            return '<div class="col-md-12 text-center"><span  class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded font-size-1_0 kt-badge--rounded" style="font-weight: bold !important; text-align: center !important">'+a+'</span></div>'
                        }
                    },{
                        targets:-6, title:"Taux/Prix d'achat", orderable:!1, render:function(a, t, e, s) {
                            // console.log(a.lib_devise);

                            return '<div class="col-md-12 text-center"><span  class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded font-size-1_0 kt-badge--rounded" style="font-weight: bold !important; text-align: center !important">'+a+'</span></div>'
                        }
                    }
                        ,/* {
                            targets:-3, render:function(a, t, e, s) {
                                var n= {
                                        1: {
                                            title: "Pending", class: "kt-badge--brand"
                                        }
                                        , 2: {
                                            title: "Delivered", class: " kt-badge--danger"
                                        }
                                        , 3: {
                                            title: "Canceled", class: " kt-badge--primary"
                                        }
                                        , 4: {
                                            title: "Success", class: " kt-badge--success"
                                        }
                                        , 5: {
                                            title: "Info", class: " kt-badge--info"
                                        }
                                        , 6: {
                                            title: "Danger", class: " kt-badge--danger"
                                        }
                                        , 7: {
                                            title: "Warning", class: " kt-badge--warning"
                                        }
                                    }
                                ;
                                return void 0===n[a]?a:'<span class="kt-badge '+n[a].class+' kt-badge--inline kt-badge--pill">'+n[a].title+"</span>"
                            }
                        }
                        , {
                            targets:-2, render:function(a, t, e, s) {
                                var n= {
                                        1: {
                                            title: "Online", state: "danger"
                                        }
                                        , 2: {
                                            title: "Retail", state: "primary"
                                        }
                                        , 3: {
                                            title: "Direct", state: "success"
                                        }
                                    }
                                ;
                                return void 0===n[a]?a:'<span class="kt-badge kt-badge--'+n[a].state+' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-'+n[a].state+'">'+n[a].title+"</span>"
                            }
                        }*/
                    ]
                }
            ),
            $("#kt_table_global_settings").DataTable( {
                        responsive:!0, searchDelay:500, processing:!0, serverSide:!0,lengthMenu:[5], pageLength:5,
                        ajax:basePath+"administration/equivalences-settings/list-global-settings",
                        type:'POST',
                        columns:[

                            {
                                data: "devise"
                            },{
                                data: "prix_achat"
                            }
                            , {
                                data: "prix_vente"
                            }
                            , {
                                data: "quantite_min"
                            }

                        ], columnDefs:[ {
                            targets:-1, title:"Actions", orderable:!1, render:function(a, t, e, s) {



                                return'' +
                                    '<a title="Modifier le paramétrage" href="javascript:void(0);" onclick="updateGlobalSettings(\''+e.id_equiv+'\')">' +
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <rect x="0" y="0" width="24" height="24"/>\n' +
                                    '        <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\n' +
                                    '    </g>\n' +
                                    '</svg>' +
                                    '</a>';
                            }
                        },{
                            targets:0, title:"Devises", orderable:!0, render:function(a, t, e, s) {

                                return '<span><div class="col-md-12"><img src="'+a.image_devise+'" class="img_rounded" width="50" height="50"/> </div><div class="col-md-12 text-center">'+a.lib_devise+'</div></span>'
                            }
                        },{
                            targets:1, orderable:!0, render:function(a, t, e, s) {


                                 return '<div class="col-md-12 text-center"><span  class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill text-center font-size-1_0 kt-badge--rounded" style="font-weight: bold !important; text-align: center !important">'+a+'</span></div>'


                    }
                        },
                            {
                                targets:2, orderable:!1, render:function(a, t, e, s) {
                                    // console.log(a.lib_devise);

                                    return '<div class="col-md-12 text-center"><span  class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill text-center font-size-1_0 kt-badge--rounded" style="font-weight: bold !important; text-align: center !important">'+a+'</span></div>'

                                }
                            }
                            ,/* {
                            targets:-3, render:function(a, t, e, s) {
                                var n= {
                                        1: {
                                            title: "Pending", class: "kt-badge--brand"
                                        }
                                        , 2: {
                                            title: "Delivered", class: " kt-badge--danger"
                                        }
                                        , 3: {
                                            title: "Canceled", class: " kt-badge--primary"
                                        }
                                        , 4: {
                                            title: "Success", class: " kt-badge--success"
                                        }
                                        , 5: {
                                            title: "Info", class: " kt-badge--info"
                                        }
                                        , 6: {
                                            title: "Danger", class: " kt-badge--danger"
                                        }
                                        , 7: {
                                            title: "Warning", class: " kt-badge--warning"
                                        }
                                    }
                                ;
                                return void 0===n[a]?a:'<span class="kt-badge '+n[a].class+' kt-badge--inline kt-badge--pill">'+n[a].title+"</span>"
                            }
                        }
                        , {
                            targets:-2, render:function(a, t, e, s) {
                                var n= {
                                        1: {
                                            title: "Online", state: "danger"
                                        }
                                        , 2: {
                                            title: "Retail", state: "primary"
                                        }
                                        , 3: {
                                            title: "Direct", state: "success"
                                        }
                                    }
                                ;
                                return void 0===n[a]?a:'<span class="kt-badge kt-badge--'+n[a].state+' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-'+n[a].state+'">'+n[a].title+"</span>"
                            }
                        }*/
                        ]
                    }
            )

        }
    }

;
jQuery(document).ready(function() {
        KTDatatablesDataSourceAjaxServer.init()
    }

);

function switchDevise(source,destination)
{

    source = parseInt(source);
    destination = parseInt(destination);

    console.log(source);
    console.log(destination);
    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "La permutation des devises aura d'impact sur le calcul des prix. Voulez-vous continuer le processus ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, poursuivre'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type:'POST',
                url:'/administration/equivalences-settings/switch-devise',
                data:
                    {
                        source:source,
                        destination:destination,

                    },
                success:function(response)
                {
                    console.log(response);
                    if(response.success !== undefined)
                    {
                        Swal.fire('Terminé',response.success,'success');
                        $("#kt_table_1").DataTable().destroy();
                        setTimeout(function(){
                            KTDatatablesDataSourceAjaxServer.init();
                        },2e1);

                    }
                    else
                    {
                        if(response.error !== undefined)
                        {
                            Swal.fire('Oops',response.error,'error');
                        }
                    }
                },
                error:function(state)
                {
                    if(state.status === 404)
                    {
                        swal.fire('Erreur',"Une erreur s'est produite lors du traitement",'error');
                    }
                }
            })
        }
    })


}

function updateGlobalSettings(id)
{
    id = parseInt(id);

    if(id !== "" && id !== 0)
    {
            $.ajax({
                type:'POST',
                url:'/dashboard/administration/equivalences-settings/std-get?type=unique&table=equivalence_globale',
                data:
                    {
                        id_equiv: id
                    },
                success:function(response)
                {
                    selectOptionOfSelect("update_electronic_currency",response.devise);
                    $('#update_purchase_price').val(response.prix_achat);
                    $('#update_sale_price').val(response.prix_vente);
                    $('#update_quantite_min').val(response.quantite_min);

                    $('#modal_list_global_settings').modal('hide');
                    setTimeout(function()
                    {

                        $('#modal_update_global_settings').modal('show');

                    },300);


                },
                error:function(state)
                {
                    if(state.status === 404)
                    {
                        Swal.fire('Oops',"Une erreur veuillez vérifier votre connexion internet","error");
                    }
                }
            })
    }
    else
    {
        Swal.fire('Oops',"Vous tentez une opération non autorisée","error");
    }

}