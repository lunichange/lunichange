$(document).ready(function(){

    $("#kt_btn_numbers_add_form").click(function(i) {
        i.preventDefault();
        var e = $(this),
            n = $(this).closest("form");
        let lib_number = $('#libelle_number');

        let checkIfTelValid = $('#checkIfTelValid');

        if(lib_number.attr('type') === 'tel')
        {
            if(checkIfTelValid.val() === true)
            {
                swal.fire('Erreur',"Format du numéro de téléphone invalide","error");
            }
            else
            {
                n.validate({
                    rules: {

                        libelle_number:
                            {
                                required: !0
                            },
                        type_numero_add_number_form:
                            {
                                required: !0
                            },
                        kt_select_devise_add_number: {
                            required: !0
                        }
                    }
                }),
                n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),$('#kt_btn_cancel_add_number').attr('disabled',!0),
                        n.ajaxSubmit({
                            url: basePath+"administration/numbers-management/add-number",
                            type:'POST',
                            dataType:'JSON',
                            success: function(response) {

                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                    $('#kt_btn_cancel_add_number').attr('disabled',!1);




                                if(response.error !== undefined )
                                {
                                    swal.fire('Error',response.error,'error');
                                }
                                else
                                {
                                    swal.fire('Terminé',"Numéro créé avec succès !",'success');
                                    setTimeout(function(){
                                        document.location.href = location.origin+'/dashboard/administration/numbers-management'
                                    },2e1);

                                }

                            },
                            error:function()
                            {
                                swal.fire('Erreur',"Une erreur s'est produite au cours de l'opération.","error");
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                }, 2e3)
                            }
                        })
                )

            }
        }
        else
        {

                n.validate({
                    rules: {

                        libelle_number:
                            {
                                required: !0
                            },
                        type_numero_add_number_form:
                            {
                                required: !0
                            },
                        kt_select_devise_add_number: {
                            required: !0
                        }
                    }
                }),
                n.valid() && (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),$('#kt_btn_cancel_add_number').attr('disabled',!0),
                        n.ajaxSubmit({
                            url: basePath+"administration/numbers-management/add-number",
                            type:'POST',
                            dataType:'JSON',
                            success: function(response, s, r, a) {
                                e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                $('#kt_btn_cancel_add_number').attr('disabled',!1);




                                //console.log(response);

                                if(response.error !== undefined)
                                {

                                    swal.fire('Error',response.error,'error');

                                }
                                else if(response.success !== undefined)
                                {
                                    swal.fire('Terminé',"Numéro créé avec succès !",'success');
                                    setTimeout(function(){
                                        document.location.href = location.origin+'/dashboard/administration/numbers-management'
                                    },2e3);
                                }

                            },
                            error:function()
                            {
                                swal.fire('Erreur',"Une erreur s'est produite au cours de l'opération.","error");
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                }, 2e3)
                            }
                        })
                )


        }


    })

});