$(document).ready(function(){


    let kt_select_categorie_devise_add_number_form = $('#kt-select_categorie_devise_add_number_form');
    let results_according_type_number_selected = $('#results_according_type_number_selected');
    let inputs = null;

    let kt_select_devise_add_number = $('#kt_select_devise_add_number');
    kt_select_categorie_devise_add_number_form.on('change',function(){

        let current_value = $(this).val();
       // alert(current_value);

        if(current_value !== "")
        {
            if(parseInt(current_value) === 2)
            {

                let arrayConditions =
                    {

                        'statut':'1',
                        'categorie_devise':2
                    };

                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: basePath+'administration/numbers-management/std-get?table=devises&type=all',
                    data: arrayConditions,
                    beforeSend: function () {
                        blockUI('Veuillez patienter...')
                    },
                    success: function (response) {
                        unblockUI();
                        let inputSelect = "";

                        response.forEach(function (elements) {
                            inputSelect += "<option value='" + elements.id_devise + "'>" + elements.lib_devise + "</option>";
                        });


                         kt_select_devise_add_number.selectpicker();
                         kt_select_devise_add_number.html(inputSelect);

                       }
                    });


                    inputs='<div class="form-group">' +
                    '<label class="form-control-label">' +
                    'Nom enregistré sur le numéro' +
                    '</label> ' +
                    '<input type="text" required placeholder="Nom enregistré sur le numéro" class="form-control" name="number_name" id="number_name"/> ' +
                    '</div> ' +
                    '<div class="form-group">' +
                    '<label class="form-control-label" for="libelle_number">Renseigner un numbéro de téléphone</label>' +
                    '<input type="tel" class="form-control col-md-12 margin-top-5" id="libelle_number" required placeholder="Numéro de téléphone" name="libelle_number"/> ' +
                    '<input type="hidden" name="type_input" value="tel"/> ' +
                    '<div class="col-md-12" id="valid-msg"></div> ' +
                    '<div class="col-md-12" id="error-msg"></div> ' +
                    '</div><input type="hidden" id="checkIfTelValid"/>';


                results_according_type_number_selected.html(inputs);

                if($('#libelle_number').attr('type') === 'tel')
                {

                    let input = document.querySelector("#libelle_number"),
                        errorMsg = document.querySelector("#error-msg"),

                        validMsg = document.querySelector("#valid-msg")

                    ;


// here, the index maps to the error code returned from getValidationError - see readme
                    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

// initialise plugin
                    let iti = window.intlTelInput(input, {
                        onlyCountries: ["bj"],
                        separateDialCode:true,
                        utilsScript: location.origin+'/assets/plugins/intl-tel-input/build/js/utils.js'
                    });


                    let reset = function() {
                        input.classList.remove("error");
                        errorMsg.innerHTML = "";
                        errorMsg.classList.add("hide");
                        validMsg.classList.add("hide");
                    };

                    input.addEventListener('blur', function() {
                        reset();
                        if (input.value.trim()) {
                            if (iti.isValidNumber()) {
                                validMsg.classList.remove("hide");
                                $('#checkIfTelValid').val(true)
                            } else {
                                input.classList.add("error");
                                $('#checkIfTelValid').val(false);
                                swal.fire('Error',"Veuillez vérifier le format du numéro","error");
                            }
                        }
                    });

// on keyup / change flag: reset
                    input.addEventListener('change', reset);
                    input.addEventListener('keyup', reset);


                }
            }
            else if(parseInt(current_value) === 1)
            {
                let arrayConditions =
                    {

                        'statut':'1',
                        'categorie_devise':1
                    };

                    $.ajax({
                        type:'POST',
                        dataType:'JSON',
                        url:basePath+'administration/numbers-management/std-get?table=devises&type=all',
                        data:arrayConditions,
                        beforeSend:function()
                        {
                            blockUI('Veuillez patienter...')
                        },
                        success:function(response)
                        {
                            unblockUI();
                            let inputSelect ="";

                            response.forEach(function(elements){
                                inputSelect +="<option value='"+elements.id_devise+"'>"+elements.lib_devise+"</option>";
                            });

                            kt_select_devise_add_number.selectpicker();
                            kt_select_devise_add_number.html(inputSelect);

                            let inputsOther  ='<div class="col-md-12 padding-r-0 padding-l-0 contain_elements"><input type="hidden" id="checkIfTelValid"/>';



                            if(kt_select_devise_add_number.val() !== "")
                            {
                                let currentValue = kt_select_devise_add_number.val();
                                if(currentValue !== "")
                                {
                                    if(parseInt(currentValue) === 1 || parseInt(currentValue) === 3)
                                    {
                                        inputsOther = '<div class="form-group">' +
                                            '<label for="libelle_number">Numéro AdvCash</label>' +
                                            '<input type="email" required placeholder="Ex: johndoe@gmail.com" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                            '<input type="hidden" name="type_input" value="email">' +
                                            '</div>';
                                    }
                                    else
                                    {
                                        if(parseInt(currentValue) === 2 || parseInt(currentValue) ===4)
                                        {
                                            inputsOther = '<div class="form-group" id="contain_element">' +
                                                '<label for="libelle_number">Numéro Payeer</label>' +
                                                '<input type="text" required placeholder="Ex: P124569829" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                                '<input type="hidden" name="type_input" value="text"/> ' +
                                                '<input type="hidden" name="type_devise" value="payeer"/> ' +
                                                '</div>';
                                        }
                                        else
                                        {
                                            if(parseInt(currentValue) === 6)
                                            {
                                                inputsOther = '<div class="form-group">' +
                                                    '<label for="libelle_number">Numéro Perfect Money Euro</label>' +
                                                    '<input type="text" required placeholder="Ex: E124569829" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                                    '<input type="hidden" name="type_input" value="text"/> ' +
                                                    '<input type="hidden" name="type_devise" value="perfect_money_euro"/> ' +
                                                    '</div>';
                                            }
                                            else
                                            {
                                                inputsOther = '<div class="form-group">' +
                                                    '<label for="libelle_number">Numéro Perfect Money USD</label>' +
                                                    '<input type="text" required placeholder="Ex:U852963546" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                                    '<input type="hidden" name="type_input" value="text"/> ' +
                                                    '<input type="hidden" name="type_devise" value="perfect_money_dollar"/> ' +
                                                    '</div>';
                                            }

                                        }
                                    }
                                    inputsOther ="<div class='col-md-12 padding-r-0 padding-l-0 contain_elements'>"+inputsOther+"</div>";

                                    if($('.col-md-12').hasClass('contain_elements'))
                                    {
                                        //alert('ok')
                                        $('.contain_elements').empty();
                                        $('.contain_elements').html(inputsOther);
                                    }
                                    else
                                    {
                                        results_according_type_number_selected.html(inputsOther);
                                    }

                                }


                            }

                            kt_select_devise_add_number.on('change',function(){
                                let currentValue = $(this).val();

                                if(currentValue !== "")
                                {
                                    if(parseInt(currentValue) === 1 || parseInt(currentValue) === 3)
                                    {
                                        inputsOther = '<div class="form-group">' +
                                            '<label for="libelle_number">Numéro AdvCash</label>' +
                                            '<input type="email" required placeholder="Ex: johndoe@gmail.com" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                            '<input type="hidden" name="type_input" value="email">' +
                                            '</div>';
                                    }
                                    else
                                    {
                                       if(parseInt(currentValue) === 2 || parseInt(currentValue) ===4)
                                       {
                                           inputsOther = '<div class="form-group" id="contain_element">' +
                                               '<label for="libelle_number">Numéro Payeer</label>' +
                                               '<input type="text" required placeholder="Ex: P124569829" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                               '<input type="hidden" name="type_input" value="text"/> ' +
                                               '<input type="hidden" name="type_devise" value="payeer"/> ' +
                                               '</div>';
                                       }
                                       else
                                       {
                                           if(parseInt(currentValue) === 6)
                                           {
                                               inputsOther = '<div class="form-group">' +
                                                   '<label for="libelle_number">Numéro Perfect Money Euro</label>' +
                                                   '<input type="text" required placeholder="Ex: E124569829" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                                   '<input type="hidden" name="type_input" value="text"/> ' +
                                                   '<input type="hidden" name="type_devise" value="perfect_money_euro"/> ' +
                                                   '</div>';
                                           }
                                           else
                                           {
                                               inputsOther = '<div class="form-group">' +
                                                   '<label for="libelle_number">Numéro Perfect Money USD</label>' +
                                                   '<input type="text" required placeholder="Ex:U852963546" name="libelle_number" class="form-control" id="libelle_number"/>' +
                                                   '<input type="hidden" name="type_input" value="text"/> ' +
                                                   '<input type="hidden" name="type_devise" value="perfect_money_dollar"/> ' +
                                                   '</div>';
                                           }

                                       }
                                    }
                                    inputsOther ="<div class='col-md-12 padding-r-0 padding-l-0 contain_elements'>"+inputsOther+"</div>";

                                    if($('.col-md-12').hasClass('contain_elements'))
                                    {
                                        //alert('ok')
                                        $('.contain_elements').empty();
                                         $('.contain_elements').html(inputsOther);
                                    }
                                    else
                                    {
                                        results_according_type_number_selected.html(inputsOther);
                                    }

                                }

                            });
                        },
                        error:function()
                        {
                            unblockUI();
                            swal.fire('Error',"Une erreur s'est produite au cours de l'opération","error");
                        }
                    })
            }
            else
            {

                let arrayConditions =
                    {

                        'statut':'1',
                        'categorie_devise':parseInt(current_value)
                    };

                $.ajax({
                    type:'POST',
                    dataType:'JSON',
                    url:basePath+'administration/numbers-management/std-get?table=devises&type=all',
                    data:arrayConditions,
                    beforeSend:function()
                    {
                        blockUI('Veuillez patienter...')
                    },
                    success:function(response)
                    {
                        unblockUI();
                        let inputSelect ="";
                        console.log(response);

                        response.forEach(function(elements){
                            inputSelect +="<option value='"+elements.id_devise+"'>"+elements.lib_devise+"</option>";
                        });


                        kt_select_devise_add_number.html(inputSelect);
                        kt_select_devise_add_number.selectpicker();

                        let inputsOther  ='<div class="col-md-12 padding-r-0 padding-l-0 contain_elements"><input type="hidden" id="checkIfTelValid"/>';



                        if(kt_select_devise_add_number.val() !== "")
                        {
                                let currentValue = kt_select_devise_add_number.val();
                                if(currentValue !== "")
                                {

                                    inputsOther = "<div class='col-md-12 padding-l-0 padding-r-0'>" +
                                    "<div class='form-group'>" +
                                    "<label for='libelle_number'> Numéro Crypto-Monnaie</label>" +
                                    "<textarea rows='3' cols='5' class='form-control' id='libelle_number' name='libelle_number' placeholder='Numero Crypto-Monnaie...'></textarea> " +
                                    "</div>" +
                                    "<input type='hidden' id='checkIfTelValid'/> " +
                                    "<input type='hidden' id='type_input' name='type_input' value='textarea'/> " +
                                    "</div>";


                                }
                                inputsOther ="<div class='col-md-12 padding-r-0 padding-l-0 contain_elements'>"+inputsOther+"</div>";

                                if($('.col-md-12').hasClass('contain_elements'))
                                {
                                    //alert('ok')
                                    $('.contain_elements').empty();
                                    $('.contain_elements').html(inputsOther);
                                }
                                else
                                {
                                    results_according_type_number_selected.append(inputsOther);
                                }




                        }

                        kt_select_devise_add_number.on('change',function(){
                            let currentValue = $(this).val();

                            if(currentValue !== "")
                            {
                                inputsOther = "<div class='col-md-12 padding-l-0 padding-r-0'>" +
                                "<div class='form-group'>" +
                                "<label for='libelle_number'> Numéro Crypto-Monnaie</label>" +
                                "<textarea rows='3' cols='5' class='form-control' id='libelle_number' name='libelle_number' placeholder='Numero Crypto-Monnaie...'></textarea> " +
                                "</div>" +
                                "<input type='hidden' id='checkIfTelValid'/> " +
                                "<input type='hidden' id='type_input' name='type_input' value='textarea'/> " +
                                "</div>";

                                inputsOther ="<div class='col-md-12 padding-r-0 padding-l-0 contain_elements'>"+inputsOther+"</div>";

                                if($('.col-md-12').hasClass('contain_elements'))
                                {
                                    //alert('ok')
                                    $('.contain_elements').empty();
                                    $('.contain_elements').html(inputsOther);
                                }
                                else
                                {
                                    results_according_type_number_selected.append(inputsOther);
                                }

                            }

                        });
                    },
                    error:function()
                    {
                        unblockUI();
                        swal.fire('Error',"Une erreur s'est produite au cours de l'opération","error");
                    }
                })


                results_according_type_number_selected.html(inputs);
            }


        }
        else
        {
            results_according_type_number_selected.empty();
        }


    });



});

function deleteNumber(id_numero)
{
    id_numero = parseInt(id_numero);
    Swal.fire({
        title: 'Supprimer ce numéro ?',
        text: "Voulez-vous supprimer ce numéro ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, confirmer!'
    }).then((result) => {
        if (result.value) {
            $.ajax({

                type:'POST',
                url:basePath+'administration/numbers-management/delete-number',
                dataType:'JSON',
                data:
                    {
                        id_numero:id_numero
                    },
                success:function(response)
                {
                    if(response.success !== undefined)
                    {
                        swal.fire('Terminé',response.success,"success");
                        setTimeout(function(){

                            $("#kt_table_numbers-management").DataTable().destroy();

                            KTDatatablesSearchOptionsAdvancedSearch.init();

                            //document.location.href= location.origin+'/dashboard/administration/numbers-management';

                        },2e2);
                    }
                    else
                    {
                        swal.fire('Oops',response.error,"error");
                    }
                },
                error:function()
                {
                    swal.fire('Erreur',"Une erreur s'est produite avec Ajax !",'error');
                }

            })
        }
    });
}

function updateStatusNumber(id_numero,typeAction)
{
   id_numero = parseInt(id_numero);

    Swal.fire({
        title: 'Mettre à jour le statut ?',
        text: "Voulez-vous mettre à jour le statut ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, confirmer!'
    }).then((result) => {
        if (result.value) {
            $.ajax({

                type:'POST',
                url:basePath+'administration/numbers-management/update-status-number',
                dataType:'JSON',
                data:
                    {
                        id_numero:id_numero,
                        action:typeAction
                    },
                success:function(response)
                {
                    if(response.success !== undefined)
                    {
                        swal.fire('Terminé',response.success,"success");
                        setTimeout(function(){

                            $("#kt_table_numbers-management").DataTable().destroy();

                            KTDatatablesSearchOptionsAdvancedSearch.init();

                            //document.location.href= location.origin+'/dashboard/administration/numbers-management';

                        },2e1);
                    }
                    else
                    {
                        swal.fire('Oops',response.error,"error");
                    }
                },
                error:function()
                {
                    swal.fire('Erreur',"Une erreur s'est produite avec Ajax !",'error');
                }

            })
        }
    });



}


