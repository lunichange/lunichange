"use strict";
var KTDatatableAutoColumnHideDemo = {
    init: function () {
        var t;
        (t = $("#kt_datatable_transactions_bonus").KTDatatable({
            data: { type: "remote", source: { read: {
                url: basePath+"customer/list-transactions-bonus"

            } }, pageSize: 10, saveState: !1, serverPaging: !0, serverFiltering: !0, serverSorting: !0 },
            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },
            columns: [
                { field: "ticket_demande", title: "Ticket de la demande" },
                {
                    field: "moyen_paiement",
                    title: "Moyen de paiement",
                    template: function (t) {
                        return t.lib_devise;
                    },
                },
                { field: "montant", title: "Montant", width: "auto",template:function(t){

                    return '<h2 class="font-weight-bold font-size-1_0">'+t.montant+'&nbsp;&nbsp;&nbsp;<span class="kt-badge text-info font-weight-bold font-size-0_9 kt-badge--square">FCFA</span></h2>'

                    } },
                { field: "date_transaction", title: "Date", type: "date", format: "MM/DD/YYYY" },
                { field: "telephone", title: "Téléphone", width:"auto",template:function(t){

                    return '<button class="btn btn-elevate btn-square btn-primary" type="button"><b>+</b>'+t.telephone+'</button>';
                    }},
                { field: "commentaire", title: "Commentaire", width: "auto" },
                {
                    field: "statut_transaction",
                    title: "Status",
                    template: function (t) {
                        console.log(t);
                        var e = {
                            '0': { title: "En attente", class: "kt-badge--brand" },
                            '1': { title: "Acceptée", class: " kt-badge--success" },
                            '2': { title: "Rejetée", class: " kt-badge--danger" }
                        };
                        return '<span class="kt-badge ' + e[t.statut_transaction].class + ' kt-badge--inline kt-badge--pill">' + e[t.statut_transaction].title + "</span>";
                    },
                }
            ],
        })),
            $("#kt_form_status").on("change", function () {
                t.search($(this).val().toLowerCase(), "Status");
            }),
            $("#kt_form_type").on("change", function () {
                t.search($(this).val().toLowerCase(), "Type");
            }),
            $("#kt_form_status,#kt_form_type").selectpicker();
    },
};
jQuery(document).ready(function () {
    KTDatatableAutoColumnHideDemo.init();
});
