"use strict";
var btn_continue_transaction = $('#btn_continue_transaction'),
    id_transaction_modal  = $('#id_transaction_modal'),
    btn_cancel_transaction   = $('#btn_cancel_transaction');
var KtGeneralTreatment = {
    init: function () {
        let e = $("#currencies_source"),
            t = $("#currencies_destination");
        e.on("change", function () {
            let n = e.val(),
                i = { "id_devise<>?": n, dispo_destination: "1", statut: "1" };
            $.ajax({
                type: "POST",
                url: basePath+"customer/load-devises-according-to-another",
                data: i,
                beforeSend: function () {
                    blockUI("Traitement en cours...");
                },
                success: function (n) {
                    let i = n.categories_devises,
                        r = n.devises;
                    unblockUI();
                    let s = null;
                    i.forEach(function (e) {
                        (s += '<optgroup class="font-size-1_0 font-weight-bold" label="' + e.lib_categorie_devise + '">'),
                            r.forEach(function (t) {
                                parseInt(t.categorie_devise) === parseInt(e.id_categorie_devise) &&
                                (s +=
                                    '<option style="font-size:0.9em !important; font-weight: 400 !important;" value="' +
                                    t.id_devise +
                                    "\" data-content=\"<div class='row margin-0-auto display-flex display-flex-nowrap'><span><img width='60' height='60' class='img_rounded' src='" +
                                    t.image_devise +
                                    "'></span>&nbsp;<span style='padding-left:8px !important; padding-top:20px !important'>" +
                                    t.lib_devise +
                                    '</span></div>"></option>');
                            }),
                            (s += "</optgroup>");
                    }),
                        t.selectpicker("destroy"),
                        t.html(s),
                        t.selectpicker("refresh"),
                        loadInputElements(parseInt(e.val()), parseInt(t.val()));
                },
                error: function (e) {
                    404 !== e.statut && swal.fire("Erreur", "Une erreur s'est produite. Veuillez vérifier votre connexion internet.", "error");
                },
            });
        }),
            t.on("change", function () {
                let t = parseInt($(this).val());
                loadInputElements(parseInt(e.val()), t);
            });
    },
};
function loadInputElements(e, t) {
    let n = $("#kt-portlet_results");
    $.ajax({
        type: "POST",
        url: basePath+"customer/load-elements-inputs",
        dataType: "JSON",
        data: { source: e, destination: t },
        beforeSend: function () {},
        success: function (e) {
            if (void 0 !== e.error) Swal.fire({ title: "Oops", text: e.error, imageUrl: location.origin + "/img/vector_transaction.jpg", imageWidth: 300, imageHeight: 300, icon: "warning" }), n.hasClass("hide") || n.addClass("hide");
            else {
                n.hasClass("hide") && n.removeClass("hide");
                let t = $("#container_results"),
                    i = 0,
                    r = e.results,
                    s = '<form class="kt-form kt-form--label-right width-100" id="kt-form_make_transaction" method="post"><div class="kt-portlet__body">';
                for (let e in r)
                    if ("hidden_source" !== e && "hidden_destination" !== e && "hidden_type_action" !== e)
                        if ("what_it_receives" === e || "what_it_gives" === e)
                            s +=
                                '<div class="form-group"><label for="' +
                                r[e].placeholder_label +
                                '" class="' +
                                r[e].label_class +
                                '">' +
                                r[e].placeholder_label +
                                '</label><div class="input-group input-group-md"><div class="input-group-prepend"> <span class="input-group-text"><i class="flaticon-coins"></i></span> <span class="input-group-text">0.00</span></div><input step="any" type="' +
                                r[e].type +
                                '" maxlength="12" id="' +
                                r[e].id +
                                '" name="' +
                                r[e].name +
                                '" placeholder="' +
                                r[e].placeholder_label +
                                '" required="' +
                                r[e].required +
                                '" min="' +
                                r[e].min +
                                '" class="' +
                                r[e].class +
                                '" /><div class="col-md-12" id="error_div_' +
                                r[e].id +
                                '"></div></div></div>';
                        else if ("type_input" === e) s += '<div class="form-group"><input type="hidden" id="type_input" name="type_input"value="' + r[e] + '"/></div>';
                        else {
                            let t,
                                n = "",
                                o = "",
                                a = null;
                            (t = void 0 !== r[e].pattern ? r[e].pattern : ""),
                                void 0 !== r[e].data_country_code
                                    ? ((n = r[e].data_country_code),
                                        (o =
                                            '<div class="col-md-12"><div id="valid-msg" class="hide col-md-12">✓ Correcte</div>\n<div id="error-msg" class="hide col-md-12"></div></div><div class="col-md-12"><div id="valid-msg-confirm" class="hide col-md-12">✓ Correcte</div><div id="error-msg-confirm" class="hide col-md-12"></div></div>'))
                                    : void 0 !== r[e].min && ((a = r[e].min), (i = r[e].min));
                            let l = "";
                            null !== a && (l = "min='" + a + "'"),
                                (s +=
                                    '<div class="form-group"><label for="' +
                                    r[e].placeholder_label +
                                    '" class="' +
                                    r[e].label_class +
                                    '">' +
                                    r[e].placeholder_label +
                                    '</label><input type="' +
                                    r[e].type +
                                    '" id="' +
                                    r[e].id +
                                    '" name="' +
                                    r[e].name +
                                    '" data-country-code="' +
                                    n +
                                    '" placeholder="' +
                                    r[e].placeholder_label +
                                    '" required="' +
                                    r[e].required +
                                    '" ' +
                                    l +
                                    ' class=" ' +
                                    r[e].class +
                                    '" />' +
                                    o +
                                    "</div>");
                        }
                    else s += '<input name="' + r[e].name + '" value="' + r[e].value + '" type="' + r[e].type + '"/>';
                (s +=
                    '</div><div class="kt-portlet__foot">\n                    <div class="kt-form__actions">\n                        <button type="submit" id="kt-btn-transaction" class="btn btn-primary">Soumettre</button>\n                        <button type="reset" id="kt-btn-cancel-transaction" class="btn btn-secondary">Annuler</button>\n                    </div>\n                </div></form>'),
                    t.html(s),
                    disabledScroll("currency_gave"),
                    disabledScroll("currency_receive"),
                    treatInputs();
                let o = $("#currency_gave"),
                    a = $("#currency_receive");
                "tel" === o.attr("type") && "tel" === a.attr("type") && (disabledScroll("currency_gave"), disabledScroll("currency_receive"));
                let l = $("#address_receive");
                if ("tel" === l.attr("type")) {
                    let e = l.attr("data-country-code"),
                        t = null;
                    e.length > 2 ? ((t = e.split(",")), (e = t)) : (e = [e]);
                    let n = document.querySelector("#address_receive"),
                        i = document.querySelector("#confirm_address_receive"),
                        r = document.querySelector("#error-msg"),
                        s = (document.querySelector("#error-msg-confirm"), document.querySelector("#valid-msg")),
                        o = (document.querySelector("#valid-msg-confirm"), ["Numéro de téléphone invalide", "Code du pays invalide", "Numéro de téléphone trop court", "Numéro de téléphone trop long", "Numéro invalide"]),
                        a = window.intlTelInput(n, { onlyCountries: e, separateDialCode: !0, utilsScript: location.origin + "/assets/plugins/intl-tel-input/build/js/utils.js" }),
                        d = (window.intlTelInput(i, { onlyCountries: e, separateDialCode: !0, utilsScript: location.origin + "/assets/plugins/intl-tel-input/build/js/utils.js" }), null),
                        c = null;
                    $("#kt-btn-transaction").click(function (e) {
                        e.preventDefault();
                        var t = $(this),
                            i = $(this).closest("form");
                        let l = !1,
                            u = null;
                        if (n.value.trim())
                            if (a.isValidNumber()) (l = !0), s.classList.remove("hide"), (d = a.selectedCountryData.iso2), (c = a.selectedCountryData.dialCode);
                            else {
                                n.classList.add("error");
                                var p = a.getValidationError();
                                void 0 === o[p] ? ((r.innerHTML = "Numéro de téléphone invalide"), (u = "Numéro de téléphone invalide")) : ((r.innerHTML = o[p]), (u = o[p])), (l = !1), r.classList.remove("hide");
                            }
                        i.validate({ rules: { currency_gave: { required: !0, min: 0 }, currency_receive: { required: !0 }, address_receive: { required: !0 }, confirm_receive: { required: !0 } } }),
                            i.valid(),
                            $("#address_receive").val().trim() !== $("#confirm_address_receive").val().trim()
                                ? swal.fire("Erreur", "Les adresses de réception doivent-être identiques", "error")
                                : i.valid() && !0 === l
                                ? (t.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                                    i.ajaxSubmit({
                                        url: basePath+"customer/make-transaction?dialCode=" + c,
                                        type: "POST",
                                        dataType: "JSON",
                                        success: function (e) {
                                            if (
                                                ($("#kt-btn-transaction").removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1),
                                                    $("#kt-btn-cancel-transaction").attr("disabled", !1),
                                                void 0 !== e.error)
                                            )
                                                swal.fire("Erreur", e.error, "error");
                                            else if (void 0 !== e.success) {
                                                KTApp.block("#kt-portlet_content_conversion", { overlayColor: "#000000", type: "v2", state: "success", message: "" }),
                                                    KTApp.block("#kt-portlet_results", { overlayColor: "#000000", type: "v2", state: "success", message: "" });
                                                let t = e.success,
                                                    n = $("#container_info_resume"),
                                                    i = "";
                                                void 0 !== t.nom_numero && "" !== t.nom_numero && null !== t.nom_numero && (i = " | Nom enregistré sur le numéro: " + t.nom_numero);


                                                let r =
                                                    ' <div class="modal-header">\n                <h4 class="modal-title kt-font-size-1_2-desktop" id="exampleModalLabel">RESUME DE LA TRANSACTION</h4>\n    </div>\n            <div class="modal-body">\n                <div class="table-responsive">\n\n                    <table class="table dashboard-task-infos">\n                        <thead>\n                            <tr>\n                                <th>Opération :</th>\n                                <th class="text-right"><div class="col-md-12 kt-padding-0">' +
                                                    t.devise_source.lib_devise +
                                                    '</div> <img class="img_rounded" style="width:40px;display:inline;" src="' +
                                                    t.devise_source.image_devise +
                                                    '"></th>                                <th>                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n        <polygon points="0 0 24 0 24 24 0 24"/>\n        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"/>\n        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>\n    </g>\n</svg>                                </th>                                <th><div class="col-md-12 kt-padding-0">' +
                                                    t.devise_destination.lib_devise +
                                                    '</div>                                    <img class="img_rounded" style="width:40px;display:inline;" src="' +
                                                    t.devise_destination.image_devise +
                                                    '">&nbsp;                                </th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr>\n                                <td style="font-weight: bold">Montant :</td>\n                                <td class="text-right">' +
                                                    t.quantite_a_convertir +
                                                    " (" +
                                                    t.monnaie_source +
                                                    ')</td>                                <td style="font-weight: bold">                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n        <polygon points="0 0 24 0 24 24 0 24"/>\n        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"/>\n        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>\n    </g>\n</svg>                                </td>                                <td>' +
                                                    t.quantite_a_recevoir +
                                                    " (" +
                                                    t.monnaie_destination +
                                                    ')</td>\n                            </tr>\n                            <tr>\n                                <td style="font-weight: bold">Adresse de réception : </td>\n                                <td style="font-weight: bold" class="text-center" colspan="3">' +
                                                    t.adresse_reception +
                                                    '</td>                            </tr>\n                        </tbody>\n\n                        <tfoot>\n                            <tr style="width: 100% !important">\n\n                           </tr>\n                        </tfoot>\n                    </table>\n                </div>\n            </div>\n            <div class="modal-footer">\n                <button type="button" class="btn btn-secondary" id="btn_update_form" data-dismiss="modal"><i class="flaticon-edit"></i>&nbsp;Modifier </button>\n  ' +
                                                    '              <button type="button" id="btn_continue_transaction" class="btn btn-primary">Cliquer pour continuer&nbsp;<i class="flaticon2-right-arrow"></i> </button>\n            </div>';
                                                n.html(r),
                                                    $("#btn_update_form").click(function () {
                                                        activeContainer("kt-portlet_content_conversion"), activeContainer("kt-portlet_results");
                                                    }),
                                                    console.log(t),
                                                    setTimeout(function () {
                                                        $("#kt_modal_resume_transaction").modal({ show: !0, backdrop: "static" });
                                                    }, 2),
                                                    ContinueTransaction(t.devise_source['code_devise'],t.numero_envoi,t.nom_numero);


                                            }
                                        },
                                        error: function (e) {
                                            404 !== e.statut &&
                                            (setTimeout(function () {
                                                t.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                            }, 2e3),
                                                swal.fire("Erreur", "Une erreur s'est produite lors de l'opération", "error"));
                                        },
                                    }))
                                : swal.fire("Erreur", "Une erreur s'est produite, veuillez vérifier toutes vos informations", "error");
                    });
                } else
                    $("#kt-btn-transaction").click(function (e) {
                        e.preventDefault();
                        var t = $(this),
                            n = $(this).closest("form");
                        n.validate({ rules: { currency_gave: { required: !0, min: 0 }, currency_receive: { required: !0 }, address_receive: { required: !0 }, confirm_receive: { required: !0 } } }),
                            n.valid(),
                            $("#address_receive").val().trim() !== $("#confirm_address_receive").val().trim()
                                ? swal.fire("Erreur", "Les adresses de réception doivent-être identiques", "error")
                                : n.valid()
                                ? (t.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                                    $("#kt-btn-cancel-transaction").addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                                    n.ajaxSubmit({
                                        url: basePath+"customer/make-transaction",
                                        type: "POST",
                                        dataType: "JSON",
                                        success: function (e) {
                                            if (
                                                ($("#kt-btn-transaction").removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1),
                                                    $("#kt-btn-cancel-transaction").attr("disabled", !1),
                                                void 0 !== e.error)
                                            )
                                                swal.fire("Erreur", e.error, "error");
                                            else if (void 0 !== e.success) {
                                                disabledContainer("kt-portlet_content_conversion");
                                                let t = e.success,
                                                    n = $("#container_info_resume"),
                                                    i = null,
                                                    r = "";
                                                void 0 !== t.nom_numero && "" !== t.nom_numero && null !== t.nom_numero && (r = " | Nom enregistré sur le numéro: " + t.nom_numero),

                                                    (i =
                                                        ' <div class="modal-header">\n                <h4 class="modal-title kt-font-size-1_2-desktop" id="exampleModalLabel">RESUME DE LA TRANSACTION</h4>\n    </div>\n            <div class="modal-body">\n                <div class="table-responsive">                    <table class="table dashboard-task-infos">\n                        <thead>                            <tr>                                <th>Opération :</th>                                <th class="text-left kt-font-size-1_0-desktop"><div class="col-md-12 kt-padding-0">' +
                                                        Substring(t.devise_source.lib_devise) +
                                                        '</div> <img class="img_rounded" style="width:40px;display:inline;" src="' +
                                                        t.devise_source.image_devise +
                                                        '"></th>                                <th>                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n        <polygon points="0 0 24 0 24 24 0 24"/>\n        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"/>\n        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>\n    </g>\n</svg>                                </th>                                <th class="text-right kt-font-size-1_0-desktop"><div class="col-md-12 kt-padding-0">' +
                                                        Substring(t.devise_destination.lib_devise) +
                                                        '</div>                                    <img class="img_rounded" style="width:40px;display:inline;" src="' +
                                                        t.devise_destination.image_devise +
                                                        '">&nbsp;&nbsp;                                </th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr>\n                                <td style="font-weight: bold">Montant :</td>\n                                <td class="text-right">' +
                                                        t.quantite_a_convertir +
                                                        " (" +
                                                        t.monnaie_source +
                                                        ')</td>                                <td style="font-weight: bold">                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n        <polygon points="0 0 24 0 24 24 0 24"/>\n        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"/>\n        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>\n    </g>\n</svg>                                </td>                                <td>' +
                                                        t.quantite_a_recevoir +
                                                        " (" +
                                                        t.monnaie_destination +
                                                        ')</td>\n                            </tr>\n                            <tr>\n                                <td style="font-weight: bold">Adresse de réception : </td>\n                                <td style="font-weight: bold" class="text-center" colspan="3">' +
                                                        t.adresse_reception +
                                                        '</td>                            </tr>\n                        </tbody>\n\n                        <tfoot>\n                            <tr style="width: 100% !important">\n\n\n\n                            </tr>\n                        </tfoot>\n                    </table>\n                </div>\n            </div>\n            <div class="modal-footer">\n                <button type="button" class="btn btn-secondary" id="btn_update_form" data-dismiss="modal"><i class="flaticon-edit"></i>&nbsp;Modifier </button>\n                <button type="button" id="btn_continue_transaction" class="btn btn-primary">Cliquer pour continuer&nbsp;<i class="flaticon2-right-arrow"></i> </button>\n            </div>'),
                                                    n.html(i),
                                                    $("#btn_update_form").click(function () {
                                                        activeContainer("kt-portlet_content_conversion"), activeContainer("kt-portlet_results");
                                                    }),
                                                    console.log(t),
                                                    setTimeout(function () {
                                                        $("#kt_modal_resume_transaction").modal({ show: !0, backdrop: "static" });
                                                    }, 2),
                                                    ContinueTransaction(t.devise_source['code_devise'],t.numero_envoi,t.nom_numero);

                                            }
                                        },
                                        error: function (e) {
                                            404 !== e.statut &&
                                            (setTimeout(function () {
                                                t.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1), $("#kt-btn-cancel-transaction").attr("disabled", !1);
                                            }, 2e3),
                                                swal.fire("Erreur", "Une erreur s'est produite lors de l'opération", "error"));
                                        },
                                    }))
                                : swal.fire("Erreur", "Une erreur s'est produite, veuillez vérifier toutes vos informations", "error");
                    });
            }
        },
        error: function (e) {
            404 !== e.statut && swal.fire("Erreur", "Une erreur s'est produite. Veuillez vérifier votre connexion internet.", "error");
        },
    });
}
function disabledScroll(e) {
    document.getElementById(e).addEventListener("mousewheel", function (e) {
        this.blur();
    });
}
function activeContainer(e) {
    KTApp.unblock("#" + e);
}
function disabledContainer(e) {
    KTApp.block("#" + e, { overlayColor: "#000000", type: "v2", state: "success", message: "" });
}
function Substring(e) {
    let t = e;
    return t.length >= 11 && (t = t.substring(0, 8) + "..."), t;
}
function ContinueTransaction(code_devise = null,numero_envoi = null,nom_numero=null) {
    let e = $("#btn_continue_transaction"),
        t = $("#id_transaction_modal");

    let f = $('#btn_previous');
    /*
        $("#kt-btn-cancel-transaction").addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
     */
    let nextOfTransac = $('#kt_modal_next_of_transaction');
    e.click(function () {
        /*$("#kt_modal_resume_transaction").modal("hide"),
            setTimeout(function () {
                t.modal({ show: !0, backdrop: "static" });
            }, 2);*/
        $("#kt_modal_resume_transaction").modal("hide");

        setTimeout(function(){
            nextOfTransac.modal({show: !0, backdrop: 'static'});
        },400);
        let html;

        if(code_devise === 'MTN-BJ')
        {
            $('#title_next_of_transaction').empty();
            $('#title_next_of_transaction').html('Informations de dépôt');
            html = '<div class="col-md-12" style="text-align: center !important">' +

                        '<h4>TAPER CE CODE POUR LE PAYEMENT</h4>' +
                        '<p style="color:#000000 !important; font-size: 1.5em !important"><b>*400*46*721223*montant#</b></p>' +
                        '<p class="font-size-1_4 bg-dark color-white"><small></small></p>' +
                        '<p class="font-size-1_2" style="color: red !important">' +
                        '<b class="text-decoration-underline;">NB:</b> Veuillez copier le code' +
                        '</p>' +
                   '</div>';
        }
        else
        {
            let name='';
            let titleH4;
            let Nb = '';
            if(nom_numero !== null)
            {
                name = ' | Nom enregistré sur le numéro :'+nom_numero;
                $('#title_next_of_transaction').empty();
                $('#title_next_of_transaction').html('Informations de dépôt');
                titleH4 = 'ADRESSE DE DEPOT';
            }
            else
            {
                $('#title_next_of_transaction').empty();
                $('#title_next_of_transaction').html('Informations de retrait');
                titleH4 = 'ADRESSE DE RETRAIT';
                Nb = '<p class="font-size-1_2" style="color: red !important">' +
                    '<b class="text-decoration-underline;">NB:</b> Veuillez copier l\'adresse dans votre portefeuille pour l\'envoi des sous' +
                    '</p>' ;
            }
                html = '<div class="col-md-12" style="text-align: center !important">' +

                    '<h4>'+titleH4+'</h4>' +
                    '<p style="color:#000000 !important; font-size: 1.3em !important"><b class="text-decoration-underline">'+numero_envoi+'</b>'+name+'</p>' +
                    ''+Nb+
                    '</div>';
        }

        $('#kt_modal_next_of_transaction .modal-body').html(html);

        $('#btn_next_ot_transaction_previous').click(function(){

            nextOfTransac.modal('hide');

            $("#kt_modal_resume_transaction").modal({
                show: !0, backdrop:'static'
            });

        });

        $('#kt-btn_next_ot_transaction_purchase_transaction').click(function(){


            nextOfTransac.modal('hide');

            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+"customer/save-temp-transactions",
                data:
                    {
                        save_transaction:true
                    },
                beforeSend:function()
                {
                    e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !0);
                    f.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !0);
                },
                success:function(response)
                {
                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !1);
                    f.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !1);

                    //console.log(response);
                    if(response.success !== undefined)
                    {
                        toastr.success(response.success,'Terminé!' ,{timeOut: 15000});

                        if(response.needId !== undefined)
                        {
                            if(response.needId === "yes")
                            {
                                setTimeout(function () {
                                    t.modal({ show: !0, backdrop: "static" });
                                }, 2);
                            }
                            else if(response.needId === "no")
                            {
                                swal.fire({
                                    title: 'Vérification ID de transaction',
                                    text: "Avez-vous un ID de transaction ? \n" +
                                        "Si vous en avez veillez l'indiquer. Cela peut-être utile en cas de réclamation. Dans le contraire un ID de transaction vous sera fourni par la plateforme.",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    cancelButtonText:'Non',
                                    confirmButtonText: 'Oui'
                                }).then((result) => {
                                    if (result.value) {
                                        t.modal({ show: !0, backdrop: "static" });
                                    }
                                    else
                                    {
                                        $.ajax({

                                            type:'POST',
                                            url:basePath+"customer/another-finish-transaction",
                                            dataType:'JSON',
                                            beforeSend:function()
                                            {
                                                blockUI('Traitement en cours...');
                                            },
                                            success:function(response)
                                            {

                                                if(response.error !== undefined)
                                                {
                                                    swal.fire("Oops",response.error,"error");

                                                    unblockUI();

                                                }
                                                else if(response.success !== undefined)
                                                {


                                                    toastr.success(response.success,'Terminé');
                                                    setTimeout(function(){
                                                        document.location.href= basePath+'/transactions';
                                                    },2e3)
                                                }

                                            },
                                            error:function(state)
                                            {
                                                if(state.statut === 404)
                                                {

                                                }
                                            }

                                        })
                                    }
                                });
                            }
                        }

                    }
                    else if(response.error !== undefined)
                    {
                        toastr.error(response.error,'Erreur');
                    }
                },
                error:function(state)
                {
                    //console.log(state);
                    //swal.fire('Erreur',"Une erreur s'est produite lors du traitement")
                }
            })


        });



    });
        f.click(function () {
            activeContainer("kt-portlet_content_conversion"),activeContainer("kt-portlet_results"), t.modal("hide");
        });
}

function checkIfTransactionInProgress()
{
    let returnValue = false;
    window.sessionStorage;

    //sessionStorage.setItem('TransactionInProgress',returnValue);

    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:basePath+"customer/check-if-transaction-in-progress",
        data:
            {
                returnValue:returnValue
            },
        success:function(response)
        {

            if(response.result !== undefined)
            {

                if(parseBoolean(response.result) === false)
                {
                    KtGeneralTreatment.init();
                }
                else
                {
                    /*toastr.options.timeOut = 0;
                    toastr.options.extendedTimeOut = 0;
                    toastr.info("<p class='font-size-1_1 font-weight-400'>Vous devez finaliser cette transaction avant de pouvoir convertir à nouveau, merci.</p>",'<h4 class="text-white">Message</h4>',"warning");
                    */
                    swal.fire({
                        title: 'IMPORTANT !!!',
                        text: "Vous avez actuellement une transaction en cours. Vous devez finaliser cette transaction avant de pouvoir convertir à nouveau, merci.",
                        type: 'info'
                    });

                }

            }
        },
        error:function(state)
        {
            if(state.status === 404)
            {
                swal.fire('Oops',"Page ".state.statusText, 'error');
            }
            else
            {
                swal.fire("Oops","Une erreur s'est produite au cours du traitement","error");
            }
        }
    });

    returnValue = sessionStorage.getItem('TransactionInProgress');

    return returnValue;
}
function parseBoolean (string){
    switch(string){
        case "true": case "yes": case "1": case 1: return true;
        case "false": case "no": case "0": case 0: case null: return false;
        default: return Boolean(string);
    }
}
jQuery(document).ready(function () {
    checkIfTransactionInProgress();

    let e = $("#currencies_source");
    screen.width < 992 ? e.attr("data-dropup-auto", "auto") : e.attr("data-dropup-auto", "false");
});

function finishTransaction(state)
{
    let stateValue = parseBoolean(state);




        if(stateValue == false)
        {
            swal.fire({
                title: 'Transaction en cours',
                text: "Voulez-vous annuler la transaction ? \n" +
                    "Cette opération est irréversible.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText:'Non',
                confirmButtonText: 'Oui'
            }).then((result) => {
                if (result.value) {

                    subFinishTransaction(stateValue);

                }

            });

        }
        else
        {
            subFinishTransaction(stateValue)
        }

}

function subFinishTransaction(stateValue)
{
    $.ajax({
        type:'POST',
        url:basePath+'customer/continue-transaction',
        dataType:'JSON',
        data:
            {
                continueTransaction:stateValue
            },
        beforeSend:function()
        {
            btn_continue_transaction.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !0);
            btn_cancel_transaction.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !0);
            blockUI('Veuillez patienter...');
        },
        success:function(response)
        {
            unblockUI();
            btn_continue_transaction.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !1);
            btn_cancel_transaction.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr('disabled', !1);
            if(response.success !== undefined)
            {
                switch (response.success) {
                    case 'get_transaction_id':

                        id_transaction_modal.modal({ show: !0, backdrop: "static" });

                        break;
                    case 'not_get_transaction_id':

                        swal.fire({
                            title: 'Vérification ID de transaction',
                            text: "Avez-vous un ID de transaction ? \n" +
                                "Si vous en avez veillez l'indiquer. Cela peut-être utile en cas de réclamation. Dans le contraire un ID de transaction vous sera fourni par la plateforme.",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            cancelButtonText:'Non',
                            confirmButtonText: 'Oui'
                        }).then((result) => {
                            if (result.value) {

                                id_transaction_modal.modal({ show: !0, backdrop: "static" });

                            }
                            else
                            {
                                $.ajax({

                                    type:'POST',
                                    url:basePath+"customer/another-finish-transaction",
                                    dataType:'JSON',
                                    beforeSend:function()
                                    {
                                        blockUI('Traitement en cours...')
                                    },
                                    success:function(response)
                                    {
                                        unblockUI();

                                        if(response.error!== undefined)
                                        {
                                            toastr.error(response.error,'')
                                        }
                                        else if(response.success !== undefined)
                                        {
                                            toastr.success(response.success,'');
                                            setTimeout(function(){
                                                document.location.href= basePath+'transactions';
                                            },2e1)
                                        }
                                    },
                                    error:function(state)
                                    {
                                        unblockUI();
                                        if(state.statut === 404)
                                        {
                                            swal.fire('Oops',"Page non trouvée",'error');
                                        }
                                        else
                                        {
                                            swal.fire('Oops',"Une erreur inattendue s'est produite",'error');
                                        }
                                    }

                                })
                            }
                        });

                        break;
                    case 'success':

                        custom_std('success',response.return,'fire');
                        setTimeout(function(){
                            location.reload();
                        },2e2);
                        break;
                    default:
                        return 0;

                }
            }
            else if(response.error !== undefined)
            {
                swal.fire('Oops',response.error,'error');
            }
        },
        error:function(state)
        {

        }
    })
}