

function treatInputs()
{
    let currency_gave = $('#currency_gave');
    let currency_receive = $('#currency_receive');


    currency_gave.keyup(function(){

        let current_value = $(this).val();

        if(current_value.trim() !== "")
        {
            if(CheckDecimal(currency_gave) === true)
            {
                $('#error_div_currency_gave').empty();
                $.ajax({
                    type:'POST',
                    url:basePath+'customer/treat-inputs',
                    dataType:'JSON',
                    data:
                        {
                            'input_source':current_value
                        },
                    success:function(response)
                    {
                        if(response['error'] !== undefined)
                        {
                            swal.fire('Erreur',response['error'],'error')
                        }
                        else
                        {
                              currency_receive.val(response['results']);
                            //currency_receive.attr('disabled',!0);
                        }
                    },
                    error:function()
                    {
                        swal.fire("Erreur","Une erreur s'est produite avec Ajax !","error");
                    }
                });

            }
            else
            {
                $('#error_div_currency_gave').html('<b style="color: red !important"> Format invalide de la quantité à convertir</b>');
            }
        }
        else
        {
            currency_receive.val(0);
            swal.fire('Erreur',"Veuillez renseigner le montant à convertir.",'error');
        }



    });

    treatInputsByDestination();
}

function treatInputsByDestination()
{
    let currency_gave = $('#currency_gave');
    let currency_receive = $('#currency_receive');

    currency_receive.keyup(function(){
        let current_value = $(this).val();
        if(current_value.trim() !== "")
        {
            if(CheckDecimal(currency_receive) === true)
            {
                $('#error_div_currency_receive').empty();
                $.ajax({
                    type:'POST',
                    url:basePath+'customer/treat-inputs',
                    dataType:'JSON',
                    data:
                        {
                            'input_destination':current_value
                        },
                    success:function(response)
                    {
                       
                        if(response['error'] !== undefined)
                        {
                            swal.fire('Erreur',response['error'],'error')
                        }
                        else
                        {
                            currency_gave.val(response['results']);
                            
                        }
                    },
                    error:function()
                    {
                        swal.fire("Erreur","Une erreur s'est produite avec Ajax !","error");
                    }
                });

            }
            else
            {
                $('#error_div_currency_receive').html('<b style="color: red !important"> Format invalide de la quantité à convertir</b>');
            }
        }
        else
        {
            currency_gave.val(0);
            swal.fire('Erreur',"Veuillez renseigner le montant à convertir.",'error');
        }

    });
}

function CheckDecimal(inputtxt)
{
    var decimal=  /^[0-9]+[.][0-9]*$/;
    if(inputtxt.val().match(decimal) || Number.isInteger(parseInt(inputtxt.val())))
    {
        //alert('Correct, try another...')
        return true;
    }
    else
    {

        return false;
    }
}
function number_test(n)
{
    var result = (n - Math.floor(n)) !== 0;

    if (result)
        return true;
    else

         if(Number.isInteger(parseInt(n)))
         {
             return true;
         }
         else
         {
             return false
         }
}
$(document).ready(function(){


    $("#kt-btn_purchase_transaction").click(function(i) {
        i.preventDefault();
        var e = $(this),
            n = $(this).closest("form");

        n.validate({
            rules: {
                text: {
                    required: !0,
                    minLength: 12
                }
            }
        }),n.valid();

            if(n.valid())
            {

                (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                        $('#btn_previous').attr('disabled',!0),


                        n.ajaxSubmit({
                            url: basePath+"customer/finish-transaction",
                            type:'POST',
                            dataType:'JSON',
                            success: function(response)
                            {


                                if(response.error !== undefined)
                                {
                                    toastr.error(response.error);

                                    $("#kt-btn_purchase_transaction").removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                    $('#btn_previous').attr('disabled',!1);

                                }
                                else if(response.success !== undefined)
                                {


                                    toastr.success('Opération effectué avec succès !');
                                    setTimeout(function(){
                                        document.location.href= basePath+'transactions';
                                    },2e3)
                                }

                            },
                            error:function()
                            {
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                    $('#kt-btn_purchase_transaction').attr('disabled', !1);
                                }, 2e3)
                                toastr.error("Une erreur s'est produite au cours du traitement !");
                            }
                        })
                )
            }
            else
            {
                swal.fire("Erreur","Une erreur s'est produite, veuillez vérifier toutes vos informations","error");
            }




    });



});