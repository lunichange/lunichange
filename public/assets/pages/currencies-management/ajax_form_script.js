var KTFormControls= {
        init:function() {

                $("#kt_devise_add_form").validate( {
                        rules: {
                            categorie_devise: {
                                required: !0
                            }
                            , libelle_devise: {
                                required: !0, minlength: 2
                            }
                            , solde_minimum: {
                                required: !0, minlength:2, min:0
                            }
                            , solde: {
                                required: !0, minlength:2, min:0
                            }
                            , famille_devise: {
                                required: !0,
                            }
                            , fileToUpload: {
                                required: !0
                            }

                        }
                        , invalidHandler:function(e, r) {
                            swal.fire( {
                                    title:"", text:"Il y a quelques erreurs dans votre soumission. Veuillez les corriger.", type:"error", confirmButtonClass:"btn btn-secondary", onClose:function(e) {

                                    }
                                }
                            ), e.preventDefault()
                        }
                        , submitHandler:function(e) {

                          //  e.preventDefault();


                            $.ajax({
                                type:'POST',
                                url:basePath+'administration/currencies-management/add-currencies',
                                data:new FormData(e) ,
                                success:function(response)
                                {
                                    //console.log(response);
                                   /* swal.fire( {
                                            title: "", text: "Form validation passed. All good!", type: "success", confirmButtonClass: "btn btn-secondary"
                                        }
                                    )*/
                                },
                                error:function()
                                {
                                    swal.fire('Erreur',"Une erreur s'est produite lors du traitement","erreur");
                                }

                            })
                        }
                    }
                )

        }
    }

;

var KTFormUpdateDeviseControls = {

                init:function ()
                {
                    $('#kt_devise_update_form').validate({
                            rules:
                                {
                                    edit_categorie_devise:
                                        {
                                            required: !0,
                                            number: !0,
                                            maxlength:2
                                        },
                                    edit_libelle_devise:
                                        {
                                            required: !0,
                                            minlength:2

                                        },
                                    edit_solde_minimum:
                                        {
                                            required:!0,
                                            number:!0,
                                            min:0,


                                        },
                                    edit_solde:
                                        {
                                            required:!0,
                                            number:!0,
                                            min:0,
                                            //toBeGreaterThan:$('input[name="edit_solde_minimum"]').val()
                                        },
                                    edit_famille_devise:
                                        {
                                            required:!0,
                                            number:!0,
                                            maxlength: 2

                                        }

                                },
                                invalidHandler:function(e, r) {
                                    swal.fire( {
                                            title:"", text:"Il y a quelques erreurs dans votre soumission. Veuillez les corriger.", type:"error", confirmButtonClass:"btn btn-secondary", onClose:function(e) {

                                            }
                                        }
                                    ), e.preventDefault()
                                },
                                submitHandler:function(e) {
                                    let btn_submit_update_devise_form = $('#btn_submit_update_devise_form');
                                    let btn_cancel_update_devise_form = $('#btn_cancel_update_devise_form');
                                    let modal_for_update_devise = $('#kt_modal_update_devise');


                                    btn_submit_update_devise_form.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),

                                        btn_cancel_update_devise_form.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),


                                        $.ajax({
                                            type:'POST',
                                            url:basePath+'administration/currencies-management/update-currencies',
                                            dataType:'JSON',
                                            data:$('#kt_devise_update_form').serialize(),
                                            success:function(response)
                                            {
                                                btn_submit_update_devise_form.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                                btn_cancel_update_devise_form.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);

                                                if(response.error !== undefined)
                                                {

                                                    swal.fire('Erreur',response.error,'error');
                                                }
                                                else if(response.success !== undefined)
                                                {
                                                    swal.fire('Terminé',response.success,'success');

                                                    /*$("#kt_table_currencies").DataTable({
                                                        'destroy':true
                                                    });*/
                                                    $("#kt_table_currencies").DataTable().destroy();

                                                    setTimeout(function()
                                                    {
                                                        document.location.href="";


                                                    },2e3);

                                                }
                                            },
                                            error:function()
                                            {
                                                swal.fire('Erreur',"Une erreur s'est produite au cours du traitement. Veuillez vérifier votre connexion internet","error");
                                            }
                                        });
                        }

                    })
                },

}
jQuery(document).ready(function() {
        KTFormControls.init()
        KTFormUpdateDeviseControls.init();

        $('#btn_update_logo').click(function(){
           // e.preventDefault();

       //    alert('ok')




                        let btn_submit_update_logo_form = $('#btn_update_logo');
                        let btn_cancel_update_logo_form = $('#btn_disable_update_logo');
                        let modal_for_update_logo_devise = $('#kt_modal_update_logo_devise');

                        let id_devise = $("#hidden_id_devise_for_logo_update").val();



                        var formData = new FormData();
                        formData.append('file', $('#logo_devise')[0].files[0]);
                        $.ajax({
                            type:'POST',
                            url:basePath+'administration/currencies-management/update-logo?id_devise='+id_devise,
                            dataType:'JSON',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data:formData,
                            beforeSend:function()
                            {
                                btn_submit_update_logo_form.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                                btn_cancel_update_logo_form.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0)

                            },
                            success:function(response)
                            {
                                console.log(response);
                                btn_submit_update_logo_form.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                btn_cancel_update_logo_form.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);

                                if(response.error !== undefined)
                                {

                                    swal.fire('Erreur',response.error,'error');
                                }
                                else if(response.success !== undefined)
                                {
                                    swal.fire('Terminé',response.success,'success');

                                    /*$("#kt_table_currencies").DataTable({
                                        'destroy':true
                                    });*/
                                    $("#kt_table_currencies").DataTable().destroy();

                                    setTimeout(function()
                                    {
                                        KTDatatablesSearchOptionsAdvancedSearch.init();

                                        $('#kt_modal_update_logo_devise').modal('hide');

                                    },2e0);

                                }
                            },
                            error:function()
                            {
                                btn_submit_update_logo_form.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                btn_cancel_update_logo_form.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);

                                swal.fire('Erreur',"Une erreur s'est produite au cours du traitement. Veuillez vérifier votre connexion internet","error");
                            }
                        });




        })

    }

);