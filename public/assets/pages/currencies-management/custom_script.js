$(document).ready(function(){

   let kt_select_categorie_devise_add_devise_form = $('#kt-select_categorie_devise_add_devise_form');
   let kt_select_monnaie_add_devise_form = $('#kt-select_monnaie_add_devise_form');
    //alert('ok cest bon !');
    let checkIfHasClass = kt_select_monnaie_add_devise_form.hasClass('kt-selectpicker');

    //console.log(checkIfHasClass);
    if(checkIfHasClass)
    {

    }
    kt_select_categorie_devise_add_devise_form.on('change',function(){

        let value = $(this).val();
        $.ajax({
            type:'POST',
            dataType:'JSON',
            cache:false,
            url:basePath+'std-get?table=luni_monnaies&type=all',
            data:
                {
                    'categorie_devise':parseInt(value),
                    'statut':'1',

                },
            success:function(response)
            {
                let options = "";
                response.forEach(function(elements){
                    options += "<option value='"+elements.id_famille+"'>"+elements.libelle_famille+"</option>";
                });
                kt_select_monnaie_add_devise_form.html(options);
                console.log(checkIfHasClass);
                if(checkIfHasClass === false)
                {
                    kt_select_monnaie_add_devise_form.selectpicker('destroy');
                    setTimeout(function(){
                        kt_select_monnaie_add_devise_form.selectpicker();
                    },2e0)
                }
            },
            error:function()
            {
                swal.fire('Erreur','Une erreur s\'est produite lors du traitement. Veuillez vérifier votre connexion internet.','error');
            }
        })
    });
    let hiddenMessage = $('#hiddenMessage');


    if(hiddenMessage.val() !== "" || hiddenMessage.val() !== null)
    {
        //alert(hiddenMessage.val());
        let typeMessage = hiddenMessage.attr('data-type');
        if(typeMessage === "success")
        {
            swal.fire('Terminé',hiddenMessage.val(),"success");
        }
        else if(typeMessage === "error")
        {
            swal.fire('Error',hiddenMessage.val(),'error');
            setTimeout(function(){
                $('#kt_modal_4').modal('show');
            },2e3)

        }
    }



});

function updateDevise(id)
{
    let id_devise = parseInt(id);
    let modal_for_update_devise = $('#kt_modal_update_devise');

    let edit_dispo_source = $('#edit_dispo_source');
    let edit_dispo_destination = $('#edit_dispo_destination');
    $.ajax({
        type:'POST',
        url:basePath+'administration/currencies-management/std-get?table=devises&type=unique',
        data:
            {
                'id_devise':id_devise
            },
        success:function(response)
        {

                $('input[name="edit_libelle_devise"]').val(response.lib_devise);
                $('input[name="edit_solde_minimum"]').val(response.solde_minimum);
                $('input[name="edit_solde"]').val(response.solde);

                if(response.dispo_source === "1")
                {
                    edit_dispo_source.attr('checked',true);
                }
                else
                {
                    edit_dispo_source.attr('checked',false);
                }
                if(response.dispo_destination === "1")
                {
                    edit_dispo_destination.attr('checked',true)
                }
                else
                {
                    edit_dispo_destination.attr('checked',false);
                }

                $('input[name="hidden_id_devise"]').val(response.id_devise);

                selectOptionOfSelect('kt-select_monnaie_edit_devise_form',response.famille_devise);
                selectOptionOfSelect('kt-select_categorie_devise_update_devise_form',response.categorie_devise);
                //$('#cate')
                setTimeout(function(){
                    modal_for_update_devise.modal('show');
                },2e1);

        },
        error:function()
        {
            swal.fire('Erreur',"Une erreur s'est produite lors du traitement","erreur");
        }
    })


}

function updateStatusForCurrencies(id,valeur)
{
    let id_currencies = parseInt(id);


    swal.fire(
        {
            title:"Mis à jour du status",
            text:"Voulez-vous mettre à jour le status de cette devise ?",
            type:"info",
            showCancelButton:!0,
            confirmButtonText:"Oui, confirmer"}).
    then(function(e){

        if(e.value === true)
        {
            $.ajax({
                type:'post',
                dataType:'JSON',
                url:basePath+'administration/currencies-management/update-status-currencies?id='+id_currencies+'&valeur='+valeur,
                data:
                    {
                        'id_devise':id_currencies
                    },
                success:function(response)
                {

                    if(response.error !== undefined)
                    {
                        swal.fire('Erreur',response.error,'error');
                    }
                    else if(response.success !== undefined)
                    {
                        swal.fire('Terminé',response.success,'success');
                        $("#kt_table_currencies").DataTable().destroy();
                        setTimeout(function(){

                            setTimeout(function(){
                                KTDatatablesSearchOptionsAdvancedSearch.init();
                            },200) ;
                        },2e0)
                    }
                },
                error:function()
                {
                    swal.fire('Erreur','Une erreur s\'est produite au cours de l\'opération.Veuillez vérifier votre connexion','error');
                }

            });
        }
        //swal.fire("Deleted!","Your file has been deleted.","success")
    });
}

function callModalSettings(id_devise,categorie_devise)
{
    id_devise = parseInt(id_devise);
    categorie_devise = parseInt(categorie_devise);


    //console.log(lib_devise);

    $.ajax({
        type:'POST',
        url:basePath+'administration/currencies-management/std-get?type=unique&table=devises_input',
        dataType:'json',
        data:
            {
                id_devise:id_devise
            },
        success:function(response)
        {
            if(response.length !== 0)
            {

            }
            else
            {

                let inputs = "";
                let kt_add_select_type_input = $('#kt-add-select_type_input');
               // $("#span_lib_devise").html(htmlEntities(lib_devise));
                if(categorie_devise === 2)
                {
                    inputs = '<option value="tel">Téléphone</option>' ;
                    if($('#container_select_country').hasClass('hide'))
                    {
                        $('#container_select_country').removeClass('hide');
                    }


                }
                else
                {
                    if(!$('#container_select_country').hasClass('hide'))
                    {
                        $('#container_select_country').addClass('hide');
                    }
                    inputs = '<option value="email">Email</option>' +
                        '<option value="text">Text(Champ texte)</option>' +
                        '<option value="textarea">Textarea(Champ message)</option>';
                }

                kt_add_select_type_input.html(inputs);
                kt_add_select_type_input.addClass('kt-selectpicker');
                $('#kt_modal_add_settings_input').modal("show");



            }
        }
    })
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function updateLogoDevise(id_devise)
{
    id_devise = parseInt(id_devise);
    let kt_modal_update_logo_devise = $('#kt_modal_update_logo_devise');
    let kt_modal_update_logo_devise_body = $('#kt_modal_update_logo_devise .modal-body');
   // console.log(id_devise);
    $.ajax({
        type:'POST',
        dataType:'JSON',
        url:basePath+'administration/currencies-management/std-get?type=unique&table=devises',
        data:
            {
                id_devise:id_devise
            },
        success:function(response)
        {

            console.log(response);
            if(response.length !== 0)
            {
                $('#hidden_id_devise_for_logo_update').val(response.id_devise);
                $('.kt-avatar__holder').css('background-image',"url("+response.image_devise+")");
                kt_modal_update_logo_devise.modal('show');
                $('.kt-avatar__cancel').on('click',function(){
                    $('.kt-avatar__holder').css('background-image',"url("+response.image_devise+")");
                });
            }
        },
        error:function(state)
        {
            if(state.status === 404)
            {
                Swal.fire('Oops',"Une erreur s'est produite","error")
            }
        }
    })
}