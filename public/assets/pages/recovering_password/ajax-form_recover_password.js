"use strict";
$(document).ready(function(){



  $("#kt_form_recover_pwd_submit").click(function(i) {
        i.preventDefault();
        var e = $(this),
            n = $(this).closest("form");

        n.validate({
            rules: {
                text: {
                    required: !0,

                }
            }
        }),n.valid();

            if(n.valid())
            {

                (e.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !0),
                        $('#btn_previous').attr('disabled',!0),


                        n.ajaxSubmit({
                            url: "/login-api/treat-form",
                            type:'POST',
                            dataType:'JSON',
                            success: function(response) {
                                console.log(response);


                                if(response.error !== undefined)
                                {
                                    $("#kt_form_recover_pwd_submit").removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);



                                    swal.fire('Attention',response.error,'warning');


                                }
                                else if(response.success !== undefined)
                                {

                                    setTimeout(function(){
                                        document.location.href= location.origin;
                                    },2e3)
                                }

                            },
                            error:function()
                            {
                                setTimeout(function() {
                                    e.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", !1);
                                    $('#kt_form_recover_pwd_submit').attr('disabled', !1);
                                }, 2e3)
                                toastr.error("Une erreur s'est produite au cours du traitement !");
                            }
                        })
                )
            }
            else
            {
                swal.fire("Erreur","Une erreur s'est produite, veuillez vérifier toutes vos informations","error");
            }




    });


});
