"use strict";
var KTDatatableAutoColumnHideDemo = {
    init: function () {
        var t;
        var start = moment().subtract(29, 'days');
        var end = moment();
        var checkNumber;
        (t = $(".kt-datatable").KTDatatable({
            data: { type: "remote", source: { read: {

                url: basePath+"transactions/list-transactions"
            } },

                pageSize: 5, saveState: !1, serverPaging: !0, serverFiltering: !0, serverSorting: !0 },
            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            columns: [
                { field: "transaction_id",
                    title: "Id Transaction" ,
                    width:'150',
                    template:function(e)
                    {
                        var id_transaction = e.transaction_id;
                        var code_transaction = e.code_transaction;



                        return '<div class="row margin-0-auto text-center">' +
                            '<div class="row margin-0-auto padding-r-0 padding-l-0 font-size-0_9">' +
                            '<div class="col-md-3 text-left font-weight-bold font-size-1_0" style="padding-top: 2px !important">ID </div>' +
                            '<div class="col-md-9 text-right"><span class="kt-badge kt-badge--inline font-size-1_1 kt-badge--primary font-weight-500">'+id_transaction+'</span> </div> ' +
                            '</div>' +
                            '<div class="row margin-0-auto padding-r-0 padding-l-0 font-size-0_9" style="margin-top: 20px !important">' +
                            '<div class="col-md-3 text-left font-weight-bold font-size-1_0">Ticket </div>' +
                            '<div class="col-md-9 text-right"><span class="kt-badge kt-badge--inline font-size-1_1 kt-badge--brand font-weight-500">'+code_transaction+'</span></div>' +
                            '</div>' +
                            '</div>';
                    }

                },
                {
                    field: "devise_source",
                    title: "Ce que je donne",
                    width:'120',
                    template: function (e) {
                        var devise_source = e.devise_source;

                        return '<div class="row margin-0-auto text-center">' +
                                    '<img class="img_rounded" src="'+devise_source.image_devise+'" width="25px" height="25px" />' +
                                    '<h5 class="font-size-1_0 text-center width-100">'+devise_source.lib_devise+'</h5>' +
                                '</div>';
                    },
                },
                { field: "devise_destination", title: "Ce que je reçois", width: "120",
                    template:function(e)
                    {
                        var devise_cible = e.devise_cible;

                        return '<div class="row margin-0-auto text-center">' +
                                    '<img width="25px" height="25px" src="'+devise_cible.image_devise+'" class="img_rounded"/>' +
                                    '<h5 class="font-size-1_0 text-center width-100">'+devise_cible.lib_devise+'</h5>' +
                                '</div>';
                    }

                },
                { field: "date_transaction", title: "Date de la transaction", type: "date", format: "MM/DD/YYYY" },
                { field: "quantite_source", title: "Montants transaction", width: "180",template:function(e){
                        var quantite_source = e.quantite_source;
                        var quantite_cible = e.quantite_cible;

                        var devise_source = e.devise_source;
                        var devise_cible = e.devise_cible;

                        var currency_source;
                        var currency_cible;

                        switch (devise_source.famille_devise) {

                            case '1':
                                currency_source = "dollar";
                                break;
                            case '2':
                                currency_source = "euro";
                                break;
                            case '3':
                                currency_source = "FCFA";
                                break;
                            case '4':
                                currency_source = "XRP";
                                break;
                            case '5':
                                currency_source = "ETH";
                                break;
                            case '6':
                                currency_source = "BTC";
                                break;
                            case '7':
                                currency_source = "LTC";
                                break;
                            case '8':
                                currency_source = "TRX";
                                break;
                            case '9':
                                currency_source = "XMR";
                                break;
                        }

                        switch (devise_cible.famille_devise) {

                            case '1':
                                currency_cible = "dollar";
                                break;
                            case '2':
                                currency_cible = "euro";
                                break;
                            case '3':
                                currency_cible = "FCFA";
                                break;
                            case '4':
                                currency_cible = "XRP";
                                break;
                            case '5':
                                currency_cible = "ETH";
                                break;
                            case '6':
                                currency_cible = "BTC";
                                break;
                            case '7':
                                currency_cible = "LTC";
                                break;
                            case '8':
                                currency_cible = "TRX";
                                break;
                            case '9':
                                currency_cible = "XMR";
                                break;
                        }
                        return '<div class="row margin-0-auto padding-r-0 padding-l-0">' +
                            '<div class="col-md-12 padding-r-0 padding-l-0 font-size-0_9"><b>C que je donne:</b>&nbsp;<span class="kt-badge kt-badge--info kt-badge--inline font-size-1_0">'+quantite_source+'</span>&nbsp;<b>('+currency_source+')</b></div>' +
                            '<div class="col-md-12 padding-r-0 padding-l-0 margin-top-10 font-size-0_9"><b>C que je reçois:</b>&nbsp;<span class="kt-badge kt-badge--dark kt-badge--inline font-size-1_0">'+quantite_cible+'</span>&nbsp;<b>('+currency_cible+')</b></div>' +
                            '</div>';
                    }
                 },
                { field: "adresse_reception", title: "Adresses",width:'180',template:function(e)
                    {

                        var adresse_reception = e.adresse_reception;
                        var numero_envoi = e.numero_envoi;

                        return '<div class="row margin-0-auto padding-r-0 padding-l-0">' +
                                    '<div class="col-md-12 padding-r-0 padding-l-0 font-size-0_9"><b>Réception:</b>&nbsp;<span class="kt-badge kt-badge--primary kt-badge--inline font-size-1_0" style="font-weight: 600 !important">'+adresse_reception+'</span></div>' +
                                    '<div class="col-md-12 padding-r-0 padding-l-0 margin-top-10 font-size-0_9"><b>Dépôt:</b>&nbsp;<span class="kt-badge kt-badge--success color-white kt-badge--inline font-size-1_0" style="font-weight: 600 !important">'+numero_envoi+'</span></div>' +
                                '</div>';
                    }

                 },

                {
                    field: "status_transaction",
                    title: "Status",
                    template: function (t) {
                        var e = {
                            '0': { title: "Attente", class: "kt-badge--brand" },
                            '1': { title: "Valider", class: " kt-badge--success" },
                            '2': { title: "Annuler", class: " kt-badge--warning color-black" },
                            '3': { title: "Archiver", class: " kt-badge--danger" },

                        };
                        return '<span class="kt-badge ' + e[t.status_transaction].class + ' kt-badge--inline kt-badge--pill">' + e[t.status_transaction].title + "</span>";
                    },
                },
                {
                    field: "Actions",
                    title: "Actions",
                    sortable: !1,
                    width: 110,
                    overflow: "visible",
                    autoHide: !1,
                    template: function (e) {



                        if(e.numero_envoi.search("229") !== -1)
                        {
                            checkNumber = "<span> <a class='btn btn-sm btn-clean btn-icon btn-icon-md' onclick='showNumberDetails("+e.id_transaction+")'><i class='flaticon-arrows'></i> </a> </span>";
                        }
                        else
                        {
                            checkNumber = "";
                        }

                        return '  <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Archiver"><i class="la la-archive text-danger"></i> </a>'+checkNumber


                    },
                },
            ],
        })),
            $("#kt_search").on("click", function (a) {

                a.preventDefault();
                var e = {};

               $(".kt-input").each(function () {

                   var t = $(this).data("col-index");
                        e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()

                    }

                );
                t.search(e, "columns");


              }
            ),
            $("#kt_reset").on("click", function (a) {
                a.preventDefault();

                $(".kt-input").each(function () {
                        $(this).val("");
                    }
                );
                    var i ={
                        0:"",
                        1:"",
                        2:"",
                        3:"",
                        4:""
                    };
                    t.search(i,"columns");

                }
            ),
            $('#kt_daterangepicker_transactions .form-control').daterangepicker({
                    "locale": {
                        "format": "DD/MM/YYYY",
                        "separator": " - ",
                        "applyLabel": "Appliquer",
                        "cancelLabel": "Annuler",
                        "fromLabel": "De",
                        "toLabel": "A",
                        "customRangeLabel": "Personnaliser",
                        "daysOfWeek": [
                            "Di",
                            "Lu",
                            "Ma",
                            "Me",
                            "Je",
                            "Ve",
                            "Sa"
                        ],
                        "monthNames": [
                            "Janvier",
                            "Février",
                            "Mars",
                            "Avril",
                            "Mai",
                            "Juin",
                            "Juillet",
                            "Août",
                            "Septembre",
                            "Octobre",
                            "Novembre",
                            "Décembre"
                        ],
                        "firstDay": 1
                    },
                    startDate:start,
                    endDate:end,
                    ranges: {
                        "Aujourd'hui": [moment(), moment()],
                        "Hier": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                        "Ces 07 derniers jours": [moment().subtract(6, "days"), moment()],
                        "Ces 30 derniers jours": [moment().subtract(29, "days"), moment()],
                        "Ce mois": [moment().startOf("month"), moment().endOf("month")],
                        "Le mois dernier": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
                    },
                },
                function (a, t, e) {
                    $("#kt_form_period").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"));
                }
            ),
            $("#kt_form_status,#kt_form_type").selectpicker();
    },
};
jQuery(document).ready(function () {
    KTDatatableAutoColumnHideDemo.init();
});
function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}



function showNumberDetails(id)
{
    var kt_modal_info_number_depot = $('#kt_modal_info_number_depot');

    $.ajax({

        type:'POST',
        url:basePath+'transactions/show-number-details',
        dataType:'JSON',
        data:
            {
                id_transaction:id
            },
        success:function(response)
        {
            if(response.error === undefined)
            {

                var html = '<table class="table table-responsive table-borderless width-100">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Numéro</th>' +
                    '<th>Propriétaire</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><span class="kt-badge kt-badge--inline kt-badge--primary">00229 '+response.libelle_numero+'</span></td>' +
                    '<td><span class="kt-badge kt-badge--inline kt-badge--info">'+response.number_name+'</span></td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>';
                $('#kt_modal_info_number_depot .modal-body').html(html);
            }
            else
            {
                $('#kt_modal_info_number_depot .modal-body').html("<p style='text-align: center !important' class='text-center text-danger font-size-1_0 font-weight-500'>"+response.error+"</p>");
            }

            kt_modal_info_number_depot.modal('show');

        },
        error:function()
        {
            swal.fire('Oops',"Une erreur s'est produite. Veuillez vérifier votre connexion internet et rééssayer","error");
        }
    });
}