"use strict";
var KTDatatableAutoColumnHideDemo = {
    init: function () {
        var t;
        (t = $("#kt-datatable-demandes-retrait").KTDatatable({
            data: { type: "remote", source: { read: {
                url: basePath+"transactions/list-bonus"

            } }, pageSize: 10, saveState: !1, serverPaging: !0, serverFiltering: !0, serverSorting: !0 },
            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },
            columns: [
                { field: "ticket_demande", title: "Ticket de la demande", },
                {
                    field: "customer",
                    title: "Client", overflow: "hide",
                    autoHide: !1,
                    template: function (t) {
                        return t.nom_complet;
                    },
                },
                { field: "moyen_paiement", title: "Méthode de paiement", overflow: "visible",
                    autoHide: !1, width: "auto",template:function(t){
                    return t.lib_devise;
                    } },
                { field: "montant", title: "Montant du retrait",width:'auto', overflow: "visible",
                    autoHide: !1,template:function(t){

                     return '<h2 class="font-weight-bold font-size-1_0">'+t.montant+'&nbsp;&nbsp;&nbsp;<span class="kt-badge text-info font-weight-bold font-size-0_9 kt-badge--square">FCFA</span></h2>'

                    }
                    },
                { field: "date_transaction", title: "Date", type: "date", format: "MM/DD/YYYY" , overflow: "visible",
                    autoHide: !1},
                { field: "proprietaire", title: "Propietaire du compte", width: "auto" },
                { field: "commentaire", title: "Commentaire" ,overflow: "visible",
                    autoHide: !1 },
                {
                    field: "statut_transaction",
                    title: "Status",
                    overflow: "visible",
                    autoHide: !1,
                    template: function (t) {
                        var e = {
                            '0': { title: "En attente", class: "kt-badge--brand" },
                            '1': { title: "Acceptée", class: " kt-badge--success" },
                            '2': { title: "Rejetée", class: " kt-badge--danger" }

                                };
                        return '<span class="kt-badge ' + e[t.statut_transaction].class + ' kt-badge--inline kt-badge--pill">' + e[t.statut_transaction].title + "</span>";
                    },
                },
                {
                    field: "Actions",
                    title: "Actions",
                    sortable: !1,
                    width: 110,
                    overflow: "visible",
                    autoHide: !1,
                    template: function (t) {
                        console.log(t);
                        let html = '';
                            if(t.statut_transaction === '0')
                            {
                                html = '<div class="btn-group" style="padding: 12px !important">' +
                                    '<button type="button" title="Accepter la demande de retrait" class="btn btn-elevate btn-success btn-square" onclick="updateTransaction(1,'+t.id_transaction_bonus+')"><i class="flaticon2-check-mark"></i> </button> ' +
                                    '<button type="button" title="Rejeter la demande de retrait" class="btn btn-elevate btn-danger btn-square" onclick="updateTransaction(2,'+t.id_transaction_bonus+')"><i class="flaticon2-cancel"></i> </button> ' +

                                    '</div>'
                            }
                            else
                            {
                                html = '<button class="btn btn-danger btn-elevate btn-square" type="button">Rejetée</button>';
                                if(t.statut_transaction = '1')
                                {
                                    html = '<button class="btn btn-success btn-elevate btn-square" type="button">Acceptée</button>'
                                }
                            }

                            return html;

                    },
                },
            ],
        })),
            $("#searchScustomer").on("keydown", function () {
                t.search($(this).val().toLowerCase(), "customer");
            });

    },
};
jQuery(document).ready(function () {
    KTDatatableAutoColumnHideDemo.init();
});

function updateTransaction(action,id)
{
    let current_value = parseInt(id);
    let img = '/img/transactions/transaction_cancel.png';
    let text = "Voulez vous rejeter cette demande ?";
    if(action === 1)
    {
        text = "Voulez vous accepter cette demande ?";
        img = '/img/transactions/transactions_validate.png'
    }

    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: text,
        imageUrl: location.origin+img,
        imageWidth: 200,
        imageHeight: 200,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#aaa',
        confirmButtonText: 'Oui, confirmer',
        cancelButtonText:"Annuler"
    }).then((result) => {
        if(result.value)
        {
          let kt_modal_accepted = $('#kt_modal_accepted');
          let kt_modal_rejected = $('#kt_modal_rejected');

          if(action === 1)
          {
              kt_modal_accepted.modal('show');
              let kt_form_accept_recall = $('#kt_form_accept_recall');

              kt_form_accept_recall.submit(function(e){
                  e.preventDefault();

                  if($('#id_transaction').val().trim() === '')
                  {
                      toastr.error('Veuillez renseigner l\'identifiant de la transaction');
                  }
                  else
                  {
                      $.ajax({

                          type:'POST',
                          url:basePath+'transactions/update-transaction',
                          dataType:'JSON',
                          data:
                              {
                                  id: current_value,
                                  action:'1',
                                  transaction_id:$('#id_transaction').val()
                              },
                          beforeSend:function()
                          {
                              blockUI('Traitement en cours...')
                          },
                          success:function(response)
                          {

                              if(response.error !== undefined)
                              {
                                  unblockUI();
                                  toastr.error(response.error);
                              }
                              else if(response.success !== undefined)
                              {
                                  document.location.href = basePath+'transactions/retrait-bonus';
                              }
                          },
                          error:function()
                          {
                              unblockUI();
                              custom_std('error',"Une erreur s'est produite au cours du traitement",'fire');
                          }

                      })
                  }


              })
          }
          else if(action === 2)
          {
              kt_modal_rejected.modal('show');
          }
        }
    })
}