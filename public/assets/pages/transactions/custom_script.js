$(document).ready(function(){

   let hidden_welcome_message = $('#hidden_welcome_message');

   if(hidden_welcome_message.val() !== "" && hidden_welcome_message.val() !== undefined)
   {
       toastr.options.timeOut = 0;
       toastr.success(hidden_welcome_message.val());

   }
});