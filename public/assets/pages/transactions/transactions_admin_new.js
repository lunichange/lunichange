"use strict";
var KTDatatablesSearchOptionsAdvancedSearch=function() {
    $.fn.dataTable.Api.register("column().title()", function() {
            return $(this.header()).text().trim()
        }
    );
    return {
        init:function() {
            var t;
            t=$("#kt_table_transactions").DataTable( {
                    responsive:!0, dom:"<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>", lengthMenu:[5, 10, 25, 50], pageLength:5, language: {
                        lengthMenu: "Display _MENU_"
                    }
                    , searchDelay:500, processing:!0, serverSide:!0, ajax: {
                        url:basePath+"transactions/list-transactions",
                        type:"POST",
                        data: {
                            columnsDef: ["transaction_id", "code_transaction", "context", "quantite_source","adresse_reception",'date_transaction',"nom_complet", "status_transaction","Actions"]
                        }
                    }
                    , columns:[
                        {
                            data: "transaction_id"
                        }
                        , {
                            data: "context"
                        }

                        , {
                            data: "quantite_source"
                        }

                        , {
                            data: "adresse_reception"
                        },
                        {
                            data: "date_transaction"
                        },
                        {
                            data: "nom_complet"
                        },
                        {
                            data: "status_transaction"
                        }
                        , {
                            data: "Actions", responsivePriority: -1
                        }
                    ], initComplete:function() {
                        this.api().columns().every(function() {
                                switch(this.title()) {

                                    case "status_transaction":var t= {
                                            '0': {
                                                title: "Attente", class: "kt-badge--brand"
                                            }
                                            , '1': {
                                                title: "Valider", class: " kt-badge--success"
                                            }
                                            , '2': {
                                                title: "Annuler", class: " kt-badge--warning"
                                            }
                                            , '3': {
                                                title: "Archiver", class: " kt-badge--danger"
                                            }
                                        }
                                    ;
                                        this.data().unique().sort().each(function(a, e) {
                                                $('.kt-input[data-col-index="6"]').append('<option value="'+a+'">'+t[a].title+"</option>")
                                            }
                                        );
                                        break;

                                }
                            }
                        )
                    }
                    , columnDefs:[ {
                        targets:-1, title:"Actions", class:'colonne_actions', width:"70px", orderable:!1, render:function(t, a, e, n) {


                            let current_user = parseInt(n.settings.json.current_user.code_profil);

                            let html;
                            let status = e.status_transaction;
                            let iconDoneBy = "";
                            if(status === '0')
                            {

                                html = '<a  class="kt-btn_validate hover_elements" onclick="ValidateTransaction('+e.id_transaction+')" data-id="'+e.id_transaction+'" title="Valider">' +
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px" viewBox="0 0 18 18" version="1.1" class="kt-svg-icon">\n' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <polygon points="0 0 24 0 24 24 0 24"/>\n' +
                                    '        <path d="M9.26193932,16.6476484 C8.90425297,17.0684559 8.27315905,17.1196257 7.85235158,16.7619393 C7.43154411,16.404253 7.38037434,15.773159 7.73806068,15.3523516 L16.2380607,5.35235158 C16.6013618,4.92493855 17.2451015,4.87991302 17.6643638,5.25259068 L22.1643638,9.25259068 C22.5771466,9.6195087 22.6143273,10.2515811 22.2474093,10.6643638 C21.8804913,11.0771466 21.2484189,11.1143273 20.8356362,10.7474093 L17.0997854,7.42665306 L9.26193932,16.6476484 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(14.999995, 11.000002) rotate(-180.000000) translate(-14.999995, -11.000002) "/>\n' +
                                    '        <path d="M4.26193932,17.6476484 C3.90425297,18.0684559 3.27315905,18.1196257 2.85235158,17.7619393 C2.43154411,17.404253 2.38037434,16.773159 2.73806068,16.3523516 L11.2380607,6.35235158 C11.6013618,5.92493855 12.2451015,5.87991302 12.6643638,6.25259068 L17.1643638,10.2525907 C17.5771466,10.6195087 17.6143273,11.2515811 17.2474093,11.6643638 C16.8804913,12.0771466 16.2484189,12.1143273 15.8356362,11.7474093 L12.0997854,8.42665306 L4.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.999995, 12.000002) rotate(-180.000000) translate(-9.999995, -12.000002) "/>\n' +
                                    '    </g>\n' +
                                    '</svg>' +
                                    '</a>&nbsp;&nbsp;&nbsp;<button  onclick="CancelTransaction('+e.id_transaction+')" class="btn btn-warning btn-xs kt-btn_cancel text-cente" style="padding-left: 12px !important; padding-right: 8px !important" data-id="'+e.id_transaction+'" title="Annuler"><i class="fa fa-times" style="color: white !important"></i> </button>';

                            }
                            else
                            {
                                if(e.done_by !== "" && e.done_by !== null)
                                {
                                    let manager = e.done_by;
                                    //console.log(manager);
                                    if(current_user === 0 || current_user === 1)
                                    {
                                        iconDoneBy = '<a href="javascript:void(0);" title="Réalisé par" onclick="showManager(\''+manager.nom_complet+'\',\''+manager.email+'\')"> <svg style="width: 28px !important; height: 28px !important" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                            '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                            '        <rect x="0" y="0" width="24" height="24"/>\n' +
                                            '        <path d="M12,3 C16.418278,3 20,6.581722 20,11 L20,21 C20,21.5522847 19.5522847,22 19,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,11 C4,6.581722 7.581722,3 12,3 Z M9,10 C7.34314575,10 6,11.3431458 6,13 C6,14.6568542 7.34314575,16 9,16 L15,16 C16.6568542,16 18,14.6568542 18,13 C18,11.3431458 16.6568542,10 15,10 L9,10 Z" fill="#000000"/>\n' +
                                            '        <path d="M15,14 C14.4477153,14 14,13.5522847 14,13 C14,12.4477153 14.4477153,12 15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 Z M9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 C9.55228475,12 10,12.4477153 10,13 C10,13.5522847 9.55228475,14 9,14 Z" fill="#000000" opacity="0.3"/>\n' +
                                            '    </g>' +
                                            '</svg></a>';
                                    }


                                }
                                if(status === '1')
                                {
                                    html = '<img title="Transaction validée" src="'+location.origin+'/img/transaction_approved.png" height="40" width="auto" />'
                                }
                                else
                                {
                                    html='<img title="Transaction annulée" src="'+location.origin+'/img/transaction_declined.png" height="40" width="auto"/>';
                                }

                            }
                            return '<div class="btn-group-sm group_btn">'+html+'&nbsp;&nbsp;&nbsp;'+iconDoneBy+'</div>';
                        }
                    }
                        , {
                            targets:0, id:'tr_code_id_transaction', width:'170px', render:function(t, a, e, n) {


                                let id_transaction = e.transaction_id;
                                let code_transaction = e.code_transaction;



                                return '<div class="row margin-0-auto text-center">' +
                                    '<div class="row margin-0-auto padding-r-0 padding-l-0 font-size-0_9">' +
                                    '<div class="col-md-3 text-left font-weight-bold font-size-1_0" style="padding-top: 2px !important">ID </div>' +
                                    '<div class="col-md-9 text-right"><span class="kt-badge kt-badge--inline font-size-1_1 kt-badge--primary font-weight-500">'+id_transaction+'</span> </div> ' +
                                    '</div>' +
                                    '<div class="row margin-0-auto padding-r-0 padding-l-0 font-size-0_9" style="margin-top: 20px !important">' +
                                    '<div class="col-md-3 text-left font-weight-bold font-size-1_0">Ticket </div>' +
                                    '<div class="col-md-9 text-right"><span class="kt-badge kt-badge--inline font-size-1_1 kt-badge--brand font-weight-500">'+code_transaction+'</span></div>' +
                                    '</div>' +
                                    '</div>';
                            }
                        },
                        {
                            targets:1, width:'170px', render:function(t, a, e, n) {

                                let devise_cible = e.devise_cible;
                                let devise_source = e.devise_source;




                                return '<div class="row margin-0-auto">' +
                                    '<div class="col-md-5">' +
                                    '<img class="img_rounded" src="'+devise_source.image_devise+'" width="25px" height="25px" />' +
                                    '<h5 class="font-size-0_72 text-center font-weight-bold width-100">'+devise_source.lib_devise+'</h5>' +
                                    '</div>' +
                                    '<div class="col-md-2 kt-padding-t-30">' +
                                    '<svg xmlns="http://www.w3.org/2000/svg" style="width: 20px !important; height: 20px !important" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <polygon points="0 0 24 0 24 24 0 24"/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" transform="translate(13.000000, 6.000000) rotate(-450.000000) translate(-13.000000, -6.000000) " x="12" y="8.8817842e-16" width="2" height="12" rx="1"/>\n' +
                                    '        <path d="M9.79289322,3.79289322 C10.1834175,3.40236893 10.8165825,3.40236893 11.2071068,3.79289322 C11.5976311,4.18341751 11.5976311,4.81658249 11.2071068,5.20710678 L8.20710678,8.20710678 C7.81658249,8.59763107 7.18341751,8.59763107 6.79289322,8.20710678 L3.79289322,5.20710678 C3.40236893,4.81658249 3.40236893,4.18341751 3.79289322,3.79289322 C4.18341751,3.40236893 4.81658249,3.40236893 5.20710678,3.79289322 L7.5,6.08578644 L9.79289322,3.79289322 Z" fill="#000000" fill-rule="nonzero" transform="translate(7.500000, 6.000000) rotate(-270.000000) translate(-7.500000, -6.000000) "/>\n' +
                                    '        <rect fill="#000000" opacity="0.3" transform="translate(11.000000, 18.000000) scale(1, -1) rotate(90.000000) translate(-11.000000, -18.000000) " x="10" y="12" width="2" height="12" rx="1"/>\n' +
                                    '        <path d="M18.7928932,15.7928932 C19.1834175,15.4023689 19.8165825,15.4023689 20.2071068,15.7928932 C20.5976311,16.1834175 20.5976311,16.8165825 20.2071068,17.2071068 L17.2071068,20.2071068 C16.8165825,20.5976311 16.1834175,20.5976311 15.7928932,20.2071068 L12.7928932,17.2071068 C12.4023689,16.8165825 12.4023689,16.1834175 12.7928932,15.7928932 C13.1834175,15.4023689 13.8165825,15.4023689 14.2071068,15.7928932 L16.5,18.0857864 L18.7928932,15.7928932 Z" fill="#000000" fill-rule="nonzero" transform="translate(16.500000, 18.000000) scale(1, -1) rotate(270.000000) translate(-16.500000, -18.000000) "/>\n' +
                                    '    </g>\n' +
                                    '</svg>' +
                                    '</div>' +
                                    '<div class="col-md-5">' +
                                    '<img width="25px" height="25px" src="'+devise_cible.image_devise+'" class="img_rounded"/>' +
                                    '<h5 class="font-size-0_72 text-center font-weight-bold width-100">'+devise_cible.lib_devise+'</h5>' +
                                    '</div> ' +
                                    '</div>';
                            }
                        },
                        {
                            targets:3, width:'150px', render:function(t, a, e, n) {

                                let adresse_reception = e.adresse_reception;
                                let numero_envoi = e.numero_envoi;



                                return '<div class="row margin-0-auto padding-r-0 padding-l-0">' +
                                    '<div class="col-md-12 padding-r-0 padding-l-0 font-size-0_9"><b>Réception:</b>&nbsp;<span class="kt-badge kt-badge--primary kt-badge--inline font-size-1_0" style="font-weight: 600 !important">'+adresse_reception+'</span></div>' +
                                    '<div class="col-md-12 padding-r-0 padding-l-0 margin-top-10 font-size-0_9"><b>Dépôt:</b>&nbsp;<span class="kt-badge kt-badge--success color-white kt-badge--inline font-size-1_0" style="font-weight: 600 !important">'+numero_envoi+'</span></div>' +
                                    '</div>';
                            }
                        },
                        {
                            targets:4, title:'Date transaction', width:'100px', render:function(t, a, e, n) {





                                return '<div class="row text-center">' +
                                    '<img class="img_rounded border-0 img-fluid img-responsive calendar_date_transaction" onclick="showDateTransaction('+e.id_transaction+')" title="Cliquer pour afficher la date de transaction" style="box-shadow: none !important;padding: 2px !important; border: solid 1px #efefef !important" width="30px" height="30px" src="'+location.origin+'/img/calendar.png"/>' +
                                    '</div>';
                            }
                        },
                        {
                            targets:5, title:'Client', width:'50px', render:function(t, a, e, n) {






                                return '<div data-offset="20px 20px" onclick="showUsername('+e.id_user+',\'username_tooltip\')"  class="row text-center " title="Cliquer pour afficher le nom du client">' +
                                    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" version="1.1" style="box-shadow: none !important; border: solid 1px #efefef !important; height: 35px !important; width: 35px !important; padding: 2px !important"  class="kt-svg-icon img_rounded hover_elements kt-padding-10">' +
                                    '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                    '        <polygon points="0 0 24 0 24 24 0 24"/>\n' +
                                    '        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>\n' +
                                    '        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>\n' +
                                    '    </g>\n' +
                                    '</svg>' +
                                    '</div>';
                            }
                        },
                        {
                            targets:2, width:'170px', render:function(t, a, e, n) {

                                let quantite_source = e.quantite_source;
                                let quantite_cible = e.quantite_cible;

                                let devise_source = e.devise_source;
                                let devise_cible = e.devise_cible;

                                let currency_source;
                                let currency_cible;

                                switch (devise_source.famille_devise) {

                                    case '1':
                                        currency_source = "dollar";
                                        break;
                                    case '2':
                                        currency_source = "euro";
                                        break;
                                    case '3':
                                        currency_source = "FCFA";
                                        break;
                                    case '4':
                                        currency_source = "XRP";
                                        break;
                                    case '5':
                                        currency_source = "ETH";
                                        break;
                                    case '6':
                                        currency_source = "BTC";
                                        break;
                                    case '7':
                                        currency_source = "LTC";
                                        break;
                                    case '8':
                                        currency_source = "TRX";
                                        break;
                                    case '9':
                                        currency_source = "XMR";
                                        break;
                                }

                                switch (devise_cible.famille_devise) {

                                    case '1':
                                        currency_cible = "dollar";
                                        break;
                                    case '2':
                                        currency_cible = "euro";
                                        break;
                                    case '3':
                                        currency_cible = "FCFA";
                                        break;
                                    case '4':
                                        currency_cible = "XRP";
                                        break;
                                    case '5':
                                        currency_cible = "ETH";
                                        break;
                                    case '6':
                                        currency_cible = "BTC";
                                        break;
                                    case '7':
                                        currency_cible = "LTC";
                                        break;
                                    case '8':
                                        currency_cible = "TRX";
                                        break;
                                    case '9':
                                        currency_cible = "XMR";
                                        break;
                                }





                                return '<div class="row margin-0-auto padding-r-0 padding-l-0">' +
                                    '<div class="col-md-12 padding-r-0 padding-l-0 font-size-0_9"><b>Source:</b>&nbsp;<span class="kt-badge kt-badge--info kt-badge--inline font-size-1_0">'+quantite_source+'</span>&nbsp;<b>('+currency_source+')</b></div>' +
                                    '<div class="col-md-12 padding-r-0 padding-l-0 margin-top-10 font-size-0_9"><b>Cible:</b>&nbsp;<span class="kt-badge kt-badge--dark kt-badge--inline font-size-1_0">'+quantite_cible+'</span>&nbsp;<b>('+currency_cible+')</b></div>' +
                                    '</div>';
                            }
                        }
                        , {
                            targets:6, render:function(t, a, e, n) {
                                var i= {
                                        '0': {
                                            title: "Attente", class: "kt-badge--brand"
                                        }
                                        , '1': {
                                            title: "Valider", class: " kt-badge--success"
                                        }
                                        , '2': {
                                            title: "Annuler", class: " kt-badge--warning"
                                        }
                                        , '3': {
                                            title: "Archiver", class: " kt-badge--danger"
                                        }

                                    }
                                ;
                                return void 0===i[t]?t:'<span class="kt-badge '+i[t].class+' kt-badge--inline kt-badge--pill" style="font-size: 1.0em !important">'+i[t].title+"</span>"
                            }
                        }



                    ]
                }
            ),
                $("#kt_search").on("click", function(a) {
                        a.preventDefault();
                        var e= {}
                        ;
                        blockUI('Recherche en cours...');
                        $(".kt-input").each(function() {
                                var t=$(this).data("col-index");
                                e[t]?e[t]+="|"+$(this).val(): e[t]=$(this).val()
                            }
                        ), $.each(e, function(a, e) {
                                t.column(a).search(e||"", !1, !1)
                            }
                        ), t.table().draw(),unblockUI()
                    }
                ),
                $("#kt_reset").on("click", function(a) {
                        blockUI('Chargement en cours...');
                        a.preventDefault(), $(".kt-input").each(function() {
                                $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1);
                                $('input[data-col-index="1"]').val("");
                            }
                        ), t.table().draw(),unblockUI(), selectOptionOfSelect('kt_list_customers',"")

                    }
                ),
                $('#kt_daterangepicker_transactions .form-control').daterangepicker({
                        "locale": {
                            "format": "DD/MM/YYYY",
                            "separator": " - ",
                            "applyLabel": "Appliquer",
                            "cancelLabel": "Annuler",
                            "fromLabel": "De",
                            "toLabel": "A",
                            "customRangeLabel": "Personnaliser",
                            "daysOfWeek": [
                                "Di",
                                "Lu",
                                "Ma",
                                "Me",
                                "Je",
                                "Ve",
                                "Sa"
                            ],
                            "monthNames": [
                                "Janvier",
                                "Février",
                                "Mars",
                                "Avril",
                                "Mai",
                                "Juin",
                                "Juillet",
                                "Août",
                                "Septembre",
                                "Octobre",
                                "Novembre",
                                "Décembre"
                            ],
                            "firstDay": 1
                        },

                        ranges: {
                            "Aujourd'hui": [moment(), moment()],
                            "Hier": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                            "Ces 07 derniers jours": [moment().subtract(6, "days"), moment()],
                            "Ces 30 derniers jours": [moment().subtract(29, "days"), moment()],
                            "Ce mois": [moment().startOf("month"), moment().endOf("month")],
                            "Le mois dernier": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
                        },
                    },
                    function (a, t, e) {
                        $("#kt_form_period").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"));
                    }

                )
        }
    }
}

();
jQuery(document).ready(function() {
    KTDatatablesSearchOptionsAdvancedSearch.init();
    $('.title_devise').tooltip();

    let kt_btn_validate = $('.kt-btn_validate');

    kt_btn_validate.on('click',function(){
        let current_value = $(this).val();

        //  console.log(current_value);
    });






});



function ValidateTransaction(id)
{
    let current_value = parseInt(id);


    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Voulez-vous valider cette transaction ?",
        imageUrl: location.origin+'/img/vector_check.jpg',
        imageWidth: 300,
        imageHeight: 200,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#aaa',
        confirmButtonText: 'Oui, confirmer',
        cancelButtonText:"Annuler"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'transactions/validate-transaction',
                data:
                    {

                        id_transaction:current_value
                    },
                beforeSend:function()
                {
                    blockUI('Traitement en cours...')
                },
                success:function(response)
                {
                    unblockUI();
                    if(response.error !== undefined)
                    {


                        toastr.error(response.error);
                    }
                    else
                    {
                        toastr.success('Transaction validée !');
                        setTimeout(function(){
                            document.location.href = location.origin+'/transactions';
                        },2e2);

                    }
                },
                error:function()
                {

                }
            });
        }
    })

    /*const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: 'Signed in successfully'
    })*/

}

function CancelTransaction(id)
{
    let current_value = parseInt(id);


    Swal.fire({
        title: 'Êtes-vous sûr ?',
        text: "Voulez-vous annuler cette transaction ?",
        imageUrl: location.origin+'/img/transactions/transaction_cancel.png',
        imageWidth: 200,
        imageHeight: 200,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#aaa',
        confirmButtonText: 'Oui, confirmer',
        cancelButtonText:"Annuler"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type:'POST',
                dataType:'JSON',
                url:basePath+'transactions/cancel-transaction',
                data:
                    {

                        id_transaction:current_value
                    },
                beforeSend:function()
                {
                    blockUI('Traitement en cours...')
                },
                success:function(response)
                {
                    unblockUI();
                    if(response.error !== undefined)
                    {


                        toastr.error(response.error);
                    }
                    else
                    {
                        toastr.success('Transaction annulée !');
                        setTimeout(function(){
                            document.location.href = location.origin+'/transactions';
                        },2e2);

                    }
                },
                error:function()
                {

                }
            });
        }
    })
}

function RefreshStateTransaction(id)
{
    //alert(id);
}
function ArchiveTransaction(id)
{
    // alert(id);
}
function showDateTransaction(id_transaction)
{
    id_transaction = parseInt(id_transaction);

    if(id_transaction !== "")
    {
        $.ajax({

            type:'POST',
            url:basePath+'luni-api/std-get?type=unique&table=transactions',
            dataType:'JSON',
            data:
                {
                    id_transaction:id_transaction
                },
            success:function(response)
            {
                $('#kt_modal_date_transaction .modal-body').html(response.date_transaction);
                $('#kt_modal_date_transaction').modal('show');
            },
            error:function()
            {
                swal.fire('Oops',"Une erreur s'est produite. Veuillez vérifier votre connexion internet et rééssayer","error");
            }
        });
    }
}
function showUsername(id_user,element)
{
    id_user = parseInt(id_user);

    if(id_user !== "")
    {
        $.ajax({

            type:'POST',
            url:basePath+'luni-api/std-get?type=unique&table=customers',
            dataType:'JSON',
            data:
                {
                    id_user:id_user
                },
            success:function(response)
            {
                $('#kt_modal_info_customer .modal-body').html('<h5> <b style="text-decoration: underline !important">Nom Complet:</b>&nbsp;'+response.nom_complet+'</h5><br><h5><b style="text-decoration: underline !important">Téléphone:</b> +'+response.dial_code+'&nbsp;'+response.telephone+'</h5><br><h5><b style="text-decoration: underline !important">Email:</b>&nbsp;'+response.email+'</h5>');
                $('#kt_modal_info_customer').modal('show');

            },
            error:function()
            {
                swal.fire('Oops',"Une erreur s'est produite. Veuillez vérifier votre connexion internet et rééssayer","error");
            }
        });
    }
}

function showManager(object,email)
{
    let kt_modal_info_manager = $('#kt_modal_info_manager');

    let bodyHtml = $('#kt_modal_info_manager .modal-body');

    console.log(email);
    bodyHtml.html('<h5 class="font-size-1_2"><b style="text-decoration: underline !important;">Gestionnaire:</b>&nbsp;&nbsp;'+object+'</h5><br><h5 class="kt-font-size-1_0-desktop font-size-1_2"><b style="text-decoration: underline">Email:</b>&nbsp;'+email+'</h5>');

    kt_modal_info_manager.modal('show');

}