"use strict";
var KTDatatableAutoColumnHideDemo = {
    init: function () {
        var t;
        var start = moment().subtract(29, 'days');
        var end = moment();
        $('#kt_form_period').val('');

        (t = $("#kt_table_transactions").KTDatatable({
            data: { type: "remote", source: { read: {
            url: basePath+"transactions/list-transactions"
            } },

            pageSize: 10, saveState: !1, serverPaging: !0, serverFiltering: !0, serverSorting: !0 },
            layout: { scroll: !0, height: 550 },
            sortable: !0,
            pagination: !0,
            search: { input: $("#generalSearch") },
            columns: [
                { field: "transaction_id", title: "ID/Ticket de la transaction",template:function(e){

                        let id_transaction = e.transaction_id;
                        let code_transaction = e.code_transaction;



                        return '<div class="row margin-0-auto text-center">' +
                            '<div class="row margin-0-auto padding-r-0 padding-l-0 font-size-0_9">' +
                            '<div class="col-md-3 text-left font-weight-bold font-size-1_0" style="padding-top: 2px !important">ID </div>' +
                            '<div class="col-md-9 text-right"><span class="kt-badge kt-badge--inline font-size-1_1 kt-badge--primary font-weight-500">'+id_transaction+'</span> </div> ' +
                            '</div>' +
                            '<div class="row margin-0-auto padding-r-0 padding-l-0 font-size-0_9" style="margin-top: 20px !important">' +
                            '<div class="col-md-3 text-left font-weight-bold font-size-1_0">Ticket </div>' +
                            '<div class="col-md-9 text-right"><span class="kt-badge kt-badge--inline font-size-1_1 kt-badge--brand font-weight-500">'+code_transaction+'</span></div>' +
                            '</div>' +
                            '</div>';
                    } },
                {
                    field: "devise_source",
                    title: "Transactions",
                    width:'230',
                    template: function (e) {

                            let devise_source = e.devise_source;
                            let devise_cible = e.devise_cible;

                            let icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">\n' +
                                '    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                                '        <polygon points="0 0 24 0 24 24 0 24"/>\n' +
                                '        <rect fill="#000000" opacity="0.3" transform="translate(13.000000, 6.000000) rotate(-450.000000) translate(-13.000000, -6.000000) " x="12" y="8.8817842e-16" width="2" height="12" rx="1"/>\n' +
                                '        <path d="M9.79289322,3.79289322 C10.1834175,3.40236893 10.8165825,3.40236893 11.2071068,3.79289322 C11.5976311,4.18341751 11.5976311,4.81658249 11.2071068,5.20710678 L8.20710678,8.20710678 C7.81658249,8.59763107 7.18341751,8.59763107 6.79289322,8.20710678 L3.79289322,5.20710678 C3.40236893,4.81658249 3.40236893,4.18341751 3.79289322,3.79289322 C4.18341751,3.40236893 4.81658249,3.40236893 5.20710678,3.79289322 L7.5,6.08578644 L9.79289322,3.79289322 Z" fill="#000000" fill-rule="nonzero" transform="translate(7.500000, 6.000000) rotate(-270.000000) translate(-7.500000, -6.000000) "/>\n' +
                                '        <rect fill="#000000" opacity="0.3" transform="translate(11.000000, 18.000000) scale(1, -1) rotate(90.000000) translate(-11.000000, -18.000000) " x="10" y="12" width="2" height="12" rx="1"/>\n' +
                                '        <path d="M18.7928932,15.7928932 C19.1834175,15.4023689 19.8165825,15.4023689 20.2071068,15.7928932 C20.5976311,16.1834175 20.5976311,16.8165825 20.2071068,17.2071068 L17.2071068,20.2071068 C16.8165825,20.5976311 16.1834175,20.5976311 15.7928932,20.2071068 L12.7928932,17.2071068 C12.4023689,16.8165825 12.4023689,16.1834175 12.7928932,15.7928932 C13.1834175,15.4023689 13.8165825,15.4023689 14.2071068,15.7928932 L16.5,18.0857864 L18.7928932,15.7928932 Z" fill="#000000" fill-rule="nonzero" transform="translate(16.500000, 18.000000) scale(1, -1) rotate(270.000000) translate(-16.500000, -18.000000) "/>\n' +
                                '    </g>\n' +
                                '</svg>';
                            let html = '<div class="row margin-0-auto text-center display-flex display-no-wrap">' +
                                          '<div class="col-md-5 col-xl-5">' +
                                            '<span class="badge font-size-1_0 badge-warning btn-elevate btn-elevate-air">' +
                                            ''+devise_source.lib_devise +
                                            '</span>'+
                                          '</div>' +
                                          '<div class="col-md-2 col-xl-2 text-center">' +
                                            ''+icon +
                                          '</div> ' +
                                          '<div class="col-md-4 col-xl-4 padding-left-10">' +
                                              '<span class="badge badge-success btn-elevate btn-elevate-hover btn-elevate-air">' +
                                                ''+devise_cible.lib_devise +
                                              '</span>' +
                                          '</div> ' +

                                        '</div>';
                            return html;

                    },
                },
                { field: "date_transaction", title: "Date transaction", type: "date", format: "MM/DD/YYYY",width: "auto",template:function(e){

                        return e.date_transaction;

                    } },
                { field: "quantite_source", title: "Montants transaction",width:'200', template:function(e){


                        var quantite_source = e.quantite_source;
                        var quantite_cible = e.quantite_cible;

                        var devise_source = e.devise_source;
                        var devise_cible = e.devise_cible;

                        var currency_source;
                        var currency_cible;

                        switch (devise_source.famille_devise) {

                            case '1':
                                currency_source = "dollar";
                                break;
                            case '2':
                                currency_source = "euro";
                                break;
                            case '3':
                                currency_source = "FCFA";
                                break;
                            case '4':
                                currency_source = "XRP";
                                break;
                            case '5':
                                currency_source = "ETH";
                                break;
                            case '6':
                                currency_source = "BTC";
                                break;
                            case '7':
                                currency_source = "LTC";
                                break;
                            case '8':
                                currency_source = "TRX";
                                break;
                            case '9':
                                currency_source = "XMR";
                                break;
                        }

                        switch (devise_cible.famille_devise) {

                            case '1':
                                currency_cible = "dollar";
                                break;
                            case '2':
                                currency_cible = "euro";
                                break;
                            case '3':
                                currency_cible = "FCFA";
                                break;
                            case '4':
                                currency_cible = "XRP";
                                break;
                            case '5':
                                currency_cible = "ETH";
                                break;
                            case '6':
                                currency_cible = "BTC";
                                break;
                            case '7':
                                currency_cible = "LTC";
                                break;
                            case '8':
                                currency_cible = "TRX";
                                break;
                            case '9':
                                currency_cible = "XMR";
                                break;
                        }

                        return '<div class="row margin-0-auto padding-r-0 padding-l-0">' +
                            '<div class="col-md-12 padding-r-0 padding-l-0 font-size-0_9"><b>C que je donne:</b>&nbsp;<span class="kt-badge kt-badge--info kt-badge--inline font-size-1_0">'+quantite_source+'</span>&nbsp;<b>('+currency_source+')</b></div>' +
                            '<div class="col-md-12 padding-r-0 padding-l-0 margin-top-10 font-size-0_9"><b>C que je reçois:</b>&nbsp;<span class="kt-badge kt-badge--dark kt-badge--inline font-size-1_0">'+quantite_cible+'</span>&nbsp;<b>('+currency_cible+')</b></div>' +
                            '</div>';

                    } },
                { field: "adresse_reception", title: "Adresses", width: "auto",template:function(e){


                        var adresse_reception = e.adresse_reception;
                        var numero_envoi = e.numero_envoi;

                        return '<div class="row margin-0-auto padding-r-0 padding-l-0">' +
                            '<div class="col-md-12 padding-r-0 padding-l-0 font-size-0_9"><b>Réception:</b>&nbsp;<span class="kt-badge kt-badge--primary kt-badge--inline font-size-1_0" style="font-weight: 600 !important">'+adresse_reception+'</span></div>' +
                            '<div class="col-md-12 padding-r-0 padding-l-0 margin-top-10 font-size-0_9"><b>Dépôt:</b>&nbsp;<span class="kt-badge kt-badge--success color-white kt-badge--inline font-size-1_0" style="font-weight: 600 !important">'+numero_envoi+'</span></div>' +
                            '</div>';

                    } },
                {
                    field: "status_transaction",
                    title: "Status",
                    template: function (t) {
                        var e = {
                            '0': { title: "En attente", class: "kt-badge--brand" },
                            '1': { title: "Acceptée", class: " kt-badge--success" },
                            '2': { title: "Annuler", class: " kt-badge--danger" },
                        };
                        return '<span class="kt-badge ' + e[t.status_transaction].class + ' kt-badge--inline kt-badge--pill">' + e[t.status_transaction].title + "</span>";
                    },
                },

                {
                    field: "Actions",
                    title: "Actions",
                    sortable: !1,
                    width: 110,
                    overflow: "visible",
                    autoHide: !1,
                    template: function (e) {

                        let checkNumber = '';


                        if(e.numero_envoi.search("229") !== -1)
                        {
                            checkNumber = "<span> <a class='btn btn-sm btn-clean btn-icon btn-icon-md' onclick='showNumberDetails("+e.id_transaction+")'><i class='flaticon-arrows'></i> </a> </span>";
                        }
                        else
                        {
                            checkNumber = "";
                        }

                        return '  <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Archiver"><i class="la la-archive text-danger"></i> </a>'+checkNumber


                    },
                },
            ],
             })),
            $("#kt_search").on("click", function (a) {
                    a.preventDefault();
                    var e = {};

                    let idTransaction = $('#id_transaction').val();
                    let codeTransaction = $('#code_transaction').val();
                    let kt_select_status = $('#kt_select_status').val();
                    let kt_form_period = $('#kt_form_period').val();


                   e = {

                               0: {
                                   'search':
                                       {
                                           'value': idTransaction !== '' && idTransaction !== undefined ? idTransaction: ''
                                       }
                               },
                               1: {
                                   'search':
                                       {
                                           'value':codeTransaction !== '' && codeTransaction !== undefined ? codeTransaction : ''
                                       }
                               },
                               2: {
                                   'search':
                                       {
                                           'value': kt_select_status !== '' && kt_select_status !== undefined ? kt_select_status: ''
                                       }
                               },
                               3:
                                   {
                                       'search':
                                           {
                                               'value':kt_form_period !== '' && kt_form_period !== undefined ? kt_form_period : ''
                                           }
                                   }

                   };

                   t.search(e,'columns');

                }
            ),
            $("#kt_reset").on("click", function (a) {
                    a.preventDefault(), $(".kt-input").each(function () {
                          //  $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                            var e = {};

                            let idTransaction = $('#id_transaction').val().trim();
                            let codeTransaction = $('#code_transaction').val().trim();
                            let kt_select_status = $('#kt_select_status').val();
                            let kt_form_period = $('#kt_form_period').val();

                            e = {

                                0: {
                                    'search':
                                        {
                                            'value': ''
                                        }
                                },
                                1: {
                                    'search':
                                        {
                                            'value':''
                                        }
                                },
                                2: {
                                    'search':
                                        {
                                            'value': ''
                                        }
                                },
                                3:
                                    {
                                        'search':
                                            {
                                                'value':''
                                            }
                                    }

                            };

                            $('#kt_form_period').val('');
                            $('#code_transaction').val('');
                            $('#kt_select_status').val('');
                            $('#kt_form_period').val('');
                            t.search(e,'columns');

                        }
                    ), t.table().draw()
                }
            ),
            $('#kt_daterangepicker_transactions .form-control').daterangepicker({
                    "locale": {
                        "format": "DD/MM/YYYY",
                        "separator": " - ",
                        "applyLabel": "Appliquer",
                        "cancelLabel": "Annuler",
                        "fromLabel": "De",
                        "toLabel": "A",
                        "customRangeLabel": "Personnaliser",
                        "daysOfWeek": [
                            "Di",
                            "Lu",
                            "Ma",
                            "Me",
                            "Je",
                            "Ve",
                            "Sa"
                        ],
                        "monthNames": [
                            "Janvier",
                            "Février",
                            "Mars",
                            "Avril",
                            "Mai",
                            "Juin",
                            "Juillet",
                            "Août",
                            "Septembre",
                            "Octobre",
                            "Novembre",
                            "Décembre"
                        ],
                        "firstDay": 1
                    },
                    startDate:start,
                    endDate:end,
                    ranges: {
                        "Aujourd'hui": [moment(), moment()],
                        "Hier": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                        "Ces 07 derniers jours": [moment().subtract(6, "days"), moment()],
                        "Ces 30 derniers jours": [moment().subtract(29, "days"), moment()],
                        "Ce mois": [moment().startOf("month"), moment().endOf("month")],
                        "Le mois dernier": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
                    },
                },
                function (a, t, e) {

                   $("#kt_form_period").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"));
                    //$("#kt_form_period").val('');

                }

            ),
            $("#kt_form_status").on("change", function () {
                t.search($(this).val().toLowerCase(), "Status");
            }),
            $("#kt_form_type").on("change", function () {
                t.search($(this).val().toLowerCase(), "Type");
            }),
            $("#kt_form_status,#kt_form_type").selectpicker();
    },
};
jQuery(document).ready(function () {

    KTDatatableAutoColumnHideDemo.init();
});


function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}



function showNumberDetails(id)
{
    var kt_modal_info_number_depot = $('#kt_modal_info_number_depot');

    $.ajax({

        type:'POST',
        url:basePath+'/transactions/show-number-details',
        dataType:'JSON',
        data:
            {
                id_transaction:id
            },
        success:function(response)
        {
            if(response.error === undefined)
            {

                var html = '<table class="table table-responsive table-borderless width-100">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Numéro</th>' +
                    '<th>Propriétaire</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><span class="kt-badge kt-badge--inline kt-badge--primary">00229 '+response.libelle_numero+'</span></td>' +
                    '<td><span class="kt-badge kt-badge--inline kt-badge--info">'+response.number_name+'</span></td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>';
                $('#kt_modal_info_number_depot .modal-body').html(html);
            }
            else
            {
                $('#kt_modal_info_number_depot .modal-body').html("<p style='text-align: center !important' class='text-center text-danger font-size-1_0 font-weight-500'>"+response.error+"</p>");
            }

            kt_modal_info_number_depot.modal('show');

        },
        error:function()
        {
            swal.fire('Oops',"Une erreur s'est produite. Veuillez vérifier votre connexion internet et rééssayer","error");
        }
    });
}