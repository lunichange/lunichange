"use strict";

var KTDatatableRecordSelectionDemo = (function () {
    var t = {
        data: {
            type: "remote",
            source: {read: {url: basePath+"administration/requests/get-requests"}},
        
            serverPaging: !0,
            serverFiltering: !0,
            serverSorting: !0
          
        },
        layout: {scroll: !0, height: 350, footer: !1},
        sortable: !0,
        pagination: !0,
        columns: [
            {
                field: "id_document",
                title: "#",
                sortable: !1,
                width: 20,
                selector: {class: "kt-checkbox--solid"},
                textAlign: "center"
            },

            {
                field: "nom_complet",
                title: "Client",
                width:200,
                template: function (t) {
                    return ''+t.nom_complet+'';
                },
            },
            {field: "lib_type_piece", title: "Type du document",template:function(t)
                {
                    return '<button class="btn btn-success btn-elevate btn-square padding-left-0 padding-right-0">'+t.lib_type_piece+'</button>';
                }

            },
            {field: "numero_piece", title: "Numéro de la pièce", template:function(t)
                {

                    return '<span class="btn btn-primary  btn-elevate btn-square padding-left-0 padding-right-0"><i class="la la-paperclip"></i>&nbsp;'+t.numero_piece+'</span>';
                }

            },
            {
              field: 'telephone', title:"Téléphone", template:function(t)
              {
                  return '<span class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill">+'+t.dial_code+'</span> '+t.telephone;
              }
            },
            {field: "publish_date", title: "Date", type: "date", format:"MM/DD/YYYY H:i:s"},
            {
                field: "statut_document",
                title: "Status",
                template: function (t) {
                    var e = {
                        '0': {title: "En attente", class: "kt-badge--brand"},
                        '1': {title: "Validé", class: " kt-badge--success"},
                        '2': {title: "Rejeté", class: " kt-badge--danger"},
                        '3': {title: "Archivé", class: " kt-badge--warning"},
                    };
                    return '<span class="kt-badge ' + e[t.statut_document].class + ' kt-badge--inline kt-badge--pill">' + e[t.statut_document].title + "</span>";
                },
            },

            {
                field: "Actions",
                title: "Actions",
                sortable: !1,
                width: 250,
                overflow: "visible",
                textAlign: "left",
                autoHide: !1,
                template: function (t) {
                    //console.log(t);

                    let html = '';
                    let consultDocument = '<div class="dropdown">  <a href="javascript:;" title="Cliquer pour plus de détails" class="btn btn-sm btn-clean  btn-icon btn-icon-sm" data-toggle="dropdown">  <i class="flaticon2-file"></i>   </a>   <div class="dropdown-menu dropdown-menu-right">                            <a class="dropdown-item" target="_blank" href="'+dataPath+''+t.id_user+'/'+t.document+'"><i class="la la-user"></i> Consulter document d\'identité</a>  ' +
                        '<a target="_blank" class="dropdown-item" href="'+dataPath+''+t.id_user+'/'+t.document_selfie+'"><i class="flaticon2-file"></i>Consulter selfie</a> ' +
                        ' </div> ' +
                        '</div> ';


                    let archiveBtn = '<a href="javascript:void(0);" class="btn btn-warning btn-sm btn-elevate btn-square padding-left-0 padding-right-0" onclick="archiveDocument('+t.id_document+')" title="Archiver"><i class="flaticon2-trash color-white"></i></a>';
                    let unarchiveBtn = '<a href="javascript:void(0);" class="btn btn-primary btn-sm btn-elevate btn-square padding-left-0 padding-right-0" onclick="unarchiveDocument('+t.id_document+')" title="Désarchivé"><i class="flaticon2-trash"></i><i style="font-size:0.7em !important; margin-left: -10px !important" class="flaticon2-reply"></i> </a>';

                    let lock_btn = '';


                    if(t.lock_doc !== '0')
                    {
                        lock_btn = '<a href="" onclick="updateLock(\'unlock\','+t.id_document+')" class="btn btn-sm btn-clean  btn-icon btn-icon-sm"><i class="la la-unlock-alt" style="color: forestgreen !important"></i> </a>'
                    }
                    else
                    {
                        lock_btn = '<a href="" onclick="updateLock(\'lock\','+t.id_document+')" class="btn btn-sm btn-clean  btn-icon btn-icon-sm"><i class="flaticon2-lock" style="color: red !important"></i> </a>';

                    }
                    if(t.statut_document !== '0')
                    {
                        consultDocument = '<div class="dropdown">  <a href="javascript:;" title="Cliquer pour plus de détails" class="btn btn-primary btn-sm btn-elevate btn-square padding-left-0 padding-right-0" data-toggle="dropdown">  <i class="flaticon2-file"></i>   </a>   <div class="dropdown-menu dropdown-menu-right">  ' +
                            '<a class="dropdown-item" target="_blank" href="'+dataPath+''+t.id_user+'/'+t.document+'"><i class="la la-user"></i> Consulter document d\'identité</a>  ' +
                            '<a target="_blank" class="dropdown-item" href="'+dataPath+''+t.id_user+'/'+t.document_selfie+'"><i class="flaticon2-file"></i>Consulter selfie</a> ' +
                            ' </div> ' +
                            '</div> ' ;
                    }

                    if(t.statut_document === '1' || t.statut_document ==='2' || t.statut_document === '3')
                    {
                        switch (t.statut_document) {
                            case '1':
                                html = '<button class="btn btn-success btn-sm btn-elevate btn-square padding-left-0 padding-right-0">Validé</button>&nbsp;'+consultDocument+'&nbsp;'+archiveBtn;
                            break;

                            case '2':
                                html= '<button class="btn btn-danger btn-xs btn-elevate btn-square padding-left-0 padding-right-0">Rejeté</button>&nbsp;'+consultDocument+'&nbsp;'+archiveBtn;
                                break;

                            default:
                                html = '<a href="javascript:void(0);">'+unarchiveBtn+'</a>';

                        }
                    }
                    else
                    {

                        html = lock_btn+'&nbsp;'+consultDocument +
                            '<a href="javascript:;" onclick="updateStatusDocument(\'validate\','+t.id_document+')" class="btn btn-sm btn-clean  btn-icon btn-icon-sm" title="Valider la demande">  <i class="flaticon2-check-mark text-success"></i>   </a>  ' +
                            '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-sm" onclick="updateStatusDocument(\'reject\','+t.id_document+')" title="Rejeter la demande">   <i class="flaticon2-delete text-danger"></i> </a> ';

                    }
                    return html;

                },
            },
        ],
    };
    return {
        init: function () {
            !(function () {
                t.search = { input: $("#generalSearch").keyup( function(){
                    var s=$(this).val();
                }) };
                var e = $("#local_record_selection").KTDatatable(t);
                $("#kt_form_status").on("change", function () {
                    e.search($(this).val().toLowerCase(), "status");
                }),
                    $("#kt_form_type").on("change", function () {
                        e.search($(this).val().toLowerCase(), "type");
                    }),
                    $("#kt_form_status,#kt_form_type").selectpicker(),
                    e.on("kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated", function (t) {
                        var a = e.rows(".kt-datatable__row--active").nodes().length;
                        $("#kt_datatable_selected_number").html(a), a > 0 ? $("#kt_datatable_group_action_form").collapse("show") : $("#kt_datatable_group_action_form").collapse("hide");
                    }),
                    $("#kt_modal_fetch_id")
                        .on("show.bs.modal", function (t) {
                            for (
                                var a = e
                                        .rows(".kt-datatable__row--active")
                                        .nodes()
                                        .find('.kt-checkbox--single > [type="checkbox"]')
                                        .map(function (t, e) {
                                            return $(e).val();
                                        }),
                                    n = document.createDocumentFragment(),
                                    l = 0;
                                l < a.length;
                                l++
                            ) {
                                var i = document.createElement("li");
                                i.setAttribute("data-id", a[l]), (i.innerHTML = "Selected record ID: " + a[l]), n.appendChild(i);
                            }
                            $(t.target).find(".kt-datatable_selected_ids").append(n);
                        })
                        .on("hide.bs.modal", function (t) {
                            $(t.target).find(".kt-datatable_selected_ids").empty();
                        });
            })()

        },
    };
})();
jQuery(document).ready(function () {
    KTDatatableRecordSelectionDemo.init();
});


function updateStatusDocument(action,id)
{
    let Id = parseInt(id);
    let text = '';
    let title ='';
    let statut = null;

    let t = $('#kt_modal_motif');
    if(action === 'validate')
    {
        title = 'Validation du dossier';
        text = 'Poursuivre la validation ?';
        statut = '1';
    }
    else if(action === 'reject')
    {
        title = 'Rejeter le dossier ?';
        text = 'Poursuivre le rejet du dossier ?';
        statut = '2';
    }

    $.ajax({

        type:'POST',
        url:'/luni-api/std-get?type=all&table=documents',
        dataType:'JSON',
        data:
            {
                id_document:Id
            },
        success:function(response)
        {
            if(response.length !== 0)
            {

                swal.fire({
                    title: title,
                    text:''+text,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText:'Non',
                    confirmButtonText: 'Oui'
                }).then((result) => {
                    if (result.value) {
                        if(action === 'reject')
                        {
                            t.modal({ show: !0, backdrop: "static" });

                            $('#kt_form_motif').submit(function(e){
                                e.preventDefault();
                                let kt_btn_motif = $('#kt_btn_motif');

                                $.ajax({

                                    type:'POST',
                                    url:'/administration/requests/update-document?action='+action+'&statut='+statut,
                                    data:$(this).serialize(),
                                    beforeSend:function()
                                    {
                                        kt_btn_motif.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !0);
                                    },
                                    success:function(response)
                                    {
                                        kt_btn_motif.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                                        if(response.success !== undefined)
                                        {
                                            custom_std('success',response.success,'fire');
                                            $("#local_record_selection").KTDatatable('destroy');
                                            setTimeout(function(){
                                                KTDatatableRecordSelectionDemo.init();
                                            },2e1)

                                        }
                                        else if(response.error !== undefined)
                                        {
                                            custom_std('error',response.error,'fire');
                                        }

                                    },
                                    error:function()
                                    {
                                        kt_btn_motif.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', !1);
                                        toastr.error('Oops','Une erreur s\'est produite au cours du traitement');
                                    }

                                })

                            });
                        }
                        else
                        {
                            $.ajax({
                                type:'POST',
                                url:'/administration/requests/update-document?action='+action+'&statut='+statut,
                                dataType:'JSON',
                                success:function(response)
                                {

                                    if(response === 1)
                                    {
                                        custom_std('success','Opération terminée','fire');
                                        $("#local_record_selection").KTDatatable('destroy');
                                        setTimeout(function(){
                                            KTDatatableRecordSelectionDemo.init();
                                        },2e1)
                                    }
                                    else
                                    {

                                    }
                                },
                                error:function(response)
                                {
                                    if(response.status === 404)
                                    {
                                        toastr.error('error','Page non trouvée');

                                    }
                                    else
                                    {
                                        custom_std('error',"Une erreur s'est produite au cours du processus",'fire');
                                    }
                                }

                            });
                           /*fetch(`/dashboard/administration/requests/update-document?action=${action}&statut=${statut}`)
                               .then(function(response){

                                   if(response.status === 200)
                                   {
                                       custom_std('success','Opération effectuée avec succès','fire');

                                       $("#local_record_selection").KTDatatable('destroy');

                                      setTimeout(function(){
                                          KTDatatableRecordSelectionDemo.init();
                                      },2e1) ;

                                   }
                                   else
                                   {
                                       custom_std('error',"Une erreur s'est produite au cours du processus",'fire');
                                   }

                           });*/
                        }

                    }

                });



            }
        },
        error:function(state)
        {
            if(state.status === 404)
            {
                toastr.error('Oops',"Page non trouvée");
            }
            else
            {
                toastr.error('Oops',"Un erreur s'est porduite");
            }
        }


    });



    /*then((result) => {
        if (result.value) {
            //t.modal({ show: !0, backdrop: "static" });
            alert('OKay !');
        }
        else
        {
            alert('Je suis magnifique !');
        }
    });*/
}
