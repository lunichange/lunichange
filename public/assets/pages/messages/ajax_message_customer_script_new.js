"use strict";
var KTDatatablesSearchOptionsAdvancedSearch;
KTDatatablesSearchOptionsAdvancedSearch = function () {
    return {
        init: function () {
            var t;
            t = $("#kt_table_messages").DataTable({
                    responsive: !0,
                    dom: "<'row'<'col-sm-12'tr>>\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    lengthMenu: [5, 10, 25, 50],
                    pageLength: 10,
                    language: {
                        lengthMenu: "Display _MENU_"
                    }
                    ,
                    searchDelay: 500,
                    processing: !0,
                    serverSide: !0,
                    ajax: {
                        url: "/dashboard/list-messages",
                        type: "POST",
                        data:
                            {
                                columnsDef: ["objet", 'messages','date', "statut"]
                            }
                    }
                    ,
                    columns: [{
                        data: "objet"
                    }
                        , {
                            data: "messages"
                        }
                        ,  {
                            data: "date"
                        }
                        , {
                            data: "statut"
                        }
                    ],

                    columnDefs: [

                      /*  {
                            targets: 3,
                            title: "Devise source",
                            width: '100px',
                            orderable: !1,
                            render: function (t, a, e, n) {


                                return '<span class="kt-badge kt-badge--primary kt-badge--inline kt-font-size-0_8-desktop kt-font-boldest" data-toggle="kt-tooltip" data-placement="top" title="' + e.devise_source.lib_devise + '">' + e.devise_source.lib_devise.substr(0, 6) + '...</span> ';
                            }
                        },*/
                        {
                            targets: 1, title: "Contenu", width: "250px", orderable: !1, render: function (t, a, e, n)
                            {
                                        let span = null;
                                        if(e.statut === '0')
                                        {
                                            span = '<div class="col-md-12 padding-l-0 padding-r-0 container_message" onclick="showMessage('+e.id_message+')"> <span class="kt-font-size-1_2-desktop">'+e.message.substr(0,30)+'...</span>&nbsp;<span class="kt-badge kt-badge--brand kt-badge--dot"></span></div>';
                                        }
                                        else
                                        {

                                            span = '<div class="col-md-12 padding-r-0 padding-l-0 container_message" onclick="showMessage('+e.id_message+')"><span class="kt-font-size-1_2-desktop" style="color: #4a4c4c !important;">'+e.message.substr(0,30)+'...</span></div>';
                                        }


                               return span;
                            }
                        },
                        {
                            targets: -1, title:"Status", render: function (t, a, e, n) {
                                var i = {
                                        '0': {
                                            title: "Non lu", class: "kt-badge--warning"
                                        }
                                        , '1': {
                                            title: "Lu", class: " kt-badge--success"
                                        }

                                    }
                                ;
                                return void 0 === i[t] ? t : '<span style="color: #fff !important" class="kt-badge ' + i[t].class + ' kt-badge--inline kt-badge--pill">' + i[t].title + "</span>"
                            }
                        }

                    ]
                }
            ),
                $("#kt_search").on("click", function (a) {
                        a.preventDefault();
                        var e = {}
                        ;
                        $(".kt-input").each(function () {
                                var t = $(this).data("col-index");
                                e[t] ? e[t] += "|" + $(this).val() : e[t] = $(this).val()
                            }
                        ), $.each(e, function (a, e) {
                                t.column(a).search(e || "", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_reset").on("click", function (a) {
                        a.preventDefault(), $(".kt-input").each(function () {
                                $(this).val(""), t.column($(this).data("col-index")).search("", !1, !1)
                            }
                        ), t.table().draw()
                    }
                ),
                $("#kt_datepicker").datepicker({
                        todayHighlight: !0, templates: {
                            leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    }
                )
        }

    }
}

();
jQuery(document).ready(function() {
        KTDatatablesSearchOptionsAdvancedSearch.init()
    }

);



function showMessage(id)
{
    id = parseInt(id);

    if(id !== '')
    {
        $.ajax({

            type:'POST',
            dataType:'JSON',
            url:'/dashboard/std-get?table=messages&type=unique&update=true',
            data:
                {
                    id_message:id
                },
            success:function(response)
            {
                let kt_modal_show_message = $('#kt_modal_show_message');
                let kt_body_message = $('#kt_body_message');

                let results= response.success;
                kt_body_message.html('<p style="font-size: 1.1em !important">'+results.message+'</p>');
                kt_modal_show_message.modal('show');
                if(response.actualisation !== undefined && response.actualisation === true)
                {
                    setTimeout(function(){
                        $("#kt_table_messages").DataTable().destroy();
                        setTimeout(function(){
                            KTDatatablesSearchOptionsAdvancedSearch.init();
                        },200) ;
                    },2e1)
                }


            },
            error:function(state)
            {
                if(state.statut !== 404)
                {
                    console.log("Une erreur s'est produite");
                }

            }
        })
    }
    else
    {
        toastr.error("Impossible d'effectuer le traiement demandé !");
    }
}